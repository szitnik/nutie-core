package si.zitnik.research.iobie.test

import scala.xml.XML
import collection.mutable.HashSet
import java.io.PrintWriter

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/13/13
 * Time: 11:50 AM
 * To change this template use File | Settings | File Templates.
 */
object DrugBankParser {


  def toFile(list: HashSet[String], filename: String): Unit = {
    val ps = new PrintWriter(filename)
    ps.write("#File of chemicals generated for Chemdner 2013 participation\n")
    ps.write("#Source: drugbank.ca\n")
    list.map(_.trim).filter(_.size > 0).foreach(v => ps.write("%s\n".format(v)))
    ps.close()
  }

  def main(args: Array[String]): Unit = {
    val outFolderPath = "/home/slavkoz/IdeaProjects/iobie/src/main/resources/gazeteers/CHEMICALS/"
    val xmlFile = XML.loadFile("/home/slavkoz/Downloads/drugbank.xml")

    val names = HashSet[String]()
    val synonyms = HashSet[String]()
    val brands = HashSet[String]()
    val molecularFormula = HashSet[String]()
    val iupacName = HashSet[String]()
    val inChi = HashSet[String]()

    val drugs = xmlFile \\ "drugs"

    for (drug <- drugs \ "drug") {
      names += (drug \ "name").text
      ((drug \ "synonyms") \ "synonym").foreach(t => synonyms += t.text.replaceAll("\\[.*\\]", "").trim)
      ((drug \ "brands") \ "brand").foreach(t => brands += t.text.replaceAll("\\(.*\\)", "").trim)
      ((drug \ "experimental-properties") \ "property").filter(v => (v \ "kind").text.equals("Molecular Formula")).foreach(t => molecularFormula += (t \ "value" text))
      ((drug \ "calculated-properties") \ "property").filter(v => (v \ "kind").text.equals("IUPAC Name")).foreach(t => iupacName += (t \ "value" text))
      ((drug \ "calculated-properties") \ "property").filter(v => (v \ "kind").text.equals("InChi")).foreach(t => inChi += ((t \ "value") text).replaceFirst("InChi=", ""))
    }

    toFile(names, outFolderPath+"names.txt")
    toFile(synonyms, outFolderPath+"synonyms.txt")
    toFile(brands, outFolderPath+"brands.txt")
    toFile(molecularFormula, outFolderPath+"molecularFormulas.txt")
    toFile(iupacName, outFolderPath+"iupacNames.txt")
    toFile(inChi, outFolderPath+"inChis.txt")



  }

}
