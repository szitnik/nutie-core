package si.zitnik.research.iobie.test;

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/7/12
 * Time: 11:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class CoreNLPTestJ {
    /*
    public static Annotation getAnnotation1() {
        return new Annotation("I am jonny from Slovenia. This is a very beautiful country!");
    }

    public static Annotation getAnnotation2() {
        Examples examples = new Examples();
        examples.add(new Example(Label.OBS(),  new String[]{ "I", "am", "jonny", "from", "Slovenia", ".", "This", "is",  "a", "very", "beautiful", "country", "!" }));

        List<CoreLabel> tokens = new ArrayList<CoreLabel>();
        for (Example example : examples) {
            for (Object word : example.getLabeling(Label.OBS())) {
                CoreLabel token = new CoreLabel();
                token.set(CoreAnnotations.TextAnnotation.class, word.toString());
                tokens.add(token);
            }
        }

        Annotation annotation = new Annotation();
        annotation.set(CoreAnnotations.TokensAnnotation.class, tokens);

        return annotation;
    }

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma, parse, ner, dcoref");
        //props.put("annotators", "ssplit, pos, lemma, parse, ner, dcoref");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        Annotation document = getAnnotation1();
        pipeline.annotate(document);
        writeResult(document);
    }

    public static void writeResult(Annotation document) {
        for (CoreLabel token: document.get(CoreAnnotations.TokensAnnotation.class)) {
            String word = token.get(CoreAnnotations.TextAnnotation.class);
            String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
            String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
            String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
            String chunk = token.get(CoreAnnotations.ChunkAnnotation.class);

            System.out.println(String.format("%s, %s, %s, %s, %s", word, lemma, pos, ne, chunk));
        }
    }
    */
}
