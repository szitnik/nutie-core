package si.zitnik.research.iobie.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;


import java.io.*;
import java.util.*;

import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.*;

public class StanfordCoreNlpDemo {

    public static void main(String[] args) throws IOException {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit");
        props.setProperty("tokenize.options","ptb3Escaping=false");

        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        Annotation annotation;
        if (args.length > 0) {
            annotation = new Annotation(IOUtils.slurpFileNoExceptions(args[0]));
        } else {
            annotation = new Annotation("Kosgi Santosh sent an email (to) Stanford University. He didn't get a reply.");
        }

        pipeline.annotate(annotation);


        for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                //CharacterOffsetBeginAnnotation, CharacterOffsetEndAnnotation, TextAnnotation
                Integer startIdx = token.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class);
                String text = token.get(CoreAnnotations.TextAnnotation.class);
                System.out.println(text + " - " + startIdx);
            }
        }

    }

}
