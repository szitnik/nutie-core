package si.zitnik.research.iobie.test

import org.apache.commons.math3.stat.inference.TestUtils

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 5/6/14
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
object CommonsMathTest {

  def main(args: Array[String]): Unit = {
    val population = Array(1d, 2d, 3d, 4, 5, 6, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10)
    val populationMean = population.sum*1.0/population.length
    val sample = population.to[Set].subsets(10).next().toArray[Double]
    val alpha = 0.05

    //for our cases: population mean = result on the whole dataset, sample = result on subdatasets


    println("t-statistic: " + TestUtils.t(populationMean, sample))
    println("p-value: " + TestUtils.tTest(populationMean, sample))
    println("rejected with confidence level " + (1-alpha) + ": " + TestUtils.tTest(populationMean, sample, alpha))
  }

}
