package si.zitnik.research.iobie.domain.cluster

import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable.HashSet

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 6/13/12
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */

class Cluster extends HashSet[Constituent]() {

  def this(mention: Constituent) {
    this()
    mention.cluster = this
    this.add(mention)
  }


  def this(mention1: Constituent, mention2: Constituent) {
    this()
    this.add(mention1)
    mention1.cluster = this
    this.add(mention2)
    mention2.cluster = this
  }

  override def add(c: Constituent) = {
      super.add(c)
      c.cluster = this
    true
  }

  def addAll(constituent: Constituent*) = {
    for (c <- constituent) {
      super.add(c)
      c.cluster = this
    }
    true
  }

  override def toString = this.mkString("Cluster(",", ",")")
}
