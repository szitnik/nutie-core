package si.zitnik.research.iobie.domain.parse

import java.util.ArrayList
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable.ArrayBuffer


/**
 * Tree must be build in bottom-up approach.
 */
class ParseNode(
                 var value: String,
                 var children: Array[ParseNode] = null,
                 var parent: ParseNode = null) extends Ordered[ParseNode] {

  lazy val text = this.currentLeaves().map(v => v.value).mkString(" ")
  lazy val startIdx: Int = if (children != null) children(0).startIdx else allLeaves().indexOf(this)
  lazy val endIdx: Int = if (children != null) children(children.length-1).endIdx else allLeaves().indexOf(this)

  /**
   * It returns path to root inclusive root and current node.
   * @return
   */
  def getPathToRoot(): ArrayBuffer[ParseNode] = {
    val retVal = ArrayBuffer(this)

    var curNode = this.parent
    while (curNode != null) {
      retVal.append(curNode)
      curNode = curNode.parent
    }

    retVal
  }

  /**
   * Returns leaves that are just from current node
   * @return
   */
  def currentLeaves(): ArrayList[ParseNode] = {
    val retVal = new ArrayList[ParseNode]()

    if (this.children == null) {
      retVal.add(this)
    } else {
      this.children.foreach(c => retVal.addAll(c.currentLeaves))
    }

    retVal
  }

  /**
   * This method returns leafs of the whole tree, not just current node
   */
  def allLeaves() = {
    //get root node
    var root = this
    while (root.parent != null) {
      root = root.parent
    }
    root.currentLeaves()
  }

  def nonLeafValues(): ArrayList[String] = {
    val retVal = new ArrayList[String]()

    if (this.children != null) {
      retVal.add(value)
      this.children.foreach(c => retVal.addAll(c.nonLeafValues()))
    }

    retVal
  }

  def nonLeafNodes(): ArrayList[ParseNode] = {
    val retVal = new ArrayList[ParseNode]()

    if (this.children != null) {
      retVal.add(this)
      this.children.foreach(c => retVal.addAll(c.nonLeafNodes()))
    }

    retVal
  }

  /**
   * Example must me the example that this parse tree belongs to.
   * @param example
   * @param nodeValue
   * @return
   */
  def getConstituents(example: Example, nodeValue: String): ArrayList[Constituent] = {
    val retVal = new ArrayList[Constituent]()
    nonLeafNodes.foreach(node =>
      if (node.value.equals(nodeValue))
        retVal.add(new Constituent(example, node.startIdx, node.endIdx+1))
    )
    retVal
  }

  override def compare(c: ParseNode) = {
    var diff = this.startIdx - c.startIdx
    if (diff == 0) {
      diff = this.endIdx - c.endIdx
    }
    diff
  }

  override def toString: String = "[Node: %s]".format(this.value)
}

object ParseNode {

  /**
   * It retrieves the node that represents constituent.
   * @param constituent
   * @param treeNode
   * @return
   */
  def getClosestCommonRootNode(constituent: Constituent, treeNode: ParseNode): ParseNode = {
    if (constituent.startIdx == treeNode.startIdx && constituent.endIdx == treeNode.endIdx+1) {
      return treeNode
    } else {
      for (child <- treeNode.children) {
        if (child.startIdx <= constituent.startIdx && child.endIdx+1 >= constituent.endIdx) {
          return getClosestCommonRootNode(constituent, child)
        }
      }
      if (treeNode.startIdx <= constituent.startIdx && treeNode.endIdx+1 >= constituent.endIdx) {
        return treeNode
      } else {
        throw new NoSuchElementException("Constituent '%s' not found in tree '%s'".format(constituent, treeNode))
      }
    }
  }

  /**
   * It returns path between two nodes inclusive.
   * @param node1
   * @param node2
   * @return
   */
  def getNodePath(node1: ParseNode, node2: ParseNode, incl: NodePath.Value) = {
    val sorted = Array(node1, node2).sorted
    val p1 = sorted(0).getPathToRoot()
    val p2 = sorted(1).getPathToRoot().reverse

    p1.find(p2.contains(_)) match {
      case Some(commonNode) => {
        if (incl.equals(NodePath.INCLUSIVE)) {
          p1.take(p1.indexOf(commonNode)) ++ p2.drop(p2.indexOf(commonNode))
        } else {
          p1.take(p1.indexOf(commonNode)).drop(1) ++ p2.drop(p2.indexOf(commonNode)).dropRight(1)
        }
      }
      case None => throw new NoSuchElementException("No path between nodes '%s' and '%s' found.".format(node1, node2))
    }
  }

}

object NodePath extends Enumeration {
  val INCLUSIVE = Value("INCL")
  val NON_INCLUSIVE = Value("N_INCL")
}
