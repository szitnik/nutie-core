package si.zitnik.research.iobie.domain

import si.zitnik.research.iobie.algorithms.crf.Label
import java.util.HashMap

import breeze.linalg.SparseVector
import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/7/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */

class Token extends HashMap[Label.Value, Any] with StrictLogging {

  def this(attrName: Label.Value, attrVal: Any) {
    this()
    this.put(attrName, attrVal)
  }

  //features are flags if specific feature function was triggered
  var ufeatures: SparseVector[Double] = null
  var bfeatures: SparseVector[Double] = null

  def initFeatures(featuresNumber: Int): Unit = {
    this.ufeatures = SparseVector.zeros[Double](featuresNumber)
    this.bfeatures = SparseVector.zeros[Double](featuresNumber)
  }

  def getStringValue(labelType: Label.Value): Set[String] = {
    this.get(labelType) match {
      case t: String => Set(t)
      case t: Array[String] => t.toSet
      case t: Set[_] => t.map(_.toString).toSet
      case t: Int => Set(t.toString)
      case t => {
        logger.error("Wrong type within tokens. Type %s, Value: %s".format(labelType, t))
        System.exit(-1)
        Set("") //TODO: Weird = code after System.exit ???
      }
    }
  }

  //copies only attributes with values
  override def clone() = {
    val retVal = new Token()

    for (key <- this.keySet()) {
      retVal.put(key, this.get(key))
    }

    retVal
  }

}


