package si.zitnik.research.iobie.domain.relationship

import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.{Token, Example}
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 *
 */
class Relationship(var example: Example,
                   val relationshipName: String,
                   var subj: Constituent,
                   var obj: Constituent) extends Token(Label.REL, relationshipName) {

  override def toString() = "%s -> %s -> %s".format(subj.toString(), this.get(Label.REL), obj.toString())
}
