package si.zitnik.research.iobie.domain.sampling

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.Examples

import util.Random
import si.zitnik.research.iobie.algorithms.crf.ExampleLabel

import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/28/12
 * Time: 8:00 PM
 * To change this template use File | Settings | File Templates.
 */
object RandomSampler extends StrictLogging {


  /**
   * The method samples the dataset to:
   *  trainingData = trainPercent*numberOfDocuments
   *  testData = (1-trainPercent)*numberOfDocuments
   *
   * it returns new datasets (trainingData, testData)
   * @param examples
   * @param trainPercent
   * @param randomSeed
   * @return
   */
  def sample(examples: Examples, trainPercent: Double = 0.7, randomSeed: Int = Random.nextInt()): (Examples, Examples) = {
    //get all document IDs into list
    var docIDs = examples.getAllExampleLabelValues(ExampleLabel.DOC_ID).toList

    //shuffle the list
    val trainingDocIDs = new Random(randomSeed).shuffle(docIDs).take(math.round(docIDs.size*trainPercent).toInt).toSet

    val trainingData = new Examples()
    val testData = new Examples()
    //fill the result values
    for (example <- examples) {
      if (trainingDocIDs.contains(example.get(ExampleLabel.DOC_ID))) { //add to train domain
        trainingData.add(example)
      } else { //add to test domain
        testData.add(example)
      }
    }

    //warn if one of the datasets is 0-sized
    if (trainingData.size() == 0) {
      logger.warn("Sampled training domain contains no examples! Possible error of too few documents od no document identifier defined!")
    }
    if (testData.size() == 0) {
      logger.warn("Sampled test domain contains no examples!  Possible error of too few documents od no document identifier defined!")
    }

    (trainingData, testData)
  }
}
