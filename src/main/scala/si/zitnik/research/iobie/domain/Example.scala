package si.zitnik.research.iobie.domain

import constituent.Constituent
import relationship.Relationship

import scala.collection.JavaConversions._
import java.util.{ArrayList, Collections, HashMap}

import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.algorithms.crf.{Classifier, ExampleLabel, Label}

import collection.mutable.ArrayBuffer
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import breeze.linalg.SparseVector
import java.util

import com.typesafe.scalalogging.StrictLogging


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/7/11
 * Time: 9:04 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Represents training examples
 * obs - observables sequence
 * labels - labels for sequence
 */
class Example extends ArrayList[Token] with StrictLogging {
  var examples: Examples = null
  private var mentions = new ArrayBuffer[Constituent] //this value is set in DatasetUtil at dataset loading, value is used by MentionExamplesBuilder.buildFromTagged
  private var relationships = new ArrayBuffer[Relationship]
  private val attributes = new HashMap[ExampleLabel.Value, Any]() //intended for example level labels
  private val tokenRelationships = new ArrayBuffer[(String, Option[Token], Option[Token])]()

  def put(attr: ExampleLabel.Value, attrVal: Any): Unit = {
    attributes.put(attr, attrVal)
  }

  def get(attr: ExampleLabel.Value): Any = {
    attributes.get(attr)
  }

  def containsLabel(label: ExampleLabel.Value) = attributes.containsKey(label)

  def exampleLabelSet() = attributes.keySet()

  def transferLabeling(sourceLabelType: Label.Value, targetLabelType: Label.Value, removeSource: Boolean = false): Unit = {
    this.setLabeling(targetLabelType, this.getLabeling(sourceLabelType))
    if (removeSource) {
      this.removeLabeling(sourceLabelType)
    }
  }


  override def add(e: Token) = super.add(e)
  override def addAll(c: util.Collection[_ <: Token]) = super.addAll(c)

  /**
   * Warning: USE WITH CAUTION!
   * @param index
   * @param element
   */
  override def add(index: Int, element: Token) = {
    super.add(index, element)
  }

  override def addAll(index: Int, c: util.Collection[_ <: Token]) = throw new NotImplementedException()

  /**
   * Creating an example using array of tokens
   */
  def this(tokens: util.Collection[_ <: Token]) {
    this()
    tokens.foreach(token => this.add(token))
  }

  /**
   * Creating an example using array of observables and corresponding labels
   */
  def this(obs: Array[String], label: Label.Value, labels: Array[String]) {
    this()
    if (obs.length != labels.length) {
      throw new Exception("Observables and Lables sizes do not match!")
    } else {
      for ((o, l) <- obs zip labels) {
        val map = new Token(Label.OBS, o)
        map.put(label, l)
        this.add(map)
      }
    }
  }

  def this(attrName: Label.Value, attrVals: Array[String]) {
    this()
    for (attrVal <- attrVals) {
      this.add(new Token(attrName, attrVal))
    }
  }

  /**
   * Assumes Label.OBS type values as input.
   * @param observables
   * @return
   */
  def this(observables: Array[String]) {
    this(Label.OBS, observables)
  }

  def addAttrs(attrName: Label.Value, attrVals: Array[String]): Unit = {
    if (attrVals.length == this.size) {
      for ((attrVal, idx) <- attrVals.zipWithIndex) {
        this.get(idx).put(attrName, attrVal)
      }
    } else {
      logger.error("AttrVals size does not match sequence length. No attribute \"%s\" added.".format(attrName))
    }
  }

  def setAttr(idx: Int, attrName: Label.Value, attrVal: String): Unit = {
    this.get(idx).put(attrName, attrVal)
  }

  def get(idx: Int, label: Label.Value): Any = {
    this.get(idx).get(label)
  }

  def getLabeling(label: Label.Value): ArrayList[Any] = {
    val retVal = new ArrayList[Any]()
    this.foreach(labels => retVal.add(labels.get(label)))
    retVal
  }

  /**
   * This methods support standard and B-,I-,O- notation of tagged entities regarding labelType
   *
   * Tags to include should be possible values of selected labeltype without B,I,O- notation.
   * @param labelType
   * @param tagsToInclude
   * @return
   */
  def getLabelingConstituents(labelType: Label.Value, tagsToInclude: Set[String]): ArrayList[Constituent] = {
    val retVal = new ArrayList[Constituent]()

    val labeling = this.getLabeling(labelType)

    var idx = 0
    while (idx < labeling.size()) {
      val endIdx = labeling.indexWhere(_.equals("O"), idx)
      if (endIdx > idx && tagsToInclude.contains(labeling.get(idx).replaceFirst("B-", "").replaceFirst("I-", ""))) {
        retVal.add(new Constituent(this, idx, endIdx))
        idx = endIdx+1
      } else if (endIdx == idx) {
        idx += 1
      } else { //end or one more
        if (idx < labeling.size() && tagsToInclude.contains(labeling.get(idx).replaceFirst("B-", "").replaceFirst("I-", ""))) {
          retVal.add(new Constituent(this, idx, labeling.size()))
        }
        idx = labeling.size()
      }
    }
    retVal
  }

  def subLabeling(labelType: Label.Value, startIdx: Int, endIdx: Int) = {
    this.subList(startIdx, endIdx).map(_.get(labelType))
  }

  def observables() = {
    getLabeling(Label.OBS)
  }

  def removeLabeling(labelType: Label.Value): Unit = {
    this.foreach(labels => labels.remove(labelType))
  }

  /**
   * Changes labels from source to target
   * @param labelType
   * @param sourceValue
   * @param targetValue
   */
  def relabel(labelType: Label.Value, sourceValue: String, targetValue: String): Unit = {
    for (token <- this) {
      if (token.get(labelType).equals(sourceValue)) {
        token.put(labelType, targetValue)
      }
    }
  }

  /**
   * Updates or sets labels for selected labelType
   */
  def setLabeling(labelType: Label.Value, labels: Array[String]): Unit = {
    if (labels.length != this.size) {
      throw new Exception("Example and Labels size do not match!")
    } else {
      for ((l, i) <- labels.zipWithIndex) {
        this.get(i).put(labelType, l)
      }
    }
  }

  def setLabeling(labelType: Label.Value, labels: ArrayList[Any]): Unit = {
    if (labels.length != this.size) {
      throw new Exception("Example and Labels size do not match!")
    } else {
      for ((l, i) <- labels.zipWithIndex) {
        this.get(i).put(labelType, l)
      }
    }
  }

  def setLabeling(labelType: Label.Value, labels: Array[AnyRef]): Unit = {
    if (labels.length != this.size) {
      throw new Exception("Example and Labels size do not match!")
    } else {
      for ((l, i) <- labels.zipWithIndex) {
        this.get(i).put(labelType, l)
      }
    }
  }

  def setLabeling(labelType: Label.Value, classifier: Classifier): Unit = {
    val labels = classifier.classify(this)
    for ((token, label) <- this.zip(labels._1)) {
      token.put(labelType, label)
    }
  }

  def getAllMentions(): ArrayBuffer[Constituent] = {
    this.mentions
  }

  /**
   * CAUTION: If you set sort to false, mentions must be pre-sorted manually
   * reference to original example within Constituent is intact!!
   * @param mentions
   * @param sort
   */
  def setMentions(mentions: ArrayBuffer[Constituent], sort: Boolean = true): Unit = {
    this.mentions = mentions
    sortMentions()
  }

  def getFirstMentionConstituent(key: Label.Value, value: String) = {
    mentions.find(_.get(key).equals(value))
  }

  def getFirstConstituent(key: Label.Value, value: String): Option[Token] = {
    this.find(_.get(key).equals(value))
  }

  /**
   * CAUTION: If you set sort to false, mentions must be sorted manually
   * reference to original example within Constituent is intact!!
   * @param mention
   * @param sort
   */
  def addMention(mention: Constituent, sort: Boolean = true) = {
    var retVal = false
    if (!this.mentions.contains(mention)) {
      this.mentions.append(mention)
      retVal = true
    }
    if (sort) {
      this.sortMentions()
    }
    retVal
  }

  def sortMentions(): Unit = {
    Collections.sort(this.mentions)
  }

  def getAllRelationships(): ArrayBuffer[Relationship] = {
    this.relationships
  }

  def getAllTokenRelationships() = {
    this.tokenRelationships
  }

  def addTokenRelationship(name: String, from: Option[Token], to: Option[Token]): Unit = {
    this.tokenRelationships += ((name, from, to))
  }

  //reference to original example within Relationship is intact!!
  def setRelationships(relationships: ArrayBuffer[Relationship]): Unit = {
    this.relationships = relationships
  }

  def getFirstRelationship(key: Label.Value, value: String) = {
    relationships.find(_.get(key).equals(value)).get
  }

  //reference to original example within Relationship is intact!!
  def addRelationship(relationship: Relationship): Unit = {
    this.relationships.append(relationship)
  }

  //reference to original example within Relationship is intact!!
  def addRelationships(relationships: Iterable[Relationship]): Unit = {
    this.relationships.appendAll(relationships)
  }

  def getRelationship(leftCons: Constituent, rightCons: Constituent): Option[Relationship] = {
    for (relationship <- this.relationships) {
      if (relationship.subj.equals(leftCons) && relationship.obj.equals(rightCons) ||
          relationship.subj.equals(rightCons) && relationship.obj.equals(leftCons)) {
        return Some(relationship)
      }
    }
    return None
  }

  def ufeatures(i: Int): SparseVector[Double] = {
    this.get(i).ufeatures
  }

  def bfeatures(i: Int): SparseVector[Double] = {
    this.get(i).bfeatures
  }


  def toStringLabeling(label: Label.Value) =
    "Example \"%s\" labeled as:\n\t\"%s\"".format(
      this.observables().mkString(" "),
      this.getLabeling(label).mkString(" "))

  def printLabeling(label: Label.Value): Unit = {
    println(this.getLabeling(label).mkString(" "))
  }

  override
  def toString() = {
    this.observables().mkString(" ")
  }

  //only copies attributes!!
  override def clone() = {
    val retVal = new Example()

    for (key <- this.attributes.keySet()) {
      retVal.attributes.put(key, this.attributes.get(key))
    }

    retVal
  }

}