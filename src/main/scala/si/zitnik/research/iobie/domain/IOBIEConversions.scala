package si.zitnik.research.iobie.domain

import constituent.{Constituent}
import parse.ParseNode
import java.util.ArrayList

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/28/12
 * Time: 7:46 PM
 * To change this template use File | Settings | File Templates.
 */

object IOBIEConversions {
  //implicit conversions
  implicit def anyToString(any: Any) = any.asInstanceOf[String]

  implicit def anyToStringArray(any: Any) = any.asInstanceOf[Array[String]]

  implicit def anyToStringArrayList2(any: ArrayList[ArrayList[Any]]) = any.asInstanceOf[ArrayList[ArrayList[String]]]
  implicit def anyToStringArrayList3(any: ArrayList[ArrayList[String]]) = any.asInstanceOf[ArrayList[ArrayList[Any]]]

  //implicit def anyToStringArrayList(any: ArrayList[Any]) = any.asInstanceOf[ArrayList[String]]
  //implicit def anyToStringArrayList1(any: ArrayList[String]) = any.asInstanceOf[ArrayList[Any]]

  implicit def anyToInt(any: Any) = any.asInstanceOf[Int]
  implicit def anyToIntSet(any: Any) = any.asInstanceOf[Set[Int]]


  implicit def anyToIntParseTree(any: Any) = any.asInstanceOf[ParseNode]

  implicit def tokenToConstituent(token: Token) = token.asInstanceOf[Constituent]
}
