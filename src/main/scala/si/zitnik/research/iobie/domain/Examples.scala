package si.zitnik.research.iobie.domain

import cluster.Cluster
import constituent.Constituent
import parse.ParseNode

import scala.collection.JavaConversions._
import java.util.HashMap
import java.util.ArrayList
import java.util.HashSet

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.algorithms.crf.{Classifier, ExampleLabel, Label}
import si.zitnik.research.iobie.util.{AdderMap, CounterMap}

import collection.mutable.ArrayBuffer
import scala.collection.mutable


/**
 * The class containing all examples needed for learning.
 *
 */

class Examples extends ArrayList[Example] with StrictLogging {

  def text = {
    //Precalculate all the example texts
    for (example <- this) {
      example.put(ExampleLabel.TEXT, example.map(_.get(Label.OBS).asInstanceOf[String]).mkString(" "))
    }
    this.map(_.get(ExampleLabel.TEXT)).mkString(" ")
  }

  /**
   * Sets MentionConstituents from the CoreferenceResolution results.
   * 
   * Length of list of clusters must match the number of documents in the examples.
    *
    * @param clusters
   */
  def setMentionConstituents(clusters: ArrayBuffer[mutable.HashSet[Cluster]]): Unit = {
    //remove existing mention constituents
    this.foreach(_.getAllMentions().clear())
    
    //set mentions to appropriate examples
    if (this.getAllDocIds().length != clusters.length) {
      throw new Exception("Mention clusters and number of documents differ.")
    }
    for (documentClusters <- clusters) {
      for ((cluster, idx) <- documentClusters.zipWithIndex) {
        for (mention <- cluster) {
          mention.put(Label.COREF, idx)
          mention.example.addMention(mention)
        }
      }
    }
    this.foreach(_.sortMentions())
  }


  def getAllDocIds() = {
    val docIds = this.map(_.get(ExampleLabel.DOC_ID).asInstanceOf[String])

    var dSet = docIds.toSet
    val retVal = new ArrayBuffer[String]()
    docIds.foreach(v => {
      if (dSet.contains(v)) {
        retVal.append(v)
        dSet = dSet - v
      }
    })
    retVal
  }


  /**
   * Changes labels from source to target
   * @param labelType
   * @param sourceValue
   * @param targetValue
   */
  def relabel(labelType: Label.Value, sourceValue: String, targetValue: String): Unit = {
    for (example <- this) {
      example.relabel(labelType, sourceValue, targetValue)
    }
  }


  /**
   * The method returns hashmap containing arraylist of same document id examples as value.
   * If exampleLabel is null, then all examples are treated as one document
   * @return
   */
  def splitExamplesByDocuments(exampleLabel: ExampleLabel.Value = ExampleLabel.DOC_ID) = {
    val retVal = new AdderMap[String, Example]()

    if (exampleLabel == null) {
      val v = new ArrayBuffer[Example]()
      this.copyToBuffer(v)
      retVal.map.put("NO_ID", v)
    } else {
      for (example <- this) {
        retVal.put(example.get(exampleLabel), example)
      }
    }

    retVal.map
  }

  def transferLabeling(sourceLabelType: Label.Value, targetLabelType: Label.Value, removeSource: Boolean = false): Unit = {
    for (example <- this) {
      example.transferLabeling(sourceLabelType, targetLabelType, removeSource)
    }
  }


  def setLabeling(labelType: Label.Value, classifier: Classifier): Unit = {
    this.foreach(_.setLabeling(labelType, classifier))
  }


  /*
  deprecate
  def setLabeling(labelType: Label.Value, labeling: ArrayList[Any]) {
    var idx = 0
    for (example <- this) {
      for (token <- example) {
        token.put(labelType, labeling(idx))
        idx += 1
      }
    }
  }
  */

  //TODO: remove this -- transformed to ArrayBuffer
  def setLabeling(labelType: Label.Value, labeling: ArrayList[ArrayList[Any]]): Unit = {
    for ((example, labels) <- this.zip(labeling)) {
      for ((token, label) <- example.zip(labels)) {
        token.put(labelType, label)
      }
    }
  }

  def setLabeling(labelType: Label.Value, labeling: ArrayBuffer[ArrayBuffer[String]]): Unit = {
    for ((example, labels) <- this.zip(labeling)) {
      for ((token, label) <- example.zip(labels)) {
        token.put(labelType, label)
      }
    }
  }

  def setCorefLabelingUsingClusters(labelType: Label.Value): Unit = {
    for (example <- this) {
      for ((constituent,idx) <- example.zipWithIndex) {
        if (idx > 0 && constituent.cluster == example.get(idx-1).cluster) {
          constituent.put(labelType, "C")
        } else {
          constituent.put(labelType, "O")
        }
      }
    }
  }

  def this(array: Array[Example]) {
    this()
    this.addAll(array)
  }

  override def add(example: Example) = {
    super.add(example)
    example.examples = this
    true
  }

  def add(examples: Examples) = {
    for (example <- examples) {
      super.add(example)
      example.examples = this
    }
    true
  }

  override def addAll(examples: java.util.Collection[_ <: Example]) = {
    for (example <- examples) {
      super.add(example)
      example.examples = this
    }


    true
  }

  def addAll(examples: Array[Example]) = {
    for (example <- examples) {
      super.add(example)
      example.examples = this
    }
    true
  }

  /**
   * CAUTION: these values are returned as HashSet, so ordering is
   * not guaranteed!!!
   */
  def getAllLabelValues(labelType: Label.Value) = {
    val retVal = new HashSet[String]()
    this.foreach(example =>
      example.foreach(v => retVal.addAll(v.getStringValue(labelType)))
    )
    retVal
  }

  /**
   * CAUTION: these values are returned as HashSet, so ordering is
   * not guaranteed!!!
   */
  def getAllMentionLabelValues(labelType: Label.Value) = {
    val retVal = new HashSet[String]()
    this.foreach(example =>
      example.getAllMentions().foreach(v => retVal.addAll(v.getStringValue(labelType)))
    )
    retVal
  }

  /**
   * CAUTION: these values are returned as HashSet, so ordering is
   * not guaranteed!!!
   */
  def getAllRelationshipLabelValues(labelType: Label.Value) = {
    val retVal = new HashSet[String]()
    this.foreach(example =>
      example.getAllRelationships().foreach(v => retVal.addAll(v.getStringValue(labelType)))
    )
    retVal
  }

  def getAllExampleLabelValues(labelType: ExampleLabel.Value) = {
    val retVal = new HashSet[String]()
    this.foreach(example =>
      example.get(labelType) match {
        case t: ParseNode => retVal.addAll(t.nonLeafValues())
        case t: Any => retVal.add(t)
      })
    retVal
  }

  def getLabeling(labelType: Label.Value): ArrayList[ArrayList[Any]] = {
    val retVal = new ArrayList[ArrayList[Any]]()

    for (example <- this) {
      retVal.add(example.getLabeling(labelType))
    }

    retVal
  }

  def getLabelingWithObservables(labelType: Label.Value): ArrayList[ArrayList[String]] = {
    val retVal = new ArrayList[ArrayList[String]]()

    for (example <- this) {
      val tempList = new ArrayList[String]()
      example.foreach(token => tempList.add("%s/%s".format(token.get(Label.OBS), token.get(labelType))))
      retVal.add(tempList)
    }

    retVal
  }

  /**
   * This method returns all mentions, where they are separated by documents.
   * Default document identifier is DOC_ID.
   * @return
   */
  def getAllMentions(splitLabel: ExampleLabel.Value = ExampleLabel.DOC_ID): HashMap[String, ArrayList[Constituent]] = {
    val retVal = new HashMap[String, ArrayList[Constituent]]()

    for ((docId, exampleList) <- this.splitExamplesByDocuments(splitLabel)) {
      val documentMentions = new ArrayList[Constituent]()
      exampleList.foreach(v => documentMentions.addAll(v.getAllMentions()))
      retVal.put(docId, documentMentions)
    }
    retVal
  }

  /**
   * This method returns all mentions, where document identifier value is docId.
   * @return
   */
  def getAllMentionsFromDocument(docId: String, exampleLabel: ExampleLabel.Value = ExampleLabel.DOC_ID): ArrayBuffer[Constituent] = {
    val retVal = new ArrayBuffer[Constituent]()
    getDocumentExamples(docId, exampleLabel).foreach(v => retVal.addAll(v.getAllMentions()))
    retVal
  }

  def getExampleByToken(token: Token): Example = {
    for (example <- this) {
      if (example.contains(token)) {
        return example
      }
    }
    throw new Exception("Example for input token not found!")
  }

  def getExampleByTokenId(id: String): Example = {
    for (example <- this) {
      if (example.getLabeling(Label.ID).contains(id)) {
        return example
      }
    }
    throw new Exception("Example for input token not found!")
  }

  /**
   * It returns clusters that contain all appropriate mentions.
   * @param splitLabel
   */
  def getAllMentionClusters(splitLabel: ExampleLabel.Value = ExampleLabel.DOC_ID): HashMap[String, ArrayList[Cluster]] = {
    val retVal = new HashMap[String, ArrayList[Cluster]]

    for ((documentId, mentions) <- this.getAllMentions()) {
      val map = new HashMap[Int, Cluster]() //mapping from clusterId -> cluster

      for (mention <- mentions) {
        if (!map.containsKey(mention.get(Label.COREF))) {
          map.put(mention.get(Label.COREF).asInstanceOf[Int], new Cluster(mention))
        } else {
          map.get(mention.get(Label.COREF).asInstanceOf[Int]).add(mention)
        }
      }

      retVal.put(documentId, new ArrayList[Cluster](map.values()))
    }

    retVal
  }

  def removeLabeling(labelType: Label.Value): Unit = {
    for (example <- this) {
      example.removeLabeling(labelType)
    }
  }

  def removeDocumentExamples(docId: String) = {
    val newExamples = this.filter(!_.get(ExampleLabel.DOC_ID).equals(docId))
    val retVal = newExamples.size != this.size()

    this.clear()
    this.addAll(newExamples)

    retVal
  }

  def removeDocumentExamples(docIds: Set[String]) = {
    val newExamples = this.filter(v => !docIds.contains(v.get(ExampleLabel.DOC_ID)))
    val retVal = newExamples.size != this.size()

    this.clear()
    this.addAll(newExamples)

    retVal
  }

  def selectDocumentExamples(docIds: Set[String]) = {
    val newExamples = this.filter(v => docIds.contains(v.get(ExampleLabel.DOC_ID)))
    val retVal = newExamples.size != this.size()

    this.clear()
    this.addAll(newExamples)

    retVal
  }

  def subExamples(startIdx: Int, endIdx: Int) = {
    new Examples(this.subList(startIdx, endIdx).toArray[Example](Array[Example]()))
  }

  def getDocumentExamples(docId: String, exampleLabel: ExampleLabel.Value = ExampleLabel.DOC_ID): Examples = {
    val retVal = new Examples()

    for (example <- this) {
      if (example.get(exampleLabel).equals(docId)) {
        retVal.add(example)
      }
    }

    retVal
  }

  /**
   *
   * @param documentID
   * @return tuple: (inExamples, outExamples)
   */
  def leaveOneOut(documentID: String): (Examples, Examples) = {
    val leftOutExamples = new Examples()
    val inExamples = new Examples()

    for (example <- this) {
      if (example.get(ExampleLabel.DOC_ID).equals(documentID)) {
        leftOutExamples.add(example)
      } else {
        inExamples.add(example)
      }
    }

    (inExamples, leftOutExamples)
  }

  def printLabeling(labelType: Label.Value): Unit = {
    this.foreach(example => {
      example.printLabeling(labelType);
      println();
    })
  }


  def printStatistics(
                       ommited: Array[Label.Value] = Array(Label.OBS, Label.PARSE_NODE),
                       ommitedExample: Array[ExampleLabel.Value] = Array(ExampleLabel.DOC_ID),
                       ommitMentions: Boolean = true,
                       ommitedMentionAttributes: Array[Label.Value] = Array(),
                       ommitedRelationshipAttributes: Array[Label.Value] = Array()): Unit = {
    val allAttrs = new HashSet[Label.Value]()
    this.foreach(e =>
      e.foreach(a => allAttrs.addAll(a.keySet()))
    )
    val allExampleAttrs = new HashSet[ExampleLabel.Value]()
    this.foreach(e => allExampleAttrs.addAll(e.exampleLabelSet()))

    val allMentionAttrs = new HashSet[Label.Value]()
    this.foreach(_.getAllMentions().foreach(m => allMentionAttrs.addAll(m.keySet()) ))

    val allRelationshipAttrs = new HashSet[Label.Value]()
    this.foreach(_.getAllRelationships().foreach(r => allRelationshipAttrs.addAll(r.keySet()) ))

    var tokensNum = 0
    this.foreach(e => {
      tokensNum += e.size()
    })

    println("DATASET statistics: ")
    println(" No. of examples: " + this.size())
    println(" No. of attributes: " + allAttrs.size)
    println(" No. of tokens: " + tokensNum)
    println(" Attributes: " + allAttrs.toList.sorted.mkString(", "))
    println(" Attribute values:")
    allAttrs.foreach(attr => {
      if (ommited.contains(attr)) {
        println("\t%s: - ommited".format(attr.toString))
      } else {
        val labelValues = getAllLabelValues(attr)
        println("\t%s (%d): %s".format(attr.toString, labelValues.size, labelValues.mkString(", ")))
      }
    })
    println(" Example-level attributes:")
    allExampleAttrs.foreach(attr => {
      if (ommitedExample.contains(attr)) {
        println("\t%s: - ommited".format(attr.toString))
      } else {
        val labelValues = getAllExampleLabelValues(attr)
        println("\t%s (%d): %s".format(attr.toString, labelValues.size, labelValues.mkString(", ")))
      }
    })
    println(" Mentions:")
    if (!ommitMentions) {
      print("\t")
      var mentionCounter = 0
      this.foreach(example => {
        print(example.getAllMentions().mkString(", ")+", ")
        mentionCounter += example.getAllMentions().size
      })
      println()
      println("\tNo. of mentions: %d".format(mentionCounter))
    } else {
      println("\t - ommited")
    }
    println(" Mention attributes: " + allMentionAttrs.toList.sorted.mkString(", "))
    println(" Mention attribute values:")
    allMentionAttrs.foreach(attr => {
      if (ommitedMentionAttributes.contains(attr)) {
        println("\t%s: - ommited".format(attr.toString))
      } else {
        val labelValues = getAllMentionLabelValues(attr)
        println("\t%s (%d): %s".format(attr.toString, labelValues.size, labelValues.mkString(", ")))
      }
    })
    println(" Relationship attributes: " + allRelationshipAttrs.toList.sorted.mkString(", "))
    println(" Relationship attribute values:")
    allRelationshipAttrs.foreach(attr => {
      if (ommitedRelationshipAttributes.contains(attr)) {
        println("\t%s: - ommited".format(attr.toString))
      } else {
        val labelValues = getAllRelationshipLabelValues(attr)
        println("\t%s (%d): %s".format(attr.toString, labelValues.size, labelValues.mkString(", ")))
      }
    })
  }

  def printLabelingDistribution(labelType: Label.Value): Unit = {
    val cntrMap = new CounterMap[String]()

    for (example <- this) {
      for (token <- example) {
        cntrMap.put(token.get(labelType))
      }
    }

    println("Label %s value distribution:".format(labelType.toString))
    println("\t%s: %s\n\t-------------------".format("VALUE", "No.OCCURED"))
    for ((k, v) <- cntrMap.map) {
      println("\t%s: %d".format(k, v))
    }
  }

  def printDocument(docID: String): Unit = {
    println("### START DOCUMENT: %s".format(docID))
      for (example <- this) {
        if (example.get(ExampleLabel.DOC_ID).equals(docID)) {
          print(example.toString()+ " ")
        }
      }
    println()
    println("### END DOCUMENT: %s".format(docID))
  }

  def printMentionDetails(docID: String, ommitSingles: Boolean = false): Unit = {
    println("### START MENTION DETAILS: %s".format(docID))


    val multipleCorefIds = if (ommitSingles) {
      val counters = new CounterMap[Any]()
      for (example <- this) {
        if (example.get(ExampleLabel.DOC_ID).equals(docID)) {
          example.getAllMentions().foreach(v => counters.put(v.get(Label.COREF)))
        }
      }
      Some(counters.toSet(2))
    } else {None}

    var mentionID = 0
    for (example <- this) {
      if (example.get(ExampleLabel.DOC_ID).equals(docID)) {
        if (ommitSingles) {
          example.getAllMentions().foreach(v => {if (multipleCorefIds.get.contains(v.get(Label.COREF))) {println("\tmID: %d, %s".format(mentionID, v.toDetailString()))}; mentionID+=1})
        } else {
          example.getAllMentions().foreach(v => {println("\tmID: %d, %s".format(mentionID, v.toDetailString())); mentionID+=1})
        }
      }
    }
    println("### END MENTION DETAILS: %s".format(docID))
  }


}