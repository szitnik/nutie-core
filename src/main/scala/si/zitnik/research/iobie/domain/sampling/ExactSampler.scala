package si.zitnik.research.iobie.domain.sampling

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.algorithms.crf.ExampleLabel

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.IOBIEConversions._

import util.Random

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/14/13
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */
object ExactSampler extends StrictLogging {

  /**
   * The method samples the dataset to:
   *  trainingData = numOfTrainDocuments
   *  testData = numberOfDocuments-numOfTrainDocuments
   *
   * it returns new datasets (trainingData, testData)
   * @param examples
   * @param numOfTrainDocuments
   * @return
   */
  def sampleNumber(examples: Examples, numOfTrainDocuments: Int, random: Boolean = true): (Examples, Examples) = {
    //get all document IDs into list
    var docIDs = examples.getAllExampleLabelValues(ExampleLabel.DOC_ID).toList.sortWith(_.toString < _.toString)
    if (random) {
      docIDs = Random.shuffle(docIDs)
    }
    val trainingDocIDs = docIDs.take(numOfTrainDocuments).toSet
    doSampling(examples, trainingDocIDs)
  }



  /**
   * The method samples the dataset to:
   *  trainingData = trainPercent*numberOfDocuments
   *  testData = (1-trainPercent)*numberOfDocuments
   *
   * it returns new datasets (trainingData, testData)
   * @param examples
   * @param trainPercent
   * @return
   */
  def samplePercent(examples: Examples, trainPercent: Double = 0.7, random: Boolean = true): (Examples, Examples) = {
    //get all document IDs into list
    var docIDs = examples.getAllExampleLabelValues(ExampleLabel.DOC_ID).toList.sortWith(_.toString < _.toString)
    if (random) {
      docIDs = Random.shuffle(docIDs)
    }
    val trainingDocIDs = docIDs.take(math.round(docIDs.size*trainPercent).toInt).toSet
    doSampling(examples, trainingDocIDs)
  }


  private def doSampling(examples: Examples, trainingDocIDs: Set[String]) = {
    val trainingData = new Examples()
    val testData = new Examples()
    //fill the result values
    for (example <- examples) {
      if (trainingDocIDs.contains(example.get(ExampleLabel.DOC_ID))) { //add to train domain
        trainingData.add(example)
      } else { //add to test domain
        testData.add(example)
      }
    }

    //warn if one of the datasets is 0-sized
    if (trainingData.size() == 0) {
      logger.warn("Sampled training domain contains no examples! Possible error of too few documents od no document identifier defined!")
    }
    if (testData.size() == 0) {
      logger.warn("Sampled test domain contains no examples!  Possible error of too few documents od no document identifier defined!")
    }

    (trainingData, testData)
  }
}
