package si.zitnik.research.iobie.domain.parse

import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/28/12
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
object ParseTree extends StrictLogging {

  def setParseTree(example: Example, fullParseString: String): Unit = {
    val parseTree = ParseTree.parseParseString(fullParseString)

    //add to example attributes
    example.put(ExampleLabel.PARSE_TREE, parseTree)

    example zip parseTree.allLeaves() foreach {case (token, leaf) => {
      //map parse tree strings to token OBS values to save space
      leaf.value = token.get(Label.OBS)
      //link example labeling to parse node
      token.put(Label.PARSE_NODE, leaf)
    }}
  }


  /**
   * Create parse node (= root of parse tree)
   * @param value
   * @return
   */
  def parseParseString(value: String): ParseNode = {
    if (value.startsWith("(") && value.endsWith(")")) {
      var curString = value.substring(1, value.length() - 1)

      if (curString.contains("(")) {
        //get node's value
        val curValue = curString.substring(0, curString.indexOf("("))
        curString = curString.replaceFirst(curValue, "")


        //process children children
        val childrens = new ArrayList[ParseNode]()
        var braceletCounter = 0
        var left = 0
        for (position <- 0 until curString.length()) {
          if (curString.charAt(position) == '(') {
            braceletCounter += 1
          } else if (curString.charAt(position) == ')') {
            braceletCounter -= 1
          }
          if (braceletCounter == 0) {
            childrens.add(parseParseString(curString.substring(left, position + 1)))
            left = position + 1
          }
        }
        //create children nodes & return

        val retVal = new ParseNode(curValue)
        childrens.foreach(c => c.parent = retVal)
        retVal.children = childrens.toArray(Array[ParseNode]())
        retVal
      } else {
        val labels = curString.split(" ")

        if (labels.length != 2) {
          logger.warn("Parse tree parsed wrong. There are words containing spaces: \"%s\" in \"%s\"!".format(curString, value))
        }

        val retVal = new ParseNode(labels(0))
        val childrens = new ArrayList[ParseNode]()
        childrens.add(new ParseNode(labels(1), parent = retVal))

        retVal.children = childrens.toArray(Array[ParseNode]())
        retVal
      }


    } else {
      logger.error("Error building parse tree! The input value is '%s'.".format(value))
      System.exit(-1)
      null
    }
  }


  //merges parse trees (1 or more, ...)
  def merge(value: String, nodes: ParseNode*): ParseNode = {
    val retVal = new ParseNode(value, nodes.toArray)
    nodes.foreach(t => t.parent = retVal)
    retVal
  }
}
