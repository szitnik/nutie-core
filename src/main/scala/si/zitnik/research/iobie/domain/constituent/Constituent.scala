package si.zitnik.research.iobie.domain.constituent

import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.{Example, Token}
import si.zitnik.research.iobie.domain.cluster.Cluster

import scala.collection.JavaConversions._

/**
 *  Source: Wikipedia, "In syntactic analysis, a constituent is a word or a group of words that functions
 *  as a single unit within a hierarchical structure."
 *
 *  We use this trait for presentation of group of words, for example:
 *     - used by multiple word gazeteer
 *     - presents mentions, ...
 *
 *  endIdx should be position+1 = like it is in sublist methods
 */
class Constituent(val example: Example,
                  val startIdx: Int,
                  val endIdx: Int) extends Token(Label.OBS, example.getLabeling(Label.OBS).subList(startIdx, endIdx).mkString(" ")) with Ordered[Constituent]  {
  var cluster: Cluster = null
  /**
   * oldConstituent is used when clustering after coreference classification. By that we can get
   * reference to original mention constituent. The value is set by Constituent.clone method.
   *
   * Original mention constituents:
   *  - when learning: mentions cloned from examples.mentions, so they have reference to real mentionIds
   *  - when inferencing: plain mentions, detected by Mention detection module
   */
  var oldConstituent: Constituent = null

  def this(example: Example, startIdx: Int, endIdx: Int, mentionId: Int) {
    this(example, startIdx, endIdx)
    this.put(Label.COREF, mentionId)
  }

  /**
    * Returns starting character idx within a document.
    * @return
    */
  def startTextIdx = {
    val docTokens = example.examples.getDocumentExamples(example.get(ExampleLabel.DOC_ID).asInstanceOf[String]).flatten

    //Get all tokens left from the token and sum all the lengths + spaces
    docTokens.slice(0, docTokens.indexOf(example.get(startIdx))).
      foldLeft(0)((a,v) => a+v.get(Label.OBS).asInstanceOf[String].length+1)
  }

  /**
    * Returns ending character idx within a document (non-inclusive).
    * @return
    */
  def endTextIdx = {
    val docTokens = example.examples.getDocumentExamples(example.get(ExampleLabel.DOC_ID).asInstanceOf[String]).flatten
    val endToken = example.get(endIdx-1)

    //Get all tokens left from the token and sum all the lengths + spaces
    val tokenStartCharacterIdx = docTokens.slice(0, docTokens.indexOf(endToken)).
      foldLeft(0)((a,v) => a+v.get(Label.OBS).asInstanceOf[String].length+1)

    //Sum the length of the token
    tokenStartCharacterIdx + endToken.get(Label.OBS).asInstanceOf[String].length
  }


  override def get(key: Any) = {
    if (super.containsKey(key)) {
      super.get(key)
    } else { //try first token in the example
      example.get(startIdx, key.asInstanceOf[Label.Value])
    }
  }

  def getLemma() = {
    example.subLabeling(Label.LEMMA, startIdx, endIdx).mkString(" ")
  }


  def getText() = this.get(Label.OBS).toString

  override def toString() = this.get(Label.OBS).toString

  def toDetailString() = "\t corefID: %s - %s".format(this.get(Label.COREF), this.toString())

  override def compare(c: Constituent) = {
    var diff = this.startIdx - c.startIdx
    if (diff == 0) {
      diff = this.endIdx - c.endIdx
      if (diff == 0) {
        if (this.example != c.example) {
          diff = -1
        }
      }
    }
    diff
  }


  override def equals(o: Any) = {
    if (this.hashCode() == o.hashCode()) {
      true
    } else {
      false
    }
  }

  override
  def hashCode() = {
    var h = 0
    //h = 31*h+super.hashCode()
    h = 31*h+example.hashCode()
    h = 31*h+startIdx.hashCode()
    h = 31*h+endIdx.hashCode()
    //TODO stack overflow problem here - when hashcode on cluster initiated (recursion)
    //h = 31*h+ (if (cluster == null) {0} else {cluster.hashCode()})
    h = 31*h+ (if (oldConstituent == null) {0} else {oldConstituent.hashCode()})
    h
  }
}

object Constituent {

  /**
   * It is important to acknowledge that example is still the old one.
   * @param oldConstituent
   * @return
   */
  def clone(oldConstituent: Constituent): Constituent = {
    val newToken = new Constituent(oldConstituent.example, oldConstituent.startIdx, oldConstituent.endIdx)
    for (labelKey <- oldConstituent.keySet()) {
      newToken.put(labelKey, oldConstituent.get(labelKey))
    }
    newToken.oldConstituent = oldConstituent
    newToken
  }

}
