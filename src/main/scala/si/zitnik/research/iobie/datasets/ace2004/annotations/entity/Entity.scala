package si.zitnik.research.iobie.datasets.ace2004.annotations.entity

import java.util.ArrayList

class Entity(
              var entityMentions: ArrayList[EntityMention],
              var entityAttributes: ArrayList[EntityAttribute],
              val id: String,
              val entityType: String,
              val subType: String,
              val entityClass: String) {

  override def toString() = {
    Map("Mentions" -> entityMentions, "Attributes" -> entityAttributes,
      "id" -> id, "type" -> entityType, "subType" -> subType,
      "entityClass" -> entityClass).mkString(",")
  }


}