package si.zitnik.research.iobie.datasets.conll2012

import java.io.FileWriter

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.Examples

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer

/**
 * This class exports CoNLL 2012 domain from Examples data type.
 *
 * File structure type:
 *
 *

Column 	Type 	Description
    1 	Document ID 	This is a variation on the document filename
    2 	Part number 	Some files are divided into multiple parts numbered as 000, 001, 002, ... etc.
    3 	Word number
    4 	Word itself 	This is the token as segmented/tokenized in the Treebank. Initially the *_skel file contain the placeholder [WORD] which gets replaced by the actual token from the Treebank which is part of the OntoNotes release.
    N 	Coreference 	Coreference chain information encoded in a parenthesis structure.
 */
class CoNLL2012Exporter extends StrictLogging {


  def exportConll(examples: Examples, filename: String): Unit = {

    val fw = new FileWriter(filename, false)

    var currentMeta: String = null
    for (example <- examples) {

      if (currentMeta != null && !currentMeta.equals(example.get(ExampleLabel.METADATA))) {
        fw.write("#end document" + System.lineSeparator())
      }
      if (!example.get(ExampleLabel.METADATA).equals(currentMeta)) {
        currentMeta = example.get(ExampleLabel.METADATA).asInstanceOf[String]
        fw.write(currentMeta + System.lineSeparator())
      }

      for ((token, wordNo) <- example.zipWithIndex) {
        val docId = example.get(ExampleLabel.DOC_ID)
        val partNo = example.get(ExampleLabel.PART_NO)
        val word = token.get(Label.OBS)

        var coreferenceArray = ArrayBuffer[String]()
        for (mention <- example.getAllMentions()) {
          if (mention.startIdx == wordNo && mention.startIdx == mention.endIdx-1) {
            coreferenceArray += "(" + mention.get(Label.COREF) + ")"
          } else if (mention.startIdx == wordNo) {
            coreferenceArray += "(" + mention.get(Label.COREF)
          } else if (mention.endIdx-1 == wordNo) {
            coreferenceArray += mention.get(Label.COREF) + ")"
          }
        }
        val coreference =
          if (coreferenceArray.isEmpty) {
            "-"
          } else {
            coreferenceArray.mkString("|")
          }

        fw.write("%s\t%s\t%s\t%s\t%s".format(docId, partNo, wordNo, word, coreference) + System.lineSeparator())
      }

      //add empty line
      fw.write(System.lineSeparator())
    }
    fw.write("#end document" + System.lineSeparator())

    fw.close()

  }

}

