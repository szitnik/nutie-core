package si.zitnik.research.iobie.datasets.conll2012

import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.domain.Examples

import scala.Array
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 5/9/12
 * Time: 10:07 AM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2012ImporterApp {
  def main(args: Array[String]): Unit = {

    val path = "/Users/slavkoz/Documents/Research/Datasets/CoNLL2012/dataset/mergedFinalData_v4dt_v9t/conll-2012/v4/data"
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = CoNLL2012ImporterDatasetTypeEnum.DEVELOPMENT,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = Array(CoNLL2012ImporterSourceTypeEnum.NEWSWIRE),
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    //val conll2012Importer = new CoNLL2012Importer(path)

    val examples = conll2012Importer.importForIE()
    examples.printStatistics(ommited = Array(Label.OBS, Label.PARSE_NODE))


    //TEST COREF data
    val filteredExamples = new Examples(examples.filter(e => e.get(ExampleLabel.DOC_ID).equals("nw/wsj/00/wsj_0020") || e.get(ExampleLabel.DOC_ID).equals("nw/wsj/00/wsj_0037") ||
                         e.get(ExampleLabel.DOC_ID).equals("nw/wsj/00/wsj_0049") || e.get(ExampleLabel.DOC_ID).equals("nw/wsj/00/wsj_0089")).toArray)

    val mentionFilteredExamples = new MentionExamplesBuilder(
      filteredExamples,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    val allRealClusters = TaggedClusterer.doClustering(mentionFilteredExamples, ommitSingles = false)
    filteredExamples.setMentionConstituents(allRealClusters)

    new CoNLL2012Exporter().exportConll(filteredExamples, "/Users/slavkoz/Downloads/homogenized-test.out")
  }
}
