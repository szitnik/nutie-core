package si.zitnik.research.iobie.datasets.bionlp2013.util

import si.zitnik.research.iobie.domain.{Example, Examples}

import io.Source
import collection.mutable

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.util.MapperMap

import java.util.Collections

import com.typesafe.scalalogging.StrictLogging

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 1:56 PM
 * To change this template use File | Settings | File Templates.
 */
object A1Importer extends StrictLogging {

  def mapMentions(example: Example, mentions: Iterable[A1Mention], docId: String): Unit = {
    var mentionNotExact = 0
    for (mention <- mentions) {
      Util.createConstituent(example, mention) match {
        case (cons, isExact) => {
          example.getAllMentions().append(cons)
          if (!isExact) {
            mentionNotExact += 1
          }
        }
      }
    }
    if (mentionNotExact > 0) {
      logger.info("Wrongly extracted %d mention of all %d mentions from %s.".format(mentionNotExact, mentions.size, docId))
    }
  }

  def enrich(filename: String, docId: String, examples: Examples) = {
    val corefMapping = new MapperMap[String]()

    //1. read from file
    val mentions = new mutable.HashMap[String, A1Mention]()

    try {
      for (line <- Source.fromFile(filename).getLines() if !line.isEmpty) {
        line.charAt(0) match {
          case 'T' => {
            val newline = line.split("\t")
            val loc = newline(1).split("\t| |;")
            mentions.put(newline(0), A1Mention(newline(0), loc.head, loc(1).toInt, loc.last.toInt, newline(2)))
          }
          case 'N' => {
            val newline = line.split("\t| |:")
            //mentions.get(newline(3)).get.coref_id = corefMapping.put(newline(5))
            mentions.get(newline(3)).get.coref_id = newline(5)
          }
          case _ => throw new Exception()
        }
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        logger.error("BioNLP .a1 file \"%s\" does not follow the rules".format(filename))
        System.exit(-1)
    }


    if (examples.size() > 1) {
      logger.error("Each document must consist of a single sentence!")
      System.exit(-1)
    }

    //2. map mentions
    mapMentions(
      examples.get(0),
      mentions.values.toList.sortBy(_.start_id),
      docId
    )

    //3.
    for (example <- examples) {
      //order mentions
      Collections.sort(example.getAllMentions())

      //need to set all mention constituents attribute type due to IOBIE constraints
      example.getAllMentions().foreach(_.put(Label.ATTRIBUTE_TYPE, "MENTION"))
    }
  }

}
