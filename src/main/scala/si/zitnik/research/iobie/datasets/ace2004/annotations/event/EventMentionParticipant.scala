package si.zitnik.research.iobie.datasets.ace2004.annotations.event

import si.zitnik.research.iobie.datasets.ace2004.annotations.RawTag

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/5/11
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */

class EventMentionParticipant(
                               val entityId: String,
                               val entityMentionId: String,
                               val role: String,
                               val source: String,
                               val extent: RawTag,
                               val evMenParRelExtent: RawTag) {

}