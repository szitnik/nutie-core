package si.zitnik.research.iobie.datasets.ace2004.annotations.entity

import si.zitnik.research.iobie.datasets.ace2004.annotations.RawTag

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/25/11
 * Time: 1:44 PM
 * To change this template use File | Settings | File Templates.
 */

class EntityMention(
                     val id: String,
                     val typem: String,
                     val ldctye: String,
                     val role: String,
                     val reference: String,
                     val metonymyMention: String,
                     val ldcatr: String,
                     val extent: RawTag,
                     val head: RawTag) {

}