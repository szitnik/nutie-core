package si.zitnik.research.iobie.datasets.ace2004.util

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import java.util._

import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.datasets.ace2004.doc.TextDocument
import si.zitnik.research.iobie.datasets.ace2004.annotations.{ACE2004Token, AnnotationsDocument}
import si.zitnik.research.iobie.datasets.ace2004.annotations.entity.{Entity, EntityAttribute, EntityMention}

import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.thirdparty.opennlp.api.SentenceDetector
import si.zitnik.research.iobie.domain.relationship.Relationship

import com.typesafe.scalalogging.StrictLogging


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/6/11
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */

class DatasetManager(val tDoc: TextDocument, val annoDoc: AnnotationsDocument) extends StrictLogging {

  def printAllEntities(): Unit = {
    for ( entity: Entity <- annoDoc.entities) {
      for ( entityMention: EntityMention <- entity.entityMentions) {
        println(
          "Annotated extent mention: \"%s\" at [%d-%d]\nMention in text: \"%s\".".format(
            entityMention.extent.text,
            entityMention.extent.start,
            entityMention.extent.end,
            tDoc.text.substring(entityMention.extent.start, entityMention.extent.end + 1))
        )
        if (entityMention.extent.text != tDoc.text.substring(entityMention.extent.start, entityMention.extent.end + 1)) {
          println("%s".format(tDoc.text.substring(entityMention.extent.start, entityMention.extent.end + 1)))
          println("%s".format(entityMention.extent.text))
        }
      }
    }
  }

  def getEntityMentionTypes(): HashSet[String] = {
    val retVal = new HashSet[String]()
    for ( entity: Entity <- annoDoc.entities) {
      for ( entityMention: EntityMention <- entity.entityMentions) {
        retVal.add(entityMention.typem)
      }
    }
    retVal
  }

  def getRelationTypes(): HashSet[String] = {
    val retVal = new HashSet[String]()
    for (relation <- annoDoc.relations) {
      retVal.add(relation.typer)
    }
    retVal
  }

  /**
   * returns (start idx, end idx, label value, label type)
   * @return
   */
  def getAllRelTagRelations(): ArrayList[(Int, Int, String, Label.Value)] = {
    val retVal = new ArrayList[(Int, Int, String, Label.Value)]()

    val acceptedTypes = new HashSet[String](Array[String]("PHYS", "PER-SOC", "EMP-ORG", "GPE-AFF").toSeq)
    val acceptedLC = new HashSet[String](Array[String]("Possessive", "Preposition", "Formulaic", "PreMod").toSeq)

    for (relation <- annoDoc.relations) {
      if (acceptedTypes.contains(relation.typer)) {
        for (relationMention <- relation.relMentions) {
          if (acceptedLC.contains(relationMention.ldlexicalcondition)) {
            retVal.add((relationMention.ldcExtent.start, relationMention.ldcExtent.end, relation.typer, Label.REL))
          }
        }
      }
    }

    retVal
  }

  def addRelTagRelations(examples: Examples): Unit = {
    doTag(Label.REL, getAllRelTagRelations(), examples)
  }

  private def getMentionConstituent(consId: String, examples: Examples) = {
    val retVal = new ArrayBuffer[Constituent]
    for (example <- examples) {
      retVal.appendAll( example.getAllMentions().filter(_.get(Label.ID).asInstanceOf[String].equals(consId)) )
    }

    if (retVal.size != 1) {
      logger.error("Multiple constituents across sentences in a document '%s', sentences, consId = %s : '\n\t%s'".format(
        tDoc.docno,
        consId,
        retVal.map(_.example.getLabeling(Label.OBS).mkString("\n\t"))))
      System.exit(-1)
    }
    retVal(0)
  }

  def addRelations(examples: Examples): Unit = {
    for (relation <- annoDoc.relations) {
      for (relationMention <- relation.relMentions) {
        if (
          !(
              (relation.id.equals("APW20001127.1346.0419-R4") && relationMention.id.equals("4-1")) ||
              (relation.id.equals("APW20001127.1346.0419-R5") && relationMention.id.equals("5-1")) ||
              (relation.id.equals("APW20001127.1346.0419-R6") && relationMention.id.equals("6-1")) ||
              (relation.id.equals("ABC20001103.1830.1134-R1") && relationMention.id.equals("1-1")) ||
              (relation.id.equals("ABC20001213.1830.0502-R7") && relationMention.id.equals("7-1"))
            )
        ) {
          if (relationMention.relMentionArgs.size() != 2) {
            logger.error("There exist a non-binary relation '%s' in a document '%s'".format(relation, tDoc.docno))
            System.exit(-1)
          }

          val (subjectr, objectr) = if (relationMention.relMentionArgs(0).argnum.equals("1")) {
            (relationMention.relMentionArgs(0), relationMention.relMentionArgs(1))
          } else {
            (relationMention.relMentionArgs(1), relationMention.relMentionArgs(0))
          }

          val docIdExamples = examples.getDocumentExamples(tDoc.docno)
          val subjectCons = getMentionConstituent(subjectr.entityMentionId, docIdExamples)
          val objectCons = getMentionConstituent(objectr.entityMentionId, docIdExamples)

          if (subjectCons.example != objectCons.example) {
            logger.error("There exist a pair of mentions in %s for relation %s, id %s,mention %s \n\t'%s'\n\t'%s' \nthat are across multiple examples: \n\t'%s', \n\t'%s'".format(
              tDoc.docno,
              relation.typer,
              relation.id,
              relationMention.id,
              subjectr.extent.text,
              objectr.extent.text,
              subjectCons.example.getLabeling(Label.OBS),
              objectCons.example.getLabeling(Label.OBS)
            ))
            System.exit(-1)
          }

          val example = subjectCons.example
          val relationship = new Relationship(example, relation.typer, subjectCons, objectCons)
          relationship.put(Label.RELATION_SUBTYPE, relation.subtype)
          relationship.put(Label.ID, relationMention.id)
          relationship.put(Label.EXTENT, relationMention.ldcExtent.text)
          relationship.put(Label.LEXICAL_CONDITION, relationMention.ldlexicalcondition)



          example.addRelationship(relationship)
        }
      }
    }
  }


  /**
   * returns (start idx, end idx, label value, label type)
   * @return
   */
  def getAllNAMTagEntities(): ArrayList[(Int, Int, String, Label.Value)] = {
    val retVal = new ArrayList[(Int, Int, String, Label.Value)]()

    for (entity <- annoDoc.entities) {
      for (entityMention <- entity.entityMentions) {
        if (entityMention.typem.equals("NAM")) {
          retVal.add((entityMention.head.start, entityMention.head.end, entity.entityType, Label.NE))
        }
      }
    }

    retVal
  }

  /**
   * returns (start idx, end idx, label value, label type)
   * @return
   */
  def getAllMentions(): ArrayList[(EntityMention, Entity)] = {
    val retVal = new ArrayList[(EntityMention, Entity)]()

    for (entity <- annoDoc.entities) {
      for (entityMention <- entity.entityMentions) {
          retVal.add((entityMention, entity))
      }
    }

    retVal
  }

  def addNAMTagEntities(examples: Examples): Unit = {
    doTag(Label.NE, getAllNAMTagEntities(), examples)
  }

  private def doTag(labelType: Label.Value, tagList: ArrayList[(Int, Int, String, Label.Value)], examples: Examples): Unit = {
    if (tagList.size() == 0) { //no tags within examples
      for (example <- examples) {
        for (token <- example) {
          token.put(labelType, "O")
        }
      }
      return
    }
    var tags = tagList.sortWith(_._1 < _._1)

    //check for overlaps
    for (i <- 1 until tags.length) {
      if (tags(i - 1)._2 > tags(i)._1) {
        logger.warn("There is tag overlap for label type %s!".format(labelType.toString))
      }
    }

    for (tag <- tags) {
      if (!tag._4.equals(labelType)) {
        logger.error("Error, not all labeltypes in tags are the same!!!")
        System.exit(-1)
      }
    }

    //do tagging
    var tagIdx = 0
    for (example <- examples) {
      for (token <- example) {
        val aceToken = token.asInstanceOf[ACE2004Token]
        aceToken.put(labelType, "O")

        if (tagIdx < tags.length) {
          val tag = tags(tagIdx)

          var endIdx = aceToken.startIdx + aceToken.get(Label.OBS).asInstanceOf[String].length() - 1
          if (aceToken.get(Label.OBS).asInstanceOf[String].contains("&")) {
            endIdx += 4
          }

          if ((tag._1 to tag._2).intersect(aceToken.startIdx to endIdx).size != 0) {
            aceToken.put(tag._4, tag._3)
            while (tagIdx < tags.length && tags(tagIdx)._2 <= endIdx) {
              tagIdx += 1
            }
          }

        }
      }
    }

    if (tagIdx < tags.size) {
      val sIdx = examples.last.last.asInstanceOf[ACE2004Token].startIdx
      logger.warn("Not all tags were tagged - there is highly possibly an error! Doc ID: %s".format(tDoc.docno))
    }
  }


  def addCOREFConstituents(examples: Examples): Unit = {
    //currently selected: extend
    val aceMentions = getAllMentions().sortWith(_._1.head.start < _._1.head.start)
    var aceMentionIdx = 0

    for (example <- examples) {
      val mentions = new ArrayBuffer[Constituent]()

      val exampleBounds = example(0).asInstanceOf[ACE2004Token].startIdx to example.last.asInstanceOf[ACE2004Token].startIdx+example.last.get(Label.OBS).toString.length
      while (aceMentionIdx < aceMentions.length &&
             (aceMentions(aceMentionIdx)._1.head.start to aceMentions(aceMentionIdx)._1.head.end).intersect(exampleBounds).size != 0) {

        val aceMention = aceMentions(aceMentionIdx)
        var startIdx = 0
        while (startIdx < example.length &&
               !(example(startIdx).asInstanceOf[ACE2004Token].startIdx <= aceMention._1.head.start &&
               aceMention._1.head.start < example(startIdx).asInstanceOf[ACE2004Token].startIdx+example(startIdx).get(Label.OBS).toString.length)) {
          startIdx += 1
        }

        var endIdx = startIdx + 1
          var fw = false
          while (!example.subList(startIdx, endIdx).map(_.get(Label.OBS)).mkString("").equals(aceMention._1.head.text.replaceAll(" ", "")) && !fw) {
            endIdx += 1
            if (endIdx >= example.size()) {
              logger.warn("Mention '%s' splitted across two examples.".format(aceMention._1.extent.text))
              endIdx -= 1
              fw = true
            }
          }



          val mention = new Constituent(example, startIdx, endIdx, aceMention._1.id.split("-")(0).toInt)
          mention.put(Label.EXTENT, aceMention._1.extent.text)
          mention.put(Label.MENTION_TYPE, aceMention._1.typem)
          mention.put(Label.LDC_MENTION_TYPE, aceMention._1.ldctye)
          mention.put(Label.ENTITY_CLASS, aceMention._2.entityClass)
          mention.put(Label.ENTITY_TYPE, aceMention._2.entityType)
          mention.put(Label.ENTITY_SUBTYPE, aceMention._2.subType)
          mention.put(Label.ID, aceMention._1.id)

          mentions += mention
          aceMentionIdx += 1

      }


      //order mentions
      Collections.sort(mentions)
      //delete token mentions if exist
      example.setMentions(mentions)
      example.removeLabeling(Label.COREF)
    }
  }

  /*
  //TODO: this one not working
  def toExamplesOLD(): Examples = {
    var textPosId = tDoc.text.indexOf(tDoc.textOnly)
    val examples = new StanfordCoreNLPTokenizerAndSsplit().tokenizeAndSsplit(tDoc.textOnly, Some(tDoc.docno),
      (startIdx: Int, text: String) => {
        val retVal = new ArrayBuffer[Token]()

        val token = new ACE2004Token(textPosId + startIdx, Label.OBS, text)
        retVal.add(token)

        retVal
      }
    )
    examples
  } */

  /**
   * Tokenized only by whitespaces. Examples contain only OBS values.
   */
  //TODO: refactor into nicer code :)
  def toExamples(): Examples = {
    val examples = new Examples()

    val exampleBoundaries = new HashSet[Char](Array[Char]('?', '.', '!').toSeq)

    val tokenSplitters = new HashSet[Char](Array[Char](',', ':', ';', '-', '\"', '\'', '(', ')').toSeq)
    tokenSplitters.addAll(exampleBoundaries)


    val addToExample = (example: Example, textPosId: Int, word: String) => {
      if (word.length() > 0) {
          example.add(new ACE2004Token(textPosId, Label.OBS, word))
      }
    }


    var textPosId = tDoc.text.indexOf(tDoc.textOnly)

    //remove everything before TEXT tag
    for (entity <- annoDoc.entities) {
      entity.entityMentions = new ArrayList(entity.entityMentions.filter(_.extent.start >= textPosId))
      val list = new ArrayList[EntityAttribute]();
      for (attribute <- entity.entityAttributes) {
        val entAttr = new EntityAttribute()
        entAttr.addAll(attribute.filter(_.start >= textPosId))
        list.add(entAttr)
      }
      entity.entityAttributes = list
    }
    //TODO: do this for events also
    //annoDoc.events(0).eventMentions(0)
    for (relation <- annoDoc.relations) {
      relation.relMentions = new ArrayList(relation.relMentions.filter(v => {
        v.relMentionArgs(0).extent.start >= textPosId && v.relMentionArgs(1).extent.start >= textPosId
      }))
    }

    val endTextPostId = textPosId+tDoc.textOnly.length

    val sentences = new SentenceDetector().detectSentences(tDoc.textOnly, Array(
        " mt."
      , " st."
      , " Mich."
      , " mr."
      , "u.s."
      , "the u.s."
      , " Tenn."
      , " Wis."
      , "Onvia.com"
      , " u.n."
      , " Mass."
      , " n."
      , " o.j."
      , " Gen."
      , " Mt."
      , " ft."
      , " Sgt."
      , " Calif."
      , " Yahoo!"
      , " Wyo."
      , " Creditcards.com"
      , "Tickets.com,"
      , "team.victorious"
    ))

    //val sentences = new StanfordCoreNLPTokenizerAndSsplit().detectSentences(tDoc.textOnly)
    var sentenceIdx = 0
    //+1 is for whitespace
    var nextSentenceStartIdx = textPosId + sentences(sentenceIdx).length

    var example = new Example()
    while (textPosId < endTextPostId) {
      var endWordIdx = tDoc.text.indexOf(" ", textPosId)



        //trim head of word
        if (textPosId != endWordIdx && !tDoc.text.substring(textPosId).charAt(0).isLetterOrDigit && !tDoc.text.substring(textPosId, endWordIdx).startsWith("&AMP;")) {
          endWordIdx = textPosId+1
        }
        //trim tail of word
        var trimmed = false
        var idx = textPosId+1
        while (idx <= endWordIdx && !trimmed) {
          if (tDoc.text.substring(idx, endWordIdx).startsWith("&AMP;")) {
            idx += 5
          } else if (tDoc.text.substring(idx-1, endWordIdx).startsWith("&AMP;")) {
            idx += 4
          } else if (!tDoc.text.charAt(idx).isLetterOrDigit) {
            endWordIdx = idx
            trimmed = true
          } else {
            idx += 1
          }
        }


      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("its")) {
        endWordIdx = textPosId + 2
      }

      //correct mistakes in ACE2004 dataset
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("butader") && tDoc.docno.equals("ABC20001017.1830.0661")) {
        endWordIdx = textPosId + 3
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("thinpeople") && tDoc.docno.equals("ABC20001025.1830.0369")) {
        endWordIdx = textPosId + 4
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("storeowners") && tDoc.docno.equals("VOA20001008.2100.0715")) {
        endWordIdx = textPosId + 5
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("lets") && tDoc.docno.equals("PRI20001211.2000.0650")) {
        endWordIdx = textPosId + 3
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("drugmakers") && tDoc.docno.equals("CNN20001012.1400.1027")) {
        endWordIdx = textPosId + 9
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("teams") && tDoc.docno.equals("VOA20001031.2100.1212")) {
        endWordIdx = textPosId + 4
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("myob") && tDoc.docno.equals("ABC20001219.1830.0368")) {
        endWordIdx = textPosId + 2
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("titsance") && tDoc.docno.equals("ABC20001031.1830.1508")) {
        endWordIdx = textPosId + 4
      }
      if (tDoc.text.substring(textPosId, endWordIdx).toLowerCase.equals("states") && tDoc.docno.equals("PRI20001129.2000.0376")) {
        endWordIdx = textPosId + 5
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("Chings") && tDoc.docno.equals("PRI20001204.2000.1488")) {
        endWordIdx = textPosId + 5
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("your") && tDoc.docno.equals("fsh_60298")) {
        endWordIdx = textPosId + 3
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("whose") && tDoc.docno.equals("fsh_61053")) {
        endWordIdx = textPosId + 3
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("goran") && tDoc.docno.equals("MNB20001113.2100.3195")) {
        endWordIdx = textPosId + 3
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("where") && tDoc.docno.equals("fsh_60337")) {
        endWordIdx = textPosId + 3
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("crews") && tDoc.docno.equals("20000815_AFP_ARB.0010.eng")) {
        endWordIdx = textPosId + 4
      }
      if (tDoc.text.substring(textPosId, endWordIdx).equals("golfers") && tDoc.docno.equals("VOA20001117.2000.1387")) {
        endWordIdx = textPosId + 6
      }




      if (endWordIdx == -1) {
        endWordIdx = tDoc.text.length()
      }
      val word = tDoc.text.substring(textPosId, endWordIdx).replaceAll("&AMP;", "&")

      //do sentence splitting
      if (textPosId >= nextSentenceStartIdx) {
        examples.add(example)
        example = new Example()

        sentenceIdx += 1
        //+1 is for whitespace
        nextSentenceStartIdx = tDoc.text.indexOf(sentences(sentenceIdx), nextSentenceStartIdx+1) + sentences(sentenceIdx).length()
      }
      addToExample(example, textPosId, word)




      textPosId = if (textPosId == endWordIdx) endWordIdx+1 else endWordIdx
    }
    if (example.size() != 0) {
      examples.add(example)
    }

    //add document id
    examples.foreach(_.put(ExampleLabel.DOC_ID, tDoc.docno))

    //examples.foreach(_.foreach(t => println(t.asInstanceOf[ACE2004Token].startIdx+" - " + t.get(Label.OBS))))
    examples
  }

}