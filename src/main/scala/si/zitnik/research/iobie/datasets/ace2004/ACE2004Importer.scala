package si.zitnik.research.iobie.datasets.ace2004

import annotations.{AnnotationsDocument, XMLImporter}
import doc.{SGMLImporter, TextDocument}

import collection.JavaConversions._
import java.util.HashMap

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.datasets.ace2004.util.{DatasetManager, OntologyManagerACE2004}
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/6/11
 * Time: 11:43 AM
 * To change this template use File | Settings | File Templates.
 */

class ACE2004Importer(
        val datasetPath: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.ACE2004_PATH),//"/Volumes/ace_tides_multling_train/domain/",
        val language: ACE2004Language.Value = ACE2004Language.ENGLISH,
        val documentTypes: Array[ACE2004DocumentType.Value] = ACE2004DocumentType.values.toArray,
        val exportOntology: Boolean = false) extends StrictLogging {

  def importForIE(): Examples = {
    val allFolders = documentTypes.map(dType => datasetPath+"/"+language+"/"+dType).toList
    val sgmls = SGMLImporter.importAll(allFolders).sortBy(_.docno)
    val xmls = XMLImporter.importAll(allFolders).sortBy(_.documentId)
    val ontologyManager = new OntologyManagerACE2004()

    val allMap = new HashMap[String, (TextDocument, AnnotationsDocument)]()
    if ((sgmls zip xmls).forall(p => p._1.docno == p._2.documentId))
      (sgmls zip xmls).foreach(p => allMap.put(p._1.docno, p))
    logger.info("There were %d SGML and Annotations ACE2004 documents merged.".format(allMap.keySet().size()))

    val examples = new Examples()
    for ((textDoc, annoDoc) <- allMap.values()) {
      val dm = new DatasetManager(textDoc, annoDoc)
      val curExamples = dm.toExamples()
      dm.addNAMTagEntities(curExamples)
      dm.addCOREFConstituents(curExamples)
      //dm.addRelTagRelations(curExamples)
      dm.addRelations(curExamples)
      examples.add(curExamples)

      if (exportOntology)
        ontologyManager.addToOntology(annoDoc)
    }

    if (exportOntology)
      ontologyManager.writeToFile(IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie_ACE2004.ttl")

    examples
  }

}

object ACE2004Language extends Enumeration {
  val ENGLISH = Value("English")
}

object ACE2004DocumentType extends Enumeration {
  val ARABIC_TREEBANK = Value("arabic_treebank")
  val BROADCAST_NEWS = Value("bnews")
  val CHINESE_TREEBANK = Value("chinese_treebank")
  val FISHER_TRANSCRIPTS = Value("fisher_transcripts")
  val NEWSWIRE = Value("nwire")
}