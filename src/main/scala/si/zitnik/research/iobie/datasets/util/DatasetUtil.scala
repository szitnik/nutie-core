package si.zitnik.research.iobie.datasets.util

import si.zitnik.research.iobie.domain.{Example, Examples}
import java.util.{ArrayList, Collections, HashSet}

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label

import scala.collection.JavaConversions._
import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/14/12
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */

object DatasetUtil extends StrictLogging {
  /**
   * This function transforms CoNLL-like coref-tagged dataset.
   * COREF strings are transformed into HashSets for each COREF value.
   *
   * Normally toCOREF_Constituents is the final result we want.
   * @param examples
   */
  def toCOREF_Tags(examples: Examples, betweenOtherSeparator: String = "-"): Unit = {
    val curCorefs = new HashSet[Int]()

    for (example <- examples) {
      if (curCorefs.size() > 0) {
        logger.error("Coref tag error! Inconsisted algorithm or tagged domain!\nNext example: %s".format(example.getLabeling(Label.OBS).mkString(" ")))
        System.exit(-1)
      }
      for (token <- example) {
        val label = token.get(Label.COREF)
        if (label.equals(betweenOtherSeparator)) {
          //curCorefs.toArray(Array[Int]())
          token.put(Label.COREF, curCorefs.toSet)
        } else {
          val splits = label.split("\\|")
          val toRemove = new HashSet[Int]()
          for (split: String <- splits) {
            var s = split.replaceAll("\\(", "")
            if (s.endsWith(")")) {
              s = s.substring(0, s.length() - 1)
              toRemove.add(s.toInt)
            }
            curCorefs.add(s.toInt)
          }
          token.put(Label.COREF, curCorefs.toSet)
          curCorefs.removeAll(toRemove)
        }
      }
    }
  }


  /**
   * This function transforms SIMILAR TO CoNLL-like coref-tagged dataset (no start/end brackets).
   * COREF strings are transformed into HashSets for each COREF value.
   *
   * Normally toCOREF_Constituents is the final result we want.
   * @param examples
   */
  def toCOREF_TagsBasic(examples: Examples, betweenOtherSeparator: String = "-"): Unit = {
    for (example <- examples) {
      for (token <- example) {
        val label = token.get(Label.COREF)
        if (!label.equals(betweenOtherSeparator)) {
          token.put(Label.COREF, label.split("\\|").map(_.toInt).toSet)
        } else {
          token.put(Label.COREF, Set[Int]())
        }
      }
    }
  }

  /**
   * The dataset should already have HashSets as COREF values (as resulted by toCOREF_Tags)
   *
   * COREF tags are removed and mention constituents are generated in examples.
   * @param examples
   */
  def toCOREF_Constituents(examples: Examples): Unit = {
    for (example <- examples) {
      val mentions = new ArrayBuffer[Constituent]()

      val allMentionIds = example.getLabeling(Label.COREF).flatten.toSet
      for (mentionId <- allMentionIds) {
        for ((startIdx, endIdx) <- getMentionIntervals(mentionId, example)){
          mentions += new Constituent(example, startIdx, endIdx, mentionId)
        }
      }
      //order mentions
      Collections.sort(mentions)

      //delete token mentions
      example.setMentions(mentions)
      example.removeLabeling(Label.COREF)
    }
  }

  /**
   * Only as helper method for toCOREF_Constituents
   * @param mentionId
   * @param example
   * @return
   */
  private def getMentionIntervals(mentionId: Int, example: Example): ArrayList[(Int, Int)] = {
    val retVal = new ArrayList[(Int, Int)]()

    var insideLabel = false
    var startIdx = 0
    for ((token,idx) <- example.zipWithIndex) {
      //inside label & do not match
      if (insideLabel && !token.get(Label.COREF).contains(mentionId)) {
        insideLabel = false
        retVal.add((startIdx, idx))
        //outside label & match
      } else if (!insideLabel && token.get(Label.COREF).contains(mentionId)) {
        insideLabel = true
        startIdx = idx
      }
    }

    if (insideLabel) {
      retVal.add((startIdx, example.size()))
    }

    retVal
  }

  /**
   * Transforms selected labels to BIO notation. If there exist multiple labels for a token
   * use toBIO_TagsArray (used by CoNLL 2012)
   * @param examples
   * @param labelType
   */
  def toBIO_Tags_CoNLL(examples: Examples, labelType: Label.Value, betweenLineCharachter: String = "*"): Unit = {
    var curNE = "O"
    for (example <- examples) {
      for (token <- example) {

        val label: String = token.get(labelType)
        if (label.startsWith("(")) {
          if (label.endsWith(")")) {
            token.put(labelType, "B-" + label.substring(1, label.size - 1))
          } else {
            token.put(labelType, "B-" + label.substring(1, label.size - betweenLineCharachter.length))
            curNE = "I-" + label.substring(1, label.size - betweenLineCharachter.length)
          }
        } else if (label.endsWith(")")) {
          token.put(labelType, curNE)
          curNE = "O"
        } else {
          token.put(labelType, curNE)
        }

      }
    }
  }

  /**
   * Transforms consecutive labelType values to BIO notation.
   * @param examples
   * @param labelType
   */
  def toBIO_Tags_Generic(examples: Examples, labelType: Label.Value): Unit = {
    var prevLabel: String = null
    for (example <- examples) {
      for {
        token <- example
        label: String = token.get(labelType)
        if (!label.equals("O"))
      } {
        if (prevLabel != null && prevLabel.equals(label)) {
          token.put(labelType, "I-"+label)
        } else {
          token.put(labelType, "B-"+label)
        }
        prevLabel = label
      }
    }
  }

  /**
   * For multiple tags (used by CoNLL 2012)
   * @param examples
   * @param labelType
   */
  def toBIO_TagsArray_CoNLL(examples: Examples, labelType: Label.Value): Unit = {
    var curNE = "O"
    for (example <- examples) {
      for (i <- 0 until example.get(0).get(labelType).asInstanceOf[Array[String]].size) {
        //get first token size (all sizes in example must be the same)
        for (token <- example) {
          var label: String = token.get(labelType).asInstanceOf[Array[String]](i)
          if (label.startsWith("(")) {
            if (label.substring(1).contains("(")) {
              label = label.substring(0, label.substring(1).indexOf("(") + 1)
            }

            if (label.endsWith(")")) {
              token.get(labelType)(i) = "B-" + label.substring(1, label.size - 2)
            } else {
              token.get(labelType)(i) = "B-" + label.substring(1, label.size - 1)
              curNE = "I-" + label.substring(1, label.size - 1)
            }
          } else if (label.endsWith(")")) {
            token.get(labelType)(i) = curNE
            curNE = "O"
          } else {
            token.get(labelType)(i) = curNE
          }
        }
      }
    }
  }
}
