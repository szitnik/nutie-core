package si.zitnik.research.iobie.datasets.conll2000.test

import si.zitnik.research.iobie.datasets.conll2000.CoNLL2000Importer

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/19/11
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2000ImporterApp {

  def main(args: Array[String]): Unit = {


    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2000/"

    val examples1 = new CoNLL2000Importer(path + "si.zitnik.research.iobie.test.txt") importForIE
    val examples2 = new CoNLL2000Importer(path + "train.txt") importForIE

    println("Memory situation: Total: %dMB, Free: %dMB, Max: %dMB".format(
      Runtime.getRuntime.totalMemory() / 1024 / 1024,
      Runtime.getRuntime.freeMemory() / 1024 / 1024,
      Runtime.getRuntime.maxMemory() / 1024 / 1024))
  }
}