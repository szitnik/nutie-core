package si.zitnik.research.iobie.datasets.conll2012


import scala.collection.JavaConversions._
import java.io.{File, FileFilter, FilenameFilter}

import io.Source
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.parse.ParseTree
import java.util.ArrayList


import si.zitnik.research.iobie.datasets.util.DatasetUtil

import com.typesafe.scalalogging.StrictLogging

/**
 * This class imports CoNLL 2012 domain generated using task accompanying script files.
 *
 * File structure type:
 *
 *

Column 	Type 	Description
    1 	Document ID 	This is a variation on the document filename
    2 	Part number 	Some files are divided into multiple parts numbered as 000, 001, 002, ... etc.
    3 	Word number
    4 	Word itself 	This is the token as segmented/tokenized in the Treebank. Initially the *_skel file contain the placeholder [WORD] which gets replaced by the actual token from the Treebank which is part of the OntoNotes release.
    5 	Part-of-Speech
    6 	Parse bit 	This is the bracketed structure broken before the first open parenthesis in the parse, and the word/part-of-speech leaf replaced with a *. The full parse can be created by substituting the asterix with the "([pos] [word])" string (or leaf) and concatenating the items in the rows of that column.
    7 	Predicate lemma 	The predicate lemma is mentioned for the rows for which we have semantic role information. All other rows are marked with a "-"
    8 	Predicate Frameset ID 	This is the PropBank frameset ID of the predicate in Column 7.
    9 	Word sense 	This is the word sense of the word in Column 3.
    10 	Speaker/Author 	This is the speaker or author name where available. Mostly in Broadcast Conversation and Web Log domain.
    11 	Named Entities 	These columns identifies the spans representing various named entities.
    12:N 	Predicate Arguments 	There is one column each of predicate argument structure information for the predicate mentioned in Column 7.
    N 	Coreference 	Coreference chain information encoded in a parenthesis structure.

 *
 * - path: path to ...conll-2012/v1/domain  (example: /Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2012/dataset/v1/conll-2012/v1/domain)
 */
class CoNLL2012Importer(
                         val rootPath: String,
                         val datasetType: CoNLL2012ImporterDatasetTypeEnum.Value = CoNLL2012ImporterDatasetTypeEnum.TRAIN,
                         val language: CoNLL2012ImporterLanguageEnum.Value = CoNLL2012ImporterLanguageEnum.ENGLISH,
                         val sources: Array[CoNLL2012ImporterSourceTypeEnum.Value] = CoNLL2012ImporterSourceTypeEnum.values.toArray,
                         val annoType: CoNLL2012ImporterAnnoTypeEnum.Value = CoNLL2012ImporterAnnoTypeEnum.GOLD) extends StrictLogging {
  val importFileType = "conll"

  val datasetVersion = "v4" //the last released dataset version
  //              $type/domain/$lang/annotations/$source /dsprovider/XX /file
  val fullPath = "%s/data/%s/annotations/%s"

  def importForIE(): Examples = {
    val retVal = new Examples()

    for (source <- sources) {
      val dirPath: String = "%s/%s".format(rootPath, fullPath.format(datasetType.toString, language.toString, source.toString))
      for (dsProvider: File <- new File(dirPath).listFiles(new FileFilter {
        def accept(f: File) = f.isDirectory
      })) {
        for (xx: File <- dsProvider.listFiles(new FileFilter {
          def accept(f: File) = f.isDirectory
        })) {
          for (conllFile: File <- xx.listFiles(new FilenameFilter {
            def accept(f: File, fn: String) = { fn.endsWith(datasetVersion+annoType+importFileType) && fn.contains(annoType.toString) }
          })) {
            val curExamples: Examples = parseConll(conllFile)
            retVal.add(curExamples)
          }
        }
      }
    }

    DatasetUtil.toBIO_Tags_CoNLL(retVal, Label.NE)
    DatasetUtil.toBIO_TagsArray_CoNLL(retVal, Label.REL)
    DatasetUtil.toCOREF_Tags(retVal)
    DatasetUtil.toCOREF_Constituents(retVal)
    createParseTrees(retVal)

    retVal
  }



  def createParseTrees(examples: Examples): Unit = {
    for (example <- examples) {
      val fullParseString = example.map(token => token.get(Label.PARSE_NODE)).mkString("")
      ParseTree.setParseTree(example, fullParseString)
    }
  }

  def parseConll(file: File): Examples = {
    val examples = new Examples()

    var currentMeta: String = null
    var curExample = new Example()
    for (line <- Source.fromFile(file).getLines()) {
      if (!line.startsWith("#")) {
        if (line.isEmpty) {

          //add new example
          if (curExample.size() > 0) {
            curExample.put(ExampleLabel.METADATA, currentMeta)
            examples.add(curExample)
            curExample = new Example()
          }

        } else {

          //add token to example
          val splitLine = line.split(" +")


          val token = new Token(Label.OBS, splitLine(3))
          curExample.add(token)
          //          1 	Document ID 	This is a variation on the document filename
          curExample.put(ExampleLabel.DOC_ID, splitLine(0))
          //          2 	Part number 	Some files are divided into multiple parts numbered as 000, 001, 002, ... etc.
          curExample.put(ExampleLabel.PART_NO, splitLine(1))
          //          3 	Word number
          //          4 	Word itself 	This is the token as segmented/tokenized in the Treebank. Initially the *_skel file contain the placeholder [WORD] which gets replaced by the actual token from the Treebank which is part of the OntoNotes release.
          // = already in constructor
          //          5 	Part-of-Speech
          token.put(Label.POS, splitLine(4))
          //          6 	Parse bit 	This is the bracketed structure broken before the first open parenthesis in the parse, and the word/part-of-speech leaf replaced with a *. The full parse can be created by substituting the asterix with the "([pos] [word])" string (or leaf) and concatenating the items in the rows of that column.
          token.put(Label.PARSE_NODE, splitLine(5).replace("*", "(%s %s)".format(token.get(Label.POS), token.get(Label.OBS))))
          //          7 	Predicate lemma 	The predicate lemma is mentioned for the rows for which we have semantic role information. All other rows are marked with a "-"
          token.put(Label.LEMMA, splitLine(6))
          //          8 	Predicate Frameset ID 	This is the PropBank frameset ID of the predicate in Column 7.
          token.put(Label.PROPBANK_FID, splitLine(7))
          //          9 	Word sense 	This is the word sense of the word in Column 3.
          token.put(Label.W_SENSE, splitLine(8))
          //          10 	Speaker/Author 	This is the speaker or author name where available. Mostly in Broadcast Conversation and Web Log domain.
          token.put(Label.SPEAKER, splitLine(9))
          //          11 	Named Entities 	These columns identifies the spans representing various named entities.
          token.put(Label.NE, splitLine(10))
          //          12:N 	Predicate Arguments 	There is one column each of predicate argument structure information for the predicate mentioned in Column 7.
          var predArgs = new ArrayList[String]()
          for (i <- 11 to splitLine.length - 2) {
            predArgs.add(splitLine(i))
          }
          token.put(Label.REL, predArgs.toArray(Array[String]()))
          //          N 	Coreference
          token.put(Label.COREF, splitLine(splitLine.length - 1))
        }
      } else if (line.startsWith("#begin document")) {
        currentMeta = line
      }
    }

    examples
  }

}

object CoNLL2012ImporterDatasetTypeEnum extends Enumeration {
  //dataset type
  val DEVELOPMENT = Value("development")
  val TRAIN = Value("train")
  val TEST = Value("test")
}

object CoNLL2012ImporterLanguageEnum extends Enumeration {
  //language
  val ARABIC = Value("arabic")
  val CHINESE = Value("chinese")
  val ENGLISH = Value("english")
}

object CoNLL2012ImporterSourceTypeEnum extends Enumeration {
  //source type
  val BROADCAST_NEWS = Value("bn")
  val BROADCAST_CONVERSATION = Value("bc")
  val MAGAZINE = Value("mz")
  val NEWSWIRE = Value("nw")
  val PIVOT_CORPUS = Value("pt")
  val TELEPHONE_CONVERSATION = Value("tc")
  val WEB_TEXT = Value("wb")
}

//TODO: maybe would be useful to have also source level, e.g. abc, cnn, ...

object CoNLL2012ImporterAnnoTypeEnum extends Enumeration {
  //type of tagged domain
  val AUTO = Value("_auto_")
  val GOLD = Value("_gold_")
}

