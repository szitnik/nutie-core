package si.zitnik.research.iobie.datasets.ace2004.annotations

import entity.{Entity, EntityAttribute, EntityMention}
import event.{Event, EventMention, EventMentionParticipant}
import relation._

import scala.xml.{Node, NodeSeq, XML}
import io.Source
import scala.collection.JavaConversions._
import java.util.ArrayList
import java.io.{File, FilenameFilter}

import com.typesafe.scalalogging.StrictLogging

class EntityXMLImporter(val document: NodeSeq) {

  def parseExtent(node: NodeSeq): RawTag = {
    val child = node.head.descendant apply 1
    if (child.label != "bblist") {
      val start: Int = (child \ "@START" text).toInt
      val end: Int = (child \ "@END" text).toInt
      val text = child text

      return new RawTag(start, end, text)
    }
    null
  }

  def parseHead(node: NodeSeq): RawTag = {
    val child = node.head.descendant apply 1
    if (child.label != "bblist") {
      val start: Int = (child \ "@START" text).toInt
      val end: Int = (child \ "@END" text).toInt
      val text = child text

      return new RawTag(start, end, text)
    }
    null
  }

  def parseName(node: NodeSeq): RawTag = {
    val child = node.head.descendant apply 1
    if (child.label != "bblist") {
      val start: Int = (child \ "@START" text).toInt
      val end: Int = (child \ "@END" text).toInt
      val text = child text

      return new RawTag(start, end, text)
    }
    null
  }

  def parseEntityMentions(entity: NodeSeq): ArrayList[EntityMention] = {
    val entMentions = new ArrayList[EntityMention]()
    for ( entityMention <- entity \\ "entity_mention") {
      val extent = parseExtent(entityMention \ "extent")
      val head = parseHead(entityMention \ "head") //TODO: are there more heads

      val id = entityMention \ "@ID" text
      val typem = entityMention \ "@TYPE" text
      val ldctye = entityMention \ "@LDCTYPE" text
      val role = entityMention \ "@ROLE" text
      val reference = entityMention \ "@REFERENCE" text
      val metonymyMention = entityMention \ "@METONYMY_MENTION" text
      val ldcatr = entityMention \ "@LDCATR" text

      entMentions.add(new EntityMention(id, typem, ldctye, role, reference, metonymyMention, ldcatr, extent, head))
    }
    entMentions
  }

  def parseEntityAttributes(entity: NodeSeq): ArrayList[EntityAttribute] = {
    val entAttrs = new ArrayList[EntityAttribute]()
    for ( entityAttributeTag <- entity \ "entity_attributes") {
      val entityAttribute = new EntityAttribute()
      for ( nameTag <- entityAttributeTag \\ "name") {
        val name = parseName(nameTag)
        entityAttribute.add(name)
      }
      entAttrs.add(entityAttribute)
    }
    entAttrs
  }

  def parseEntities(): ArrayList[Entity] = {
    val entities = new ArrayList[Entity]()
    for ( entity: NodeSeq <- document \\ "entity") {
      //entity mention
      val entityMentions = parseEntityMentions(entity)
      //entity attributes
      val entityAttributes = parseEntityAttributes(entity)
      val id = entity \ "@ID" text
      val typee = entity \ "@TYPE" text
      val subtype = entity \ "@SUBTYPE" text
      val classe = entity \ "@CLASS" text

      entities.add(new Entity(entityMentions, entityAttributes, id, typee, subtype, classe))
    }

    entities
  }
}

class RelationXMLImporter(val document: NodeSeq) {

  def parseRelArgs(relation: NodeSeq): ArrayList[RelEntityArg] = {
    val relArgs = new ArrayList[RelEntityArg]()
    for ( relArg: NodeSeq <- relation \\ "rel_entity_arg") {
      val entityId = relArg \ "@ENTITYID" text
      val argnum = relArg \ "@ARGNUM" text;
      relArgs.add(new RelEntityArg(entityId, argnum))
    }
    relArgs
  }

  def parseExtent(node: NodeSeq): RawTag = {
    val child = node.head.descendant apply 1
    if (child.label != "bblist") {
      val start: Int = (child \ "@START" text).toInt
      val end: Int = (child \ "@END" text).toInt
      val text = child text

      return new RawTag(start, end, text)
    }
    null
  }

  def parseRelMentionArgs(node: Node): ArrayList[RelMentionArg] = {
    val relMentionArgs = new ArrayList[RelMentionArg]()
    for ( relMentionArg <- node \\ "rel_mention_arg") {
      val entityMentionId = relMentionArg \ "@ENTITYMENTIONID" text
      val argnum = relMentionArg \ "@ARGNUM" text
      val extent = parseExtent(relMentionArg \ "extent")
      relMentionArgs.add(new RelMentionArg(entityMentionId, argnum, extent))
    }
    relMentionArgs
  }

  def parseSources(node: Node): ArrayList[RawTag] = {
    val retVal = new ArrayList[RawTag]()
    for ( source <- node \\ "extent") {
      retVal.add(parseExtent(source))
    }
    retVal
  }

  def parseRelMentionTimes(node: Node): ArrayList[RelMentionTime] = {
    val relMentionTimes = new ArrayList[RelMentionTime]()
    for ( relMentionTime <- node \\ "relation_mention_time") {
      val typer = relMentionTime \ "@TYPE" text
      val valr = relMentionTime \ "@VAL" text
      val mod = relMentionTime \ "@MOD" text
      val dir = relMentionTime \ "@DIR" text
      val sources = parseSources(relMentionTime)
      relMentionTimes.add(new RelMentionTime(typer, valr, mod, dir, sources))
    }
    relMentionTimes
  }

  def parseRelMentions(relation: NodeSeq): ArrayList[RelMention] = {
    val relMentions = new ArrayList[RelMention]()
    for ( relMention <- relation \\ "relation_mention") {
      val id = relMention \ "@ID" text
      val ldclexicalcondition = relMention \ "@LDCLEXICALCONDITION" text
      val ldcExtent = parseExtent(relMention \ "ldc_extent")
      val relMentionArgs = parseRelMentionArgs(relMention)
      val relMentionTimes = parseRelMentionTimes(relMention)
      relMentions.add(new RelMention(id, ldclexicalcondition, ldcExtent, relMentionArgs, relMentionTimes))
    }
    relMentions
  }

  def parseRelations(): ArrayList[Relation] = {
    val relations = new ArrayList[Relation]()
    for ( relation: NodeSeq <- document \\ "relation") {
      val id = relation \ "@ID" text
      val typer = relation \ "@TYPE" text
      val subtype = relation \ "@SUBTYPE" text
      val relArgs = parseRelArgs(relation)
      val relMentions = parseRelMentions(relation)
      //TODO: "relation_mentions" tag check if exists
      relations.add(new Relation(id, typer, subtype, relArgs, relMentions))
    }
    relations
  }
}

class EventXMLImporter(val document: NodeSeq) {

  def parseExtent(node: NodeSeq): RawTag = {
    val child = node.head.descendant apply 1
    if (child.label != "bblist") {
      val start: Int = (child \ "@START" text).toInt
      val end: Int = (child \ "@END" text).toInt
      val text = child text

      return new RawTag(start, end, text)
    }
    null
  }

  def parseEventParticipants(eventMention: Node): ArrayList[EventMentionParticipant] = {
    val retVal = new ArrayList[EventMentionParticipant]()
    for ( partMention <- eventMention \\ "event_mention_participant") {
      val entityId = partMention \ "@ENTITYID" text
      val entityMentionId = partMention \ "@ENTITYMENTIONID" text
      val role = partMention \ "@ROLE" text
      val source = partMention \ "@SOURCE" text
      val extent = parseExtent(partMention \ "extent")
      val evMenParRelExtent = parseExtent(
        partMention \ "event_mention_participant_relation_extent" \ "extent"
      )
      retVal.add(new EventMentionParticipant(entityId, entityMentionId, role, source, extent, evMenParRelExtent))
    }
    retVal
  }

  def parseEventMentions(node: NodeSeq): ArrayList[EventMention] = {
    val eventMentions = new ArrayList[EventMention]()
    for ( eventMention <- node \\ "event_mention") {
      val id = eventMention \ "@ID" text
      val typem = eventMention \ "@TYPE" text
      val extent = parseExtent(eventMention \ "extent")
      val anchor = parseExtent(eventMention \ "anchor")
      val eventMentPart = parseEventParticipants(eventMention)
      eventMentions.add(new EventMention(id, typem, extent, anchor, eventMentPart))
    }
    eventMentions
  }

  def parseEvents(): ArrayList[Event] = {
    val events = new ArrayList[Event]()
    for ( event: NodeSeq <- document \\ "event") {
      val id = event \ "@ID" text
      val typee = event \ "@TYPE" text
      val modality = event \ "@MODALITY" text
      val eventMentions = parseEventMentions(event)
      events.add(new Event(id, typee, modality, eventMentions))
    }
    events
  }
}

class XMLImporter(val xmlFile: String) extends StrictLogging {
  def parseXml(): AnnotationsDocument = {
    val xml = Source.fromFile(xmlFile).getLines.mkString(" ").replaceFirst("<!DOCTYPE .*dtd\">", "")
    val xmlLoaded = XML.loadString(xml)
    val source_file = xmlLoaded \\ "source_file"

    val uri = source_file \ "@URI" text
    val source = source_file \ "@SOURCE" text
    val typed = source_file \ "@TYPE" text
    val version = source_file \ "@VERSION" text
    val author = source_file \ "@AUTHOR" text
    val encoding = source_file \ "@ENCODING" text

    val document = source_file \ "document"
    val docid = document \ "@DOCID" text
    //TODO: are there more than one documents

    val entities = new EntityXMLImporter(document) parseEntities
    val relations = new RelationXMLImporter(document) parseRelations
    val events = new EventXMLImporter(document) parseEvents

    new AnnotationsDocument(uri, source, typed, version, author, encoding, docid, entities, relations, events)
  }
}

object XMLImporter extends StrictLogging {
  def importAll(folders: List[String], ending: String = ".apf.xml"): ArrayList[AnnotationsDocument] = {
    //PREPARATION
    val allFiles = new ArrayList[String]()
    for ( fldrName <- folders) {
      val srcFiles = new File(fldrName).list(new FilenameFilter {
        def accept(f1: File, f2: String): Boolean = f2.endsWith(ending)
      })
      for ( srcFile <- srcFiles) {
        allFiles.add(fldrName + "/" + srcFile)
      }

    }


    //PROCESSING
    val xmls = new ArrayList[AnnotationsDocument]()
    for ( filename <- allFiles) {
      logger.trace("Starting parsing file: " + filename)
      val annoDoc = new XMLImporter(filename) parseXml;
      xmls.add(annoDoc)
      logger.trace("Parsing XML done!")
    }
    logger.trace("Parsed %d documents.".format(xmls.size))

    xmls
  }
}