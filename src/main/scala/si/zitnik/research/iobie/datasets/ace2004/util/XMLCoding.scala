package si.zitnik.research.iobie.datasets.ace2004.util


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/6/11
 * Time: 6:25 PM
 * To change this template use File | Settings | File Templates.
 */

object XMLCoding {

  /**
  Escape characters for text appearing as XML domain, between tags.

   <P>The following characters are replaced with corresponding character entities :
   <table border='1' cellpadding='3' cellspacing='0'>
   <tr><th> Character </th><th> Encoding </th></tr>
   <tr><td> < </td><td> &lt; </td></tr>
   <tr><td> > </td><td> &gt; </td></tr>
   <tr><td> & </td><td> &amp; </td></tr>
   <tr><td> " </td><td> &quot;</td></tr>
   <tr><td> ' </td><td> &#039;</td></tr>
   </table>

   <P>Note that JSTL's {@code <c:out>} escapes the exact same set of
   characters as this method. <span class='highlight'>That is, {@code <c:out>}
    is good for escaping to produce valid XML, but not for producing safe
    HTML.</span>
   */
  def decode(text: String): String = {
    var result: String = text.replaceAll("&lt;", "<")
    result = result.replaceAll("&gt;", ">")
    result = result.replaceAll("&quot;", "\"")
    result = result.replaceAll("&#039;", "\'")
    result = result.replaceAll("&amp;", "&")

    result = result.replaceAll("&lt;".toUpperCase, "<")
    result = result.replaceAll("&gt;".toUpperCase, ">")
    result = result.replaceAll("&quot;".toUpperCase, "\"")
    result = result.replaceAll("&#039;".toUpperCase, "\'")
    result = result.replaceAll("&amp;".toUpperCase, "&")


    result
  }
}