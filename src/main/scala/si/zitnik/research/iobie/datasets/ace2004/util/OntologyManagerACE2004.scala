package si.zitnik.research.iobie.datasets.ace2004.util

import si.zitnik.research.iobie.ontology.OntologyManager
import si.zitnik.research.iobie.datasets.ace2004.annotations.AnnotationsDocument
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 18/03/14
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */
class OntologyManagerACE2004 {
  val modelFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie.ttl"
  val ontoManager = new OntologyManager(modelFile)

  def addToOntology(annoDoc: AnnotationsDocument): Unit = {
    val mentionIdToValueMapping = new mutable.HashMap[String, String]()

    //add entities
    annoDoc.entities.foreach(entity => {
      entity.entityMentions.foreach(mention => {
        val typem = if (entity.subType == null || entity.subType.equals("")) entity.entityType else entity.subType
        val obs = mention.head.text
        mentionIdToValueMapping.put(mention.id, obs)

        ontoManager.addEntity(typem, obs)
      })
    })

    //add relations
    annoDoc.relations.foreach(relation => {
      relation.relMentions.foreach(relationMention => {

        val (subjectr, objectr) = if (relationMention.relMentionArgs(0).argnum.equals("1")) {
          (relationMention.relMentionArgs(0), relationMention.relMentionArgs(1))
        } else {
          (relationMention.relMentionArgs(1), relationMention.relMentionArgs(0))
        }

        val subj = mentionIdToValueMapping.get(subjectr.entityMentionId).get
        val obj = mentionIdToValueMapping.get(objectr.entityMentionId).get
        val relationName = if (relation.subtype == null || relation.subtype.equals("")) relation.typer else relation.subtype


        ontoManager.addRelation(relationName, subj, obj)
      })
    })
  }

  def writeTTL(): Unit = {
    ontoManager.writeTTL()
  }

  def writeToFile(filename: String): Unit = {
    ontoManager.writeToFile(filename)
  }

}
