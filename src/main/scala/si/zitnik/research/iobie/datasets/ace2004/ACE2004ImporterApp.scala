package si.zitnik.research.iobie.datasets.ace2004

import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/12/12
 * Time: 3:33 PM
 * To change this template use File | Settings | File Templates.
 */
object ACE2004ImporterApp {
  def main(args: Array[String]): Unit = {
    val importer = new ACE2004Importer()
    val examples = importer.importForIE()

    examples.printStatistics(ommitMentions = false)
    examples.printLabelingDistribution(Label.NE)
  }
}
