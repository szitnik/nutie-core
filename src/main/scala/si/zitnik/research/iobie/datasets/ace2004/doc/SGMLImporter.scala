package si.zitnik.research.iobie.datasets.ace2004.doc

import io.Source
import scala.xml.XML
import java.util.ArrayList
import java.io.{File, FilenameFilter}

import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/5/11
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */

class SGMLImporter(val SGMLFilename: String) {

  def replaceBack(str: String): String = {
    str.replaceAll("!!!AND!!!", "&")
  }

  def parseSGML(): TextDocument = {
    var sgml = Source.fromFile(SGMLFilename, "utf-8").getLines.mkString(" ")


    //TODO: nasty hacks:
    sgml = sgml.replaceAll("<TURN>", "")
    sgml = sgml.replaceAll("&", "!!!AND!!!")

    val xml = XML.loadString(sgml)

    var docno = (xml \\ "DOCNO" text).trim
    if (docno.isEmpty) {
      docno = (xml \\ "DOCID" text).trim
    }
    var doctypeText = (xml \\ "DOCTYPE" text).trim
    var doctypeSource = (xml \\ "DOCTYPE" \ "@SOURCE" text).trim
    var datetime = (xml \\ "DATE_TIME" text).trim
    if (datetime.isEmpty) {
      datetime = (xml \\ "DATE" text).trim
    }
    var endtime = (xml \\ "END_TIME" text).trim

    /**
     * Text is presented in a way that can be properly indexed by the positions in apf.xml.
     */
    var text = sgml replaceAll("<.*?>", "")
    var textOnly = (xml \\ "TEXT" text).trim

    var slug = xml \\ "SLUG" text
    var headline = xml \\ "HEADLINE" text

    docno = docno.trim

    docno = replaceBack(docno)
    doctypeText = replaceBack(doctypeText)
    doctypeSource = replaceBack(doctypeSource)
    datetime = replaceBack(datetime)
    endtime = replaceBack(endtime)
    textOnly = replaceBack(textOnly)
    text = replaceBack(text)
    slug = replaceBack(slug)
    headline = replaceBack(headline)

    new TextDocument(docno, doctypeText, doctypeSource, datetime, endtime, text, slug, headline, textOnly)
  }
}

object SGMLImporter extends StrictLogging {
  def importAll(folders: List[String], ending: String = ".sgm"): ArrayList[TextDocument] = {
    //PREPARATION
    val allFiles = new ArrayList[String]()
    for ( fldrName <- folders) {
      val srcFiles = new File(fldrName).list(new FilenameFilter {
        def accept(f1: File, f2: String): Boolean = f2.endsWith(ending)
      })
      for ( srcFile <- srcFiles) {
        allFiles.add(fldrName + "/" + srcFile)
      }

    }
    //PROCESSING
    val sgmls = new ArrayList[TextDocument]();
    for ( filename <- allFiles) {
      logger.trace("Starting parsing file: " + filename)
      val textdoc = new SGMLImporter(filename) parseSGML()

      sgmls.add(textdoc)
      logger.trace("Parsing SGML done!")
    }
    logger.trace("There are %d found text docs.".format(sgmls.size))

    sgmls
  }
}