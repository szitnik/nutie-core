package si.zitnik.research.iobie.datasets.ijcnlp2011

import com.typesafe.scalalogging.StrictLogging

import io.Source
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/22/12
 * Time: 12:10 PM
 * To change this template use File | Settings | File Templates.
 */

class IJCNLP2011Importer(val filePath: String) extends StrictLogging {

  def importForIE() = {
    val examples = new Examples()

    var tempExample = new Example()

    for (line <- Source.fromFile(filePath, "iso-8859-1").getLines()) {
      if (line.isEmpty) {
        //put example & create new tempExample
        if (tempExample.size != 0) {
          examples.add(tempExample)
        }
        tempExample = new Example()
      } else {
        //process line
        val lineTab = line.split("\t")
        if (lineTab.length == 5) {
          val token = new Token(Label.OBS, lineTab(1))
          token.put(Label.POS, lineTab(2))
          token.put(Label.PHRB, lineTab(3))
          token.put(Label.REL, lineTab(4))
          tempExample.add(token)
        }
      }
    }
    if (tempExample.size != 0) {
      examples.add(tempExample)
    }


    examples
  }

}

object IJCNLP2011Importer {
  def main(args: Array[String]): Unit = {
    val rootPath = "/Users/slavkoz/Documents/DR_Research/Datasets/DataSet-IJCNLP2011/domain sets/"
    val dsNyt = "New York Times.txt"
    val dsWiki = "Wikipedia.txt"

    val examplesNyt = new IJCNLP2011Importer(rootPath + dsNyt).importForIE
    examplesNyt.printStatistics()

    val examplesWiki = new IJCNLP2011Importer(rootPath + dsWiki).importForIE
    examplesWiki.printStatistics()
  }
}
