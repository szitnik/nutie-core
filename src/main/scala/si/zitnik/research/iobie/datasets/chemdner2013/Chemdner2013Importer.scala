package si.zitnik.research.iobie.datasets.chemdner2013

import annotations.{Abstract, Annotation}
import com.typesafe.scalalogging.StrictLogging

import collection.mutable
import io.Source
import si.zitnik.research.iobie.domain.{Examples, Token}

import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.util.AdderMap

import scala.collection.JavaConversions._
import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.thirdparty.stanford.api.StanfordCoreNLPTokenizerAndSsplit

import util.matching.Regex
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/20/13
 * Time: 12:09 PM
 * To change this template use File | Settings | File Templates.
 */
class Chemdner2013Importer(datasetType: Chemdner2013DatasetType.Value) extends StrictLogging {
  private val stanfordTokenAndSsplit = new StanfordCoreNLPTokenizerAndSsplit()
  private var abstractsFile: String = null
  private var annotationsFile: String = null

  init()
  private def init(): Unit = {
    datasetType match {
      case Chemdner2013DatasetType.training => {
        abstractsFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" + Chemdner2013Paths.training_abs
        annotationsFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" +Chemdner2013Paths.training_ann
      }
      case Chemdner2013DatasetType.development => {
        abstractsFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" + Chemdner2013Paths.development_abs
        annotationsFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" + Chemdner2013Paths.development_ann
      }

      case Chemdner2013DatasetType.test => {
        abstractsFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" + Chemdner2013Paths.test_abs
        annotationsFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" + Chemdner2013Paths.test_ann
      }
    }
  }

  private def tokenProcessorHelper(startIdx: Int, text: String) = {
    val retVal = new ArrayBuffer[Token]()

    def leftRightSplitHelper(regex: Regex): Unit = {
      regex.findFirstIn(text) match {
        case Some(regex(left, right)) => {
          val leftToken = new Token()
          leftToken.put(Label.START_IDX, startIdx)
          leftToken.put(Label.OBS, left)
          retVal.add(leftToken)

          val rightToken = new Token()
          rightToken.put(Label.START_IDX, startIdx + left.size)
          rightToken.put(Label.OBS, right)
          retVal.add(rightToken)
        }
        case None => {
          throw new Exception("Error parsing text '%s'!".format(text))
          System.exit(-1)
        }
      }
    }

    val greekAlphabet = "[Α|α|Β|β|Γ|γ|Δ|δ|Ε|ε|Ζ|ζ|Η|η|Θ|θ|Ι|ι|Κ|κ|Λ|λ|Μ|μ|Ν|ν|Ξ|ξ|Ο|ο|Π|π|Ρ|ρ|Σ|σ|ς|Τ|τ|Υ|υ|Φ|φ|Χ|χ|Ψ|ψ|Ω|ω]"

    if (text.matches("[a-zA-Z]{3,}[0-9]{1,}")) {
      val parts = new scala.util.matching.Regex("""([a-zA-Z]{3,})([0-9]{1,})""", "left", "right")
      leftRightSplitHelper(parts)
    } else if (text.matches("[0-9]{1,}[a-zA-Z]{2,}")) {
      val parts = new scala.util.matching.Regex("""([0-9]{1,})([a-zA-Z]{2,})""", "left", "right")
      leftRightSplitHelper(parts)
    } else if (text.matches("dietary[a-zA-Z]{3,}")) {
      val parts = new scala.util.matching.Regex("""(dietary)([a-zA-Z]{2,})""", "left", "right")
      leftRightSplitHelper(parts)
    } else if (text.matches(greekAlphabet+"[a-zA-Z]{2,}")) {
      val parts = new scala.util.matching.Regex("""([Α|α|Β|β|Γ|γ|Δ|δ|Ε|ε|Ζ|ζ|Η|η|Θ|θ|Ι|ι|Κ|κ|Λ|λ|Μ|μ|Ν|ν|Ξ|ξ|Ο|ο|Π|π|Ρ|ρ|Σ|σ|ς|Τ|τ|Υ|υ|Φ|φ|Χ|χ|Ψ|ψ|Ω|ω])([a-zA-Z]{2,})""", "left", "right")
      leftRightSplitHelper(parts)
    }
    //DIRECT RULES
    else if (text.matches("[Cu|Ag|Zn]{1,2}BL")) {
      val parts = new scala.util.matching.Regex("""([Cu|Ag|Zn]{1,2})(BL)""", "left", "right")
      leftRightSplitHelper(parts)
    }
    else if (text.matches("pSer")) {
      val parts = new scala.util.matching.Regex("""(p)(Ser)""", "left", "right")
      leftRightSplitHelper(parts)
    }
    else if (text.matches("pThr")) {
      val parts = new scala.util.matching.Regex("""(p)(Thr)""", "left", "right")
      leftRightSplitHelper(parts)
    }
    else if (text.matches("CONCLUSIONS.*")) {
      val parts = new scala.util.matching.Regex("""(CONCLUSIONS)(.*)""", "left", "right")
      leftRightSplitHelper(parts)
    }
    else if (text.matches("OBJECTIVE.*")) {
      val parts = new scala.util.matching.Regex("""(OBJECTIVE)(.*)""", "left", "right")
      leftRightSplitHelper(parts)
    }




    else {
      var rightStartIdx = startIdx
      var rightText = text

      while (!rightText.isEmpty) {
        if (rightText.contains("-") | rightText.contains("/") | rightText.contains(".") | rightText.contains("+") | rightText.contains("@") | rightText.contains(",") | rightText.contains(":") | rightText.contains("˙") | rightText.contains(" ")) {
          val idx = Array(rightText.indexOf("-"), rightText.indexOf("/"), rightText.indexOf("."), rightText.indexOf("+"), rightText.indexOf("@"), rightText.indexOf(","), rightText.indexOf(":"), rightText.indexOf("˙"), rightText.indexOf(" ")).filter(_ >= 0).min

          //left
          val leftText = rightText.substring(0, idx)
          if (!leftText.isEmpty) {
            val leftToken = new Token()
            leftToken.put(Label.START_IDX, rightStartIdx)
            leftToken.put(Label.OBS, leftText)
            retVal.add(leftToken)
          }

          //symbol
          val symbolToken = new Token()
          symbolToken.put(Label.START_IDX, rightStartIdx+idx)
          symbolToken.put(Label.OBS, rightText.substring(idx, idx+1))
          retVal.add(symbolToken)

          //right part
          rightStartIdx = rightStartIdx + idx + 1
          rightText = rightText.substring(idx + 1)
        } else {
          val token = new Token()
          token.put(Label.START_IDX, rightStartIdx)
          token.put(Label.OBS, rightText)
          retVal.add(token)

          rightText = ""
        }
      }

    }



    retVal
  }

  val customStanfordTokenProcessor = (startIdx: Int, text: String) => {
    var prevRetVal = new ArrayBuffer[Token]()
    var curRetVal = tokenProcessorHelper(startIdx, text)

    while (prevRetVal.size != curRetVal.size) {
      val tempRetVal = new ArrayBuffer[Token]()
      for (token <- curRetVal) {
        tempRetVal.addAll(tokenProcessorHelper(token.get(Label.START_IDX).toString.toInt, token.get(Label.OBS).toString))
      }
      prevRetVal = curRetVal
      curRetVal = tempRetVal
    }


    curRetVal
  }

  def importForIE(sublistFromTo: Option[(Int, Int)] = None): Examples = {
    val retExamples = new Examples()

    //Read data
    val abstracts = importAbstracts(abstractsFile)
    val annotations = importAnnotations(annotationsFile)

    //Merge annotations and abstracts
    if (abstracts.keySet.union(annotations.map.keySet).size != abstracts.keySet.size + annotations.map.keySet.size) {
      logger.warn("There are only %d documents with annotations and text data!".format(math.abs(abstracts.keySet.union(annotations.map.keySet).size - (abstracts.keySet.size + annotations.map.keySet.size))))
    }
    var allMap = new mutable.HashMap[String, (Abstract, ArrayBuffer[Annotation])]()
    abstracts.keySet.foreach(docId => {
      allMap.put(docId, (abstracts(docId), annotations.map.getOrElse(docId, ArrayBuffer[Annotation]())))
    })
    logger.info("There were %d Abstracts and Annotations merged.".format(allMap.keySet.size))

    sublistFromTo match {
      case Some((from, to)) => {
        val toTake  = allMap.keySet.toList.sorted.subList(from, to).toSet
        allMap = allMap.filter(v => toTake.contains(v._1))
        logger.info("There are %d documents after pruning.".format(allMap.keySet.size))

      }
      case None => {}
    }


    //Fill Examples
    for ((abs, ann) <- allMap.values) {
      //create examples
      val documentExamples = createExamples(abs)
      //remove empty tokens
      documentExamples.foreach(e => {
        var toRemove = new ArrayBuffer[Token]()
        e.foreach(t => if (t.get(Label.OBS).asInstanceOf[String].isEmpty) toRemove.add(t))
        e.removeAll(toRemove)
      })
      //set all NEs to O
      documentExamples.foreach(_.foreach(_.put(Label.NE, "O")))
      //add annotations
      addAnnotations(documentExamples, ann)
      //add text annotations to examples
      addTextAnnotations(documentExamples, abs)


      retExamples.add(documentExamples)
    }

    retExamples
  }

  def addTextAnnotations(examples: Examples, abs: Abstract): Unit = {
    for (example <- examples) {
      if (example.get(ExampleLabel.TYPE).equals("T")) {
        example.put(ExampleLabel.TEXT, abs.title)
      } else {
        val startIdx = example(0).get(Label.START_IDX).asInstanceOf[Int]
        val endIdx = example.last.get(Label.START_IDX).asInstanceOf[Int] + example.last.get(Label.OBS).asInstanceOf[String].size
        example.put(ExampleLabel.TEXT, abs.abstractv.substring(startIdx, endIdx))
      }
    }
  }

  def addAnnotations(documentExamples: Examples, annotations: ArrayBuffer[Annotation]) = {
    for (annotation <- annotations) {
      val tempTokens = documentExamples.
        getDocumentExamples(annotation.docId).
        filter(_.get(ExampleLabel.TYPE).equals(annotation.source)).
        flatten.
        toList

      //println("Sentence: \t%s\nOffsets: \t%d - %d".format(tempTokens.map(l => {(l.get(Label.START_IDX),l.get(Label.OBS))}).mkString(" "), annotation.startOffset, annotation.endOffset))

      //println("vv"+annotation.docId+"vv")

      var startIdx = 0
      var endIdx = 0
      while (
        !(tempTokens.get(startIdx).get(Label.START_IDX).toString.toInt <= annotation.startOffset && //!between
          annotation.startOffset <= tempTokens.get(startIdx).get(Label.START_IDX).toString.toInt+tempTokens.get(startIdx).get(Label.OBS).toString.size-1)
      ) {
        //if (annotation.docId.equals("23232461") && startIdx == 234) {
        //  print()
        //}

        //val a = tempTokens.get(startIdx).get(Label.START_IDX).toString.toInt
        //val b = tempTokens.get(startIdx).get(Label.START_IDX).toString.toInt+tempTokens.get(startIdx).get(Label.OBS).toString.size-1
        //println("%d - %d - %d - %d - %s".format(annotation.startOffset, annotation.endOffset, a, b, tempTokens.get(startIdx).get(Label.OBS).toString))
        startIdx += 1
      }
      endIdx = startIdx + 1
      while (endIdx < tempTokens.size && tempTokens.get(endIdx).get(Label.START_IDX).toString.toInt <= annotation.endOffset - 1) { endIdx += 1 }


      if (!annotation.mentionValue.replaceAll(" ", "").equals(tempTokens.subList(startIdx, endIdx).map(_.get(Label.OBS)).mkString(""))) {


        logger.info("Original mention (%d,%d): %s, extracted mention: %s, docId: %s".format(
          annotation.startOffset,
          annotation.endOffset,
          annotation.mentionValue,
          tempTokens.subList(startIdx, endIdx).mkString(""),
          annotation.docId))


      }
      tempTokens.subList(startIdx, endIdx).foreach(_.put(Label.NE, annotation.typem))
    }
  }

  def createExamples(abs: Abstract) = {
    val retVal = new Examples()

    //Process title
    var titleExamples = stanfordTokenAndSsplit.tokenizeAndSsplit(abs.title, Some(abs.docId), customStanfordTokenProcessor)
    titleExamples.foreach(_.put(ExampleLabel.TYPE, "T"))
    //merge into one example
    for (i <- 1 until titleExamples.size()) {
      titleExamples.get(0).addAll(titleExamples.get(i))
    }
    titleExamples = new Examples(Array(titleExamples(0)))
    if (titleExamples.size() > 1) {
      logger.info("There is example: '%s', which is composed of %d sentences.".format(titleExamples, titleExamples.size()))
    }

    //Process abstract
    var examples = stanfordTokenAndSsplit.tokenizeAndSsplit(abs.abstractv, Some(abs.docId), customStanfordTokenProcessor)
    examples.foreach(_.put(ExampleLabel.TYPE, "A"))

    //Add to result
    retVal.add(titleExamples)
    retVal.add(examples)

    retVal
  }

  /**
   *
   *
   * @param fileName
   * @return HashMap that has document id for the key and, value is (Title, Abstract)
   */
  private def importAbstracts(fileName: String) = {
    val retVal = new mutable.HashMap[String, Abstract]()

    var filterPMIDs: Set[String] = null
    if (datasetType.equals(Chemdner2013DatasetType.test)) {
      val file = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CHEMDNER2013_PATH) + "/" + Chemdner2013Paths.test_filter
      filterPMIDs = Source.fromFile(file, "utf-8").getLines().map(_.split("\t")).filter(_(1).equals("Y")).map(_(0)).toSet
    }

    Source.fromFile(fileName, "utf-8").getLines().foreach(line => {
      val splitLine = line.split("\t")
      if (datasetType.equals(Chemdner2013DatasetType.test)) {
        if (filterPMIDs.contains(splitLine(0))) {
          retVal.put(splitLine(0), Abstract(splitLine(0), splitLine(1), splitLine(2)))
        }
      } else {
        retVal.put(splitLine(0), Abstract(splitLine(0), splitLine(1), splitLine(2)))
      }
    })

    retVal
  }

  /**
   *
   * @param fileName
   * @return HashMap that has document id for the key and, value is (Source-T/A, Start offset, End offset, Text value mention, Type=CLASS)
   */
  private def importAnnotations(fileName: String) = {
    val retVal = new AdderMap[String, Annotation]()

    if (fileName != null)
    Source.fromFile(fileName, "utf-8").getLines().foreach(line => {
      val splitLine = line.split("\t")
      retVal.put(splitLine(0), Annotation(splitLine(0), splitLine(1), splitLine(2).toInt, splitLine(3).toInt, splitLine(4), splitLine(5)))
    })

    retVal
  }

}

object Chemdner2013DatasetType extends Enumeration {
  val training = Value("training")
  val development = Value("development")
  val test = Value("test")
}

private object Chemdner2013Paths extends Enumeration {
  val training_abs = Value("CHEMDNER_TRAIN_V01/chemdner_abs_training.txt")
  val training_ann = Value("CHEMDNER_TRAIN_V01/chemdner_ann_training_13-07-31.txt")

  val development_abs = Value("CHEMDNER_DEVELOPMENT_V02/chemdner_abs_development.txt")
  val development_ann = Value("CHEMDNER_DEVELOPMENT_V02/chemdner_ann_development_13-08-18.txt")

  val test_abs = Value("CHEMDNER_TEST_ANNOTATION/chemdner_abs_test.txt")
  val test_ann = Value("CHEMDNER_TEST_ANNOTATION/chemdner_ann_test_13-09-13.txt")
  val test_filter = Value("CHEMDNER_TEST_ANNOTATION/chemdner_abs_test_pmid_label.txt")
}

object Chemdner2013Importer {

  def main(args: Array[String]): Unit = {
    val trainExamples = new Chemdner2013Importer(Chemdner2013DatasetType.training).importForIE()
    trainExamples.printStatistics()

    //val devExamples = new Chemdner2013Importer(Chemdner2013DatasetType.development).importForIE()
    //devExamples.printStatistics()

    //val testExamples = new Chemdner2013Importer(Chemdner2013DatasetType.test).importForIE()
    //testExamples.printStatistics()
  }
}
