package si.zitnik.research.iobie.datasets.bionlp2013

import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 2:03 PM
 * To change this template use File | Settings | File Templates.
 */
object BioNLPImporterApp {
  def main(args: Array[String]): Unit = {
    val examples = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.train).importForIE()

    examples.printStatistics(ommitMentions = false)
  }
}
