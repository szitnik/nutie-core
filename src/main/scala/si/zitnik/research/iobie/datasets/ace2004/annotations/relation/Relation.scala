package si.zitnik.research.iobie.datasets.ace2004.annotations.relation

import java.util.ArrayList

class Relation(
                val id: String,
                val typer: String,
                val subtype: String,
                val relArgs: ArrayList[RelEntityArg],
                var relMentions: ArrayList[RelMention]) {

}