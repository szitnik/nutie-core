package si.zitnik.research.iobie.datasets.ace2004.annotations.event

import java.util.ArrayList
import si.zitnik.research.iobie.datasets.ace2004.annotations.RawTag

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/5/11
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */

class EventMention(
                    val id: String,
                    val typem: String,
                    val extent: RawTag,
                    val anchor: RawTag,
                    val eventMentPart: ArrayList[EventMentionParticipant]) {

}