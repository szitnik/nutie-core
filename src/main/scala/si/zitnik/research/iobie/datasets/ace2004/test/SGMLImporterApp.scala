package si.zitnik.research.iobie.datasets.ace2004.test

import scala.collection.JavaConversions._
import collection.immutable.List
import java.lang.String
import java.util.HashMap
import si.zitnik.research.iobie.datasets.ace2004.doc.{TextDocument, SGMLImporter}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/5/11
 * Time: 4:20 PM
 * To change this template use File | Settings | File Templates.
 */

object SGMLImporterApp {

  def main(args: Array[String]): Unit = {
    val mainFolder = "/Volumes/ace_tides_multling_train/domain/English"
    val allFolders = List(mainFolder + "/bnews", mainFolder + "/nwire", mainFolder + "/fisher_transcripts")
    val sgmls = SGMLImporter.importAll(allFolders)

    val sgmlMap = new HashMap[String, TextDocument]()
    sgmls.foreach(doc => sgmlMap.put(doc.docno, doc))


  }
}