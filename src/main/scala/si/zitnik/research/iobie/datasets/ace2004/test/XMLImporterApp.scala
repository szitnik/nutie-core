package si.zitnik.research.iobie.datasets.ace2004.test

import java.lang.String
import scala.collection.JavaConversions._
import collection.immutable.List
import java.util.HashMap
import si.zitnik.research.iobie.datasets.ace2004.annotations.{AnnotationsDocument, XMLImporter}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/5/11
 * Time: 1:13 PM
 * To change this template use File | Settings | File Templates.
 */

object XMLImporterApp {


  def main(args: Array[String]): Unit = {
    val mainFolder = "/Volumes/ace_tides_multling_train/domain/English"
    val allFolders = List(mainFolder + "/bnews", mainFolder + "/nwire", mainFolder + "/fisher_transcripts")
    val xmls = XMLImporter.importAll(allFolders)

    val xmlsMap = new HashMap[String, AnnotationsDocument]()
    xmls.foreach(doc => xmlsMap.put(doc.documentId, doc))
  }
}