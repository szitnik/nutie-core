package si.zitnik.research.iobie.datasets.chemdner2013.annotations

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/22/13
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
case class Annotation(docId: String,
                      source: String,
                      startOffset: Int,
                      endOffset: Int,
                      mentionValue: String,
                      typem: String) {

}
