package si.zitnik.research.iobie.datasets.ace2004.annotations

import entity.{EntityMention, Entity}
import event.Event
import relation.Relation
import scala.collection.JavaConversions._
import java.util.ArrayList
import si.zitnik.research.iobie.datasets.ace2004.doc.TextDocument

class AnnotationsDocument(
                           val uri: String,
                           val source: String,
                           val typed: String,
                           val version: String,
                           val author: String,
                           val encoding: String,
                           val documentId: String,
                           val entities: ArrayList[Entity],
                           val relations: ArrayList[Relation],
                           val events: ArrayList[Event]) {


  override def toString(): String = {
    Map("URI" -> uri, "SOURCE" -> source, "TYPE" -> typed, "VERSION" -> version,
      "AUTHOR" -> author, "ENCODING" -> encoding, "DOCID" -> documentId,
      "ENTITIES" -> entities, "RELATIONS" -> relations, "EVENTS" -> events).mkString(";")
  }

  def getEntities(textDoc: TextDocument, entityMentionTypes: Array[String] = Array("NAM")): ArrayList[String] = {
    val retVal = new ArrayList[String]()
    println("Annotated mentions:")
    for ( entity: Entity <- entities) {
      for ( entityMention: EntityMention <- entity.entityMentions) {
        if (entityMentionTypes.contains(entityMention.typem)) {
          println(
            "\t- %s".format(
              textDoc.text.substring(entityMention.extent.start, entityMention.extent.end + 1)))
          retVal.add(textDoc.text.substring(entityMention.extent.start, entityMention.extent.end + 1))
        }

      }
    }
    retVal
  }


}