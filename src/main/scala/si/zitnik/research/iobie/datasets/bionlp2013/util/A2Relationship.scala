package si.zitnik.research.iobie.datasets.bionlp2013.util

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/4/13
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
case class A2Relationship(
  val id: String,
  val name: String,
  val subjectType: String,
  val subjectId: String,
  val objectType: String,
  val objectId: String) {}
