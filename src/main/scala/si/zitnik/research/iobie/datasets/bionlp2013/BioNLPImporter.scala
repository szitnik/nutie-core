package si.zitnik.research.iobie.datasets.bionlp2013

import si.zitnik.research.iobie.domain.Examples
import java.io.{File, FilenameFilter}

import com.typesafe.scalalogging.StrictLogging
import util.{A1Importer, A2Importer}
import si.zitnik.research.iobie.preprocessing.ssplit.impl.RuleSentenceSplitter
import si.zitnik.research.iobie.preprocessing.tokenization.impl.RuleTokenizer


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */
class BioNLPImporter(val rootPath: String, dType: BioNLP2013DatasetTypes.Value, separateA2Folder: Option[String] = None) extends StrictLogging {

  def importForIE(): Examples = {
    val datasetPath = rootPath+"/"+dType.toString
    val filenames = new File(datasetPath).list(new FilenameFilter {
      def accept(dir: File, name: String): Boolean = name.endsWith(".a1")
    }).map(_.stripSuffix(".a1"))



    val retVal = new Examples()
    for (filenameID <- filenames) {
      val filename = "%s/%s.".format(datasetPath, filenameID) + "%s"

      //1. Sentence split
      var examples = RuleSentenceSplitter.ssplit(filename.format("txt"), Some(filenameID))
      //2. Tokenize
      examples = RuleTokenizer.tokenize(examples)

      //2a. clean weird sentences
      /*
      val toRemove = new ArrayBuffer[Example]()
      for (example <- examples) {
        if (example.getLabeling(Label.OBS).mkString("").equals(".")) {
          toRemove.append(example)
        }
      }
      toRemove.foreach(examples.remove(_))*/
      //manual rules for splitting
      while (examples.size() > 1) {
        examples.get(0).addAll(examples.get(1))
        examples.remove(1)
      }

      //3. Enrich with entities and coreferences
      A1Importer.enrich(filename.format("a1"), filenameID, examples)

      if (dType.equals(BioNLP2013DatasetTypes.development) || dType.equals(BioNLP2013DatasetTypes.train)) {
        //4. Enrich with relations (+additional events, entities, ...)
        new A2Importer().enrich(filename.format("a2"), filenameID, examples)
      } else if (separateA2Folder.isDefined) {
        //4. Enrich with relations (+additional events, entities, ...)
        new A2Importer().enrich(separateA2Folder.get + "/"+filenameID+".a2", filenameID, examples)
      }
      retVal.addAll(examples)
    }
    retVal
  }

}

object BioNLP2013DatasetTypes extends Enumeration {
  val development = Value("BioNLP-ST-2013_Gene_Regulation_Network_dev")
  val developmentAsTest = Value("BioNLP-ST-2013_Gene_Regulation_Network_dev")
  val train = Value("BioNLP-ST-2013_Gene_Regulation_Network_train")
  val trainAsTest = Value("BioNLP-ST-2013_Gene_Regulation_Network_train")
  val test = Value("BioNLP-ST-2013_Gene_Regulation_Network_test")
}