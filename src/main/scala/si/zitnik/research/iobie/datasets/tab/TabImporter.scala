package si.zitnik.research.iobie.datasets.tab

import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}

import io.Source

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.datasets.util.DatasetUtil


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/26/11
 * Time: 11:02 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * This class imports domain from tab separated file.
 * Each line contains OBSERVABLE\tLABELVALUE
 * Each empty line separates two examples.
 * Comments can be entered by starting line with ###
 */
class TabImporter(val labelTypes: Array[Label.Value], val filePath: String) extends StrictLogging {

  def importForIE(): Examples = {
    val examples = new Examples()

    var currentDocID = 0;
    var example = new Example()
    for (line <- Source.fromFile(filePath).getLines()) {
      if (!line.startsWith("####")) {
        try {
          if (line.isEmpty) {
            example.put(ExampleLabel.DOC_ID, currentDocID+"")
            if (example.size() != 0) {
              examples.add(example)
            }
            example = new Example()
          } else if (!line.startsWith("###")) {
            val splitLine = line.split("\t")
            val map = new Token(Label.OBS, splitLine(0))
            for ((labelType, i) <- labelTypes.zipWithIndex) {
              map.put(labelType, splitLine(i + 1))
            }
            example.add(map)
          } else if (line.startsWith("###")) {
            currentDocID += 1
          }
        } catch {
          case e: Throwable =>
            logger.error("Error in line \"%s\"".format(line))
        }
      }
    }
    if (example.size() != 0) {
      examples.add(example)
    }
    examples
  }

}

object TabImporter {
  def main(args: Array[String]): Unit = {
    var ds = ""

    //val rootPath = "/Users/slavkoz/Documents/EclipseWorkspaces/ResearchWorkspace/StanfordNER/domain/"
    //ds = rootPath+"jane-austen-emma-ch1.tsv"
    //ds = rootPath+"jane-austen-emma-ch2.tsv"

    //ds = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1.tsv"
    //ds = "/Users/slavkoz/IdeaProjects/IOBIE/Datasets/rtvslo_dec2011/articles_2.tsv"
    ds = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/rtvslo_dec2011_v2.tsv"


    var examples = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF, Label.LEMMA, Label.POS), ds)
                          .importForIE()
    examples.printStatistics(ommited = Array())


    //TRANSFORM COREF TAGS
    println("\n\n")
    examples.relabel(Label.COREF, "O", "-")
    DatasetUtil.toCOREF_Tags(examples)
    DatasetUtil.toCOREF_Constituents(examples)
    examples.printStatistics(ommited = Array())
  }
}