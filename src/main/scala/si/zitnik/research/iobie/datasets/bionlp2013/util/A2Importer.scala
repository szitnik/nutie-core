package si.zitnik.research.iobie.datasets.bionlp2013.util

import io.Source
import si.zitnik.research.iobie.domain.Examples


import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.relationship.Relationship
import si.zitnik.research.iobie.domain.constituent.Constituent

import collection.immutable.TreeMap

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 1:56 PM
 * To change this template use File | Settings | File Templates.
 */
class A2Importer extends StrictLogging {
  //define types
  private var mentions = new TreeMap[String, A1Mention]()
  private var relationships = new TreeMap[String, A2Relationship]()
  private var events = new TreeMap[String, A2Event]()
  private var negations = new TreeMap[String, String]()


  def enrich(filename: String, docId: String, examples: Examples) = {
    val docIDExamples = examples.getDocumentExamples(docId)
    if (docIDExamples.size() > 1) {
      logger.error("Each document must consist of a single sentence!")
    }
    val example = docIDExamples.get(0)


    //1. read from file
    try {
      for (line <- Source.fromFile(filename).getLines() if !line.isEmpty) {
        line.charAt(0) match {
          case 'T' => {
            val newline = line.split("\t")
            val loc = newline(1).split("\t| |;")
            mentions += ((newline(0), A1Mention(newline(0), loc.head, loc(1).toInt, loc.last.toInt, newline(2))))
          }
          case 'R' => {
            val newline = line.split("\\s+")
            val subj = newline(3).split(":")
            val obj = newline(2).split(":")
            relationships += ((newline(0), A2Relationship(newline(0), newline(1), subj(0), subj(1), obj(0), obj(1))))
          }
          case 'E' => {
            val newline = line.split("\\s+")
            val subj = newline(2).split(":")
            val obj = newline(1).split(":")
            events += ((newline(0), A2Event(newline(0), subj(0), subj(1), obj(0), obj(1))))
          }
          case 'M' => {
            val newline = line.split("\\s+")
            negations += ((newline(0), newline(2)))
          }
          case _ => throw new Exception()
        }
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        logger.error("BioNLP .a1 file \"%s\" does not follow the rules".format(filename))
        System.exit(-1)
    }


    //2. transform domain into IOBIE representation (a: mentions, b: events, c: relationships, d: negations)
    //2.a create temp mentions
    var eventIsAttribute = 0
    var mentionIsAttribute = 0
    var noOfEvents = 0

    mentions.values.map(Util.createConstituent(example, _)._1).foreach(m => {
      m.put(Label.ATTRIBUTE_TYPE, "R_MENTION")
      example.addMention(m, false)
    })
    example.sortMentions()


    def createConstituent(id: String) = {
      val cons = id.charAt(0) match {
        case 'T' => {
          val cons = example.getFirstMentionConstituent(Label.ID, id).get
          //cons.put(Label.ATTRIBUTE_TYPE, "MENTION") - already set before
          mentionIsAttribute += 1
          cons
        }
        case 'E' => {
          val cons = new Constituent(example, 0, 0)
          cons.put(Label.ATTRIBUTE_TYPE, "EVENT")
          cons.put(Label.VALUE, example.getFirstRelationship(Label.ID, id))
          eventIsAttribute += 1
          cons
        }
        case 'R' => {
          println(docId+ "," + filename)
          val cons = new Constituent(example, 0, 0)
          cons.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
          cons.put(Label.VALUE, example.getFirstRelationship(Label.ID, id))
          cons
        }
      }
      cons
    }

    //2.b fill events
    for (a2event <- events.values) {
      val relationship = new Relationship(
        example,
        "EVENT",
        createConstituent(a2event.subjectId),
        createConstituent(a2event.objectId))

      relationship.put(Label.SUBJECT_TYPE, a2event.subjectType)
      relationship.put(Label.OBJECT_TYPE, a2event.objectType)

      relationship.put(Label.ID, a2event.id)
      relationship.put(Label.ATTRIBUTE_TYPE, "EVENT")
      noOfEvents += 1

      example.addRelationship(relationship)
    }

    //2.c fill relationships
    for (a2relationship <- relationships.values) {
      val relationship = new Relationship(
        example,
        a2relationship.name,
        createConstituent(a2relationship.subjectId),
        createConstituent(a2relationship.objectId))

      relationship.put(Label.SUBJECT_TYPE, a2relationship.subjectType)
      relationship.put(Label.OBJECT_TYPE, a2relationship.objectType)

      relationship.put(Label.ID, a2relationship.id)
      relationship.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")

      example.addRelationship(relationship)
    }

    //2.d fill negations
    for (a2negation <- negations) {
      val relationship = new Relationship(
        example,
        "NEGATION",
        null,
        createConstituent(a2negation._2))

      relationship.put(Label.SUBJECT_TYPE, "")
      relationship.put(Label.OBJECT_TYPE, "NEGATED")

      relationship.put(Label.ID, a2negation._1)
      relationship.put(Label.ATTRIBUTE_TYPE, "NEGATION")

      example.addRelationship(relationship)
    }

    logger.info("DocID: %s".format(docId))
    logger.info("No. of events: %d, No. of when an event is an attribute: %d".format(noOfEvents, eventIsAttribute))
    logger.info("No. of new mentions: %d, No. of when mention is an attribute: %d".format(mentions.keySet.size, mentionIsAttribute))

  }

}
