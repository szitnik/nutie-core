package si.zitnik.research.iobie.datasets.conll2003.test

import si.zitnik.research.iobie.datasets.conll2003.CoNLL2003Importer

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/19/11
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2003ImporterApp {

  def main(args: Array[String]): Unit = {


    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2003/eng.build.si.zitnik.research.iobie.datasets/"

    val examples1 = new CoNLL2003Importer(path + "eng.testa") importForIE
    val examples2 = new CoNLL2003Importer(path + "eng.testb") importForIE
    val examples3 = new CoNLL2003Importer(path + "eng.train") importForIE

    println("Memory situation: Total: %dMB, Free: %dMB, Max: %dMB".format(
      Runtime.getRuntime.totalMemory() / 1024 / 1024,
      Runtime.getRuntime.freeMemory() / 1024 / 1024,
      Runtime.getRuntime.maxMemory() / 1024 / 1024))
  }
}