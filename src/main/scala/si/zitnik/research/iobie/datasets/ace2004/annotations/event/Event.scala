package si.zitnik.research.iobie.datasets.ace2004.annotations.event

import java.util.ArrayList

class Event(
             val id: String,
             val typee: String,
             val modality: String,
             val eventMentions: ArrayList[EventMention]) {

}