package si.zitnik.research.iobie.datasets.ace2004.annotations

import si.zitnik.research.iobie.domain.Token
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/10/12
 * Time: 1:03 PM
 * To change this template use File | Settings | File Templates.
 */

class ACE2004Token(
                    val startIdx: Int,
                    val attrName: Label.Value,
                    val attrVal: String
                    ) extends Token(attrName: Label.Value, attrVal: String) {


}
