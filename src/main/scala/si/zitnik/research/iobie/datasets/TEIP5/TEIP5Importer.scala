package si.zitnik.research.iobie.datasets.TEIP5

import java.io.{File, FilenameFilter}

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.feature.packages.FeatureFunctionPackages
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.core.coreference.learner.CorefPairwiseLearner
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.thirdparty.slopostags.{SloPosTagsData, SloPosTagsDataFactory}

import scala.collection.mutable
import scala.xml.XML
import scala.collection.JavaConversions._


class TEIP5Importer(val fileName: String) extends StrictLogging {

  def importForIE(filterMinNePerDocument:Int = 0, filterMinWordsPerDocument:Int = 0): Examples = {
    val examples = new Examples()

    val docToNeNum = mutable.Map[String, Int]()
    val docToWordNum = mutable.Map[String, Int]()

    for {
      document <- (XML.loadFile(fileName) \\  "p") if document.attributes.asAttrMap.contains("xml:id")
      sentence <- document \\ "s" if sentence.attributes.asAttrMap.contains("xml:id")

    } {
      val example = new Example()
      val docId = document.attributes.asAttrMap("xml:id")
      example.put(ExampleLabel.DOC_ID, docId)
      val sentenceId = sentence.attributes.asAttrMap("xml:id")
      example.put(ExampleLabel.ID, sentenceId)

      // Detect named entities
      var neNum = 0
      var neMap = Map[String, String]()
      for (ne <- (sentence \\ "seg").filter(n => (n \ "@type").text == "name" )) {
        val value = (ne \ "@subtype").text
        // Get all token ids
        val idToNe = (ne \\ "w").map(w => (w.attributes.asAttrMap("xml:id"), value)) ++
          (ne \\ "pc").map(w => (w.attributes.asAttrMap("xml:id"), value))
        neMap ++= idToNe
        neNum += 1
      }
      docToNeNum.put(docId, docToNeNum.getOrElse(docId, 0) + neNum)

      // Create tokens
      for (child <- sentence \\ "_") {
        if (child.isInstanceOf[scala.xml.Elem]) {
          val text = child.text.replaceAll("\t", " ").trim
          if (!text.isEmpty) {
            child.label.toString match {
              case "w" => {
                val token = new Token(Label.OBS, text)
                val wordId = child.attributes.asAttrMap("xml:id")
                token.put(Label.ID, wordId)
                token.put(Label.NE, neMap.getOrElse(wordId, "O"))
                token.put(Label.LEMMA, child \ "@lemma" text)
                token.put(Label.POS, (child \ "@ana" text).split(":")(1))
                example.add(token)
              }
              case "c" => {
                val token = new Token(Label.OBS, text)
                token.put(Label.ID, "NaN")
                token.put(Label.NE, "O")
                token.put(Label.LEMMA, text)
                token.put(Label.POS, "O")
                example.add(token)
              }
              case "pc" => {
                val token = new Token(Label.OBS, text)
                val wordId = child.attributes.asAttrMap("xml:id")
                token.put(Label.ID, wordId)
                token.put(Label.NE, neMap.getOrElse(wordId, "O"))
                token.put(Label.LEMMA, text)
                token.put(Label.POS, (child \ "@ana" text).split(":")(1))
                example.add(token)
              }
              case "seg"|"s" => {
                //Not implemented or processed somewhere else
              }
              case _ => {
                logger.error("Problem discovered for token: %s in sentence: %s".format(child.label.toString, example.get(ExampleLabel.ID)))
              }
            }
          }
        }
      }

      // Add dependencies
      for (link <- sentence \ "linkGrp" \ "link") {
        val depType = (link \ "@ana" text).split(":")(1)
        // Get token ids - [source, target]
        val tokenIds = (link \ "@target" text).replaceAll("#", "").split(" ")
        val sourceToken = example.getFirstConstituent(Label.ID, tokenIds(0))
        val targetToken = example.getFirstConstituent(Label.ID, tokenIds(1))

        example.addTokenRelationship(depType, sourceToken, targetToken)
      }


      docToWordNum.put(docId, docToWordNum.getOrElse(docId, 0) + example.size)
      examples.add(example)
    }

    val toFilter = docToNeNum.filter(_._2 >= filterMinNePerDocument).keySet intersect
      docToWordNum.filter(_._2 >= filterMinWordsPerDocument).keySet

    examples.selectDocumentExamples(toFilter.toSet)

    //SLO POS TAGS variants importer; SSJ500k-specific
    val sloTags = SloPosTagsDataFactory.instance()
    for (example <- examples) {
      for (token <- example) {
        val attributes = sloTags.get(token.get(Label.POS).toString)
        attributes match {
          case Some(x) => token.putAll(x)
          case _ => {
            token.putAll(
              SloPosTagsData.customSloPOSTag(token.get(Label.POS).toString)
            )
          }
        }
      }
    }

    examples
  }

  /**
    * This function can be used only together with dataset ssj500k for slovene coreference resolution. See method
    * @param examples
    * @param corefTaggedDir
    */
  def addTCFCoreferences(examples: Examples, corefTaggedDir: String): Unit = {
    val corefFiles = new File(corefTaggedDir).listFiles(new FilenameFilter {
      override def accept(dir: File, name: String) = name.endsWith(".tcf")
    })

    assert(corefFiles.size == 149) //as expected for slovene coreference tagging
    for (corefFile <- corefFiles) {
      val TCF = XML.loadFile(corefFile)

      val tcfTokenIds = (TCF \\ "token").map(_ \ "@ID" text)
      val examplesTokenIds = examples.getDocumentExamples(corefFile.getName().replace(".tcf", "")).flatMap(_.getLabeling(Label.ID)).map(_.toString)
      val tcfToExampleIds = (tcfTokenIds zip examplesTokenIds).toMap
      assert(tcfTokenIds.size == examplesTokenIds.size)


      for ((entity, clusterId) <- (TCF \\ "entity").zipWithIndex) {
        for (reference <- entity \ "reference") {
          val startTokenId = tcfToExampleIds((reference \  "@tokenIDs").text.split(" ").head)
          val endTokenIdInclusive = tcfToExampleIds((reference \  "@tokenIDs").text.split(" ").last)

          val example = examples.getExampleByTokenId(startTokenId)
          val startToken = example.getFirstConstituent(Label.ID, startTokenId).get
          val endToken = example.getFirstConstituent(Label.ID, endTokenIdInclusive).get

          example.addMention(new Constituent(example, example.indexOf(startToken), example.indexOf(endToken) + 1, clusterId))
        }
      }
    }
  }

}

object TEIP5Importer {
  /**
    * The dataset file should be ssj500k full, slovene, version 1.4
    * @param filename
    */
  def getSloveneCoreferenceResolutionDataset(filename: String, sloveneCoreferencesFolder: String) = {
    val importer = new TEIP5Importer(filename)
    val examples = importer.importForIE(filterMinNePerDocument = 6, filterMinWordsPerDocument = 100)
    importer.addTCFCoreferences(examples, sloveneCoreferencesFolder)
    examples
  }

  def main(args: Array[String]): Unit = {

    val ssj500kFilename = "/Users/slavkoz/OneDrive - Univerza v Ljubljani/Datasets/ssj500k/ssj500k-sl.xml"
    val sloveneCoreferencesFolder = "/Users/slavkoz/OneDrive - Univerza v Ljubljani/Datasets/ssj500k/ssj500-coreference-dataset"
    val examples = getSloveneCoreferenceResolutionDataset(ssj500kFilename, sloveneCoreferencesFolder)

    new CorefPairwiseLearner(examples, FeatureFunctionPackages.standardFFunctions)

    println(examples.getAllDocIds().size)
    examples.getDocumentExamples("ssj4.15").get(0).printLabeling(Label.ID)
    examples.getDocumentExamples("ssj4.15").get(0).printLabeling(Label.OBS)
    examples.getDocumentExamples("ssj4.15").get(0).printLabeling(Label.LEMMA)
    examples.getDocumentExamples("ssj4.15").get(0).printLabeling(Label.POS)
    println(examples.getDocumentExamples("ssj4.15").get(0).getAllTokenRelationships())
    println(examples.getAllMentionClusters())


  }
}
