package si.zitnik.research.iobie.datasets.ace2004.doc

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/5/11
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */

class TextDocument(
                    val docno: String,
                    val doctypeText: String,
                    val doctypeSource: String,
                    val datetime: String,
                    val endtime: String,
                    val text: String,
                    val slug: String,
                    val headline: String,
                    val textOnly: String) {

}