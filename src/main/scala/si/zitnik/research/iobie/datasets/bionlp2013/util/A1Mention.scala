package si.zitnik.research.iobie.datasets.bionlp2013.util

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/4/13
 * Time: 12:38 PM
 * To change this template use File | Settings | File Templates.
 */
case class A1Mention(
                      id: String,
                      typem: String,
                      start_id: Int,
                      end_id: Int,
                      text: String,
                      var coref_id: String = "") {
}