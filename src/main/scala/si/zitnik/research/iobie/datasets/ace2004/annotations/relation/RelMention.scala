package si.zitnik.research.iobie.datasets.ace2004.annotations.relation

import java.util.ArrayList
import si.zitnik.research.iobie.datasets.ace2004.annotations.RawTag

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/1/11
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */

class RelMention(
                  val id: String,
                  val ldlexicalcondition: String,
                  val ldcExtent: RawTag,
                  val relMentionArgs: ArrayList[RelMentionArg],
                  val relMentionTime: ArrayList[RelMentionTime]) {

}