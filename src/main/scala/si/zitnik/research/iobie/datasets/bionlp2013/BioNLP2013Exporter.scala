package si.zitnik.research.iobie.datasets.bionlp2013

import si.zitnik.research.iobie.domain.{Example, Examples}
import java.io.{BufferedWriter, FileWriter}

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.relationship.Relationship

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/8/13
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */
object BioNLP2013Exporter extends StrictLogging {

  def writeMentions(bw: BufferedWriter, example: Example): Unit = {
    example.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION")).foreach{ mention => {
      val startIdx = mention.get(Label.START_IDX).asInstanceOf[Int]
      val text = mention.get(Label.OBS).asInstanceOf[String]
      val endIdx = startIdx + text.size
      val id = mention.get(Label.ID).asInstanceOf[String]
      val toWrite = "%s\tAction %d %d\t%s\n".format(id, startIdx, endIdx, text)

      bw.write(toWrite)
    }}
  }

  def writeEvents(bw: BufferedWriter, example: Example): Unit = {
    example.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT")).foreach{ event => {
      val id = event.get(Label.ID)
      val subjType = event.get(Label.SUBJECT_TYPE)
      val subjTypeId = event.subj.get(Label.ID)
      val objType = event.get(Label.OBJECT_TYPE)
      val objTypeId = event.obj.get(Label.ID)
      val toWrite = "%s\t%s:%s %s:%s\n".format(id, objType, objTypeId, subjType, subjTypeId)

      bw.write(toWrite)

    }}
  }

  def writeRelationships(bw: BufferedWriter, example: Example): Unit = {
    example.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP")).foreach{ relationship => {
      val id = relationship.get(Label.ID)
      
      val subjTypeId = relationship.subj.get(Label.ATTRIBUTE_TYPE) match {
        case "EVENT" => {
          val event = relationship.subj.get(Label.VALUE).asInstanceOf[Relationship]
          event.get(Label.ID)
        }
        case "RELATIONSHIP" => {
          val rel = relationship.subj.get(Label.VALUE).asInstanceOf[Relationship]
          rel.get(Label.ID)
        }
        case _ => {
          relationship.subj.get(Label.ID)
        }
      }

      val objTypeId = relationship.obj.get(Label.ATTRIBUTE_TYPE) match {
        case "EVENT" => {
          val event = relationship.obj.get(Label.VALUE).asInstanceOf[Relationship]
          event.get(Label.ID)
        }
        case "RELATIONSHIP" => {
          val rel = relationship.obj.get(Label.VALUE).asInstanceOf[Relationship]
          rel.get(Label.ID)
        }
        case _ => {
            relationship.obj.get(Label.ID)
        }
      }

      val objType = relationship.get(Label.OBJECT_TYPE)
      val subjType = relationship.get(Label.SUBJECT_TYPE)
      

      val relName = relationship.relationshipName

      val toWrite = "%s\t%s %s:%s %s:%s\n".format(id, relName, objType, objTypeId, subjType, subjTypeId)
      bw.write(toWrite)

    }}
  }

  def writeNegations(bw: BufferedWriter, example: Example): Unit = {
    example.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("NEGATION")).foreach{ event => {
      val id = event.get(Label.ID)
      val objTypeId = event.obj.get(Label.VALUE).asInstanceOf[Relationship].get(Label.ID)
      val toWrite = "%s\tNegation %s\n".format(id, objTypeId)

      bw.write(toWrite)

    }}
  }

  def exportA2(folderPath: String, examples: Examples): Unit = {
    val docMap = examples.splitExamplesByDocuments()

    for ((docName, docExamples) <- docMap) {
      if (docExamples.size > 1) {
        logger.info("BioNLP examples have only one sentence!")
        System.exit(-1)
      }

      val bw = new BufferedWriter(new FileWriter(folderPath + "/" + docName + ".a2"))
      //write new mentions
      writeMentions(bw, docExamples(0))
      //write events
      writeEvents(bw, docExamples(0))
      //write (real) relationships
      writeRelationships(bw, docExamples(0))
      //write negations
      writeNegations(bw, docExamples(0))
      bw.close()
    }
  }

}
