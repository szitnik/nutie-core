package si.zitnik.research.iobie.datasets.semeval2010

import si.zitnik.research.iobie.domain.{Example, Examples, Token}

import si.zitnik.research.iobie.datasets.util.DatasetUtil
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}

import io.Source

import com.typesafe.scalalogging.StrictLogging


/**
 *
 *
 *
 *Dataset info (see also en.info.txt):

  1 ID: word identifiers in the sentence

  Columns 2--8: words and morphosyntactic information
  2 TOKEN: word forms
  3 LEMMA: word lemmas (gold standard manual annotation)
  4 PLEMMA: word lemmas predicted by an automatic analyzer
  5 POS: coarse part of speech
  6 PPOS: same as 5 but predicted by an automatic analyzer
  7 FEAT: morphological features (part of speech type, number, gender,
         case, tense, aspect, degree of comparison, etc., separated by
         the character "|")
  8 PFEAT: same as 7 but predicted by an automatic analyzer

  Columns 9--12: syntactic dependency tree
  9 HEAD: for each word, the ID of the syntactic head ('0' if the word
         is the root of the tree)
  10 PHEAD: same as 9 but predicted by an automatic analyzer
  11 DEPREL: dependency relation labels corresponding to the
           dependencies described in 9 ("sentence" if the word is the
           root of the tree)
  12 PDEPREL: same as 11 but predicted by an automatic analyzer

  Columns 13--14: Named entities
  13 NE: named entities
  14 PNE: same as 13 but predicted by a named entity recognizer

  Columns 15--16+N+M: semantic role labeling
  15 PRED: predicates are marked and annotated with a semantic class label
  16 PPRED: Same as 15 but predicted by an automatic analyzer
  * APREDs: N columns, one for each predicate in 15, containing the
           semantic roles/dependencies of each particular predicate
  * PAPREDs: M columns, one for each predicate in 16, with the same
            information as APREDs but predicted with an automatic analyzer.

  Last column: COREF tag.
 */
class SemEval2010Importer(val filePath: String, val filename: String) extends StrictLogging {

  def importForIE(): Examples = {
    val retVal = parse(filePath + "/" + filename)

    //tags manipulation
    DatasetUtil.toBIO_Tags_CoNLL(retVal, Label.NE, betweenLineCharachter = "")
    DatasetUtil.toCOREF_Tags(retVal, betweenOtherSeparator = "_")
    DatasetUtil.toCOREF_Constituents(retVal)

    retVal
  }

  private def parse(file: String): Examples = {
    val examples = new Examples()

    var curExample = new Example()
    var curDocID = ""
    for (line <- Source.fromFile(file).getLines()) {
      if (!line.startsWith("#")) {
        if (line.isEmpty) {

          //add new example
          if (curExample.size() > 0) {
            examples.add(curExample)
            curExample = new Example()
          }

        } else {

          //add token to example
          val splitLine = line.split("\t")

          curExample.put(ExampleLabel.DOC_ID, curDocID)
          val token = new Token(Label.OBS, splitLine(1))
          curExample.add(token)

          //1 ID: word identifiers in the sentence
            //not needed

          //Columns 2--8: words and morphosyntactic information
          //2 TOKEN: word forms
            //already done
          //3 LEMMA: word lemmas (gold standard manual annotation)
          //token.put(Label.LEMMA, splitLine(2)) //ommited = no training domain included
          //4 PLEMMA: word lemmas predicted by an automatic analyzer
            //TODO
          //5 POS: coarse part of speech
          token.put(Label.POS, splitLine(4))
          //6 PPOS: same as 5 but predicted by an automatic analyzer
            //TODO
          //7 FEAT: morphological features (part of speech type, number, gender,
          //case, tense, aspect, degree of comparison, etc., separated by
          //  the character "|")
            //TODO
          //8 PFEAT: same as 7 but predicted by an automatic analyzer
            //TODO

          //Columns 9--12: syntactic dependency tree
          //9 HEAD: for each word, the ID of the syntactic head ('0' if the word
          //  is the root of the tree)
            //TODO
          //10 PHEAD: same as 9 but predicted by an automatic analyzer
            //TODO
          //11 DEPREL: dependency relation labels corresponding to the
          //  dependencies described in 9 ("sentence" if the word is the
          //  root of the tree)
            //TODO
          //12 PDEPREL: same as 11 but predicted by an automatic analyzer
            //TODO

          //Columns 13--14: Named entities
          //13 NE: named entities
          token.put(Label.NE, splitLine(12))
          //14 PNE: same as 13 but predicted by a named entity recognizer

          //Columns 15--16+N+M: semantic role labeling
          //TODO: implement semantic role labeling within examples
          token.put(Label.REL, splitLine(14).split("\\.")(0)) //temporary relations
          //15 PRED: predicates are marked and annotated with a semantic class label
          //16 PPRED: Same as 15 but predicted by an automatic analyzer
          //  * APREDs: N columns, one for each predicate in 15, containing the
          //   semantic roles/dependencies of each particular predicate
          //  * PAPREDs: M columns, one for each predicate in 16, with the same
          //   information as APREDs but predicted with an automatic analyzer.

          //Last column: COREF tag.
          token.put(Label.COREF, splitLine(splitLine.length - 1))
        }
      } else if (line.startsWith("#begin document")) {
        curDocID = line.split(" ").last
      }
    }

    examples
  }

}

object SemEvalDataType extends Enumeration {
  val TRAIN_EN = Value("en.train.txt")
  val TEST_EN = Value("en.test.txt")
  val DEVEL_EN = Value("en.devel.txt")
}