package si.zitnik.research.iobie.datasets.bionlp2013.util

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.Example

import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/4/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
object Util extends StrictLogging {

  def createConstituent(example: Example, mention: A1Mention) = {
    //1. find start token
    var startTokenId = 0
    while (mention.start_id >= example.get(startTokenId).get(Label.START_IDX).asInstanceOf[Int]+example.get(startTokenId).get(Label.OBS).asInstanceOf[String].length) {
      startTokenId+=1
    }
    //startidx is in between
    if (mention.start_id > example.get(startTokenId).get(Label.START_IDX).asInstanceOf[Int]) {
      val splitIdx = mention.start_id - example.get(startTokenId).get(Label.START_IDX).asInstanceOf[Int]

      //println(tokens.get(endTokenId).get(Label.START_IDX).asInstanceOf[Int])

      val newToken = example.get(startTokenId).clone()
      newToken.put(Label.OBS, example.get(startTokenId).get(Label.OBS).asInstanceOf[String].substring(0, splitIdx))

      val newToken1 = example.get(startTokenId).clone()
      newToken1.put(Label.OBS, example.get(startTokenId).get(Label.OBS).asInstanceOf[String].substring(splitIdx))
      newToken1.put(Label.START_IDX, example.get(startTokenId).get(Label.START_IDX).asInstanceOf[Int]+splitIdx)

      example.set(startTokenId, newToken)
      example.insert(startTokenId+1, newToken1)
      startTokenId += 1
    }
    //2. find end token
    var endTokenId = startTokenId
    while (mention.end_id > example.get(endTokenId).get(Label.START_IDX).asInstanceOf[Int]+example.get(endTokenId).get(Label.OBS).asInstanceOf[String].length) {
      endTokenId+=1
    }
    //endidx is in between
    if (mention.end_id < example.get(endTokenId).get(Label.START_IDX).asInstanceOf[Int]+example.get(endTokenId).get(Label.OBS).asInstanceOf[String].length) {
      val splitIdx = mention.end_id - example.get(endTokenId).get(Label.START_IDX).asInstanceOf[Int]

      //println(tokens.get(endTokenId).get(Label.START_IDX).asInstanceOf[Int])

      val newToken = example.get(endTokenId).clone()
      newToken.put(Label.OBS, example.get(endTokenId).get(Label.OBS).asInstanceOf[String].substring(0, splitIdx))

      val newToken1 = example.get(endTokenId).clone()
      newToken1.put(Label.OBS, example.get(endTokenId).get(Label.OBS).asInstanceOf[String].substring(splitIdx))
      newToken1.put(Label.START_IDX, example.get(endTokenId).get(Label.START_IDX).asInstanceOf[Int]+splitIdx)

      example.set(endTokenId, newToken)
      example.insert(endTokenId+1, newToken1)
    }
    //3. enrich
    val cons = new Constituent(example, example.indexOf(example.get(startTokenId)), example.indexOf(example.get(endTokenId))+1)
    cons.put(Label.COREF, mention.coref_id)
    cons.put(Label.ID, mention.id)
    cons.put(Label.ENTITY_TYPE, mention.typem)



    var isExact = true
    val tokenSeq = example.subList(startTokenId, endTokenId+1)
    if (!cons.toString().equals(mention.text)) {
      logger.warn("Tagged text: \n\t\"%s\", start id: %d, end id: %d, does not equal to extracted: \n\t\"%s\", start id: %d, end id: %d.".
        format(mention.text, mention.start_id, mention.end_id,
        tokenSeq.map(_.get(Label.OBS)).mkString(" "),
        tokenSeq.head.get(Label.START_IDX),
        tokenSeq.last.get(Label.START_IDX).asInstanceOf[Int]+tokenSeq.last.get(Label.OBS).asInstanceOf[String].length))
      isExact = false
    }

    (cons, isExact)
  }

}
