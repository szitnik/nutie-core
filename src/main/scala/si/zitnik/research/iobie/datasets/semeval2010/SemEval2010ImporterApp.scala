package si.zitnik.research.iobie.datasets.semeval2010

import scala.Array

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
object SemEval2010ImporterApp  {

  def main(args: Array[String]): Unit = {
    val develPath = "/Users/slavkoz/Documents/DR_Research/Datasets/semeval-2010/semeval_2010_t1_eng/domain"
    val trainPath = "/Users/slavkoz/Documents/DR_Research/Datasets/semeval-2010/semeval_2010_t1_eng/domain"
    val testPath = "/Users/slavkoz/Documents/DR_Research/Datasets/semeval-2010/semeval_2010_t1_eng/domain"

    /*
    println("DEVEL STAT:")
    val develExamples = new SemEval2010Importer(develPath).importForIE()
    develExamples.printStatistics()
    */


    println("TRAIN STAT:")
    val trainExamples = new SemEval2010Importer(trainPath, SemEvalDataType.TRAIN_EN.toString).importForIE()
    trainExamples.printStatistics()



    /*
    println("TEST STAT:")
    val testExamples = new SemEval2010Importer(testPath).importForIE()
    testExamples.printStatistics()
    */

  }
}
