package si.zitnik.research.iobie.datasets.ace2004.annotations

class RawTag(
              val start: Int,
              val end: Int,
              val text: String) {

}