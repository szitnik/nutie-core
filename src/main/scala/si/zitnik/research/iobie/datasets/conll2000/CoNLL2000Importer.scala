package si.zitnik.research.iobie.datasets.conll2000

import io.Source
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.{Example, Examples}
import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.Label

/**

The domain files contain one word per line. Empty lines have been used
for marking sentence boundaries. Each non-empty line contains the following
tokens:

   1. the current word
   2. POS (by Brill tagger)
   3. chunk

 */

class CoNLL2000Importer(val filePath: String) {

  /**
   * Function supports English language only
   */
  def importAll(): ArrayList[Sentence] = {
    val retVal = new ArrayList[Sentence]()

    var sentence = new Sentence()
    var afterDocStart = false
    for ( line <- Source.fromFile(filePath).getLines()) {
      if (line.isEmpty) {
        //put sentence to the article
        retVal.add(sentence)
        sentence = new Sentence()
      } else {
        //put token to the sentence
        val splitLine = line.split(" ")
        sentence.add(new Token(splitLine(0), splitLine(1), splitLine(2)))
      }
    }

    //put last article
    retVal.add(sentence)
    retVal
  }

  def importForIE(): Examples = {
    val examples = new Examples()

    val importedData = importAll()



    for (sentence <- importedData) {
      val example = new Example()


      sentence.foreach(v => {
        val positionMap = new si.zitnik.research.iobie.domain.Token(Label.OBS, v.word)
        positionMap.put(Label.POS, v.pos)
        positionMap.put(Label.CHUNK, v.chunk)
        example.add(positionMap)
      })


      examples.add(example)
    }



    examples.printStatistics()
    examples
  }

}