package si.zitnik.research.iobie.datasets.tab

import si.zitnik.research.iobie.algorithms.crf.Label
import java.io.FileWriter

import si.zitnik.research.iobie.domain.Examples

import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/15/12
 * Time: 12:28 AM
 * To change this template use File | Settings | File Templates.
 */

class TabExporter(val labelTypes: Array[Label.Value], val filePath: String, val examples: Examples) extends StrictLogging {


  def exportForIE(newLine: String = "\r\n", separator: String = "\t"): Unit = {
    val fw = new FileWriter(filePath)

    for (example <- examples) {

      for (token <- example) {
        val line = new ArrayList[String]()
        line.add(token.get(Label.OBS))
        for (labelType <- labelTypes) {
          line.add(token.get(labelType))
        }
        fw.write("%s%s".format(line.mkString(separator), newLine))
      }
      fw.write(newLine);
    }

    fw.close()
  }

}

object TabExporter {
  def main(args: Array[String]): Unit = {

    val dsIn = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1.tsv"
    val dsOut = "/Users/slavkoz/temp/articles_1.tsv.copy"
    val labels = Array[Label.Value](Label.NE, Label.REL, Label.COREF)

    new TabExporter(labels, dsOut, new TabImporter(labels, dsIn).importForIE()).exportForIE()
  }
}