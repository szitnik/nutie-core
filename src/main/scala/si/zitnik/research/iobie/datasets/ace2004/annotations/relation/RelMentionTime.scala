package si.zitnik.research.iobie.datasets.ace2004.annotations.relation

import java.util.ArrayList
import si.zitnik.research.iobie.datasets.ace2004.annotations.RawTag

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/1/11
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */

class RelMentionTime(
                      val typer: String,
                      val valr: String,
                      val mod: String,
                      val dir: String,
                      val sources: ArrayList[RawTag]) {

}