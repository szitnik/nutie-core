package si.zitnik.research.iobie.datasets.conll2003

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/19/11
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */

class Token(
             val word: String,
             val pos: String,
             val chunk: String,
             val ne: String) {

}