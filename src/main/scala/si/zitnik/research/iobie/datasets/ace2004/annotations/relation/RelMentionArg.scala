package si.zitnik.research.iobie.datasets.ace2004.annotations.relation

import si.zitnik.research.iobie.datasets.ace2004.annotations.RawTag

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/1/11
 * Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */

class RelMentionArg(
                     val entityMentionId: String,
                     val argnum: String,
                     val extent: RawTag) {

}