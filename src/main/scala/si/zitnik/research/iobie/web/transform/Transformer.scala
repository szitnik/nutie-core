package si.zitnik.research.iobie.web.transform

import si.zitnik.research.iobie.algorithms.crf.ExampleLabel
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.web.SessionData
import si.zitnik.research.iobie.web.params.{Label, Relationship}

import scala.collection.JavaConverters._
import scala.collection.mutable


/**
 * Created by slavkoz on 02/06/15.
 */
object Transformer {
  def transform(relationship: si.zitnik.research.iobie.domain.relationship.Relationship) = {
    Relationship(relationship.relationshipName, relationship.subj.getText(), relationship.obj.getText())
  }

  def transform(label: si.zitnik.research.iobie.algorithms.crf.Label.Value) = {
    Label(label.toString, label.toString)
  }

  /**
    * Current implementations only returns mentions with coreference links.
    * @param examples
    * @return
    */
  def transformToBrat(examples: Examples, tagged: Boolean = false) = {

    var relations = List[Any]()
    var entities = List[Any]()
    val docId = examples.get(0).get(ExampleLabel.DOC_ID).asInstanceOf[String]

    val clusters: mutable.Buffer[Cluster] = if (tagged)
      SessionData.taggedCoreferenceClusters(docId)
    else
      examples.getAllMentionClusters().get(docId).asScala

    var mentionIdx = 0
    var corefLinkIdx = 0
    for ((cluster, clusterIdx) <- clusters.zipWithIndex) {
      val sortedMentions = cluster.toList.sortBy(_.startTextIdx)

      //Add the first one as later we will add only the second ones in sliding
      //Values EntityXX and CoreferenceXX define the colors in HTML. The predefined colors go from 1 to 10,
      //so that is why we use modulo10 + 1.
      entities :+= List(
                        "M"+mentionIdx,
                        "Entity"+(clusterIdx%10+1).toString,
                        List(List(sortedMentions(0).startTextIdx, sortedMentions(0).endTextIdx))
                    )
      mentionIdx+=1

      for (corefLink <- sortedMentions.sliding(2) if sortedMentions.size > 1) {
        entities :+= List(
                          "M"+mentionIdx,
                          "Entity"+(clusterIdx%10+1).toString,
                          List(List(corefLink(1).startTextIdx, corefLink(1).endTextIdx))
                    )
        mentionIdx+=1

        relations :+= List(
                          "R"+corefLinkIdx,
                          "Coreference"+(clusterIdx%10+1).toString,
                          List(List("arg1", "M"+(mentionIdx-2)), List("arg2", "M"+(mentionIdx-1)))
                    )
        corefLinkIdx+=1
      }
    }

    Map(("text", examples.text), ("entities", entities), ("relations", relations))
  }
}
