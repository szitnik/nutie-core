package si.zitnik.research.iobie.web.servlets

import org.scalatra.{Ok, ScalatraServlet}
import org.scalatra.json._
import org.json4s.{DefaultFormats, Formats}
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer
import si.zitnik.research.iobie.core.coreference.learner.CorefPairwiseLearnerExactMatch
import si.zitnik.research.iobie.core.coreference.test.SSJ500kCoreference.{clusterResults, printClusters}
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.datasets.TEIP5.TEIP5Importer
import si.zitnik.research.iobie.domain.sampling.RandomSampler
import si.zitnik.research.iobie.statistics.cluster._
import si.zitnik.research.iobie.web.SessionData
import si.zitnik.research.iobie.web.params._
import si.zitnik.research.iobie.web.transform.Transformer
import si.zitnik.research.iobie.web.util.ParameterHelper
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.cluster.Cluster

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
 * Created by slavkoz on 27/05/15.
 */
class ExamplesServlet extends ScalatraServlet with JacksonJsonSupport {
  protected implicit val jsonFormats: Formats = DefaultFormats
  final val CORS_HEADERS = Map(
    "Access-Control-Allow-Origin" -> "*",
    "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS, DELETE",
    "Access-Control-Max-Age" -> "3600",
    "Access-Control-Allow-Headers" -> "x-requested-with, content-type")

  before() {
    contentType = formats("json")
  }

  options("/*"){
    response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
    response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"))
  }

  get("/") {
    val retVal = SessionData.examples.toString
    Ok(retVal,CORS_HEADERS)
  }

  get("/size") {
    val retVal = ResultInt(SessionData.examples.size())
    Ok(retVal,CORS_HEADERS)
  }

  get("/loaded") {
    val retVal = ResultBoolean(SessionData.examples != null && SessionData.examples.size() > 0)
    Ok(retVal,CORS_HEADERS)
  }

  get("/:examples_type/documentIds") {
    val data = ParameterHelper.selectExamples({params("examples_type")})
    val retVal = data.getAllDocIds()
    Ok(retVal,CORS_HEADERS)
  }

  get("/split/:train_size") {
    val result = RandomSampler.sample(SessionData.examples, randomSeed = 42, trainPercent = {params("train_size")}.toDouble)
    SessionData.examplesTrain = result._1
    SessionData.examplesTest = result._2
    Ok(ResultBoolean(true), CORS_HEADERS)
  }

  get("/coreferences/algorithms") {
    Ok(ParameterHelper.COREFERENCE_ALGORITHMS,CORS_HEADERS)
  }

  get("/coreferences/featureFunctionPackages") {
    Ok(ParameterHelper.FEATURE_FUNCTION_PACKAGES,CORS_HEADERS)
  }

  get("/coreferences/brat/:examples_type/:document_id") {
    val data = ParameterHelper.selectExamples({params("examples_type")})
    val retVal = Transformer.transformToBrat(data.getDocumentExamples({params("document_id")}))
    Ok(retVal,CORS_HEADERS)
  }

  get("/coreferences/brat/tagged/:examples_type/:document_id") {
    val data = ParameterHelper.selectExamples({params("examples_type")})
    val retVal = Transformer.transformToBrat(data.getDocumentExamples({params("document_id")}), tagged = true)
    Ok(retVal,CORS_HEADERS)
  }

  /**
    * ExamplesType is meant to be "examples", "examplesTrain" or "examplesTest"
    */
  post("/coreferences/train/:examples_type") {
    val data = ParameterHelper.selectExamples({params("examples_type")})
    val parameters = parsedBody.extract[CorefTrainParameters]

    ParameterHelper.setCoreferenceLearner(parameters.algorithm, parameters.featureFunctionPackage, data)
    SessionData.corefClassifier = SessionData.coreferenceLearner.train

    Ok(ResultBoolean(true), CORS_HEADERS)
  }

  /**
    * ExamplesType is meant to be "examples", "examplesTrain" or "examplesTest"
    */
  get("/coreferences/tag/:examples_type") {
    // get mention examples
    val mentionExamples = new MentionExamplesBuilder(
      ParameterHelper.selectExamples({params("examples_type")}),
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()

    val clusters = SessionData.corefClassifier.classifyClusters(mentionExamples)
    // map results by doc_id
    SessionData.taggedCoreferenceClusters = clusters.map(c => (c.head.head.oldConstituent.example.get(ExampleLabel.DOC_ID).asInstanceOf[String], c.toBuffer)).toMap

    Ok(ResultBoolean(true), CORS_HEADERS)
  }

  get("/coreferences/taggedClusters/:doc_id") {
    val docId = {params("doc_id")}

    val retVal = SessionData.taggedCoreferenceClusters(docId).map(_.toString).sortWith(_.length > _.length).toList

    Ok(retVal, CORS_HEADERS)
  }

  /**
    * ExamplesType is meant to be "examples", "examplesTrain" or "examplesTest"
    */
  get("/coreferences/realClusters/:examples_type/:doc_id") {
    val docId = {params("doc_id")}

    // get mention examples
    val mentionExamples = new MentionExamplesBuilder(
      ParameterHelper.selectExamples({params("examples_type")}),
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()


    val retVal = TaggedClusterer.doClustering(mentionExamples, ommitSingles = false).
      find(_.head.head.example.get(ExampleLabel.DOC_ID).asInstanceOf[String].equals(docId)).get.toList.
      map(_.toString).sortWith(_.length > _.length).toList

    Ok(retVal, CORS_HEADERS)
  }

  /**
    * ExamplesType is meant to be "examples", "examplesTrain" or "examplesTest"
    */
  get("/coreferences/evaluate/:examples_type") {
    // get mention examples
    val mentionExamplesToCompare = new MentionExamplesBuilder(
      ParameterHelper.selectExamples({params("examples_type")}),
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()

    //Doing some magic to align cluster list for evaluation as we above put clusters into a hashmap.
    val allRealClusters = TaggedClusterer.doClustering(mentionExamplesToCompare, ommitSingles = false).sortBy(_.head.head.example.get(ExampleLabel.DOC_ID).asInstanceOf[String])
    val allTaggedClusters = SessionData.taggedCoreferenceClusters.values.map(_.to[mutable.HashSet]).to[mutable.ArrayBuffer].sortBy(_.head.head.example.get(ExampleLabel.DOC_ID).asInstanceOf[String])

    val F = new ClusterStatistics().stat(allRealClusters, allTaggedClusters)
    val MUC = MUCClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    val BCubed = BCubedClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    val CEAFe = CEAFClusterStatistics(CEAFStatisticsType.ENTITY_BASED).scoreExamples(allRealClusters, allTaggedClusters)
    val conll2012Official = CoNLL2012Score.scoreExamples(MUC._3, BCubed._3, CEAFe._3)

    val resultList = List(
      Option(F.toString, "Pairwise"),
      Option("P: %.2f, R: %.2f, F1: %.2f".format(MUC._1, MUC._2, MUC._3), "MUC"),
      Option("P: %.2f, R: %.2f, F1: %.2f".format(BCubed._1, BCubed._2, BCubed._3), "BCubed"),
      Option("P: %.2f, R: %.2f, F1: %.2f".format(CEAFe._1, CEAFe._2, CEAFe._3), "CEAFe"),
      Option("%.2f".format(conll2012Official), "CoNLL2012")
    )

    Ok(resultList, CORS_HEADERS)
  }

  get("/sentence/:sentence_id/size") {
    val retVal = SessionData.examples.get({params("sentence_id")}.toInt).size()
    Ok(retVal,CORS_HEADERS)
  }

  get("/sentence/:sentence_id") {
    val retVal = SessionData.examples.get({params("sentence_id")}.toInt).
      map(m => (m.keySet().map(v => (v.toString, m.get(v).toString))).toMap).
      toList
    Ok(retVal,CORS_HEADERS)
  }

  get("/relationships/:sentence_id") {
    val example = SessionData.examples.get({params("sentence_id")}.toInt)
    val relationships = example.getAllRelationships()
    val retVal = relationships.map(Transformer.transform(_)).toList
    Ok(retVal,CORS_HEADERS)
  }

  get("/sentenceText/:sentence_id") {
    val retVal = SessionData.examples.get({params("sentence_id")}.toInt).toString()
    Ok(retVal,CORS_HEADERS)
  }

  get("/tokenTypes") {
    val retVal = SessionData.examples.get(0).get(0).keySet().map(Transformer.transform(_)).toList
    Ok(retVal,CORS_HEADERS)
  }

  get("/token/:sentence_id/:token_id/:type") {
    val retVal = SessionData.examples.get({params("sentence_id")}.toInt).
      get({params("token_id")}.toInt).
      get(Label.withName({params("type")}))
    Ok(retVal,CORS_HEADERS)
  }

  post("/load") {
    val dataset = parsedBody.extract[Dataset]

    if (dataset.loaderType.equals("ssj500k")) {
      SessionData.examples = new TEIP5Importer(dataset.filename1).importForIE()
      Ok(Result(true), CORS_HEADERS)
    } else if (dataset.loaderType.equals("coref149")) {
      SessionData.examples = TEIP5Importer.getSloveneCoreferenceResolutionDataset(dataset.filename1, dataset.filename2)
      Ok(Result(true), CORS_HEADERS)
    } else {
      Ok(Result(false), CORS_HEADERS)
    }
  }
}

