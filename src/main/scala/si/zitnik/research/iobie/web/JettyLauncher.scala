package si.zitnik.research.iobie.web

import java.awt.Desktop
import java.net.URI

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.{DefaultServlet, ServletContextHandler}
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener

/**
  * Created by the instructions: http://scalatra.org/guides/2.4/deployment/standalone.html
  */
object JettyLauncher {
  def main(args: Array[String]) {
    val port = if(System.getenv("PORT") != null) System.getenv("PORT").toInt else 8080

    val server = new Server(port)
    val context = new WebAppContext()
    context setContextPath "/"
    context.setResourceBase("src/main/webapp")
    context.setInitParameter(ScalatraListener.LifeCycleKey, "si.zitnik.research.iobie.web.ScalatraBootstrap")
    context.addEventListener(new ScalatraListener)
    context.addServlet(classOf[DefaultServlet], "/")

    server.setHandler(context)
    server.start

    //Run browser - TODO: useful if shipped together with web GUI
    //Desktop.getDesktop().browse(new URI("http://localhost:8888"))

    server.join
  }
}