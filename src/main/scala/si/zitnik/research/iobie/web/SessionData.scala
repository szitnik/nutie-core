package si.zitnik.research.iobie.web

import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Learner}
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier
import si.zitnik.research.iobie.core.coreference.learner.CorefLearner
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.domain.cluster.Cluster

import scala.collection.immutable.HashMap
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, HashSet}

/**
 * Created by slavkoz on 27/05/15.
 */
object SessionData {
  // EXAMPLES
  var examples = new Examples()
  var examplesTest = new Examples()
  var examplesTrain = new Examples()

  // COREFERENCES
  var taggedCoreferenceClusters: Map[String, mutable.Buffer[Cluster]] = new HashMap[String, mutable.Buffer[Cluster]]()
  var corefClassifier: CorefClassifier = null
  var coreferenceLearner: CorefLearner = null
}
