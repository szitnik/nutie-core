package si.zitnik.research.iobie.web.params

case class CorefTrainParameters(algorithm: String, featureFunctionPackage: String, parameters: Map[String, String]) {

}
