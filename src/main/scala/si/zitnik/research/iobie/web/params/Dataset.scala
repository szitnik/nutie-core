package si.zitnik.research.iobie.web.params

/**
 * Created by slavkoz on 01/06/15.
 */
case class Dataset(loaderType: String, filename1: String, filename2: String)
