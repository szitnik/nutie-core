package si.zitnik.research.iobie.web.params

/**
 * Created by slavkoz on 02/06/15.
 */
case class Result(success: Boolean, message: String = "OK")
