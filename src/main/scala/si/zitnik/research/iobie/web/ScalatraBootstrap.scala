package si.zitnik.research.iobie.web

import javax.servlet.ServletContext

import org.scalatra.LifeCycle
import si.zitnik.research.iobie.web.servlets.ExamplesServlet

/**
 * Created by slavkoz on 27/05/15.
 */
class ScalatraBootstrap extends LifeCycle {

  override def init(context: ServletContext): Unit = {
    //context.mount(new StaticServlet, "/*")
    context mount(new ExamplesServlet, "/examples/*")

  }

}
