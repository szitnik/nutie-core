package si.zitnik.research.iobie.web.util

import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, FeatureFunction, Label}
import si.zitnik.research.iobie.algorithms.crf.feature.packages.{FeatureFunctionPackages, SloCoreferenceFeatureFunctionPackages}
import si.zitnik.research.iobie.core.coreference.learner._
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.web.SessionData
import si.zitnik.research.iobie.web.params.{CorefTrainParameters, Option}

object ParameterHelper {
  def setCoreferenceLearner(selectedAlgorithm: String, selectedFeatureFunctionPackage: String, data: Examples) = {
    // get mention examples
    val mentionExamples = new MentionExamplesBuilder(
      data,
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()

    val featureFunctionSet = selectFeatureFunctions(selectedFeatureFunctionPackage)

    SessionData.coreferenceLearner = selectedAlgorithm match {
      case "singleton" => new CorefSingletonLearner()
      case "pairwise" => new CorefPairwiseLearner(mentionExamples, featureFunctionSet)
      case "pairwise_exact" => new CorefPairwiseLearnerExactMatch(maxPairwiseDistance = 50, learnLabelType = Label.OBS)
      case "all_in_one" => new CorefAllInOneLearner()
      case "skipcor_pair" => {
        new CorefPairwiseLearner(
          mentionExamples,
          featureFunctions = featureFunctionSet,
          modelSaveFilename = "SkipCor_PAIRWISE",
          maxPairwiseDistance = 50
        )
      }
      case "skipcor" => {
        new CorefMultipleLearner(
          mentionExamples,
          featureFunctions = featureFunctionSet,
          skipNumbers = (0 to 50).toArray,
          modelSaveFilename = "SkipCor_MULTIPLE"
        )
      }
    }

  }

  val COREFERENCE_ALGORITHMS = List(
    Option("singleton", "Singleton"),
    Option("all_in_one", "All in one"),
    Option("pairwise", "Pairwise similarity"),
    Option("pairwise_exact", "Pairwise exact match"),
    Option("skipcor_pair", "SkipCor pairs"),
    Option("skipcor", "SkipCor")
  )

  val FEATURE_FUNCTION_PACKAGES = List(
    Option("standardFFunctions",                    "Standard feature functions"),
    Option("standardNerFFunctions",                 "Standard NER feature functions"),
    Option("standardChemdner2013FFunctions",        "Chemdner 2013 feature functions"),
    Option("standardCorefFFunctions",               "Standard COREF feature functions"),
    Option("bestACE2004CorefFeatureFunctions",      "Best ACE 2004 COREF feature functions"),
    Option("bestCoNLL2012CorefFeatureFunctions",    "Best CoNLL 2012 COREF feature functions"),
    Option("bestSemEval2010CorefFeatureFunctions",  "Best SemEval 2010 COREF feature functions"),
    Option("standardRelFFPackages",                 "Standard REL feature functions"),
    Option("stringSloCorefFFunctions",              "String-based SLO COREF feature functions"),
    Option("lexicalSloCorefFFunctions",             "Lexical-based SLO COREF feature functions"),
    Option("semanticSloCorefFFunctions",            "Semantic-based SLO COREF feature functions"),
    Option("distanceSloCorefFFunctions",            "Distance-based SLO COREF feature functions"),
    Option("allSloCorefFeatureFunctions",           "ALL SLO COREF feature functions")
  )

  def selectFeatureFunctions(featureFunctionsType: String): ArrayList[FeatureFunction] = {
    featureFunctionsType match {
      case "standardFFunctions" => FeatureFunctionPackages.standardFFunctions
      case "standardNerFFunctions" => FeatureFunctionPackages.standardNerFFunctions
      case "standardChemdner2013FFunctions" => FeatureFunctionPackages.standardChemdner2013FFunctions(4)
      case "standardCorefFFunctions" => FeatureFunctionPackages.standardCorefFFunctions
      case "bestACE2004CorefFeatureFunctions" => FeatureFunctionPackages.bestACE2004CorefFeatureFunctions
      case "bestCoNLL2012CorefFeatureFunctions" => FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions
      case "bestSemEval2010CorefFeatureFunctions" => FeatureFunctionPackages.bestSemEval2010CorefFeatureFunctions
      case "standardRelFFPackages" => FeatureFunctionPackages.standardRelFFPackages
      case "stringSloCorefFFunctions" => SloCoreferenceFeatureFunctionPackages.stringSloCorefFFunctions
      case "lexicalSloCorefFFunctions" => SloCoreferenceFeatureFunctionPackages.lexicalSloCorefFFunctions
      case "semanticSloCorefFFunctions" => SloCoreferenceFeatureFunctionPackages.semanticSloCorefFFunctions
      case "distanceSloCorefFFunctions" => SloCoreferenceFeatureFunctionPackages.distanceSloCorefFFunctions
      case "allSloCorefFeatureFunctions" => SloCoreferenceFeatureFunctionPackages.allSloCorefFeatureFunctions
    }
  }

  def selectExamples(exampleType: String): Examples = {
    exampleType match {
      case "examples" => SessionData.examples
      case "examplesTest" => SessionData.examplesTest
      case "examplesTrain" => SessionData.examplesTrain
    }
  }

}
