package si.zitnik.research.iobie.core.relationship.classifier.abst

import si.zitnik.research.iobie.algorithms.crf.Classifier
import si.zitnik.research.iobie.domain.{Examples, Example}
import collection.mutable._
import si.zitnik.research.iobie.domain.relationship.Relationship

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/7/13
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class RelationshipClassifier extends Classifier {
    def classifyRelationships(example: Example): HashSet[Relationship]
    def classifyRelationships(examples: Examples): ArrayBuffer[HashSet[Relationship]]
}
