package si.zitnik.research.iobie.core.collective.learner

import si.zitnik.research.iobie.domain.Examples

import scala.collection.JavaConversions._
import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.algorithms.crf._
import si.zitnik.research.iobie.core.coreference.learner.CorefMultipleLearner
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.core.coreference.classifier.impl.CorefMultipleClassifier
import si.zitnik.research.iobie.core.collective.classifier.CollectiveClassifier
import si.zitnik.research.iobie.core.collective.util.CollectiveUtil

import si.zitnik.research.iobie.core.ner.mention.classifier.impl.NERMentionClassifier
import si.zitnik.research.iobie.core.relationship.classifier.impl.RelationshipMultipleClassifier
import si.zitnik.research.iobie.core.ner.mention.learner.NERMentionLearner
import si.zitnik.research.iobie.core.relationship.learner.RelationshipMultipleLearner

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.feature.packages.JointIterativeFeatureFunctionPackage
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/23/12
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */

class CollectiveLearner(
                        examples: Examples,
                        neFeatureFunctions: ArrayList[FeatureFunction],
                        relFeatureFunctions: ArrayList[FeatureFunction],
                        corefFeatureFunctions: ArrayList[FeatureFunction],
                        numOfIters: Int = 3) extends StrictLogging {




  def train(): CollectiveClassifier = {
    val neClassifiers = new Array[NERMentionClassifier](numOfIters)
    val relClassifiers = new Array[RelationshipMultipleClassifier](numOfIters)
    val corefClassifiers = new Array[CorefMultipleClassifier](numOfIters)

    val mentionExamples = new MentionExamplesBuilder(examples, null, ExampleLabel.DOC_ID).buildFromTagged()
    
    var iterativeAndOntologyFeatureFunctions = new ArrayList[FeatureFunction]()


    val modelFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie_ACE2004.ttl"
    var queryFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getTypesByName.txt"
    iterativeAndOntologyFeatureFunctions.add(new OntologyTokenFeatureFunction(Label.OBS, modelFile, queryFile, "UOType"))
    queryFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getSupertypesByName.txt"
    iterativeAndOntologyFeatureFunctions.add(new OntologyTokenFeatureFunction(Label.OBS, modelFile, queryFile, "UOSType"))
    queryFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getRelationBetweenTheTwo.txt"
    iterativeAndOntologyFeatureFunctions.add(new OntologyRelationFeatureFunction(Label.OBS, modelFile, queryFile, "UORel"))


    for (i <- 0 until numOfIters) {
      logger.info("Iteration " + i)

      //NER
      println("A")
      neClassifiers(i) = new NERMentionLearner(examples, new ArrayList(neFeatureFunctions ++ iterativeAndOntologyFeatureFunctions), Label.NE, "NECollective_%d".format(i)).train()

      //REL
      println("B")
      relClassifiers(i) = new RelationshipMultipleLearner(examples, new ArrayList(relFeatureFunctions ++ iterativeAndOntologyFeatureFunctions), Label.REL, Array(0,1,2,3), "RECollective_%d_".format(i)).train()

      //COREF
      println("C")
      corefClassifiers(i) = new CorefMultipleLearner(mentionExamples, new ArrayList(corefFeatureFunctions ++ iterativeAndOntologyFeatureFunctions), modelSaveFilename = "COREFCollective_%d_".format(i)).train()

      if (i+1 < numOfIters) { //is not the last step
        CollectiveUtil.updateIntermediateNERLabelings(examples, i, neClassifiers(i))
        val curRelLabels = relClassifiers(i).classifyRelationships(examples)
        val curCorefClusters = corefClassifiers(i).classifyClusters(mentionExamples)
        JointIterativeFeatureFunctionPackage.reindex(curRelLabels, curCorefClusters)
      }


      if (i == 0) {
        iterativeAndOntologyFeatureFunctions.addAll(JointIterativeFeatureFunctionPackage.create(examples))
      }

    }

    new CollectiveClassifier(
      numOfIters,
      neClassifiers,
      relClassifiers,
      corefClassifiers)
  }



}
