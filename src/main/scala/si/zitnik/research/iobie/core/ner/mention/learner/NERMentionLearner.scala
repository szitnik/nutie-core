package si.zitnik.research.iobie.core.ner.mention.learner

import si.zitnik.research.iobie.domain.Examples
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label, Learner}
import si.zitnik.research.iobie.thirdparty.crfsuite.api.CRFSuiteLCCRFLearner
import si.zitnik.research.iobie.core.ner.mention.classifier.impl.NERMentionClassifier

import scala.collection.JavaConversions._

/**
 *
 * Input FeatureFunctions should not refer to neighbouring examples! That is because one example represents one
 * document and therefore mentions are independent.
 *
 * The same must comply for input to classifier.
 *
 * -------------------
 *
 * Input examples should be mentions. Each example represents a document.
 * @param mentionExamples
 * @param featureFunctions
 * @param learnLabelType
 */
class NERMentionLearner(
                            mentionExamples: Examples,
                            val featureFunctions: ArrayList[FeatureFunction],
                            val learnLabelType: Label.Value = Label.NE,
                            val modelSaveFilename: String = "ner_model",
                            val epochs: Option[Int] = None) extends Learner(mentionExamples) with StrictLogging {

  def train() = {
    val classifier = new CRFSuiteLCCRFLearner(mentionExamples, learnLabelType, featureFunctions, modelSaveFilename + ".obj", maxIterations = epochs).train()
    new NERMentionClassifier(classifier, learnLabelType)
  }

  def trainAndTest(epochsBetweenTest: Int = 5, allEpochs: Int = 50, testMentionExamples: Examples = mentionExamples): NERMentionClassifier = {
    var classifier: NERMentionClassifier = null

    for (epoch <- 1 to math.max(allEpochs / epochsBetweenTest, 1)) {
      classifier = train()
      //logger.info("Training perf:")
      //classifier.test(testMentionExamples)

      if (testMentionExamples != mentionExamples) {
        logger.info("Testing perf:")
        testMentionExamples.foreach(e => classifier.classify(e))
        //classifier.test(testMentionExamples) //TODO: update methods
      }
    }

    classifier
  }

}

