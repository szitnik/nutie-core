package si.zitnik.research.iobie.core.coreference.test

import si.zitnik.research.iobie.datasets.conll2012._
import scala.Array
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, ExampleLabel, Label}
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.core.coreference.learner.CorefMultipleLearner
import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer
import java.util
import util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.feature._
import coreference.{BigramIsInTheSameSentenceFeatureFunction, BigramAppositiveFeatureFunction}
import packages.FeatureFunctionPackages
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.statistics.cluster.ClusterStatistics

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/26/12
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2012Coreference {

  def importData(dType: CoNLL2012ImporterDatasetTypeEnum.Value = CoNLL2012ImporterDatasetTypeEnum.TRAIN) = {
    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2012/dataset/v1/conll-2012/v1/domain"
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = dType,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = Array(CoNLL2012ImporterSourceTypeEnum.MAGAZINE),
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    //val conll2012Importer = new CoNLL2012Importer(path)
    val examples = conll2012Importer.importForIE()
    examples
  }

  def defineCorefFFunctions(): util.ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(FeatureFunctionPackages.standardFFunctions)


    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "UPF")) //POS unigram, for example is token pronoun, noun, ...
    featureFunctions.add(new BigramFeatureFunctionMatch(Label.OBS)) //string match  (TANL-1 Same feature)
    featureFunctions.add(new BigramFeatureFunctionMatch(Label.NE)) //semantic match (TANL-1 Type feature)
    featureFunctions.add(new BigramNumberMatchFeatureFunction()) //number match (TANL-1 Number)
    featureFunctions.add(new BigramGenderMatchFeatureFunction()) //gender match (TANL-1 Gender)
    featureFunctions.add(new BigramAppositiveFeatureFunction()) //is it appositive
    featureFunctions.add(new BigramAliasFeatureFunction()) //one is alias, abbreviation of another (TANL-1 Acronym)

    featureFunctions.add(new BigramIsPrefixFeatureFunction()) //one mention is a prefix of another (TANL-1 Prefix)
    featureFunctions.add(new BigramIsSuffixFeatureFunction()) //one mention is a suffix of another (TANL-1 Suffix)
    featureFunctions.add(new BigramSimilarityFeatureFunction()) //mentions are similar to each other (TANL-1 Edit distance)

    //TODO: unsupported feature functions (needs the whole examples)
    //TODO: sentence distance (TANL-1 Sentence distance)
    //TODO: token distance (TANL-1 Token distance)
    //TODO: mention distance (TANL-1 Mention distance)
    //TODO: mention count (TANL-1 Count)
    //TODO: is first mention (BART first mention)

    featureFunctions.add(new BigramConsecutiveFeatureFunction(Label.POS)) //POS pair (TANL-1 Head POS)
    featureFunctions.add(new UnigramPronounTypeFeatureFunction()) //pronoun type (TANL-1 Pronoun)
    //TODO: animacy agreement (BART animacy)
    //TODO: is in quoted speech (BART inquotedspeech)
    featureFunctions.add(new BigramIsInTheSameSentenceFeatureFunction())



    featureFunctions
  }

  def main(args: Array[String]): Unit = {
    val examples = importData(CoNLL2012ImporterDatasetTypeEnum.TRAIN)
    println("TRAIN RAW DATA: ")
    examples.printStatistics(ommited = Array(Label.OBS, Label.PARSE_NODE, Label.REL, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)
    val examplesTest = importData(CoNLL2012ImporterDatasetTypeEnum.DEVELOPMENT)
    println("TEST RAW DATA: ")
    examplesTest.printStatistics(ommited = Array(Label.OBS, Label.PARSE_NODE, Label.REL, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)



    val mentionExamples = new MentionExamplesBuilder(
      examples,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    println("TRAIN MENTION DATA:")
    mentionExamples.printStatistics(ommited = Array(Label.OBS, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)
    val mentionExamplesTest = new MentionExamplesBuilder(
      examplesTest,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    println("TEST MENTION DATA:")
    mentionExamplesTest.printStatistics(ommited = Array(Label.OBS, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)


    val corefFFunctions = defineCorefFFunctions()

    val classifier = new CorefMultipleLearner(mentionExamples, corefFFunctions).trainAndTest(5000, 5000, mentionExamplesTest)

    val allRealClusters = TaggedClusterer.doClustering(mentionExamples, ommitSingles = true)
    val allTaggedClusters = classifier.classifyClusters(mentionExamples)

    /*
    val distanceDistribution = CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(mentionExamples)
    println("Mention distance distribution: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))
    DistributionVisualizer.visualize(distanceDistribution)
    */

    /*
    val exampleId = 0
    //println(mentionExamples.get(exampleId).map(v => v.get(Label.COREF).toString+" "+v.get(Label.OBS)).zipWithIndex.mkString("\n"))
    examples.printMentionDetails(mentionExamples.get(exampleId).get(ExampleLabel.DOC_ID), ommitSingles = true)
    examples.printDocument(mentionExamples.get(exampleId).get(ExampleLabel.DOC_ID))
    println(allRealClusters(exampleId).mkString(" ||| "))
    println()
    println(allTaggedClusters(exampleId).mkString(" ||| "))
    */


    val F = new ClusterStatistics().stat(allRealClusters, allTaggedClusters)
    println(F)

  }

}
