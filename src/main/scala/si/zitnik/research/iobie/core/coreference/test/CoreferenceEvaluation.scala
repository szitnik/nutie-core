package si.zitnik.research.iobie.core.coreference.test

import si.zitnik.research.iobie.datasets.conll2012._

import scala.Array

import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, FeatureFunction, Label}
import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.feature.packages.FeatureFunctionPackages
import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.core.coreference.learner.CorefPairwiseLearnerExactMatch
import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer

import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.datasets.semeval2010.{SemEval2010Importer, SemEvalDataType}

import collection.mutable.{ArrayBuffer, HashSet}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.core.coreference.analysis.CoreferenceAnalysis

import collection.SortedMap
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.statistics.cluster._
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier
import si.zitnik.research.iobie.datasets.ace2004.{ACE2004DocumentType, ACE2004Importer}
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}
import si.zitnik.research.iobie.domain.sampling.{ExactSampler, RandomSampler}
import si.zitnik.research.iobie.thirdparty.opennlp.api.{ParseTagger, PoSTagger}
import si.zitnik.research.iobie.statistics.cluster.CEAFClusterStatistics
import si.zitnik.research.iobie.statistics.cluster.CEAFClusterStatistics
import si.zitnik.research.iobie.statistics.cluster.CEAFClusterStatistics
import si.zitnik.research.iobie.thirdparty.lemmagen.api.LemmaTagger


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/7/12
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */
object CoreferenceEvaluation {

  def printClusters(real: ArrayBuffer[HashSet[Cluster]], tagged: ArrayBuffer[HashSet[Cluster]]): Unit = {
    //if (real.size <= 10) {
      println("REAL:\n\t"+real)
      println("TAGGED:\n\t"+tagged)
    //}
  }

  def clusterResults(classifier: CorefClassifier, mentionExamples: Examples, realExamples: Examples, print: Boolean = true) = {
    val allRealClusters = TaggedClusterer.doClustering(mentionExamples, ommitSingles = false)
    val allTaggedClusters = classifier.classifyClusters(mentionExamples)
    //CorefVisualizer.run(mentionExamples, allRealClusters, allTaggedClusters)

    //realExamples.setMentionConstituents(allTaggedClusters)
    //new CoNLL2012Exporter().exportConll(realExamples, "/Users/slavkoz/Downloads/homogenized-CoNLL-COREFTagged-test.out")
    //realExamples.setMentionConstituents(allRealClusters)
    //new CoNLL2012Exporter().exportConll(realExamples, "/Users/slavkoz/Downloads/homogenized-CoNLL-COREF-test.out")

    //printClusters(allRealClusters, allTaggedClusters)


    val F = new ClusterStatistics().stat(allRealClusters, allTaggedClusters)
    if (print)
    println("Results Pairwise: " + F)

    val MUC = MUCClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    if (print)
    println("Results MUC:" + MUC)

    val BCubed = BCubedClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    if (print)
    println("Results BCubed: " + BCubed)

    /*
    val BLANC = BLANCClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    println("Results BLANC: " + BLANC)
    */

    val CEAFe = CEAFClusterStatistics(CEAFStatisticsType.ENTITY_BASED).scoreExamples(allRealClusters, allTaggedClusters)
    if (print)
    println("Results CEAFe: " + CEAFe)

    /*
    val CEAFm = CEAFClusterStatistics(CEAFStatisticsType.MENTION_BASED).scoreExamples(allRealClusters, allTaggedClusters)
    println("Results CEAFm: " + CEAFm)
    */

    val conll2012Official = CoNLL2012Score.scoreExamples(MUC._3, BCubed._3, CEAFe._3)
    if (print)
    println("Results CoNLL2012: " + conll2012Official)

    (MUC._3, BCubed._3, CEAFe._3)
  }

  def visualizeDistances(mentionExamples: Examples, title: String = "Dataset Distance distribution", filename: String = "/Users/slavkoz/temp/distr.png"): Unit = {
    var distanceDistribution = CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(mentionExamples)
    println("All mention distance pairs num: %d".format(distanceDistribution.values.sum))
    println("Mention distance distribution all: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))


    //R input:
    var all = distanceDistribution.map(_._2).sum
    distanceDistribution = distanceDistribution.filter(v => v._1 < 80)
    var removed = all-distanceDistribution.map(_._2).sum
    println("all: %d, removed: %d, percent: %.2f".format(all, removed, removed*1.0/all))

    val distanceDistributionSorted = SortedMap[Int, Int]() ++ distanceDistribution
    println(distanceDistributionSorted.keys.mkString("keys <- c(", ",", ")"))
    println(distanceDistributionSorted.values.mkString("values <- c(", ",", ")"))
    //println(distanceDistribution.flatMap(v => List.fill(v._2)(v._1)).mkString("domain <- c(", ",", ")"))


    println("Mention distance distribution removed: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))
  }

  def transformAndEval(evaluationName: String, featureFunctionSet: ArrayList[FeatureFunction], examples: Examples, examplesTest: Examples): Unit = {
    val mentionExamples = new MentionExamplesBuilder(
      examples,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    println("TRAIN MENTION DATA:")
    mentionExamples.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)
    val mentionExamplesTest = new MentionExamplesBuilder(
      examplesTest,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    println("TEST MENTION DATA:")
    mentionExamplesTest.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)


    //visualizeDistances(mentionExamples, title = "Consecutive Mentions Distance Distribution", filename = "/Users/slavkoz/temp/trainDistr.png")

    //Do whole runners
    println("*******")
    println("Doing runners package \"%s\"".format(evaluationName))
    println("*******")

    /*
    Evaluation for paper - SkipMention chains
     */
    /*
    val distribution = CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(mentionExamples).keySet.toList.sorted
    println("Distance: %d".format(distribution.take(1).get(0).toInt))
    val minSkipMentions = 25//1//20
    val maxSkipMentions = 25//1//math.min(30, distribution.size)//minSkipMentions //distribution.size
    for (idx <- minSkipMentions to maxSkipMentions) {
      val classifier = new CorefMultipleLearner(
        mentionExamples,
        featureFunctions = featureFunctionSet,
        skipNumbers = distribution.take(idx).toArray,
        modelSaveFilename = evaluationName
      ).train()

      println(idx)
      //clusterResults(classifier, mentionExamples)
      clusterResults(classifier, mentionExamplesTest, examplesTest)
    }
    */

    /*
    val classifier = new CorefPairwiseLearner(
      mentionExamples,
      featureFunctions = featureFunctionSet,
      modelSaveFilename = evaluationName
    ).train()
    */

    /*
    val (mucMean, bCubedMean, ceafMean) = clusterResults(classifier, mentionExamplesTest)
    val n = 30
    val mucMeans = new Array[Double](n)
    val bCubedMeans = new Array[Double](n)
    val ceafMeans = new Array[Double](n)
    for (i <- 0 until n) {
     val (sampledExamplesTest, _) = RandomSampler.sample(mentionExamplesTest, trainPercent = 0.8)
     val result = clusterResults(classifier, sampledExamplesTest, false)
     mucMeans(i) = result._1
     bCubedMeans(i) = result._2
     ceafMeans(i) = result._3
    }
    println("MUC test:")
    println(new TTest(mucMean, mucMeans, 0.05).toDetailString(), 2.093)
    println("BCubed test:")
    println(new TTest(bCubedMean, bCubedMeans, 0.05).toDetailString(), 2.093)
    println("CEAFe test:")
    println(new TTest(ceafMean, ceafMeans, 0.05).toDetailString(), 2.093)
    */

    //Check if IOBIE is working (=results should be 100%)
    for (length <- 100 to 100) {
      println("Doing length: " + length)
      val classifierExactMatchLen = new CorefPairwiseLearnerExactMatch(maxPairwiseDistance = length, learnLabelType = Label.COREF).train()
      clusterResults(classifierExactMatchLen, mentionExamplesTest, examplesTest)
    }
  }

  def evaluate(evaluationName: String, dataProvider: Data, featureFunctionSet: ArrayList[FeatureFunction]): Unit = {
          val examples = dataProvider.importTrainData()
          println("TRAIN RAW DATA: ")
          examples.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.PARSE_NODE, Label.REL, Label.COREF, Label.LEMMA), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true, ommitedMentionAttributes = Array(Label.OBS, Label.COREF, Label.EXTENT))
          val examplesTest = dataProvider.importTestData()// an ACE2004 document: .getDocumentExamples("NYT20001128.1940.0266")
          println("TEST RAW DATA: ")
          examplesTest.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.PARSE_NODE, Label.REL, Label.COREF, Label.LEMMA), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true, ommitedMentionAttributes = Array(Label.OBS, Label.COREF, Label.EXTENT))

          transformAndEval(evaluationName, featureFunctionSet, examples, examplesTest)
  }

  def main(args: Array[String]): Unit = {
    /*
    //SemEval 2010
    */
    evaluate ("SemEval2010", SemEvalData, FeatureFunctionPackages.bestSemEval2010CorefFeatureFunctions)

    //CoNLL 2012
    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS)
    //evaluate ("CoNLL2012 BROADCAST_NEWS", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_CONVERSATION)
    //evaluate ("CoNLL2012 BROADCAST_CONVERSATION", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.MAGAZINE)
    //evaluate ("CoNLL2012 MAGAZINE", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.NEWSWIRE)
    //evaluate ("CoNLL2012 NEWSWIRE", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.PIVOT_CORPUS)
    //evaluate ("CoNLL2012 PIVOT_CORPUS", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.TELEPHONE_CONVERSATION)
    //evaluate ("CoNLL2012 TELEPHONE_CONVERSATION", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.WEB_TEXT)
    //evaluate ("CoNLL2012 WEB_TEXT", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    CoNLL2012Data.sources = CoNLL2012ImporterSourceTypeEnum.values.toArray
    evaluate ("CoNLL2012 ALL_TOGETHER", CoNLL2012Data, FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions)

    //ACE2004

    //ACE2004Data.sources = Array(ACE2004DocumentType.ARABIC_TREEBANK)
    //ACE2004Data.reload()
    //evaluate ("ACE2004 ARABIC_TREEBANK", ACE2004Data, FeatureFunctionPackages.bestACE2004CorefFeatureFunctions)

    //ACE2004Data.sources = Array(ACE2004DocumentType.BROADCAST_NEWS)
    //ACE2004Data.reload()
    //evaluate ("ACE2004 BROADCAST_NEWS", ACE2004Data, FeatureFunctionPackages.bestACE2004CorefFeatureFunctions)

    //ACE2004Data.sources = Array(ACE2004DocumentType.CHINESE_TREEBANK)
    //ACE2004Data.reload()
    //evaluate ("ACE2004 CHINESE_TREEBANK", ACE2004Data, FeatureFunctionPackages.bestACE2004CorefFeatureFunctions)

    //ACE2004Data.sources = Array(ACE2004DocumentType.FISHER_TRANSCRIPTS)
    //ACE2004Data.reload()
    //evaluate ("ACE2004 FISHER_TRANSCRIPTS", ACE2004Data, FeatureFunctionPackages.bestACE2004CorefFeatureFunctions)

    //ACE2004Data.sources = Array(ACE2004DocumentType.NEWSWIRE)
    //ACE2004Data.reload()
    //evaluate ("ACE2004 NEWSWIRE", ACE2004Data, FeatureFunctionPackages.bestACE2004CorefFeatureFunctions)

    //ACE2004Data.sources = ACE2004DocumentType.values.toArray
    ACE2004Data.reloadCullota()
    evaluate ("ACE2004 ALL_TOGETHER", ACE2004Data, FeatureFunctionPackages.bestACE2004CorefFeatureFunctions)


    //SIMPLE TEST:
    //best results on train domain from skipNumbers = 0 .. 50
    /*
    val classifier = new CorefMultipleLearner(
      mentionExamples,
      featureFunctions = FeatureFunctionPackages.standardCorefFFunctions,
      skipNumbers = (0 to 50).toArray
    ).trainAndTest(1000, 1000, mentionExamplesTest)
    */
    //Baselines:
    //val classifierS = new CorefSingletonClassifier(Label.COREF)
    //clusterResults(classifierS, mentionExamplesTest)
    //val classifierAiO = new CorefAllInOneClassifier(Label.COREF)
    //clusterResults(classifierAiO, mentionExamplesTest)
    /*
    //Separate classifier test
    val learnExamples = new Examples()
    mentionExamples.foreach(v => learnExamples.add(MentionExamplesToCorefExamplesTransformer.toCorefExamples(v, 3)))
    val classifier = new CRFSuiteLCCRFLearner(learnExamples, Label.COREF, FeatureFunctionPackages.standardCorefFFunctions).train(1000)
    new Statistics(classifier, learnExamples).printStandardClassification(Label.COREF, "C")
    */
    /*
    //Manual classifier - just to test feature function two consecutive Label.OBS match
    val manualClassifier = new Classifier {
      def classify(example: Example, normalized: Boolean): (ArrayList[String], Double) = {
        val retVal = new util.ArrayList[String]()

        for (i <- 0 until example.size()) {
          if (i>0 &&example.get(i, Label.OBS).equals(example.get(i-1, Label.OBS))) {
            retVal.add("C")
          } else {
            retVal.add("O")
          }
        }

        (retVal, 0.)
      }

      def test(domain: Examples) {}
    }
    new Statistics(manualClassifier, learnExamples).printStandardClassification(Label.COREF, "C")
    */
    //VISUALIZE COREFERENCE DISTANCES
    //visualizeDistances(SemEvalData.importTrainData(), title = "Consecutive Mentions Distance Distribution", filename = "/Users/slavkoz/temp/trainDistr.png")
    //visualizeDistances(mentionExamplesTest, title = dataProvider.getClass.getSimpleName + " - Test Dataset Distance distribution", filename = "/Users/slavkoz/temp/testDistr.png")
    /*
    //SHOW AN EXAMPLE
    val exampleId = 0
    //println(mentionExamples.get(exampleId).map(v => v.get(Label.COREF).toString+" "+v.get(Label.OBS)).zipWithIndex.mkString("\n"))
    examples.printMentionDetails(mentionExamples.get(exampleId).get(ExampleLabel.DOC_ID), ommitSingles = true)
    examples.printDocument(mentionExamples.get(exampleId).get(ExampleLabel.DOC_ID))
    println(allRealClusters(exampleId).mkString(" ||| "))
    println()
    println(allTaggedClusters(exampleId).mkString(" ||| "))
    */


    //training pairs
    /*
    def trainABN = {
      ACE2004Data.sources = Array(ACE2004DocumentType.BROADCAST_NEWS)
      ACE2004Data.reload()
      ACE2004Data.importTrainData()
    }
    def trainANW = {
      ACE2004Data.sources = Array(ACE2004DocumentType.NEWSWIRE)
      ACE2004Data.reload()
      ACE2004Data.importTrainData()
    }
    def testABN = {
      ACE2004Data.sources = Array(ACE2004DocumentType.BROADCAST_NEWS)
      ACE2004Data.reload()
      ACE2004Data.importTestData()
    }
    def testANW = {
      ACE2004Data.sources = Array(ACE2004DocumentType.NEWSWIRE)
      ACE2004Data.reload()
      ACE2004Data.importTestData()
    }
    def trainCBN = {
      CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS)
      CoNLL2012Data.importTrainData()
    }
    def trainCNW = {
      CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.NEWSWIRE)
      CoNLL2012Data.importTrainData()
    }
    def testCBN = {
      CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS)
      CoNLL2012Data.importTestData()
    }
    def testCNW = {
      CoNLL2012Data.sources = Array(CoNLL2012ImporterSourceTypeEnum.NEWSWIRE)
      CoNLL2012Data.importTestData()
    }
    def trainSE = {
      SemEvalData.importTrainData()
    }
    def testSE = {
      SemEvalData.importTrainData()
    }

    println("Doing ABN on ANW")
    transformAndEval("ABN", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainABN, testANW)
    println("Doing ABN on CBN")
    transformAndEval("ABN", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainABN, testCBN)
    println("Doing ABN on CNW")
    transformAndEval("ABN", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainABN, testCNW)
    println("Doing ABN on SE")
    transformAndEval("ABN", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainABN, testSE)
    println("Doing ANW on ABN")
    transformAndEval("ANW", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainANW, testABN)
    println("Doing ANW on CBN")
    transformAndEval("ANW", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainANW, testCBN)
    println("Doing ANW on CNW")
    transformAndEval("ANW", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainANW, testCNW)
    println("Doing ANW on SE")
    transformAndEval("ANW", FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, trainANW, testSE)
    println("Doing CBN on ABN")
    transformAndEval("CBN", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCBN, testABN)
    println("Doing CBN on ANW")
    transformAndEval("CBN", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCBN, testANW)
    println("Doing CBN on CNW")
    transformAndEval("CBN", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCBN, testCNW)
    println("Doing CBN on SE")
    transformAndEval("CBN", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCBN, testSE)
    println("Doing CNW on ABN")
    transformAndEval("CNW", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCNW, testABN)
    println("Doing CNW on ANW")
    transformAndEval("CNW", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCNW, testANW)
    println("Doing CNW on CBN")
    transformAndEval("CNW", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCNW, testCBN)
    println("Doing CNW on SE")
    transformAndEval("CNW", FeatureFunctionPackages.bestCoNLL2012CorefFeatureFunctions, trainCNW, testSE)
    println("Doing SE on ABN")
    transformAndEval("SE", FeatureFunctionPackages.bestSemEval2010CorefFeatureFunctions, trainSE, testABN)
    println("Doing SE on ANW")
    transformAndEval("SE", FeatureFunctionPackages.bestSemEval2010CorefFeatureFunctions, trainSE, testANW)
    println("Doing SE on CBN")
    transformAndEval("SE", FeatureFunctionPackages.bestSemEval2010CorefFeatureFunctions, trainSE, testCBN)
    println("Doing SE on CNW")
    transformAndEval("SE", FeatureFunctionPackages.bestSemEval2010CorefFeatureFunctions, trainSE, testCNW)    */
  }

}

trait Data {
  def importTrainData(): Examples
  def importTestData(): Examples
}

object SemEvalData extends Data {
  def importTrainData() = {
    val filePath = IOBIEPropertiesUtil.getProperty(IOBIEProperties.SEMEVAL2010_PATH)
    val semEval2010Importer = new SemEval2010Importer(filePath, SemEvalDataType.TRAIN_EN.toString)
    val examples = semEval2010Importer.importForIE()
    new ParseTagger().tag(examples)
    examples
  }

  def importTestData() = {
    val filePath = IOBIEPropertiesUtil.getProperty(IOBIEProperties.SEMEVAL2010_PATH)
    val semEval2010Importer = new SemEval2010Importer(filePath, SemEvalDataType.TEST_EN.toString)
    val examples = semEval2010Importer.importForIE()
    new ParseTagger().tag(examples)
    examples
  }
}

object ACE2004Data extends Data {
  private val datasetPath: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.ACE2004_PATH)

  //private val documentTypes = Array(ACE2004DocumentType.ARABIC_TREEBANK)
  //private val documentTypes = Array(ACE2004DocumentType.BROADCAST_NEWS)
  //private val documentTypes = Array(ACE2004DocumentType.CHINESE_TREEBANK)
  //private val documentTypes = Array(ACE2004DocumentType.FISHER_TRANSCRIPTS)
  //private val documentTypes = Array(ACE2004DocumentType.NEWSWIRE)
  var sources = ACE2004DocumentType.values.toArray
  private var data: (Examples, Examples) = null

  def reload(): Unit = {
    val rawData = new ACE2004Importer(datasetPath, documentTypes = sources).importForIE()
    new PoSTagger().tag(rawData)
    new LemmaTagger().tag(rawData)
    new ParseTagger().tag(rawData)

    data = RandomSampler.sample(rawData, randomSeed = 42)
  }

  def reloadCullota(): Unit = {
    val rawData = new ACE2004Importer(datasetPath, documentTypes = sources).importForIE()
    new PoSTagger().tag(rawData)
    new LemmaTagger().tag(rawData)
    new ParseTagger().tag(rawData)

    //Cullota split:
    data = ExactSampler.sampleNumber(rawData, 336)
  }



  def importTrainData() = data._1
  def importTestData() = data._2
}

object CoNLL2012Data extends Data {
  private val path = IOBIEPropertiesUtil.getProperty(IOBIEProperties.CONLL2012_PATH)

  /*
  CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS,
  CoNLL2012ImporterSourceTypeEnum.BROADCAST_CONVERSATION,
  CoNLL2012ImporterSourceTypeEnum.MAGAZINE,
  CoNLL2012ImporterSourceTypeEnum.NEWSWIRE,
  CoNLL2012ImporterSourceTypeEnum.PIVOT_CORPUS,
  CoNLL2012ImporterSourceTypeEnum.TELEPHONE_CONVERSATION,
  CoNLL2012ImporterSourceTypeEnum.WEB_TEXT
  */
  var sources = CoNLL2012ImporterSourceTypeEnum.values.toArray

  def importTrainData() = {
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = CoNLL2012ImporterDatasetTypeEnum.TRAIN,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = sources,
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    val examples = conll2012Importer.importForIE()
    examples
  }
  def importTestData() = {
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = CoNLL2012ImporterDatasetTypeEnum.TEST,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = sources,
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    val examples = conll2012Importer.importForIE()
    examples
  }


}
