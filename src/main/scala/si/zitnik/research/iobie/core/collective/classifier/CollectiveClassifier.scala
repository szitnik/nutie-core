package si.zitnik.research.iobie.core.collective.classifier

import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}

import collection.mutable._
import si.zitnik.research.iobie.core.coreference.classifier.impl.CorefMultipleClassifier
import si.zitnik.research.iobie.core.collective.util.CollectiveUtil

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.core.ner.mention.classifier.impl.NERMentionClassifier
import si.zitnik.research.iobie.core.relationship.classifier.impl.RelationshipMultipleClassifier
import si.zitnik.research.iobie.domain.relationship.Relationship
import si.zitnik.research.iobie.algorithms.crf.feature.packages.JointIterativeFeatureFunctionPackage
import si.zitnik.research.iobie.algorithms.crf.stat.FMeasure
import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer
import si.zitnik.research.iobie.statistics.cluster._

import scala.collection.JavaConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/23/12
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */

class CollectiveClassifier(
                            numOfIters: Int,
                            neClassifiers: Array[NERMentionClassifier],
                            relClassifiers: Array[RelationshipMultipleClassifier],
                            corefClassifiers: Array[CorefMultipleClassifier]) extends StrictLogging {

  init()
  def init(): Unit = {
    if (neClassifiers.length != numOfIters || relClassifiers.length != numOfIters || corefClassifiers.length != numOfIters) {
      throw new Exception("Number od classifiers does not match the number of iterations")
    }
  }

  def classify(examples: Examples): (ArrayBuffer[ArrayBuffer[String]], ArrayBuffer[HashSet[Relationship]], ArrayBuffer[HashSet[Cluster]]) = {

    var curNeLabels: ArrayBuffer[ArrayBuffer[String]] = null
    var curRelLabels: ArrayBuffer[HashSet[Relationship]] = null
    var curCorefClusters: ArrayBuffer[HashSet[Cluster]] = null

    val mentionExamples = new MentionExamplesBuilder(examples, null, ExampleLabel.DOC_ID).buildFromTagged()

    for (i <- 0 until numOfIters) {
      logger.info("Inference Iteration %d".format(i))

      curNeLabels = neClassifiers(i).classify(examples)._1
      curRelLabels = relClassifiers(i).classifyRelationships(examples)
      curCorefClusters = corefClassifiers(i).classifyClusters(mentionExamples)

      if (i+1 < numOfIters) { //is not the last step
        CollectiveUtil.updateIntermediateNERLabelings(examples, i, curNeLabels)
        JointIterativeFeatureFunctionPackage.reindex(curRelLabels, curCorefClusters)
      }

      doEvaluation(examples, mentionExamples, curNeLabels, curRelLabels, curCorefClusters, neClassifiers(i), relClassifiers(i), corefClassifiers(i))

    }
    logger.info("Inference finished after %d iterations.".format(numOfIters))

    (curNeLabels, curRelLabels, curCorefClusters)
  }

  def doEvaluation(
                    examples: Examples,
                    mentionExamples: Examples,
                    neLabels: ArrayBuffer[ArrayBuffer[String]],
                    relLabels: ArrayBuffer[HashSet[Relationship]],
                    corefClusters: ArrayBuffer[HashSet[Cluster]],
                    neClassifier: NERMentionClassifier,
                    relClassifier: RelationshipMultipleClassifier,
                    corefClassifier: CorefMultipleClassifier): Unit = {

    //NE
    println("NE scores:")
    //new Statistics(neClassifier, examples).printAllStat(Label.NE)

    val fScore = new FMeasure(neClassifier, examples, Label.NE)
    println("\tF scores:")
    println("\tMAF: %.2f, MIF: %.2f, Avg. Precision: %.2f, Avg. Recall: %.2f".format(fScore.macroAveragedF(), fScore.microAveragedF(), fScore.averagePrecision(), fScore.averageRecall()))
    val neResult = evaluateNer(examples, neLabels)
    println("\tClassification accuracy: "+neResult._1+", Error count: "+neResult._2)
    //TODO: change to BIO notation
    /*val mucStat = new MUCStatistics(neClassifier, examples, Label.NE)
    println("MUC6 scores:")
    println("\tF-score: %.2f, Precision: %.2f, Recall: %.2f".format(mucStat.fscore(), mucStat.precision(), mucStat.recall()))*/

    //REL
    println("REL scores:")
    println("\tNumber of extracted relationships: "+relLabels.map(_.size).sum)
    println("\tNumber of real relationships: "+examples.map(_.getAllRelationships().size).sum)
    val relResult = evaluateRelationships(examples, relLabels)
    println("\tClassification accuracy: "+relResult._1+", Error count: "+relResult._2)


    //COREF
    println("COREF scores:")
    val allRealClusters = TaggedClusterer.doClustering(mentionExamples, ommitSingles = false)
    val allTaggedClusters = corefClusters
    val F = new ClusterStatistics().stat(allRealClusters, allTaggedClusters)
    println("\tResults Pairwise: " + F)
    val FMUC = MUCClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    println("\tResults MUC:" + FMUC)
    val BCubed = BCubedClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    println("\tResults BCubed: " + BCubed)
    val CEAFe = CEAFClusterStatistics(CEAFStatisticsType.ENTITY_BASED).scoreExamples(allRealClusters, allTaggedClusters)
    println("\tResults CEAFe: " + CEAFe)
  }

  def evaluateNer(examples: Examples, neLabels: ArrayBuffer[ArrayBuffer[String]]) = {


    val allRealNum = examples.map(_.size).sum
    var extractedNum = 0
    var errorCount = 0

    //calculate scores
    for ((example, extractedNes) <- examples zip neLabels) {
      for ((token, label) <- example zip extractedNes) {
        if (token.get(Label.NE).asInstanceOf[String].equals(label)) {
          extractedNum +=1
        } else {
          errorCount += 1
        }
      }
    }

    (extractedNum * 1.0 / allRealNum, errorCount)
  }


  def evaluateRelationships(examples: Examples, relLabels: ArrayBuffer[HashSet[Relationship]]) = {


    val allRealNum = examples.map(_.getAllRelationships().size).sum
    var extractedNum = 0
    var errorCount = 0

    //calculate scores
    for ((example, extractedRels) <- examples zip relLabels) {
      for (extractedRel <- extractedRels) {
        example.getRelationship(extractedRel.subj, extractedRel.obj) match {
          case Some(exampleRel) => {
            if (exampleRel.relationshipName.equals(extractedRel.relationshipName)) {
              extractedNum += 1
            } else {
              errorCount += 1
            }
          }
          case None => {errorCount += 1}
        }
      }
    }

    (extractedNum * 1.0 / allRealNum, errorCount)
  }
}
