package si.zitnik.research.iobie.core.relationship.test

import si.zitnik.research.iobie.datasets.bionlp2013.{BioNLP2013DatasetTypes, BioNLP2013Exporter, BioNLPImporter}
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}
import si.zitnik.research.iobie.domain.{Example, Examples}

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.core.relationship.analysis.RelationshipAnalysis
import si.zitnik.research.iobie.algorithms.crf.feature.packages.FeatureFunctionPackages
import si.zitnik.research.iobie.core.relationship.learner.RelationshipMultipleLearner

import collection.SortedMap
import java.util
import util.Collections

import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.domain.relationship.Relationship
import si.zitnik.research.iobie.thirdparty.opennlp.api.{ParseTagger, PoSTagger}
import java.io.File

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.thirdparty.biolemmatizer.api.BioLemmaTagger


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/6/13
 * Time: 12:58 PM
 *
 * BioNLP 2013 GRN task implementation
 */
object BioNLP2013Relationship extends StrictLogging {
  val minSkipMentions = 0
  val maxSkipMentions = 10
  val tokenThreshold = 0.0
  val sequenceThreshold = 0.0

  def importTrain() = {
    val allTrainExamples = new Examples()

    val devExamples = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.development).importForIE()
    allTrainExamples.addAll(devExamples)
    //devExamples.printStatistics(ommitMentions = false)
    /*
    devExamples.printStatistics(ommitMentions = false)
    println("#realMentions: %d".format(devExamples.flatMap(_.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("MENTION"))).size))
    println("#actionMentions: %d".format(devExamples.flatMap(_.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION"))).size))
    println("#events: %d".format(devExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).size))
    println("#relationship: %d".format(devExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP"))).size))
    println("#Interaction.* relationship: %d".format(devExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP")).filter(_.relationshipName.startsWith("Interaction."))).size))
    */
    val trainExamples = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.train).importForIE()
    allTrainExamples.addAll(trainExamples)
    //trainExamples.printStatistics(ommitMentions = false)
    /*
    println("#realMentions: %d".format(trainExamples.flatMap(_.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("MENTION"))).size))
    println("#actionMentions: %d".format(trainExamples.flatMap(_.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION"))).size))
    println("#events: %d".format(trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).size))
    println("#relationship: %d".format(trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP"))).size))
    println("#Interaction.* relationship: %d".format(trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP")).filter(_.relationshipName.startsWith("Interaction."))).size))
    */




    //allTrainExamples.printStatistics(ommitMentions = false)
    allTrainExamples
  }

  def importTest() = {
    val testExamples = new Examples()

    val devExamples = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.developmentAsTest).importForIE()
    testExamples.addAll(devExamples)

    val trainExamples = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.trainAsTest).importForIE()
    testExamples.addAll(trainExamples)

    //val testExamples1 = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.test).importForIE()
    //testExamples.addAll(testExamples1)
    //testExamples.printStatistics(ommitMentions = false)

    testExamples
  }

  def visualizeDistances(examples: Examples, title: String = "Train Dataset Distance distribution", filename: String = "/Users/slavkoz/temp/relationDistr.png"): Unit = {
    var distanceDistribution = RelationshipAnalysis.getRelationshipAttributeMentionsDistanceDistributionBioNLP(
      examples,
      valueTypeToProcess = Some(Array("RELATIONSHIP")))
    println("All mention distance pairs num: %d".format(distanceDistribution.values.sum))
    println("Mention distance distribution all: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))

    distanceDistribution.remove(-1)


    //R input:
    //var all = distanceDistribution.map(_._2).sum
    //println("all: %d".format(all))
    //println(distanceDistribution.flatMap(v => List.fill(v._2)(v._1)).mkString("domain <- c(", ",", ")"))
    val outDistr = (SortedMap[Int, Int]() ++ distanceDistribution).map(v => (v._1, v._2)).toList.sortBy(_._1)
    println("Distance distribution keys: " + outDistr.map(_._1).mkString(","))
    println("Distance distribution values: " + outDistr.map(_._2).mkString(","))


    println("Mention distance distribution removed: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))
  }

  def printlnForR(map: SortedMap[Int, Int]): Unit = {
    println(map)
    println("keys <- " + map.map(_._1).mkString("c(",",",")"))
    println("values <- " + map.map(_._2).mkString("c(",",",")"))
  }

  def doSomeAnalytics(trainExamples: Examples, testExamples: Examples): Unit = {
    //Check EVENT attribute types:
    /*val subjs = trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).map(_.get(Label.SUBJECT_TYPE)).toSet
    val objs = trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).map(_.get(Label.OBJECT_TYPE)).toSet
    println(subjs)
    println(objs)
    println("Subject types: ")
    for (subj <- subjs) {
      val vals = trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).
        filter(_.get(Label.SUBJECT_TYPE).equals(subj)).map(_.subj.get(Label.ENTITY_TYPE))
      val valsC = vals.groupBy(identity).map(x => (x._1, x._2.size))
      println("\tValues for %s: %s".format(subj, valsC))
    }
    println("Object types: ")
    for (obj <- objs) {
      val vals = trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).
        filter(_.get(Label.OBJECT_TYPE).equals(obj)).map(_.obj.get(Label.OBS).asInstanceOf[String].toLowerCase())
      val valsC = vals.groupBy(identity).map(x => (x._1, x._2.size))
      println("\tValues for %s: %s".format(obj, valsC))
    }       */


    //test hierarchical relationships
    /*trainExamples.flatMap(_.getAllRelationships().filter(!_.relationshipName.equalsIgnoreCase("negation")).filter(r => {
      "RELATIONSHIP".equals(r.subj.get(Label.ATTRIBUTE_TYPE)) ||
      "RELATIONSHIP".equals(r.obj.get(Label.ATTRIBUTE_TYPE))
    })).foreach(r => {
      println("Main: %s\n\tSubj:%s\n\tObj: %s".format(r, r.subj.get(Label.VALUE), r.obj.get(Label.VALUE)))
    })*/

    //test relationship attribute types
    /*println(trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP"))).
      filter(_.relationshipName.startsWith("Interaction")).
      map(_.get(Label.SUBJECT_TYPE)).toSet)
    println(trainExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP"))).
      filter(_.relationshipName.startsWith("Interaction")).
      map(_.get(Label.OBJECT_TYPE)).toSet)*/

    /*
    println("Relationships: \n\t%s".format(
      testExamples.flatMap(_.getAllRelationships().filter(_.relationshipName.startsWith("Interaction"))).mkString(", ")))

    println("No. of relationships: %d".format(
      testExamples.flatMap(_.getAllRelationships().filter(_.relationshipName.startsWith("Interaction"))).size))
    */
    println()

  }

  def main(args: Array[String]): Unit = {
    //delete all existing trained models
    for(file <- new File(IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_MODEL_FOLDER)).listFiles()) {
      file.delete()
    }

    evaluateStandard()
    //evaluateLeaveOneOut()
  }

  def evaluateLeaveOneOut(): Unit = {
    val trainDocIDs = new BioNLPImporter(IOBIEPropertiesUtil.getProperty(IOBIEProperties.BIONLP2013_PATH), BioNLP2013DatasetTypes.train).importForIE().
        map(_.get(ExampleLabel.DOC_ID).asInstanceOf[String]).toList.sorted
    //val trainDocIDs = importTest().map(_.get(ExampleLabel.DOC_ID).asInstanceOf[String]).toList.sorted
    println("Number of examples: %d".format(trainDocIDs.size))

    var processCounter = 1
    for (docId <- trainDocIDs) {
      //remove models
      for(file <- new File(IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_MODEL_FOLDER)).listFiles()) {
        file.delete()
      }

      println("Doing leave one out %d/%d.".format(processCounter, trainDocIDs.size))
      //setup
      val trainExamples = importTrain()
      val testExamples = importTest().getDocumentExamples(docId)

      //remove from train
      if (testExamples.size() != 1) {
        logger.error("Problem with testExamples!")
        System.exit(-1)
      }
      if (!trainExamples.removeDocumentExamples(docId)) {
        logger.error("Example not found!")
        System.exit(-1)
      }
      processSieves(trainExamples, testExamples)
      processCounter += 1
    }

  }

  def evaluateStandard(): Unit = {
    val trainExamples = importTrain()
    val testExamples = importTest()

    processSieves(trainExamples, testExamples)
  }


  def processSieves(trainExamples: Examples, testExamples: Examples): Unit = {
    //visualize distances //COMMENT TO RUN FOR REAL!!!!
    /*
    trainExamples.foreach(e => {
      //add also events as mentions
      val mentions = new ArrayBuffer[Constituent]()
      mentions.addAll(e.getAllMentions())
      //mentions.addAll(eventsToMentions(e))
      Collections.sort(mentions)
      e.setMentions(mentions)
    })
    visualizeDistances(trainExamples)
    println(trainExamples.map(e => (e.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP")).size, e.get(ExampleLabel.DOC_ID))).sortBy(_._1).reverse)
    System.exit(0)      */
    //doSomeAnalytics(trainExamples, testExamples)

    //Preprocess domain
    val taggers = Array(
      new PoSTagger(),
      new BioLemmaTagger(),
      new ParseTagger(escapeBrackets = true)
    )
    taggers.foreach(t => {
      t.tag(trainExamples)
      t.tag(testExamples)
    })



    //0. print some statistics
    //OPTIONAL: add events as mentions
    /*
    trainExamples.foreach(e => {
      val mentions = new ArrayBuffer[Constituent]()
      mentions.addAll(e.getAllMentions())
      mentions.addAll(eventsToMentions(e))
      e.setMentions(mentions)
    })*/
    /*
    //OPTIONAL: leave only B.S. genes as relation attributes
    trainExamples.foreach(e => {
      val relationships = e.getAllRelationships()
      val newRelationships = new ArrayBuffer[Relationship]()
      relationships.foreach(r => {
        if (r.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP") && BioNLP2013Rules.isBacillusSubtilisGene(r.subj) && BioNLP2013Rules.isBacillusSubtilisGene(r.obj)) {
          newRelationships.append(r)
        }
      })
      e.setRelationships(newRelationships)
    })
    */

    /*
    printlnForR(SortedMap[Int, Int]() ++ RelationshipAnalysis.getRelationshipAttributeMentionsDistanceDistribution(trainExamples))
    printlnForR(SortedMap[Int, Int]() ++ RelationshipAnalysis.getRelationshipAttributeMentionsDistanceDistribution(
      trainExamples,
      valueTypeToProcess = Some(Array("RELATIONSHIP"))
    ))
    printlnForR(SortedMap[Int, Int]() ++ RelationshipAnalysis.getRelationshipAttributeMentionsDistanceDistribution(
      trainExamples,
      valueTypeToProcess = Some(Array("EVENT"))
    ))
    printlnForR(SortedMap[Int, Int]() ++ RelationshipAnalysis.getRelationshipAttributeMentionsDistanceDistribution(
      trainExamples,
      valueTypeToProcess = Some(Array("NEGATION"))
    ))
    val relationShipsHavingRelationshipAsAnAttribute = trainExamples.flatMap(_.getAllRelationships().filter(r => {
      r.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP") && (
        r.subj.get(Label.VALUE) != null && r.subj.get(Label.VALUE).asInstanceOf[Relationship].get(Label.ID).asInstanceOf[String].startsWith("R") ||
          r.obj.get(Label.VALUE) != null && r.obj.get(Label.VALUE).asInstanceOf[Relationship].get(Label.ID).asInstanceOf[String].startsWith("R")
        )
    }))
    println(relationShipsHavingRelationshipAsAnAttribute)
    println(relationShipsHavingRelationshipAsAnAttribute.size)
    println(trainExamples.flatMap(_.getAllRelationships().filter(r => {
      r.get(Label.ATTRIBUTE_TYPE).equals("NEGATION")
    })).size)
    */


    //1. detect new mentions
    detectNewMentions(trainExamples, testExamples)

    /*println("#realMentions: %d".format(testExamples.flatMap(_.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("MENTION"))).size))
    println("#actionMentions: %d".format(testExamples.flatMap(_.getAllMentions().filter(_.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION"))).size))
    println("#events: %d".format(testExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))).size))
    println("#relationship: %d".format(testExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP"))).size))
    println("#Interaction.* relationship: %d".format(testExamples.flatMap(_.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP")).filter(_.relationshipName.startsWith("Interaction."))).size))*/


    //2a. train event detection (only mentions are attributes)
    //2b. detect events
    trainAndDetectEvents(trainExamples, testExamples)

    //3a. train full mention relationships (only mentions are attributes)
    //3b. detect full mention relationships (only mentions are attributes)
    trainAndDetectFullMentionRelationships(trainExamples, testExamples)

    //4a. train full relationships (attribute can be also relationship/event, i.e. one of them must be = both must not be mentions = already processed in 3a)
    //4b. detect full relationships (attribute can be relationship/event)
    trainAndDetectFullRelationships(trainExamples, testExamples)

    //5a. train full mention relationships (only mentions are attributes)
    //5b. detect full mention relationships (only mentions are attributes)
    trainAndDetectFullGeneMentionRelationships(trainExamples, testExamples)

    //5c. train full mention relationships from events (only mentions are attributes)
    //5d. detect full mention relationships from events (only mentions are attributes)
    trainAndDetectFullGeneMentionRelationshipsFromEvents(trainExamples, testExamples)

    //6. rule based detection
    //BioNLP2013Rules.enrichByStaticRules(trainExamples)
    BioNLP2013Rules.enrichByStaticRules(testExamples)

    //7. negations
    //trainAndDetectNegations(trainExamples, testExamples)

    //8. clean redundand and impossible relations
    BioNLP2013Rules.cleanData(testExamples)

    //9. output to A2 files
    //BioNLP2013Exporter.exportA2("/Users/slavkoz/temp/bionlpTrain", trainExamples)
    BioNLP2013Exporter.exportA2("/Users/slavkoz/temp/bionlpDev", testExamples)
    //BioNLP2013Exporter.exportA2("/Users/slavkoz/temp/bionlpTest", testExamples)
    //BioNLP2013Exporter.exportA2("/Users/slavkoz/temp/bionlpAll", testExamples)
    //doSomeAnalytics(trainExamples, testExamples)

    //10. manually delete lines from A2 that GRN.py schema validation goes through.

    //11. submit A2 files.
  }





  def detectNewMentions(trainExamples: Examples, testExamples: Examples) = {
    val trainRelationMentions = trainExamples.getAllMentions().values().flatten.filter(_.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION"))
    val lemmatizedTrainRelationMentions = trainRelationMentions.map(_.getLemma()).map(_.toLowerCase).toSet

    val process = (example: Example) => {
      //get max mention id
      var nextMentionID = example.getAllMentions().map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).max + 1

      val oneGrams = example.filter(t => lemmatizedTrainRelationMentions.contains(t.get(Label.LEMMA).asInstanceOf[String].toLowerCase()))
      val biGrams = example.sliding(2).filter(tt => lemmatizedTrainRelationMentions.contains(tt.map(_.get(Label.LEMMA)).mkString(" ").toLowerCase))

      oneGrams.foreach(v => {
        val cons = new Constituent(example, example.indexOf(v), example.indexOf(v)+1)
        cons.put(Label.ATTRIBUTE_TYPE, "R_MENTION")
        cons.put(Label.ID, "T"+nextMentionID)
        cons.put(Label.ENTITY_TYPE, "Action")
        cons.put(Label.COREF, -1)
        if (!example.getAllMentions().contains(cons)) {
          example.addMention(cons, false)
          nextMentionID += 1
        }
      })

      biGrams.foreach(v => {
        val idx = example.indexOf(v(0))
        val cons = new Constituent(example, idx, idx+2)
        cons.put(Label.LEMMA, example.get(idx, Label.LEMMA)+" "+example.get(idx+1, Label.LEMMA))
        cons.put(Label.ATTRIBUTE_TYPE, "R_MENTION")
        cons.put(Label.ID, "T"+nextMentionID)
        cons.put(Label.ENTITY_TYPE, "Action")
        cons.put(Label.COREF, -1)
        if (!example.getAllMentions().contains(cons)) {
          example.addMention(cons, false)
          nextMentionID += 1
        }
      })

      example.sortMentions()
    }



    //needed to add mentions also to train to better learn our models
    for (trainExample <- trainExamples) {
      process(trainExample)
    }
    for (testExample <- testExamples) {
      process(testExample)
    }

    //test output
    val lemmatizedTestRelationMentions = testExamples.getAllMentions().values().flatten.filter(_.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION")).map(_.get(Label.LEMMA).asInstanceOf[String]).map(_.toLowerCase).toSet
    println(lemmatizedTrainRelationMentions.toList.sorted)
    println(lemmatizedTestRelationMentions.toList.sorted)
  }

  def trainAndDetectEvents(trainExamples: Examples, testExamples: Examples): Unit = {
    for (skipNumber <- maxSkipMentions to maxSkipMentions) {
      println("Skip mentions: %s".format((minSkipMentions to skipNumber).mkString(", ")))
      val learner = new RelationshipMultipleLearner(
        trainExamples,
        featureFunctions = FeatureFunctionPackages.standardRelFFPackages(),
        skipNumbers = (minSkipMentions to skipNumber).toArray,
        modelSaveFilename = "bionlp_grn_event"
      )

      //attributes: only mentions
      //relationships: only of event type
      learner.useWithSpecificMentionsAndRelationships(trainExamples.map(e => {
        (e.getAllMentions(),
          e.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("EVENT")))
      }).toBuffer)

      val classifier = learner.train()

      /*classifier.test(trainExamples.map(e => {
        (e.getAllMentions(),
          e.getAllRelationships().filter(_.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("EVENT")))
      }).toBuffer)  */

      var identifiedCounter = 0
      val taggedEvents = classifier.classifyRelationships(testExamples.map(e => e.getAllMentions()).toBuffer, tokenThreshold, sequenceThreshold)
      taggedEvents.zip(testExamples).foreach{ case (events, example) => {
        var nextEventIdx = 1
        identifiedCounter += events.size
        if (events.size > 0) {
          events.foreach(e => {
            if (!e.subj.get(Label.ENTITY_TYPE).equals(e.obj.get(Label.ENTITY_TYPE))) {
              BioNLP2013Rules.modifyEventAttributeTypes(e)

              e.example = example
              e.put(Label.ATTRIBUTE_TYPE, "EVENT")
              e.put(Label.ID, "E"+nextEventIdx)
              nextEventIdx += 1

              example.addRelationship(e)
            }
          })

        }
      }}

      println("Identified events: %d".format(identifiedCounter))
    }
  }

  def trainAndDetectFullMentionRelationships(trainExamples: Examples, testExamples: Examples): Unit = {
    for (skipNumber <- maxSkipMentions to maxSkipMentions) {
      println("Skip mentions: %s".format((minSkipMentions to skipNumber).mkString(", ")))
      val learner = new RelationshipMultipleLearner(
        trainExamples,
        featureFunctions = FeatureFunctionPackages.standardRelFFPackages(),
        skipNumbers = (minSkipMentions to skipNumber).toArray,
        modelSaveFilename = "bionlp_grn_rel"
      )

      //attributes: only mentions (this is exactly when the attribute has ID key)
      //relationships: only of event type
      learner.useWithSpecificMentionsAndRelationships(trainExamples.map(e => {
        (e.getAllMentions(),
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              r.obj.containsKey(Label.ID) &&
              r.subj.containsKey(Label.ID)
          }))
      }).toBuffer)

      val classifier = learner.train()

      /*classifier.test(trainExamples.map(e => {
        (e.getAllMentions(),
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              r.obj.containsKey(Label.ID) &&
              r.subj.containsKey(Label.ID)
          }))
      }).toBuffer)*/

      var identifiedCounter = 0
      val taggedRelationships = classifier.classifyRelationships(testExamples.map(e => e.getAllMentions()).toBuffer, tokenThreshold, sequenceThreshold)
      taggedRelationships.zip(testExamples).foreach{ case (relationships, example) => {
        var nextRelIdx = 1
        identifiedCounter += relationships.size
        if (relationships.size > 0) {

          relationships.foreach(r => {


            r.example = example
            r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
            r.put(Label.ID, "R"+nextRelIdx)
            BioNLP2013Rules.modifyRelAttributeTypes(r)
            nextRelIdx += 1
          })
          example.addRelationships(relationships)
        }
      }}

      println("Identified full mention relationships: %d".format(identifiedCounter))
    }
  }

  def trainAndDetectFullGeneMentionRelationships(trainExamples: Examples, testExamples: Examples): Unit = {
    for (skipNumber <- maxSkipMentions to maxSkipMentions) {
      println("Skip mentions: %s".format((minSkipMentions to skipNumber).mkString(", ")))
      val learner = new RelationshipMultipleLearner(
        trainExamples,
        featureFunctions = FeatureFunctionPackages.standardRelFFPackages(),
        skipNumbers = (minSkipMentions to skipNumber).toArray,
        modelSaveFilename = "bionlp_grn_genementions"
      )

      //attributes: only mentions (this is exactly when the attribute has ID key)
      //relationships: only of event type
      learner.useWithSpecificMentionsAndRelationships(trainExamples.map(e => {
        (e.getAllMentions().filter(BioNLP2013Rules.isBacillusSubtilisGene(_)),
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
            r.relationshipName.startsWith("Interaction")  &&
              BioNLP2013Rules.isBacillusSubtilisGene(r.obj) &&
                BioNLP2013Rules.isBacillusSubtilisGene(r.subj)
          }))
      }).toBuffer)

      val classifier = learner.train()

      /*classifier.test(trainExamples.map(e => {
        (e.getAllMentions(),
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              r.obj.containsKey(Label.ID) &&
              r.subj.containsKey(Label.ID)
          }))
      }).toBuffer)*/

      var identifiedCounter = 0
      val taggedRelationships = classifier.classifyRelationships(testExamples.map(e => {
        //add also events as mentions
        val mentions = new ArrayBuffer[Constituent]()
        mentions.addAll(e.getAllMentions().filter(BioNLP2013Rules.isBacillusSubtilisGene(_)))
        Collections.sort(mentions)
        mentions
      }).toBuffer, tokenThreshold, sequenceThreshold)
      taggedRelationships.zip(testExamples).foreach{ case (relationships, example) => {
        var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
        identifiedCounter += relationships.size
        if (relationships.size > 0) {

          relationships.foreach(r => {


            r.example = example
            r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
            r.put(Label.ID, "R"+nextRelIdx)
            BioNLP2013Rules.modifyRelAttributeTypes(r)
            println(example.get(ExampleLabel.DOC_ID)+ " - R" + nextRelIdx)
            nextRelIdx += 1
          })
          example.addRelationships(relationships)

        }
      }}

      println("Identified full gene mention relationships: %d".format(identifiedCounter))
    }
  }

  def trainAndDetectFullGeneMentionRelationshipsFromEvents(trainExamples: Examples, testExamples: Examples): Unit = {
    for (skipNumber <- maxSkipMentions to maxSkipMentions) {
      println("Skip mentions: %s".format((minSkipMentions to skipNumber).mkString(", ")))
      val learner = new RelationshipMultipleLearner(
        trainExamples,
        featureFunctions = FeatureFunctionPackages.standardRelFFPackages(),
        skipNumbers = (minSkipMentions to skipNumber).toArray,
        modelSaveFilename = "bionlp_grn_genementions_events"
      )

      //attributes: only mentions (this is exactly when the attribute has ID key)
      //relationships: only of event type
      learner.useWithSpecificMentionsAndRelationships(trainExamples.map(e => {
        (e.getAllMentions().filter(BioNLP2013Rules.isBacillusSubtilisGene(_)),
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
            (r.obj.get(Label.ATTRIBUTE_TYPE).equals("EVENT") || r.subj.get(Label.ATTRIBUTE_TYPE).equals("EVENT"))
          }).map(r => {
            if (!r.obj.containsKey(Label.ID)) {
              r.obj = eventToTargetMention(r.obj.get(Label.VALUE).asInstanceOf[Relationship])
            }
            if (!r.subj.containsKey(Label.ID)) {
              r.subj = eventToTargetMention(r.subj.get(Label.VALUE).asInstanceOf[Relationship])
            }
            r
          }).filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              r.relationshipName.startsWith("Interaction")  &&
              BioNLP2013Rules.isBacillusSubtilisGene(r.obj) &&
              BioNLP2013Rules.isBacillusSubtilisGene(r.subj)
          }))
      }).toBuffer)

      val classifier = learner.train()

      /*classifier.test(trainExamples.map(e => {
        (e.getAllMentions(),
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              r.obj.containsKey(Label.ID) &&
              r.subj.containsKey(Label.ID)
          }))
      }).toBuffer)*/

      var identifiedCounter = 0
      val taggedRelationships = classifier.classifyRelationships(testExamples.map(e => {
        //add also events as mentions
        val mentions = new ArrayBuffer[Constituent]()
        mentions.addAll(e.getAllMentions().filter(BioNLP2013Rules.isBacillusSubtilisGene(_)))
        Collections.sort(mentions)
        mentions
      }).toBuffer, tokenThreshold, sequenceThreshold)
      taggedRelationships.zip(testExamples).foreach{ case (relationships, example) => {
        var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
        identifiedCounter += relationships.size
        if (relationships.size > 0) {

          relationships.foreach(r => {


            r.example = example
            r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
            r.put(Label.ID, "R"+nextRelIdx)
            BioNLP2013Rules.modifyRelAttributeTypes(r)
            println(example.get(ExampleLabel.DOC_ID)+ " - R" + nextRelIdx)
            println(r)
            r.example.printLabeling(Label.OBS)
            nextRelIdx += 1
          })
          example.addRelationships(relationships)

        }
      }}

      println("Identified full gene mention relationships with events: %d".format(identifiedCounter))
    }
  }

  def trainAndDetectFullRelationships(trainExamples: Examples, testExamples: Examples): Unit = {
    for (skipNumber <- maxSkipMentions to maxSkipMentions) {
      println("Skip mentions: %s".format((minSkipMentions to skipNumber).mkString(", ")))
      val learner = new RelationshipMultipleLearner(
        trainExamples,
        featureFunctions = FeatureFunctionPackages.standardRelFFPackages(),
        skipNumbers = (minSkipMentions to skipNumber).toArray,
        modelSaveFilename = "bionlp_grn_fullrel"
      )

      //attributes:
      //relationships: relationship type
      learner.useWithSpecificMentionsAndRelationships(trainExamples.map(e => {
        //add also events as mentions
        val mentions = new ArrayBuffer[Constituent]()
        mentions.addAll(e.getAllMentions())
        mentions.addAll(eventsToMentions(e))
        Collections.sort(mentions)
        (mentions,
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              (!r.obj.containsKey(Label.ID) || !r.subj.containsKey(Label.ID))
          }))
      }).toBuffer)

      val classifier = learner.train()

      /*classifier.test(trainExamples.map(e => {
        //add also events as mentions
        val mentions = new ArrayBuffer[Constituent]()
        mentions.addAll(e.getAllMentions())
        mentions.addAll(eventsToMentions(e))
        Collections.sort(mentions)
        (mentions,
          e.getAllRelationships().filter(r => {
            r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("RELATIONSHIP") &&
              (!r.obj.containsKey(Label.ID) || !r.subj.containsKey(Label.ID))
          }))
      }).toBuffer)*/

      var identifiedCounter = 0
      val taggedRelationships = classifier.classifyRelationships(testExamples.map(e => {
        //add also events as mentions
        val mentions = new ArrayBuffer[Constituent]()
        mentions.addAll(e.getAllMentions())
        mentions.addAll(eventsToMentions(e))
        Collections.sort(mentions)
        mentions
      }).toBuffer, tokenThreshold, sequenceThreshold)
      taggedRelationships.zip(testExamples).foreach{ case (relationships, example) => {
        var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
        identifiedCounter += relationships.size
        if (relationships.size > 0) {
          relationships.foreach(r => {


            r.example = example

            r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
            r.put(Label.ID, "R"+nextRelIdx)
            BioNLP2013Rules.modifyRelAttributeTypes(r)
            nextRelIdx += 1
          })
          example.addRelationships(relationships)
        }
      }}
      println("Identified full relationships: %d".format(identifiedCounter))
    }
  }

  def trainAndDetectNegations(trainExamples: Examples, testExamples: Examples): Unit = {
    //println(trainExamples.filter(e => e.getAllRelationships().exists(_.relationshipName.equals("NEGATION"))).map(_.get(ExampleLabel.DOC_ID)).mkString(", "))

    val trainNegativeWords = Set("not", "whereas", "neither", "nor")

    /*println(testExamples.
      filter(_.getLabeling(Label.OBS).map(_.asInstanceOf[String].toLowerCase).toSet.intersect(trainNegativeWords).size > 0).
      map(_.toStringLabeling(Label.OBS)).
      mkString("\n\n")) */
  }

  def eventsToMentions(example: Example): ArrayBuffer[Constituent] = {
    val retVal = new ArrayBuffer[Constituent]()
    val events = example.getAllRelationships().filter(r => {
      r.get(Label.ATTRIBUTE_TYPE).asInstanceOf[String].equals("EVENT")
    })

    for (event <- events) {
      val subjCons = event.subj
      val objCons = event.obj
      val minIdx = math.min(subjCons.startIdx, objCons.startIdx)
      val maxIdx = math.max(subjCons.endIdx, objCons.endIdx)

      val cons = new Constituent(example, minIdx, maxIdx)
      cons.put(Label.OBS, subjCons.get(Label.OBS)+" "+objCons.get(Label.OBS))  //TODO: currently just use both of the words
      cons.put(Label.LEMMA, subjCons.get(Label.LEMMA)+" "+objCons.get(Label.LEMMA))
      cons.put(Label.ID, event.get(Label.ID))
      cons.put(Label.ENTITY_TYPE, "EVENT")
      cons.put(Label.SUBJECT_TYPE, event.get(Label.SUBJECT_TYPE))
      cons.put(Label.OBJECT_TYPE, event.get(Label.OBJECT_TYPE))

      cons.put(Label.ATTRIBUTE_TYPE, "EVENT")
      cons.put(Label.VALUE, event)


      retVal.append(cons)
    }
    retVal
  }

  def eventToTargetMention(event: Relationship): Constituent = {
    event.subj.asInstanceOf[Constituent]
  }

}
