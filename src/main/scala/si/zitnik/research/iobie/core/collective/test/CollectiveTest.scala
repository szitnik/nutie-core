package si.zitnik.research.iobie.collective.test

import scala.Array
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.algorithms.crf.{Label, ExampleLabel}
import si.zitnik.research.iobie.algorithms.crf.feature.packages.FeatureFunctionPackages
import si.zitnik.research.iobie.core.collective.learner.CollectiveLearner
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}
import si.zitnik.research.iobie.datasets.ace2004.{ACE2004Importer, ACE2004DocumentType}
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.thirdparty.opennlp.api.{ChunkTagger, ParseTagger, PoSTagger}
import si.zitnik.research.iobie.thirdparty.lemmagen.api.LemmaTagger
import si.zitnik.research.iobie.domain.sampling.{ExactSampler, RandomSampler}
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.core.relationship.analysis.RelationshipAnalysis
import scala.collection.SortedMap
import si.zitnik.research.iobie.core.coreference.analysis.CoreferenceAnalysis

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/7/12
 * Time: 4:53 PM
 * To change this template use File | Settings | File Templates.
 */

object CollectiveTest {

  object ACE2004Data {
    private val datasetPath: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.ACE2004_PATH)

    val sources = Array(
      ACE2004DocumentType.NEWSWIRE,
      ACE2004DocumentType.ARABIC_TREEBANK,
      ACE2004DocumentType.CHINESE_TREEBANK,
      ACE2004DocumentType.BROADCAST_NEWS

      //TODO: too large sentences
      //ACE2004DocumentType.FISHER_TRANSCRIPTS
    )


    private var data: (Examples, Examples) = null

    def reload(): Unit = {
      val rawData = new ACE2004Importer(datasetPath, documentTypes = sources).importForIE()
      new PoSTagger().tag(rawData)
      new LemmaTagger().tag(rawData)
      new ParseTagger().tag(rawData)
      new ChunkTagger().tag(rawData)

      data = RandomSampler.sample(rawData, randomSeed = 42, trainPercent = 0.8)
    }

    def reloadCullota(): Unit = {
      val rawData = new ACE2004Importer(datasetPath, documentTypes = sources).importForIE()
      new PoSTagger().tag(rawData)
      new LemmaTagger().tag(rawData)
      new ParseTagger().tag(rawData)
      new ChunkTagger().tag(rawData)

      //Cullota split:
      data = ExactSampler.sampleNumber(rawData, 336)
    }

    def reloadAll(): Unit = {
      val rawData = new ACE2004Importer(datasetPath, documentTypes = sources).importForIE()
      new PoSTagger().tag(rawData)
      new LemmaTagger().tag(rawData)
      new ParseTagger().tag(rawData)
      new ChunkTagger().tag(rawData)

      data = (rawData, rawData)
    }



    def importTrainData() = data._1
    def importTestData() = data._2
  }

  def printStat(examples: Examples): Unit = {
    val docIds = examples.getAllDocIds()
    println("DocIDs: "+docIds.size)
    println("Mentions: "+ examples.map(_.getAllMentions().size).sum)
    println("Relationships: "+ examples.map(_.getAllRelationships().size).sum)
    println("Entities: "+docIds.map(id => {
      examples.getDocumentExamples(id).map(_.getAllMentions().map(_.get(Label.COREF).asInstanceOf[Int]).toSet).flatten.toSet.size
    }).sum)
  }

  def main(args: Array[String]): Unit = {
    ACE2004Data.reload()


    ACE2004Data.importTrainData().printStatistics(
      ommitedMentionAttributes = Array(Label.OBS, Label.EXTENT, Label.COREF, Label.ID),
      ommited = Array(Label.OBS, Label.PARSE_NODE, Label.LEMMA),
      ommitedRelationshipAttributes = Array(Label.EXTENT, Label.ID),
      ommitedExample = Array(ExampleLabel.DOC_ID)
    )

    ACE2004Data.importTestData().printStatistics(
      ommitedMentionAttributes = Array(Label.OBS, Label.EXTENT, Label.COREF, Label.ID),
      ommited = Array(Label.OBS, Label.PARSE_NODE, Label.LEMMA),
      ommitedRelationshipAttributes = Array(Label.EXTENT, Label.ID),
      ommitedExample = Array(ExampleLabel.DOC_ID)
    )

    printStat(ACE2004Data.importTrainData())
    printStat(ACE2004Data.importTestData())

    /*
    ACE2004Data.importTrainData().relabel(Label.NE, "FAC", "O")
    ACE2004Data.importTrainData().relabel(Label.NE, "VEH", "O")
    ACE2004Data.importTrainData().relabel(Label.NE, "WEA", "O")
    ACE2004Data.importTestData().relabel(Label.NE, "FAC", "O")
    ACE2004Data.importTestData().relabel(Label.NE, "VEH", "O")
    ACE2004Data.importTestData().relabel(Label.NE, "WEA", "O")
    */

    //println("Number of relationships: %d".format(data.map(_.getAllRelationships().size).sum))
    //ExamplesVisualizer.run(data)


    //new ACE2004Importer(exportOntology = true).importForIE()


    //visualizeRelationDistances(ACE2004Data.importTrainData())
    //visualizeCoreferenceDistances(ACE2004Data.importTrainData())

    val classifier = new CollectiveLearner(
      ACE2004Data.importTrainData(),
      FeatureFunctionPackages.standardNerFFunctions(),
      FeatureFunctionPackages.standardRelFFPackages(),
      FeatureFunctionPackages.bestACE2004CorefFeatureFunctions, 5).train()
    classifier.classify(ACE2004Data.importTestData())

  }

  def visualizeCoreferenceDistances(examples: Examples): Unit = {
    val mentionExamples = new MentionExamplesBuilder(examples, null, ExampleLabel.DOC_ID).buildFromTagged()
    val distanceDistribution = CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(mentionExamples)
    println("Mention distance distribution: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))

    val outDistr = (SortedMap[Int, Int]() ++ distanceDistribution).map(v => (v._1, v._2)).toList.sortBy(_._1)
    println("Distance distribution keys: " + outDistr.map(_._1).mkString(","))
    println("Distance distribution values: " + outDistr.map(_._2).mkString(","))

  }



  def visualizeRelationDistances(examples: Examples, title: String = "Train Dataset Distance distribution", filename: String = "/Users/slavkoz/temp/relationDistr.png"): Unit = {
    var distanceDistribution = RelationshipAnalysis.getRelationshipAttributeMentionsDistanceDistributionGeneric(examples)
    println("All mention distance pairs num: %d".format(distanceDistribution.values.sum))
    println("Mention distance distribution all: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))

    distanceDistribution.remove(-1)


    //R input:
    //var all = distanceDistribution.map(_._2).sum
    //println("all: %d".format(all))
    //println(distanceDistribution.flatMap(v => List.fill(v._2)(v._1)).mkString("domain <- c(", ",", ")"))
    val outDistr = (SortedMap[Int, Int]() ++ distanceDistribution).map(v => (v._1, v._2)).toList.sortBy(_._1)
    println("Distance distribution keys: " + outDistr.map(_._1).mkString(","))
    println("Distance distribution values: " + outDistr.map(_._2).mkString(","))


    println("Mention distance distribution removed: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))
  }


}
