package si.zitnik.research.iobie.core.relationship.analysis

import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.util.CounterMap
import si.zitnik.research.iobie.algorithms.crf.Label
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.relationship.Relationship

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/7/13
 * Time: 1:08 PM
 * To change this template use File | Settings | File Templates.
 */
object RelationshipAnalysis {

  /**
   * The method returns the counts of distances of mentions between arguments within relationships.
   *
   * The method supposes that relationship arguments are mentions and therefore can be retrieved from
   * getAllMentions. If the constituent does not exist, it is counted as -1.
   *
   * When valueTypeToProcess set, only relationships having specific typeLabelType attribute value are
   * processed
   */
  def getRelationshipAttributeMentionsDistanceDistributionBioNLP(
                                                            examples: Examples,
                                                            typeLabelType: Label.Value = Label.ATTRIBUTE_TYPE,
                                                            valueTypeToProcess: Option[Array[String]] = None) = {
    val retVal = new CounterMap[Int]()

    def process(example: Example, relationship: Relationship): Unit = {
      if (relationship.relationshipName.equals("NEGATION")) {
        retVal.put(-1)
        return
      }
      val subj  = if (relationship.subj.get(Label.ID).asInstanceOf[String] == null) {
        relationship.subj.get(Label.VALUE).asInstanceOf[Relationship].get(Label.ID).asInstanceOf[String]
      } else {
        relationship.subj.get(Label.ID).asInstanceOf[String]
      }
      val obj  = if (relationship.obj.get(Label.ID).asInstanceOf[String] == null) {
        relationship.obj.get(Label.VALUE).asInstanceOf[Relationship].get(Label.ID).asInstanceOf[String]
      } else {
        relationship.obj.get(Label.ID).asInstanceOf[String]
      }

      val subjIdx = example.getAllMentions().indexOf(example.getFirstMentionConstituent(Label.ID, subj) match {
        case Some(x) => x
        case None => -1
      })
      val objIdx  = example.getAllMentions().indexOf(example.getFirstMentionConstituent(Label.ID, obj) match {
        case Some(x) => x
        case None => -1
      })

      if (subjIdx == -1 || objIdx == -1) {
        retVal.put(-1)
      } else {
        retVal.put(math.abs(subjIdx-objIdx-1))
      }
    }

    for (example <- examples) {
      for (relationship <- example.getAllRelationships()) {
        if (valueTypeToProcess.isDefined) {
          if (valueTypeToProcess.get.contains(relationship.get(typeLabelType).toString)) {
            process(example, relationship)
          }
        } else {
          process(example, relationship)
        }

      }
    }

    retVal.map
  }


  def getRelationshipAttributeMentionsDistanceDistributionGeneric(examples: Examples) = {
    val retVal = new CounterMap[Int]()

    def process(example: Example, relationship: Relationship): Unit = {
      val subj  = relationship.subj
      val obj  = relationship.obj

      val subjIdx = example.getAllMentions().indexOf(subj)
      val objIdx  = example.getAllMentions().indexOf(obj)

      if (subjIdx == -1 || objIdx == -1) {
        retVal.put(-1)
      } else {
        retVal.put(math.abs(subjIdx-objIdx-1))
      }
    }

    for (example <- examples) {
      for (relationship <- example.getAllRelationships()) {
          process(example, relationship)
      }
    }
    retVal.map
  }
}
