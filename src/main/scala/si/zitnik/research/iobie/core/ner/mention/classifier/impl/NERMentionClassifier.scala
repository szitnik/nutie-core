package si.zitnik.research.iobie.core.ner.mention.classifier.impl

import si.zitnik.research.iobie.algorithms.crf.{Label, Classifier}
import si.zitnik.research.iobie.domain.{Example, Examples}
import java.util
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 6/21/13
 * Time: 4:08 PM
 * To change this template use File | Settings | File Templates.
 */
class NERMentionClassifier(
                            val classifier: Classifier,
                            val learnLabelType: Label.Value = Label.NE) extends Classifier {

  def classify(mentionExample: Example, normalized: Boolean): (util.ArrayList[String], util.ArrayList[Double], Double) = {
    classifier.classify(mentionExample, normalized)
  }

  def test(data: Examples): Unit = {
    new Statistics(classifier, data).printAllStat(learnLabelType)
  }
}
