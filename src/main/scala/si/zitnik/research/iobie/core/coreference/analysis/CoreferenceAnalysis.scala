package si.zitnik.research.iobie.core.coreference.analysis

import si.zitnik.research.iobie.domain.{Examples}
import si.zitnik.research.iobie.util.{CounterMap}
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.{Label}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/9/12
 * Time: 12:57 PM
 * To change this template use File | Settings | File Templates.
 */
object CoreferenceAnalysis {

  /**
   * The method returns the counts of distances between every two CONSECUTIVE coreferent mentions
   * @param mentionExamples
   */
  def getCoreferentMentionsDistanceDistribution(mentionExamples: Examples) = {
    val retVal = new CounterMap[Int]()

    for (example <- mentionExamples) {
      for (i <- 0 until example.size()) {
        val mention1 = example.get(i, Label.COREF)
        example.subList(i+1, example.size()).map(_.get(Label.COREF)).dropWhile(!_.equals(mention1)).size match {
          case 0 => {} //no contained in list
          case len => {
            retVal.put(example.size()-i-1-len)
          }
        }
      }
    }

    retVal.map
  }

}
