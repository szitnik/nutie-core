package si.zitnik.research.iobie.core.relationship.util

import com.typesafe.scalalogging.StrictLogging

import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.algorithms.crf.Label

import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.domain.relationship.Relationship

/**
 * This class is used to transform examples containing mentions and relationships into
 * Relationship examples for iterative method with skip tokens.
 *
 * In contrast to Coreference Mention transformer
 */
object ExamplesToRelationshipExamplesTransformer extends StrictLogging {
  /**
   * When using relationships/mentions from example
   * @param example
   * @param skipNumber
   * @return
   */
  def toRelationshipExamples(example: Example, skipNumber: Int): Examples = {
    val retVal = new Examples()

    for (startIdx <- 0 to skipNumber) {
      val relExample = toRelExample(example.getAllMentions(), example.getAllRelationships(), skipNumber, startIdx)
      if (relExample.size() > 1) {
        retVal.add(relExample)
      }
    }

    retVal
  }

  /**
   * When having separate relationship/mentions lists
   * @param mentions
   * @param relationships
   * @param skipNumber
   * @return
   */
  def toRelationshipExamples(mentions: ArrayBuffer[Constituent], relationships: ArrayBuffer[Relationship], skipNumber: Int): Examples = {
    val retVal = new Examples()

    for (startIdx <- 0 to skipNumber) {
      val relExample = toRelExample(mentions, relationships, skipNumber, startIdx)
      if (relExample.size() > 1) {
        retVal.add(relExample)
      }
    }

    retVal
  }

  /*
  private def getRelationship(relationships: ArrayBuffer[Relationship], leftCons: Constituent, rightCons: Constituent): Option[Relationship] = {
    for (relationship <- relationships) {
      if (relationship.subj.equals(leftCons) && relationship.obj.equals(rightCons) ||
        relationship.subj.equals(rightCons) && relationship.obj.equals(leftCons)) {
        return Some(relationship)
      }
    }
    return None
  }*/

  //TODO: BioNLP only, BioNLP-based transformer (i.e. exceptions if attributes are events/relationships)
  private def getRelationshipBioNLP(relationships: ArrayBuffer[Relationship], leftCons: Constituent, rightCons: Constituent): Option[Relationship] = {

    for (relationship <- relationships) {
      val subjId = if (relationship.subj.get(Label.ID) == null) {
        relationship.subj.get(Label.VALUE).asInstanceOf[Token].get(Label.ID)
      } else {
        relationship.subj.get(Label.ID)
      }
      val objId = if (relationship.obj.get(Label.ID) == null) {
        relationship.obj.get(Label.VALUE).asInstanceOf[Token].get(Label.ID)
      } else {
        relationship.obj.get(Label.ID)
      }

      if (subjId.equals(leftCons.get(Label.ID)) && objId.equals(rightCons.get(Label.ID)) ||
          subjId.equals(rightCons.get(Label.ID)) && objId.equals(leftCons.get(Label.ID))) {
        return Some(relationship)
      }
    }
    return None
  }

  private def getRelationship(relationships: ArrayBuffer[Relationship], leftCons: Constituent, rightCons: Constituent): Option[Relationship] = {

    for (relationship <- relationships) {
      if (relationship.subj == leftCons && relationship.obj == rightCons ||
        relationship.subj == rightCons && relationship.obj == leftCons) {
        return Some(relationship)
      }
    }
    return None
  }

  private def toRelExample(mentions: ArrayBuffer[Constituent], relationships: ArrayBuffer[Relationship], skipNumber: Int = 0, startIdx: Int = 0): Example = {
    val relExample = new Example()

    //get only those that will form an Example
    val splitTokens = mentions.zipWithIndex
      .filter{case (token, idx) => {
      if (skipNumber==0 || ((idx-startIdx)%(skipNumber+1) == 0)) {
        true
      } else {
        false
      }
    }}.map(_._1)

    if (splitTokens.size > 0) {
      //zero-th element (normally, a sentence has at least one token :))
      val newToken = Constituent.clone(splitTokens(0))
      newToken.put(Label.REL, "O")
      relExample.add(newToken)
    }


    for (i <- (1 until splitTokens.size)) {
      //create new token
      val newToken = Constituent.clone(splitTokens(i))


        //is there relationship between the two tokens (= Constituents)
        //TODO: think about relationships having subject/object difference
        getRelationship(relationships, splitTokens(i-1), splitTokens(i)) match {
          case Some(r) => newToken.put(Label.REL, r.relationshipName)
          case None => newToken.put(Label.REL, "O")
        }


      relExample.add(newToken)
    }

    relExample
  }

}
