package si.zitnik.research.iobie.core.relationship.test

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.relationship.Relationship

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent

import collection.mutable.ArrayBuffer

import collection.mutable
import io.Source

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/21/13
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
object BioNLP2013Rules extends StrictLogging {
  private val subtilisGenes: mutable.HashSet[String] = loadGenes(true)

  def enrichByStaticRules(testExamples: Examples): Unit = {
    println("\n\n\n")

    var addedNum = 0

    var regulars = Array(
      ("Interaction.Transcription", "transcrib", false)
    )

    addedNum += regulars.map(t => enrichByStaticRulesHelper(testExamples, t._1, t._2)).sum

    regulars = Array(
      ("Interaction.Transcription", ".*directs transcription.*", false),
      ("Interaction.Inhibition", ".*inactivate.*", false),
      ("Interaction.Inhibition", ".*inhibits.*", false),
      ("Interaction.Inhibition", ".*repressor to.*", false),
      ("Interaction.Inhibition", ".*is negatively regulated by.*", true),
      ("Interaction.Activation", ".*is governed by.*", true),
      ("Interaction.Activation", ".*essential.*activat.*", false),
      ("Interaction.Activation", ".*to.*activat.*", false),
      ("Interaction.Activation", ".*turns on.*", false),
      ("Interaction.Requirement", ".*requires.*", true),
      ("Interaction.Requirement", ".*required.*", false),
      ("Interaction.Binding", ".*binds.*to.*", false),
      ("Interaction.Binding", "-binding.*", false)
    )

    addedNum += regulars.map(t => enrichByStaticRulesHelperBetweenMention(testExamples, t._1, t._2, t._3)).sum

    regulars = Array(
      ("Interaction.Transcription", ".*under.*control.*of.*", false),
      ("Interaction.Activation", ".*is governed by.*", true),
      ("Interaction.Inhibition", ".*represses.*", false),
      ("Interaction.Inhibition", ".*to repress.*", false)
    )

    addedNum += regulars.map(t => enrichByStaticRulesHelperBetweenMentionWithList(testExamples, t._1, t._2, t._3)).sum

    regulars = Array(
      ("Interaction.Activation", ".*activated by.*", false),
      ("Interaction.Requirement", ".*are required.*", true)
    )

    addedNum += regulars.map(t => enrichByStaticRulesHelperBetweenMentionWithListLeft(testExamples, t._1, t._2, t._3)).sum

    regulars = Array(
      ("Interaction.Activation", ".*turns on.*", false),
      ("Interaction.Inhibition", ".*repressed.*", false)
    )

    addedNum += regulars.map(t => enrichByStaticRulesHelperBetweenMentionSubsentence(testExamples, t._1, t._2)).sum

    println("Number of all relations added by rules: %d".format(addedNum))
  }

  /**
   * Contains keyword in lowercase between two mentions.
   * @param testExamples
   * @param relName
   * @param keyword
   */
  def enrichByStaticRulesHelperBetweenMention(testExamples: Examples, relName: String, keyword: String, passiveType: Boolean = false) = {

    var numRelsAdded = 0
    for (example <- testExamples) {
      for (slice <- example.getAllMentions().filter(m => !m.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION") && isBacillusSubtilisGene(m)).
        sliding(2) if (slice.size > 1 &&
                        slice(0).endIdx < slice(1).startIdx)) {
        val betweenText = example.subLabeling(Label.OBS, slice(0).endIdx, slice(1).startIdx).mkString(" ").toLowerCase()
        if (betweenText.matches(keyword)) {
          val r = if (passiveType) {
            new Relationship(null, relName, slice(1), slice(0))
          } else {
            new Relationship(null, relName, slice(0), slice(1))
          }

          var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
          r.example = example
          r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
          r.put(Label.ID, "R"+nextRelIdx)
          modifyRelAttributeTypes(r)

          example.addRelationship(r)
          numRelsAdded += 1
          println("\t%s: %s".format(example.get(ExampleLabel.DOC_ID), example.toString()))
        }
      }
    }
    println("Manually added %d relationships for relationship %s.\n".format(numRelsAdded, relName))
    numRelsAdded
  }

  /**
   * Contains keyword in lowercase between two mentions (sentence in between).
   * @param testExamples
   * @param relName
   * @param keyword
   */
  def enrichByStaticRulesHelperBetweenMentionSubsentence(testExamples: Examples, relName: String, keyword: String) = {

    var numRelsAdded = 0
    for (example <- testExamples) {
      val inASubsentence = (m: Constituent) => {
        val commaIdxs = example.filter(_.get(Label.OBS).equals(","))
        if (commaIdxs.size >= 2) {
          val firstIdx = commaIdxs.head.get(Label.START_IDX).asInstanceOf[Int]
          val lastIdx = commaIdxs.last.get(Label.START_IDX).asInstanceOf[Int]
          val mentionIdx = m.get(Label.START_IDX).asInstanceOf[Int]
          if (firstIdx < mentionIdx && mentionIdx < lastIdx) {
            true
          } else {
            false
          }
        } else {
          false
        }
      }

      for (slice <- example.getAllMentions().filter(m => {
        !m.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION") &&
        !inASubsentence(m) && isBacillusSubtilisGene(m) //in a subsentence
      }).sliding(2) if (slice.size > 1 && slice(0).endIdx < slice(1).startIdx)) {
        val betweenText = example.subLabeling(Label.OBS, slice(0).endIdx, slice(1).startIdx).
          mkString(" ").
          toLowerCase().
          replaceAll(",.*,","")

        if (betweenText.matches(keyword)) {
          val r = new Relationship(null, relName, slice(0), slice(1))

          var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
          r.example = example
          r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
          r.put(Label.ID, "R"+nextRelIdx)
          modifyRelAttributeTypes(r)

          example.addRelationship(r)
          numRelsAdded += 1
          println("\t%s: %s".format(example.get(ExampleLabel.DOC_ID), example.toString()))
        }
      }
    }
    println("Manually added %d relationships for relationship %s.\n".format(numRelsAdded, relName))
    numRelsAdded
  }

  /**
   * Contains keyword in lowercase between mentions, where on the left is one mention and on the right are more mentions.
   * @param testExamples
   * @param relName
   * @param keyword
   */
  def enrichByStaticRulesHelperBetweenMentionWithList(testExamples: Examples, relName: String, keyword: String, passiveType: Boolean = false) = {
    val separators = Set(",", ", and", "and")

    var numRelsAdded = 0
    for (example <- testExamples) {

      val mentions = example.getAllMentions().filter(m => !m.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION") && isBacillusSubtilisGene(m))

      for (slice <- mentions.sliding(2) if (slice.size > 1 && slice(0).endIdx < slice(1).startIdx)) {
        val betweenText = example.subLabeling(Label.OBS, slice(0).endIdx, slice(1).startIdx).mkString(" ").toLowerCase()

        if (betweenText.matches(keyword)) {
          val rightMentions = mentions.subList(mentions.indexOf(slice(1)), mentions.size)
          if (rightMentions.size >= 2) {
            val mentionsToTake = rightMentions.sliding(2).
              map( m =>
                if (m(0).endIdx >= m(1).startIdx) {
                  ","
                } else {
                  example.subLabeling(Label.OBS, m(0).endIdx, m(1).startIdx).mkString(" ").toLowerCase()
                }
            ).map(_.trim).
              takeWhile(v =>
                separators.contains(v.toLowerCase())
            ).size + 1

            if (mentionsToTake >= 2) {
              for (rightMention <- rightMentions.take(mentionsToTake)) {
                val r = if (passiveType) {
                  new Relationship(null, relName, rightMention, slice(0))
                } else {
                  new Relationship(null, relName, slice(0), rightMention)
                }

                var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
                r.example = example
                r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
                r.put(Label.ID, "R"+nextRelIdx)
                modifyRelAttributeTypes(r)

                example.addRelationship(r)
                numRelsAdded += 1
              }
              println("\t%s: %s".format(example.get(ExampleLabel.DOC_ID), example.toString()))
            }
          }
        }
      }
    }
    println("Manually added %d relationships for relationship %s.\n".format(numRelsAdded, relName))
    numRelsAdded
  }

  /**
   * Contains keyword in lowercase between mentions, where on the right is one mention and on the left are more mentions.
   * @param testExamples
   * @param relName
   * @param keyword
   */
  def enrichByStaticRulesHelperBetweenMentionWithListLeft(testExamples: Examples, relName: String, keyword: String, passiveType: Boolean = false) = {
    val separators = Set(",", ", and", "and")

    var numRelsAdded = 0
    for (example <- testExamples) {
      val mentions = example.getAllMentions().filter(m => !m.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION") && isBacillusSubtilisGene(m))
      for (slice <- mentions.sliding(2) if (slice.size > 1 && slice(0).endIdx < slice(1).startIdx)) {
        val betweenText = example.subLabeling(Label.OBS, slice(0).endIdx, slice(1).startIdx).mkString(" ").toLowerCase()
        if (betweenText.matches(keyword)) {
          val leftMentions = mentions.subList(0, mentions.indexOf(slice(1)))
          if (leftMentions.size() >= 2) {
            val mentionsToTake = leftMentions.sliding(2).filter(m => m(0).endIdx < m(1).startIdx).
              map( m => example.subLabeling(Label.OBS, m(0).endIdx, m(1).startIdx).mkString(" ").toLowerCase() ).
              map(_.trim).toList.reverse.takeWhile(v=> separators.contains(v.toLowerCase())).size + 1

            if (mentionsToTake >= 2) {
              for (leftMention <- leftMentions.reverse.take(mentionsToTake)) {
                val r = if (passiveType) {
                  new Relationship(null, relName, leftMention, slice(1))
                } else {
                  new Relationship(null, relName, slice(1), leftMention)
                }

                var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
                r.example = example
                r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
                r.put(Label.ID, "R"+nextRelIdx)
                modifyRelAttributeTypes(r)

                example.addRelationship(r)
                numRelsAdded += 1
              }
              println("\t%s: %s".format(example.get(ExampleLabel.DOC_ID), example.toString()))
            }
          }
        }
      }
    }
    println("Manually added %d relationships for relationship %s.\n".format(numRelsAdded, relName))
    numRelsAdded
  }

  /**
   * Checks for triplet mentions (a,b,c) and assigns a relationship relName if one of the mentions word starts with
   * startsWithKeyword. KeywordIdx defines which of a,b,c mentions is checked using startsWith, default is b (keywordIdx=1).
   * @param testExamples
   * @param relName
   * @param startsWithKeyword
   * @param keywordIdx
   */
  def enrichByStaticRulesHelper(testExamples: Examples, relName: String, startsWithKeyword: String, keywordIdx: Int = 1) = {

    var numRelsAdded = 0
    for (example <- testExamples) {
      for (slice <- example.getAllMentions().sliding(3)) {
        val t_mentions = slice.zipWithIndex.filter{ case (mention, idx) => {
          mention.get(Label.ATTRIBUTE_TYPE).equals("R_MENTION") &&
            mention.get(Label.OBS).asInstanceOf[String].toLowerCase().startsWith(startsWithKeyword) }}
        if (t_mentions.size == 1 && t_mentions(0)._2 == keywordIdx) {
          val attrIdxs = Set(0,1,2).diff(t_mentions.map(_._2).toSet).toList
          val r = new Relationship(null, relName, slice(attrIdxs(0)), slice(attrIdxs(1)))

          var nextRelIdx = example.getAllRelationships().filter(_.get(Label.ID).asInstanceOf[String].startsWith("R")).map(_.get(Label.ID).asInstanceOf[String].substring(1).toInt).+:(0).max + 1
          r.example = example
          r.put(Label.ATTRIBUTE_TYPE, "RELATIONSHIP")
          r.put(Label.ID, "R"+nextRelIdx)
          modifyRelAttributeTypes(r)

          example.addRelationship(r)
          numRelsAdded += 1
          println("\t%s: %s".format(example.get(ExampleLabel.DOC_ID), example.toString()))
          println("\t\t%s", r.toString())
        }
      }
    }
    println("Manually added %d relationships for relationship %s.\n".format(numRelsAdded, relName))
    numRelsAdded
  }


  /**
   * Automatically repairs Relationship attribute types
   * @param relationship
   * @return
   */
  def modifyRelAttributeTypes(relationship: Relationship) = {
    relationship.relationshipName match {
      case "Promoter_of" => {
        if (getEntityTypes(relationship).intersect(Set("Gene", "Promoter")).size == 2) {
          if (!"Gene".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("Regulon", "Protein")).size == 2) {
          if (!"Protein".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("Operon", "Promoter")).size == 2) {
          if (!"Operon".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Interaction.Inhibition" => {
        if (getEntityTypes(relationship).intersect(Set("Operon", "Promoter")).size == 2) {
          if (!"Operon".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("Gene")).size == 1) {
          if (!"Gene".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "Gene")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "GeneFamily")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("GeneFamily")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("Protein", "Promoter")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Promoter")) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Interaction.Binding" => {
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "Gene")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "GeneFamily")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("GeneFamily")) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Interaction.Transcription" => {
        if (getEntityTypes(relationship).intersect(Set("Promoter", "PolymeraseComplex", "ProteinComplex")).size == 2) { //TODO
          if (!getObjEntityTypes(relationship).contains("Promoter")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "Gene")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "GeneFamily")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("GeneFamily")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("PolymeraseComplex", "Gene")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("PolymeraseComplex", "Regulon")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Regulon")) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Interaction.Regulation" => {
        if (getEntityTypes(relationship).intersect(Set("Operon", "Promoter")).size == 2) {
          if (!"Operon".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "Gene", "GeneFamily")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("Promoter", "Gene")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Promoter")) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Interaction.Activation" => {
        if (getEntityTypes(relationship).intersect(Set("Operon", "Promoter")).size == 2) {
          if (!"Operon".equals(relationship.obj.get(Label.ENTITY_TYPE))) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "Gene")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
        if (getEntityTypes(relationship).intersect(Set("ProteinComplex", "ProteinFamily", "Protein", "GeneFamily")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("GeneFamily")) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Master_of_Regulon" => {
        if (getEntityTypes(relationship).intersect(Set("Protein", "Regulon")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Protein")) {
            changeSubjObj(relationship)
          }
        }
      }
      case "Member_of_Regulon" => {
        if (getEntityTypes(relationship).intersect(Set("Gene", "Regulon")).size == 2) {
          if (!getObjEntityTypes(relationship).contains("Gene")) {
            changeSubjObj(relationship)
          }
        }
      }
      case _ => {}
    }


    val (subjType, objType) = relationship.relationshipName match {
      case "Promoter_of" => ("Promoter", "Gene")
      case "Master_of_Promoter" => ("Promoter", "Protein")
      case "Master_of_Regulon" => ("Regulon", "Master")
      case "Member_of_Regulon" => ("Regulon", "Master")
      case _ => ("Agent", "Target")
    }

    relationship.put(Label.SUBJECT_TYPE, subjType)
    relationship.put(Label.OBJECT_TYPE, objType)
  }

  /**
   * Automatically repairs Event attribute types
   * @param event
   * @return
   */
  def modifyEventAttributeTypes(event: Relationship) = {
    //check direction
    if (!event.obj.get(Label.ENTITY_TYPE).equals("Action")) {
      //change direction
      if (!event.subj.get(Label.ENTITY_TYPE).equals("Action")) {
        logger.warn("Event must have an attribute of Action type: %s".format(event.toString()))
        //System.exit(-1) //
      }
      val temp = event.subj
      event.subj = event.obj
      event.obj = temp
    }

    if (event.obj.get(Label.OBS).asInstanceOf[String].toLowerCase().startsWith("utiliz")) {
      event.put(Label.OBJECT_TYPE, "Transcription_by")
    } else {
      event.put(Label.OBJECT_TYPE, "Action_Target")
    }

    event.put(Label.SUBJECT_TYPE, event.subj.get(Label.ENTITY_TYPE) match {
      case "Protein" | "GeneFamily" | "Gene" | "Operon" | "mRNA" | "Regulon" => "Target"
      case "PolymeraseComplex" | "ProteinComplex" => "Agent"
      case "Promoter" | "Site" => "Site"
      case v => {
        logger.error("Unknown event subject entity type %s!".format(v))
        System.exit(-1)
      }
    })
  }



  def getEntityTypes(relationship: Relationship) = {
    val constituents = getEntityTypesFromConstituent(ArrayBuffer[Constituent](relationship.subj, relationship.obj))
    constituents.map(_.get(Label.ENTITY_TYPE).asInstanceOf[String]).toSet
  }

  def getObjEntityTypes(relationship: Relationship) = {
    val constituents = getEntityTypesFromConstituent(ArrayBuffer[Constituent](relationship.obj))
    constituents.map(_.get(Label.ENTITY_TYPE).asInstanceOf[String]).toSet
  }


  def changeSubjObj(relationship: Relationship): Unit = {
    val temp = relationship.obj
    relationship.obj = relationship.subj
    relationship.subj = temp
  }

  def getEntityTypesFromConstituent(input: ArrayBuffer[Constituent]) = {
    val constituents = ArrayBuffer[Constituent]()

    var toCheck = input
    while (!toCheck.isEmpty) {
      val tempToCheck = ArrayBuffer[Constituent]()
      for (curConstituent <- toCheck) {
        curConstituent.get(Label.ATTRIBUTE_TYPE) match {
          case "RELATIONSHIP" | "EVENT" => {
            val tempRel = curConstituent.get(Label.VALUE).asInstanceOf[Relationship]
            tempToCheck.append(tempRel.subj)
            tempToCheck.append(tempRel.obj)
          }
          case _ => {
            constituents.append(curConstituent)
          }
        }
      }
      toCheck = tempToCheck
    }

    constituents
  }

  def isBacillusSubtilisGene(mention: Constituent) = {
    if (
      (mention.get(Label.OBS) != null && subtilisGenes.contains(mention.get(Label.OBS).toString.toLowerCase)) ||
      (mention.get(Label.COREF) != null && subtilisGenes.contains(mention.get(Label.COREF).toString.toLowerCase))
    ) {
      true
    } else {
      false
    }
  }


  def overlaps(m1: Constituent, m2: Constituent) = {
    if ((m1.startIdx until m1.endIdx).intersect(m2.startIdx until m2.endIdx) != 0) {
      true
    } else {
      false
    }
  }


  def loadGenes(lowercase: Boolean = false) = {
    val retVal = mutable.HashSet[String]()


    for (line <- Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("specific/Bacillus-subtilis.genes")).getLines()) {
      val geneName = line.split(" ")(1).replaceAll("\\[gene=", "").replaceAll("\\]", "")
      if (lowercase) {
        retVal.add(geneName.toLowerCase)
      } else {
        retVal.add(geneName)
      }
    }

    retVal
  }



  def cleanData(testExamples: Examples): Unit = {
    var loopCounter = 0
    var doubleCounter = 0

    for (example <- testExamples) {
      val allRelationships = example.getAllRelationships()

      var fullRels = allRelationships.filter(_.get(Label.ATTRIBUTE_TYPE).equals("RELATIONSHIP"))
      val otherRelationships = allRelationships.diff(fullRels)


      //remove loops
      val tempRelationships = ArrayBuffer[Relationship]()
      for (relationship <- fullRels) {
        if (relationship.subj.get(Label.COREF) != null && !relationship.subj.get(Label.COREF).toString.isEmpty &&
          !relationship.subj.get(Label.COREF).equals(relationship.obj.get(Label.COREF))) {
          tempRelationships.append(relationship)
        } else {
          loopCounter += 1
        }
      }
      fullRels = tempRelationships

      //remove doubles
      val tempRelationships2 = ArrayBuffer[Relationship]()
      val doubleCheck = mutable.Set[(String, String, String)]()
      fullRels.foreach(r => {
        val element = (r.relationshipName, r.subj.get(Label.ID).asInstanceOf[String], r.obj.get(Label.ID).asInstanceOf[String])
        if (doubleCheck.contains(element)) {
          doubleCounter += 1
        } else {
          doubleCheck.add(element)
          tempRelationships2.append(r)
        }
      })
      fullRels = tempRelationships2

      //setback
      example.setRelationships(otherRelationships ++ fullRels)
    }

    println("Removed %d loop relationships.".format(loopCounter))
    println("Removed %d double relationships.".format(doubleCounter))
  }

}
