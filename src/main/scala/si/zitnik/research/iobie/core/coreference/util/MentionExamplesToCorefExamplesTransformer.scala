package si.zitnik.research.iobie.core.coreference.util


import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * This class is used to transform built mention examples (arbitrary by MentionDetector) into
 * Coreference examples for iterative method with skip tokens.
 */
object MentionExamplesToCorefExamplesTransformer extends StrictLogging {

  /**
   * It is Okay to add all of these together into learning/classifier set because Constituent is copied just
   * once inside return examples. But it is not thread safe!
   *
   * SkipNumber is number of skipped tokens between two used. Each token is supposed to contain Label.OBS (By default in
   * Constituents)
   *
   * If input example contains class values (Label.COREF), new Class attribute (Label.COREF) is:
   *  - "C": two consecutive tokens are in the same cluster
   *  - "O": two consecutive tokens are in different cluster or it is start token
   *
   * For example:
   *    OBS:  A  B  C  D  E  F  G
   * COREFS:  1  2  1  3  1  2  3
   *
   * skipNumber = 0:
   *results:
   *  res0: A(O) B(O) C(O) D(O) E(O) F(O) G(O)
   *
   * skipNumber = 1:
   * results:
   *  res0: A(O) C(C) E(C) G(O)
   *  res1: B(O) D(O) F(O)
   *
   * skipNumber = 2:
   * results:
   *  res0: A(O) D(O) G(C)
   *  res1: B(O) E(O)
   *  res2: C(O) F(O)
   *
   *  ....
   *
   *
   * @param example
   * @param skipNumber
   * @return
   */
  def toCorefExamples(example: Example, skipNumber: Int): Examples = {
    val retVal = new Examples()

    for (startIdx <- 0 to skipNumber) {
      val corefExample = toCorefExample(example, skipNumber, startIdx)
      if (corefExample.size() > 1) {
        retVal.add(corefExample)
      }
    }

    retVal
  }

  private def toCorefExample(example: Example, skipNumber: Int = 0, startIdx: Int = 0): Example = {
    val corefExample = new Example()

    //get only those that will form an Example
    val splitTokens = example.zipWithIndex
      .filter{case (token, idx) => {
      if (skipNumber==0 || ((idx-startIdx)%(skipNumber+1) == 0)) {
        true
      } else {
        false
      }
    }}.map(_._1)

    for (i <- (0 until splitTokens.size)) {
      //create new token
      val newToken = Constituent.clone(splitTokens(i))

      //if there is no coref label, this domain is intended for inference :)
      if (splitTokens(i).contains(Label.COREF)) {
        val classVal = if (i > 0 && splitTokens(i)(Label.COREF).equals(splitTokens(i-1)(Label.COREF))) { "C" } else { "O" }
        newToken.put(Label.COREF, classVal)
      }
      corefExample.add(newToken)
    }

    corefExample
  }

  //PAIRWISE
  /**
    *
    * @param example
    * @param maxPairwiseDistance This means on which distance pairs are generated. When set to 0, only consecutive pairs are taken
    *                            into account. When set to X - all possible pairs on sliding window of lenght X are generated.
    * @return
    */
  def toPairwiseCorefExamples(example: Example, maxPairwiseDistance: Int = 10, generatePairForSingleExampleMention: Boolean = true): Examples = {
    val retVal = new Examples()

    def generatePair(i: Int, j: Int) = {
      val leftToken = example.get(i)
      val rightToken = example.get(j)
      val corefExample = toPairwiseCorefExample(leftToken, rightToken)
      retVal.add(corefExample)
    }

    val len = example.size()
    for (i <- 0 until len) {
      for (j <- (i+1) until math.min(i+2+maxPairwiseDistance, len)) {
        generatePair(i, j)
      }
    }

    if (generatePairForSingleExampleMention && example.size() == 1) {
      generatePair(0, 0)
    }

    retVal
  }

  private def toPairwiseCorefExample(leftToken: Token, rightToken: Token): Example = {
    val corefExample = new Example()

    val newLeftToken = Constituent.clone(leftToken)
    val newRightToken = Constituent.clone(rightToken)


    //if there is no coref label, this domain is intended for inference :)
    if (newLeftToken.contains(Label.COREF) && newRightToken.contains(Label.COREF)) {
      var classVal = "O"
      newLeftToken.put(Label.COREF, classVal)
      classVal = if (rightToken(Label.COREF).equals(leftToken(Label.COREF))) { "C" } else { "O" }
      newRightToken.put(Label.COREF, classVal)
    }

    corefExample.add(newLeftToken)
    corefExample.add(newRightToken)

    corefExample
  }

}
