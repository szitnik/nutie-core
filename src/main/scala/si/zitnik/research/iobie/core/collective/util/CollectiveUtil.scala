package si.zitnik.research.iobie.core.collective.util

import si.zitnik.research.iobie.algorithms.crf.{Classifier, Label}
import si.zitnik.research.iobie.domain.Examples


import collection.mutable._


import com.typesafe.scalalogging.StrictLogging

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/7/12
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */

object CollectiveUtil extends StrictLogging {

  def updateIntermediateNERLabelings(examples: Examples, i: Int, neLabels: ArrayBuffer[ArrayBuffer[String]]): Unit = {
    logger.info("Adding intermediate labelings.")
    //add L1 labelings
    examples.setLabeling(Label.L1_NE, neLabels)
    logger.info("Intermediate labelings added.")
  }

  def updateIntermediateNERLabelings(examples: Examples, iterNum: Int, NEC: Classifier): Unit = {
    logger.info("Adding intermediate labelings.")
    //add L1 labelings
    examples.setLabeling(Label.L1_NE, NEC)
    logger.info("Intermediate labelings added.")
  }


}
