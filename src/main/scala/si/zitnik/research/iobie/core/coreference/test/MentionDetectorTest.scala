package si.zitnik.research.iobie.core.coreference.test

import scala.Array
import si.zitnik.research.iobie.datasets.conll2012._
import si.zitnik.research.iobie.core.coreference.util.{MentionExamplesToCorefExamplesTransformer, MentionExamplesBuilder}
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.{Label, ExampleLabel}


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 5/15/12
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */

object MentionDetectorTest {

  def main(args: Array[String]): Unit = {
    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2012/dataset/v1/conll-2012/v1/domain"
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = CoNLL2012ImporterDatasetTypeEnum.DEVELOPMENT,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS),
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    val examples = conll2012Importer.importForIE()
    examples.printStatistics(ommited = Array(Label.OBS, Label.PARSE_NODE))
    //examples.printLabeling(Label.OBS)
    val allMentions = examples.getAllMentions()

    //this should be output of Classifier method
    val allMentionClusters = examples.getAllMentionClusters()
    //println("mentionClusters: %s".format(allMentionClusters))
    //println("mentionClusters size: %d".format(allMentionClusters.size()))

    //detect mentions from raw examples = STEP 1 (for inference)
    val mentionBuilder = new MentionExamplesBuilder(
      examples,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID)
    val mentions = mentionBuilder.detectAndBuild()
    mentions.printStatistics()
    //mentions.foreach(mentionExample => println(mentionExample.mkString("[", " ||| ", "]")))

    //println(mentions.splitExamplesByDocuments())

    /*
    val f = MentionStatistics.multiDocumentStat(allMentions, mentions.splitExamplesByDocuments())
    println("Avg. Recall: %.2f, Avg. Precision: %.2f, F-micro: %.2f, F-macro: %.2f".format(f.averageRecall(), f.averagePrecision(), f.microAveragedF(), f.macroAveragedF()))
    */

    //Test mentionExamples:
    val firstDocumentMentions = mentions(0)
    println(firstDocumentMentions.mkString("[", " ||| ", "]"))
    val firstDocumentMentionsCorefExamples = MentionExamplesToCorefExamplesTransformer.toCorefExamples(firstDocumentMentions, 2)
    println(firstDocumentMentionsCorefExamples(0).mkString("[", " ||| ", "]"))
    println(firstDocumentMentionsCorefExamples(1).mkString("[", " ||| ", "]"))
    println(firstDocumentMentionsCorefExamples(2).mkString("[", " ||| ", "]"))


    /////////

    println(mentionBuilder.buildFromTagged().printStatistics())
    println(mentionBuilder.buildFromTagged()(0).mkString("[", " ||| ", "]"))
    mentionBuilder.buildFromTagged()(0).printLabeling(Label.COREF)
    // STEP 2
    MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionBuilder.buildFromTagged()(0), 1).printLabeling(Label.COREF)

    println("XXX")
  }

}
