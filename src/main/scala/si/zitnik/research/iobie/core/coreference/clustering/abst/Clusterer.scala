package si.zitnik.research.iobie.core.coreference.clustering.abst

import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.cluster.Cluster
import collection.mutable._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 6/15/12
 * Time: 9:52 AM
 * To change this template use File | Settings | File Templates.
 */

abstract class Clusterer {
  def doClustering(pairs: HashSet[(Constituent, Constituent)]): HashSet[Cluster]
}
