package si.zitnik.research.iobie.core.coreference.classifier.impl

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier
import si.zitnik.research.iobie.core.coreference.clustering.impl.SimpleClusterer
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesToCorefExamplesTransformer
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.{Example, Examples}
import sun.reflect.generics.reflectiveObjects.NotImplementedException

import scala.collection.JavaConversions._
import scala.collection.mutable._

/**
 * Multiple Coref. classifier with clustering.
 *
 * Examples must be separated by by DOC_ID (like for learning) or something else. Each example consists of one-document constituents.
 */

class CorefPairwiseClassifierExactMatch(maxPairwiseDistance: Int,
                                        learnLabelType: Label.Value) extends CorefClassifier with StrictLogging {


  /**
   * In the results there are only clusters that contain at least two mentions.
   * @param mentionExample
   * @return
   */
  def classifyCluster(mentionExample: Example): HashSet[Cluster] = {
    val pairs = new HashSet[(Constituent, Constituent)]()

      val examples = MentionExamplesToCorefExamplesTransformer.toPairwiseCorefExamples(mentionExample, maxPairwiseDistance)

      for (example <- examples) {
        //TODO: refactor for better handling with singletons - check CorefMultipleClassifier first
        if (example.size() != 2) {
          logger.error("Examples for pairwise coreference classifier must be of length 2!")
          sys.exit(-1)
        }

        pairs.add((example(0).asInstanceOf[Constituent].oldConstituent, example(0).asInstanceOf[Constituent].oldConstituent)) //for singletons

        if (example(0).asInstanceOf[Constituent].oldConstituent(learnLabelType).equals(example(1).asInstanceOf[Constituent].oldConstituent(learnLabelType))) {
          //merge
          pairs.add((example(0).asInstanceOf[Constituent].oldConstituent, example(1).asInstanceOf[Constituent].oldConstituent))
        } else {
          //another singleton if no match
          pairs.add((example(1).asInstanceOf[Constituent].oldConstituent, example(1).asInstanceOf[Constituent].oldConstituent))
        }
      }

    //remove mentionExample references to clusters => to take into account only current results at clustering
    //TODO: try what happens if clusters remain the same
    removeClusterReferences(mentionExample)

    SimpleClusterer.doClustering(pairs)
  }

  //only clusters containing more than 1 mention are returned
  def classifyClusters(mentionExamples: Examples): ArrayBuffer[HashSet[Cluster]] = {
    new ArrayBuffer[HashSet[Cluster]]() ++ mentionExamples.map(v => classifyCluster(v))
  }



  private def removeClusterReferences(mentionExample: Example): Unit = {
    for (constituent <- mentionExample) {
      constituent.asInstanceOf[Constituent].cluster = null
    }
  }

  def classify(example: Example, normalized: Boolean) = {
    throw new NotImplementedException()
  }

  override def classify(examples: Examples) = {
    throw new NotImplementedException()
  }

  def test(examples: Examples): Unit = {
    throw new NotImplementedException()
  }
}
