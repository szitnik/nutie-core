package si.zitnik.research.iobie.core.coreference.util


import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging

import collection.mutable


/**
 * This Mention Detector applies along our things, rules similar to:
 *  Stanford's Multi-Sieve Coreference Resolution System at the CoNLL-2011 Shared Task, Heeyoung Lee, Yves Peirsman, Angel Chang,
 *  Nathanael Chambers, Mihai Surdeanu and Dan Jurafsky, Stanford 2011
 *
 * The mentions are processed only once - at the beginning of iterating. If documentIdentifier stays null,
 * each example is treated as its own document.
 *
 * For learning and inference, these mentions need to be further transformed into Coref Examples. This class generates
 * training examples for arbitrary mentions. Each mention is represented as a token within an Example!!!!
 */
class MentionExamplesBuilder(
         val examples: Examples,
         val neTagsToGenerateConstituentsFrom: scala.collection.immutable.Set[String],
         val documentIdentifier: ExampleLabel.Value = null) extends StrictLogging {

  /**
   * Method returns consecutive constituents that represent mentions. These mentions are automatically detected
   * from text structure.
   * @return
   */
  def detectAndBuild(): Examples = {
    val constituentsGetter = (example: Example) => {
      example.get(ExampleLabel.PARSE_TREE).getConstituents(example, "NP") ++
      example.getLabelingConstituents(Label.NE, neTagsToGenerateConstituentsFrom)
    }

    genericBuilder(constituentsGetter)
  }

  /**
   * Method returns mention Examples, built from constituents in examples.
   * @return
   */
  def buildFromTagged() : Examples = {
    genericBuilder((example: Example) => example.getAllMentions().map(Constituent.clone(_)), false)
  }

  def genericBuilder(constituentsGetter: (Example)=>mutable.Buffer[Constituent], clean: Boolean = true): Examples = {
    val mentionExamples = new Examples()

    //separation into groups of examples
    val blocks = examples.splitExamplesByDocuments(documentIdentifier).values

    for (block <- blocks) {
      var mentionExample = new Example()
      //transfer DOC_ID
      if (documentIdentifier != null) {
        mentionExample.put(documentIdentifier, block(0).get(documentIdentifier))
      }

      //fill tokens
      for (example <- block) {
        val constituents = constituentsGetter(example)

        if (clean) {
          cleanConstituents(constituents) //applying stanford rules
        }
        java.util.Collections.sort(constituents)

        mentionExample.addAll(constituents)
      }

      if (mentionExample.size() > 0) {
        mentionExamples.add(mentionExample)
      }
    }

    mentionExamples
  }

  private def cleanConstituents(constituents: mutable.Buffer[Constituent]): Unit = {
    val temp = new ArrayList[Constituent]()

    //1. We remove a mention if a larger mention with the same head word exists, e.g., we remove The five insurance
    // companies in The five insurance companies approved to be established this time.
    for ((node1,idx1) <- constituents.zipWithIndex) {
      var addNode1 = true
      for ((node2,idx2) <- constituents.zipWithIndex) {
        if (node1 != node2 && node1.startIdx == node2.startIdx && node1.getText().length < node2.getText().length) {
          addNode1 = false
        } else if (node1 != node2 && node1.startIdx == node2.startIdx && node1.getText().length == node2.getText().length && idx1 < idx2) {
          //preserve one of the duplicates
          addNode1 = false
        }
      }
      if (addNode1) {
        temp.add(node1)
      }
    }
    constituents.clear()
    constituents.addAll(temp)
    temp.clear()

    //2. We discard numeric entities such as percents, money, cardinals, and quantities, e.g., 9%, $10, 000, Tens of thousands, 100 miles.
    //TODO

    //3. We remove mentions with partitive or quanti- fier expressions, e.g., a total of 177 projects.
    //TODO

    //4. We remove pleonastic it pronouns, detected us- ing a set of known expressions, e.g., It is possi- ble that.
    //TODO

    //5. We discard adjectival forms of nations, e.g., American.
    //TODO

    //6. We remove stop words in a predetermined list of 8 words, e.g., there, ltd., hmm.
    //TODO
  }

}
