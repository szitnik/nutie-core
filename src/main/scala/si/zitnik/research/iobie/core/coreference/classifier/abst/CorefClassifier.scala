package si.zitnik.research.iobie.core.coreference.classifier.abst

import si.zitnik.research.iobie.algorithms.crf.Classifier
import si.zitnik.research.iobie.domain.{Examples, Example}
import collection.mutable.{ArrayBuffer, HashSet}
import si.zitnik.research.iobie.domain.cluster.Cluster

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/21/12
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class CorefClassifier extends Classifier {
  def classifyCluster(mentionExample: Example): HashSet[Cluster]
  def classifyClusters(mentionExamples: Examples): ArrayBuffer[HashSet[Cluster]]
}
