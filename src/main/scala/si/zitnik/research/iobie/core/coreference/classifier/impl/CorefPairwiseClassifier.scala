package si.zitnik.research.iobie.core.coreference.classifier.impl

import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.{Label, Classifier}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesToCorefExamplesTransformer
import si.zitnik.research.iobie.core.coreference.clustering.impl.SimpleClusterer
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics
import collection.mutable._
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier

/**
 * Multiple Coref. classifier with clustering.
 *
 * Examples must be separated by by DOC_ID (like for learning) or something else. Each example consists of one-document constituents.
 */

class CorefPairwiseClassifier(
        classifier: Classifier,
        learnLabelType: Label.Value,
        maxPairwiseDistance: Int) extends CorefClassifier {


  /**
   * In the results there are only clusters that contain at least two mentions.
   * @param mentionExample
   * @return
   */
  def classifyCluster(mentionExample: Example): HashSet[Cluster] = {
    val pairs = new HashSet[(Constituent, Constituent)]()

      val examples = MentionExamplesToCorefExamplesTransformer.toPairwiseCorefExamples(mentionExample, maxPairwiseDistance)
      val labelings = classifier.classify(examples)

      for ((example, labeling) <- examples.zip(labelings._1)) {
        //TODO: refactor for better handling with singletons - check CorefMultipleClassifier first
        pairs.add((example(0).asInstanceOf[Constituent].oldConstituent, example(0).asInstanceOf[Constituent].oldConstituent)) //for singletons
        for (i <- 1 until(labeling.size)) {
          if (labeling(i).equals("C")) {
            //old constituents are constituents built by MentionExamplesBuilder
            pairs.add((example(i-1).asInstanceOf[Constituent].oldConstituent, example(i).asInstanceOf[Constituent].oldConstituent))
          } else { //may be singleton
            pairs.add((example(i).asInstanceOf[Constituent].oldConstituent, example(i).asInstanceOf[Constituent].oldConstituent))
          }
        }
      }

    //remove mentionExample references to clusters => to take into account only current results at clustering
    //TODO: try what happens if clusters remain the same
    removeClusterReferences(mentionExample)

    SimpleClusterer.doClustering(pairs)
  }

  //only clusters containing more than 1 mention are returned
  def classifyClusters(mentionExamples: Examples): ArrayBuffer[HashSet[Cluster]] = {
    new ArrayBuffer[HashSet[Cluster]]() ++ mentionExamples.map(v => classifyCluster(v))
  }



  private def removeClusterReferences(mentionExample: Example): Unit = {
    for (constituent <- mentionExample) {
      constituent.asInstanceOf[Constituent].cluster = null
    }
  }

  def classify(example: Example, normalized: Boolean) = {
    throw new NotImplementedException()
  }

  override def classify(examples: Examples) = {
    throw new NotImplementedException()
  }

  def test(examples: Examples): Unit = {
      val pairExamples = new Examples()
      examples.foreach(v => pairExamples.addAll(MentionExamplesToCorefExamplesTransformer.toPairwiseCorefExamples(v)))
      new Statistics(classifier, pairExamples).printStandardClassification(learnLabelType, "C")
  }

}
