package si.zitnik.research.iobie.core.coreference.learner


import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf._
import si.zitnik.research.iobie.core.coreference.classifier.impl.CorefPairwiseClassifierExactMatch


/**
 *
 * Input FeatureFunctions should not refer to neighbouring examples! That is because one example represents one
 * document and therefore coreferences are independent.
 *
 * The same must comply for input to classifier.
 *
 * Input examples should be built by MentionExamplesBuilder and should contain mentionId as Label.COREF key.
 * -------------------
 * @param learnLabelType
 */
class CorefPairwiseLearnerExactMatch(
          val maxPairwiseDistance: Int = 10,
          val learnLabelType: Label.Value = Label.OBS) extends CorefLearner(null) with StrictLogging {


  def train() = {
    new CorefPairwiseClassifierExactMatch(maxPairwiseDistance, learnLabelType)
  }

}
