package si.zitnik.research.iobie.core.coreference.learner

import si.zitnik.research.iobie.domain.Examples

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf._
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.thirdparty.crfsuite.api.CRFSuiteLCCRFLearner
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesToCorefExamplesTransformer
import si.zitnik.research.iobie.core.coreference.classifier.impl.CorefPairwiseClassifier

/**
 *
 * Input FeatureFunctions should not refer to neighbouring examples! That is because one example represents one
 * document and therefore coreferences are independent.
 *
 * The same must comply for input to classifier.
 *
 * -------------------
 *
 * Input examples should be built by MentionExamplesBuilder and should contain mentionId as Label.COREF key.
 * @param examples
 * @param featureFunctions
 * @param learnLabelType
 */
class CorefPairwiseLearner(
          examples: Examples,
          val featureFunctions: ArrayList[FeatureFunction],
          val learnLabelType: Label.Value = Label.COREF,
          val modelSaveFilename: String = "coref_p_model",
          val epochs: Option[Int] = None,
          val maxPairwiseDistance: Int = 10) extends CorefLearner(examples) with StrictLogging {


  def train() = {
    val pairExamples = new Examples()
    examples.foreach(v => pairExamples.addAll(MentionExamplesToCorefExamplesTransformer.toPairwiseCorefExamples(v, maxPairwiseDistance)))
    val classifier = new CRFSuiteLCCRFLearner(pairExamples, learnLabelType, featureFunctions, modelSaveFilename+".obj", maxIterations = epochs).train()

    new CorefPairwiseClassifier(classifier, learnLabelType, maxPairwiseDistance)
  }

  def trainAndTest(epochsBetweenTest: Int = 5, allEpochs: Int = 50, testMentionExamples: Examples = examples): CorefPairwiseClassifier = {
    var classifier: CorefPairwiseClassifier = null

    for (epoch <- 1 to math.max(allEpochs / epochsBetweenTest, 1)) {
      classifier = train()
      logger.info("Training perf:")
      classifier.test(testMentionExamples)

      if (testMentionExamples != examples) {
        logger.info("Testing perf:")
        classifier.test(testMentionExamples)
      }
    }

    classifier
  }

}
