package si.zitnik.research.iobie.core.relationship.learner

import si.zitnik.research.iobie.domain.Examples
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf._
import si.zitnik.research.iobie.thirdparty.crfsuite.api.CRFSuiteLCCRFLearner
import si.zitnik.research.iobie.core.relationship.classifier.impl.RelationshipMultipleClassifier
import si.zitnik.research.iobie.core.relationship.util.ExamplesToRelationshipExamplesTransformer

import scala.collection.JavaConversions._
import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.relationship.Relationship

import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/7/13
 * Time: 1:15 PM
 * To change this template use File | Settings | File Templates.
 */
class RelationshipMultipleLearner(
                                   examples: Examples,
                                   val featureFunctions: ArrayList[FeatureFunction],
                                   val learnLabelType: Label.Value = Label.REL,
                                   val skipNumbers: Array[Int] = Array(0,1,2,3),
                                   val modelSaveFilename: String = "rel_model",
                                   val epochs: Option[Int] = None) extends Learner(examples) with StrictLogging {

  private var learnData = examples.map(e => (e.getAllMentions(), e.getAllRelationships())).toBuffer

  def useWithSpecificMentionsAndRelationships(learnData: mutable.Buffer[(ArrayBuffer[Constituent], ArrayBuffer[Relationship])]): Unit = {
    this.learnData = learnData
  }

  def train() = {


    val classifiers = new Array[Classifier](skipNumbers.length)

    //create example sets & train classifiers
    for ((skipNumber, id) <- skipNumbers.zipWithIndex) {
      val relationshipExamples = new Examples()
      learnData.foreach(v => relationshipExamples.add(ExamplesToRelationshipExamplesTransformer.toRelationshipExamples(v._1, v._2, skipNumber)))

      //debug output
      //println("RelationshipMultipleLearner")
      //val set = new mutable.HashSet[String]()
      //relationshipExamples.foreach(_.foreach(v => set.add(v.get(Label.REL).toString)))
      //println(set)
      //debug output

      classifiers(id) = new CRFSuiteLCCRFLearner(relationshipExamples, learnLabelType, featureFunctions, modelSaveFilename+"_"+id+".obj", maxIterations = epochs).train()
    }

    new RelationshipMultipleClassifier(classifiers, skipNumbers, learnLabelType)
  }

  def trainAndTest(epochsBetweenTest: Int = 5, allEpochs: Int = 50, testRelationshipExamples: Examples = examples): RelationshipMultipleClassifier = {
    var classifier: RelationshipMultipleClassifier = null

    for (epoch <- 1 to math.max(allEpochs / epochsBetweenTest, 1)) {
      classifier = train()
      logger.info("Training perf:")
      classifier.test(testRelationshipExamples)

      if (testRelationshipExamples != examples) {
        logger.info("Testing perf:")
        classifier.test(testRelationshipExamples)
      }
    }

    classifier
  }
}
