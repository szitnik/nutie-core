package si.zitnik.research.iobie.core.ner.test

import si.zitnik.research.iobie.datasets.chemdner2013.{Chemdner2013DatasetType, Chemdner2013Importer}
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.core.ner.mention.learner.NERMentionLearner
import si.zitnik.research.iobie.algorithms.crf.feature.packages.FeatureFunctionPackages
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, FeatureFunction, Label}
import java.util.ArrayList

import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.thirdparty.opennlp.api.{ChunkTagger, PoSTagger}

import collection.mutable

import scala.collection.JavaConversions._
import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.util.AdderMap
import java.io.{File, PrintWriter}

import si.zitnik.research.iobie.core.ner.mention.classifier.impl.NERMentionClassifier
import weka.core._
import weka.core.converters.ArffSaver
import weka.classifiers.functions.LibSVM
import com.typesafe.scalalogging.StrictLogging

import scala.Option
import si.zitnik.research.iobie.thirdparty.biolemmatizer.api.BioLemmaTagger

/**
 *   
 *     
 * author: Slavko Žitnik
 *     
 * version: 11.09.2013, 09:40
 * version: 1.0.0
 * since: 1.0.0
 */
object Chemdner2013Evaluation extends StrictLogging {
  val tokenTagsToInclude = Set(
    "FAMILY",
    //"NO CLASS",
    "FORMULA",
    "TRIVIAL",
    "IDENTIFIER",
    "ABBREVIATION",
    "MULTIPLE",
    "SYSTEMATIC")
  val mentionTagsToInclude = Set("M")

  def importDevData() = {
    val devData = new Chemdner2013Importer(Chemdner2013DatasetType.development).importForIE()
    new PoSTagger().tag(devData)
    new ChunkTagger().tag(devData)
    new BioLemmaTagger().tag(devData)


    val retVal = new Examples()
    retVal.add(devData)
    retVal
  }

  def importTrainData() = {
    val trainData = new Chemdner2013Importer(Chemdner2013DatasetType.training).importForIE()
    new PoSTagger().tag(trainData)
    new ChunkTagger().tag(trainData)
    new BioLemmaTagger().tag(trainData)

    /*
    val devData = new Chemdner2013Importer(Chemdner2013DatasetType.development).importForIE()
    new PoSTagger().tag(devData)
    new ChunkTagger().tag(devData)
    new BioLemmaTagger().tag(devData)
    */

    val retVal = new Examples()
    retVal.add(trainData)
    //retVal.add(devData)
    retVal
  }

  def importTestData(sublistFromTo: Option[(Int, Int)] = None) = {
    val testData = new Chemdner2013Importer(Chemdner2013DatasetType.development).importForIE(sublistFromTo)
    new PoSTagger().tag(testData)
    new ChunkTagger().tag(testData)
    new BioLemmaTagger().tag(testData)


    val retVal = new Examples()
    retVal.add(testData)
    retVal
  }

  def relabelToMentionExamples(examples: Examples) = {
    examples.foreach(e => {
      //e.printLabeling(Label.NE); println();
      e.foreach(t => {
        if (t.get(Label.NE).asInstanceOf[String].equals("NO CLASS")) {
          t.put(Label.NE, "O")
        }
        if (!t.get(Label.NE).asInstanceOf[String].equals("O")) {
          t.put(Label.NE, "M")
        }
      })
      //e.printLabeling(Label.NE); println();
    })
    examples
  }

  def filterToNPChunks(examples: Examples) = {
    examples.foreach(e => {
      val filtered = e.filter(_.get(Label.CHUNK).asInstanceOf[String].contains("NP"))
      e.clear()
      e.addAll(filtered)
    })
    examples
  }

  def exportCDI(results: AdderMap[String, Constituent], filename: String): Unit = {
    def getConstituentCDIText(constituent: Constituent) = {
      var startIdx = constituent.get(Label.START_IDX).asInstanceOf[Int]
      var endIdx = constituent.example.get(constituent.endIdx-1).get(Label.START_IDX).asInstanceOf[Int] + constituent.example.get(constituent.endIdx-1).get(Label.OBS).asInstanceOf[String].size
      val offset = constituent.example(0).get(Label.START_IDX).asInstanceOf[Int]
      startIdx -= offset
      endIdx -= offset
      val text = constituent.example.get(ExampleLabel.TEXT).asInstanceOf[String].substring(startIdx, endIdx)
      text
    }

    val writer = new PrintWriter(filename, "UTF-8")
    results.map.foreach{ case (docId, constituents) => {
      //remove duplicates
      val map = mutable.HashMap[String, Constituent]()
      for (cons <- constituents) {
        val cdiText = getConstituentCDIText(cons)
        if (map.contains(cdiText)) {
          //maybe update
          val cons1 = map.get(cdiText).get
          if (cons.get(Label.MARGINAL_PROB).asInstanceOf[Double] > cons1.get(Label.MARGINAL_PROB).asInstanceOf[Double]) {
            map.put(cdiText, cons)
          }
        } else {
          map.put(cdiText, cons)
        }
      }

      val newList = map.values.toList.sortWith((a: Constituent, b: Constituent) => a.get(Label.MARGINAL_PROB).asInstanceOf[Double] > b.get(Label.MARGINAL_PROB).asInstanceOf[Double]).zipWithIndex
      newList.foreach{ case (constituent, idx) => {
        val line = "%s\t%s\t%d\t%.2f\n".format(docId, getConstituentCDIText(constituent), idx+1, constituent.get(Label.MARGINAL_PROB).asInstanceOf[Double])
        writer.write(line)
      }}
    }}
    writer.close()
  }

  def exportCEM(results: AdderMap[String, Constituent], filename: String): Unit = {
    val writer = new PrintWriter(filename, "UTF-8")
    results.map.foreach{ case (docId, constituents) => {

      //remove duplicates
      val merged = new mutable.HashMap[(Int,Int), Constituent]()
      constituents.sortWith((a: Constituent, b: Constituent) => a.example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double] < b.example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double]).foreach(c => {
        val startIdx = c.get(Label.START_IDX).asInstanceOf[Int]
        val endIdx = c.example.get(c.endIdx-1).get(Label.START_IDX).asInstanceOf[Int] + c.example.get(c.endIdx-1).get(Label.OBS).asInstanceOf[String].size
        merged.put((startIdx, endIdx), c)
      })

      val newList = merged.values.toList.sortWith((a: Constituent, b: Constituent) => a.example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double] > b.example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double]).zipWithIndex
      newList.foreach{ case (constituent, idx) => {
        val startIdx = constituent.get(Label.START_IDX)
        val endIdx = constituent.example.get(constituent.endIdx-1).get(Label.START_IDX).asInstanceOf[Int] + constituent.example.get(constituent.endIdx-1).get(Label.OBS).asInstanceOf[String].size
        val line = "%s\t%s:%d:%d\t%d\t0.5\n".format(docId, constituent.example.get(ExampleLabel.TYPE), startIdx, endIdx, idx+1)
        writer.write(line)
      }}
    }}
    writer.close()
  }

  def fillResultsMap(tokenFullResults: AdderMap[String, Constituent], e: Example, neLabels: ArrayList[String], neMarginalProbabilities: ArrayList[Double], seqProbability: Double): Unit = {
    //do labeling
    e.setLabeling(Label.L1_NE, neLabels.toArray)
    e.setLabeling(Label.MARGINAL_PROB, neMarginalProbabilities.toArray)
    e.put(ExampleLabel.EXAMPLE_PROB, seqProbability)
    tokenFullResults.put(e.get(ExampleLabel.DOC_ID).asInstanceOf[String], e.getLabelingConstituents(Label.L1_NE, tokenTagsToInclude))
  }

  def getResults(testData: Examples, classifier: NERMentionClassifier): AdderMap[String, Constituent] = {
    val results = new AdderMap[String, Constituent]
    testData.foreach(e => {
      val (neLabels, neMarginalProbabilities, seqProbability) = classifier.classify(e)
      fillResultsMap(results, e, neLabels, neMarginalProbabilities, seqProbability)
    })
    results
  }

  def fillMentionResultsMap(tokenFullResults: AdderMap[String, Constituent], e: Example, neLabels: ArrayList[String], neMarginalProbabilities: ArrayList[Double], seqProbability: Double): Unit = {
    //do labeling
    e.setLabeling(Label.L1_NE, neLabels.toArray)
    e.setLabeling(Label.MARGINAL_PROB, neMarginalProbabilities.toArray)
    e.put(ExampleLabel.EXAMPLE_PROB, seqProbability)
    tokenFullResults.put(e.get(ExampleLabel.DOC_ID).asInstanceOf[String], e.getLabelingConstituents(Label.L1_NE, mentionTagsToInclude))
  }

  def getMentionResults(testData: Examples, classifier: NERMentionClassifier): AdderMap[String, Constituent] = {
    val results = new AdderMap[String, Constituent]
    testData.foreach(e => {
      val (neLabels, neMarginalProbabilities, seqProbability) = classifier.classify(e)
      fillMentionResultsMap(results, e, neLabels, neMarginalProbabilities, seqProbability)
    })
    results
  }

  def intersects(constituent1: Constituent, constituent2: Constituent): Boolean = {
    var startIdx1 = constituent1.get(Label.START_IDX).asInstanceOf[Int]
    var endIdx1 = constituent1.example.get(constituent1.endIdx-1).get(Label.START_IDX).asInstanceOf[Int] + constituent1.example.get(constituent1.endIdx-1).get(Label.OBS).asInstanceOf[String].size

    var startIdx2 = constituent2.get(Label.START_IDX).asInstanceOf[Int]
    var endIdx2 = constituent2.example.get(constituent2.endIdx-1).get(Label.START_IDX).asInstanceOf[Int] + constituent2.example.get(constituent2.endIdx-1).get(Label.OBS).asInstanceOf[String].size

    (startIdx1 until endIdx1).intersect(startIdx2 until endIdx2).size > 0
  }

  def isMention(constituent: Constituent) = {
    if (constituent.get(Label.NE).asInstanceOf[String].equals("O") || constituent.get(Label.NE).asInstanceOf[String].equals("NO CLASS")) {
      false
    } else {
      true
    }
  }

  /**
   * Return value (Constituent, token full marginal prob, token full seq prob, token mention marginal prob, token mention seq prob, mention =1 or not =0)
   * @param results1
   * @param results2
   * @return
   */
  def merge(results1: AdderMap[String, Constituent],
            results2: AdderMap[String, Constituent]) = {
    val allIDs = results1.map.keySet.union(results2.map.keySet)
    val mergedResults = new AdderMap[String, (Constituent, Double, Double, Double, Double, Boolean)]

    for (docId <- allIDs) {
      val constituents1 = results1.map.getOrElse(docId, ArrayBuffer[Constituent]())
      val constituents2 = results2.map.getOrElse(docId, ArrayBuffer[Constituent]())
      var idx1 = 0
      var idx2 = 0

      while (idx1 < constituents1.size && idx2 < constituents2.size) {
        //idx1 at end
        if (idx1 == constituents1.size) {
          for (i <- idx2 until constituents2.size) {
            mergedResults.put(docId, (constituents2(i), 0.0, 0.0, constituents2(i).get(Label.MARGINAL_PROB).asInstanceOf[Double], constituents2(i).example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double], isMention(constituents2(i))))
          }
          idx2 = constituents2.size
        }
        //idx2 at end
        if (idx2 == constituents2.size) {
          for (i <- idx1 until constituents1.size) {
            mergedResults.put(docId, (constituents1(i), constituents1(i).get(Label.MARGINAL_PROB).asInstanceOf[Double], constituents1(i).example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double], 0.0, 0.0, isMention(constituents1(i))))
          }
          idx1 = constituents1.size
        }
        //check to merge
        if (constituents1(idx1).example.get(0).get(Label.START_IDX).asInstanceOf[Int] == constituents2(idx2).example.get(0).get(Label.START_IDX).asInstanceOf[Int] && intersects(constituents1(idx1), constituents2(idx2))) { //merge
          //if it comes here, they are already within the same sentence
          val startIdx = math.min(constituents1(idx1).startIdx, constituents2(idx2).startIdx)
          val endIdx = math.min(math.min(math.max(constituents1(idx1).endIdx, constituents2(idx2).endIdx), constituents2(idx2).example.size()), constituents1(idx1).example.size())
          mergedResults.put(docId, (new Constituent(constituents1(idx1).example, startIdx, endIdx),
            constituents1(idx1).get(Label.MARGINAL_PROB).asInstanceOf[Double], constituents1(idx1).example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double],
            constituents2(idx2).get(Label.MARGINAL_PROB).asInstanceOf[Double], constituents2(idx2).example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double],
            isMention(constituents1(idx1)) || isMention(constituents2(idx2))
            ))
          idx1 += 1
          idx2 += 1
        } else { //copy least to results
          if (constituents1(idx1).get(Label.START_IDX).asInstanceOf[Int] < constituents2(idx2).get(Label.START_IDX).asInstanceOf[Int]) {
            mergedResults.put(docId, (constituents1(idx1), constituents1(idx1).get(Label.MARGINAL_PROB).asInstanceOf[Double], constituents1(idx1).example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double], 0.0, 0.0, isMention(constituents1(idx1))))
            idx1 += 1
          } else {
            mergedResults.put(docId, (constituents2(idx2), 0.0, 0.0, constituents2(idx2).get(Label.MARGINAL_PROB).asInstanceOf[Double], constituents2(idx2).example.get(ExampleLabel.EXAMPLE_PROB).asInstanceOf[Double], isMention(constituents2(idx2))))
            idx2 += 1
          }
        }
      }
    }

    mergedResults
  }


  def buildClassifier(trainData: Examples, featureFunctions: ArrayList[FeatureFunction], modelFilename: String) = {
    new NERMentionLearner(trainData, featureFunctions, Label.NE, modelFilename).train()
  }

  def runClassifier1(ffLevel: Int) = {
    val featureFunctions = FeatureFunctionPackages.standardChemdner2013FFunctions(ffLevel)
    val trainData = importTrainData() //token-based, full
    logger.info("Training classifier 1")
    val tokenFullCRFClassifier = buildClassifier(trainData, featureFunctions, "tokenFull_chemdner_NER_model_"+ffLevel)

    logger.info("Loading test ...")
    var testData = importTestData()
    logger.info("Tagging with classifier 1")
    val tokenFullResults = getResults(testData, tokenFullCRFClassifier)

    exportCDI(tokenFullResults, "temp/CDI_test_tokenFull_"+ffLevel+".txt")
    exportCEM(tokenFullResults, "temp/CEM_test_tokenFull_"+ffLevel+".txt")

    (tokenFullCRFClassifier, tokenFullResults)
  }

  def runClassifier2(ffLevel: Int) = {
    val featureFunctions = FeatureFunctionPackages.standardChemdner2013FFunctions(ffLevel)
    logger.info("Loading training ...")
    val trainData = relabelToMentionExamples(importTrainData()) //token-based, mention
    logger.info("Training classifier 2")
    val tokenMentionCRFClassifier = buildClassifier(trainData, featureFunctions, "tokenMention_chemdner_NER_model_"+ffLevel)

    logger.info("Loading test ...")
    val testData = relabelToMentionExamples(importTestData())
    logger.info("Tagging with classifier 2")
    val tokenMentionResults = getMentionResults(testData, tokenMentionCRFClassifier)

    exportCEM(tokenMentionResults, "temp/CEM_test_tokenMention_"+ffLevel+".txt")
    exportCDI(tokenMentionResults, "temp/CDI_test_tokenMention_"+ffLevel+".txt")

    (tokenMentionCRFClassifier, tokenMentionResults)
  }

  def runClassifier3(ffLevel: Int) = {
    val featureFunctions = FeatureFunctionPackages.standardChemdner2013FFunctions(ffLevel)
    val trainData = filterToNPChunks(importTrainData()) //token-based, full
    logger.info("Training classifier 3")
    val tokenFullCRFClassifier = buildClassifier(trainData, featureFunctions, "tokenFullNP_chemdner_NER_model_"+ffLevel)

    logger.info("Loading test ...")
    var testData = filterToNPChunks(importTestData())
    logger.info("Tagging with classifier 3")
    val tokenFullResults = getResults(testData, tokenFullCRFClassifier)

    exportCDI(tokenFullResults, "temp/CDI_test_tokenFullNP_"+ffLevel+".txt")
    exportCEM(tokenFullResults, "temp/CEM_test_tokenFullNP_"+ffLevel+".txt")

    (tokenFullCRFClassifier, tokenFullResults)
  }

  def runClassifier4(ffLevel: Int) = {
    val featureFunctions = FeatureFunctionPackages.standardChemdner2013FFunctions(ffLevel)
    logger.info("Loading training ...")
    val trainData = relabelToMentionExamples(filterToNPChunks(importTrainData())) //token-based, mention
    logger.info("Training classifier 4")
    val tokenMentionCRFClassifier = buildClassifier(trainData, featureFunctions, "tokenMentionNP_chemdner_NER_model_"+ffLevel)

    logger.info("Loading test ...")
    val testData = relabelToMentionExamples(filterToNPChunks(importTestData()))
    logger.info("Tagging with classifier 4")
    val tokenMentionResults = getMentionResults(testData, tokenMentionCRFClassifier)

    exportCEM(tokenMentionResults, "temp/CEM_test_tokenMentionNP_"+ffLevel+".txt")
    exportCDI(tokenMentionResults, "temp/CDI_test_tokenMentionNP_"+ffLevel+".txt")

    (tokenMentionCRFClassifier, tokenMentionResults)
  }

  def runSeparateClassifiers(ffLevel: Int): Unit = {
    runClassifier1(ffLevel)
    runClassifier2(ffLevel)
    runClassifier3(ffLevel)
    runClassifier4(ffLevel)


  }

  def runBothWithSVM(ffLevel: Int, cls1: (NERMentionClassifier, AdderMap[String, Constituent]), cls2: (NERMentionClassifier, AdderMap[String, Constituent])): Unit = {
    val featureFunctions = FeatureFunctionPackages.standardChemdner2013FFunctions(ffLevel)

    //1. train
    val tokenFullResultsTrain = cls1
    val tokenMentionResultsTrain = cls2

    val mergedResultsTrain = merge(tokenFullResultsTrain._2, tokenMentionResultsTrain._2)

    //transform learning data for SVM
    val attInfo = new FastVector()
    attInfo.addElement(new Attribute("fullMarg"))
    attInfo.addElement(new Attribute("fullSeq"))
    attInfo.addElement(new Attribute("mentionMarg"))
    attInfo.addElement(new Attribute("mentionSeq"))
    val classVals = new FastVector()
    classVals.addElement("M")
    classVals.addElement("O")
    attInfo.addElement(new Attribute("mention", classVals))
    val instances = new Instances("chemdner", attInfo, 0)
    instances.setClassIndex(instances.numAttributes - 1)

    mergedResultsTrain.map.values.foreach(_.foreach(v => {
      val instance = new Instance(instances.numAttributes())
      instance.setDataset(instances)
      instance.setValue(0, v._2)
      instance.setValue(1, v._3)
      instance.setValue(2, v._4)
      instance.setValue(3, v._5)
      instance.setClassValue(if (v._6) "M" else "O")
      instances.add(instance)
    }))

    val saver = new ArffSaver()
    saver.setInstances(instances)
    saver.setFile(new File("temp/chemdner_"+ffLevel+".arff"))
    saver.writeBatch()

    //train SVM
    val svm = new LibSVM()
    logger.info("Training SVM ...")
    svm.buildClassifier(instances)
    logger.info("Training SVM done.")

    //3. label and export
    //classify instances

      var testData = importTestData()
      val tokenFullResultsTest = getResults(testData, tokenFullResultsTrain._1)

      testData = relabelToMentionExamples(importTestData())
      val tokenMentionResultsTest = getMentionResults(testData, tokenMentionResultsTrain._1)

      val mergedResultsTest = merge(tokenFullResultsTest, tokenMentionResultsTest)

    logger.info("SVM inference ...")
      val finalMergedResults = new AdderMap[String, Constituent]
      for (docId <- mergedResultsTest.map.keySet) {
        val buffer = mergedResultsTest.map.get(docId).get
        for (v <- buffer) {

          val instance = new Instance(instances.numAttributes())
          instance.setDataset(instances)
          instance.setValue(0, v._2)
          instance.setValue(1, v._3)
          instance.setValue(2, v._4)
          instance.setValue(3, v._5)

          if (svm.distributionForInstance(instance)(0) > 0.5) {
            finalMergedResults.put(docId, v._1)
          }
        }
        logger.info("End of SVM inference")


      exportCEM(finalMergedResults, "temp/CEM_test_merged_"+ffLevel+".txt")
      exportCDI(finalMergedResults, "temp/CDI_test_merged_"+ffLevel+".txt")
    }
  }

  def main(args: Array[String]): Unit = {
    println("Unigram analysis")
    (0 to 4).foreach(ffLevel => {
      println("Running feature functions, level="+ffLevel)
      println("Running separate analysis")
      val cls1 = runClassifier1(ffLevel)
      val cls2 = runClassifier2(ffLevel)
      runClassifier3(ffLevel)
      runClassifier4(ffLevel)
      println("Running analysis with SVM")
      runBothWithSVM(ffLevel, cls1, cls2)
    })




    /*

    //TEST
    var data = new Chemdner2013Importer(Chemdner2013DatasetType.test).importForIE()
    println(data.getAllDocIds().size)

    //TEST by examples
    val data1 = new Examples()
      data1.addAll(data.filter(e => {
      e.filter(!_.get(Label.NE).asInstanceOf[String].equals("O")).size > 0
    }))
    println(data1.size)
    println(data1.getAllDocIds().size)

    //TEST by whole documents
    val data2 = new Examples()
      data2.addAll(data.getAllDocIds().map(docId => {
      val docExamples = data.getDocumentExamples(docId)
      if (docExamples.filter(e => {e.filter(!_.get(Label.NE).asInstanceOf[String].equals("O")).size > 0}).size > 0) {
        docExamples.toList
      } else {
        List[Example]()
      }
    }).flatten)
    println(data2.size)
    println(data2.getAllDocIds().size)
    */
  }



}
