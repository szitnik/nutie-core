package si.zitnik.research.iobie.core.coreference.test

import java.util
import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.feature.packages.{FeatureFunctionPackages, SloCoreferenceFeatureFunctionPackages}
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, FeatureFunction, Label}
import si.zitnik.research.iobie.core.coreference.analysis.CoreferenceAnalysis
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier
import si.zitnik.research.iobie.core.coreference.classifier.impl.SingletonClassifier
import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer
import si.zitnik.research.iobie.core.coreference.learner._
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.datasets.TEIP5.TEIP5Importer
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.statistics.cluster._

import collection.{SortedMap, mutable}
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.sampling.RandomSampler
import si.zitnik.research.iobie.statistics.test.TTest
import si.zitnik.research.iobie.util.{AdderMap, CounterMap}

import scala.collection.mutable.{ArrayBuffer, HashMap, HashSet}

/**
  * Created by slavkoz on 19/09/2017.
  */
object SSJ500kCoreference {
  var REMOVE_OVERLAPPING_MENTIONS = false
  var REMOVE_VERB_BASED_MENTIONS = false

  def printClusters(real: ArrayBuffer[HashSet[Cluster]], tagged: ArrayBuffer[HashSet[Cluster]]): Unit = {
    //if (real.size <= 10) {
    println("REAL:\n\t"+real)
    println("TAGGED:\n\t"+tagged)
    //}
  }

  def clustersDistribution(examples: Examples): Unit = {
    val mentionExamples = new MentionExamplesBuilder(
      examples,
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()


    // Print some standard info
    println("All mentions: %d".format(mentionExamples.map(_.size()).sum))

    val allRealClusters = TaggedClusterer.doClustering(mentionExamples, ommitSingles = false)

    val singletonsNum = allRealClusters.flatMap(_.filter(_.size == 1)).size
    println(s" Singleton clusters: ${singletonsNum}")

    val counts = new CounterMap[Int]()
    allRealClusters.flatten.foreach(v => counts.put(v.size))
    val clustersDistribution = counts.map

    println("All clusters num: %d".format(clustersDistribution.values.sum))
    println("Cluster size distribution all: \n\t%s".format(SortedMap[Int, Int]() ++ clustersDistribution))

    //Overlapping mentions
    var overlappingCounter = 0
    var overlappingSameCounter = 0
    var appositionCounter = 0
    val betweenTokens = mutable.Set[String]()
    val documentMentionCounts = new CounterMap[Int]()
    val posCounts = new CounterMap[String]()
    for (documentClusters <- allRealClusters) {

      val documentMentions = documentClusters.flatMap(_.toList)
      documentMentionCounts.put(documentMentions.size)

      for (mention1 <- documentMentions) {
        posCounts.put(mention1.get(Label.POS))
        println(s" POS tag: ${mention1.get(Label.POS)}  - mention: ${mention1.getText()}")
        for {
          mention2 <- documentMentions if mention1 != mention2 && mention1.example == mention2.example
        } {
          //Overlapping
          if (mention2.startIdx >= mention1.startIdx && mention2.startIdx < mention1.endIdx) {
            overlappingCounter += 1
            if (mention1.cluster == mention2.cluster) {
              overlappingSameCounter += 1
            }
          }

          //Appositions
          if (mention1.endIdx == mention2.startIdx - 1 && mention1.cluster == mention2.cluster && mention1.example.get(mention1.endIdx, Label.OBS).asInstanceOf[String].length == 1) {
            val betweenToken = mention1.example.get(mention1.endIdx, Label.OBS).asInstanceOf[String]
            println(s" Apposition example: '${mention1.getText()}' - '$betweenToken' - '${mention2.getText()}'")
            betweenTokens.add(betweenToken)
            appositionCounter += 1
          }
        }
      }
    }
    println(s"Overlapping mention pairs: $overlappingCounter")
    println(s"Overlapping mention pairs of same entity: $overlappingSameCounter")
    println(s"Appositions: $appositionCounter, tokens in between: $betweenTokens")

    val documentMentionDistributionSorted = SortedMap[Int, Int]() ++ documentMentionCounts.map
    println("Document mentions distribution:")
    println(" " + documentMentionDistributionSorted.keys.mkString("keys <- c(", ",", ")"))
    println(" " + documentMentionDistributionSorted.values.mkString("values <- c(", ",", ")"))

    val posMentionDistributionSorted = SortedMap[Int, Int]() ++ posCounts.map
    println("POS mentions distribution:")
    println(" " + posMentionDistributionSorted.keys.mkString("keys <- c(", ",", ")"))
    println(" " + posMentionDistributionSorted.values.mkString("values <- c(", ",", ")"))

    println("Cluster mentions distribution:")
    val clustersDistributionSorted = SortedMap[Int, Int]() ++ clustersDistribution
    println(" " + clustersDistributionSorted.keys.mkString("keys <- c(", ",", ")"))
    println(" " + clustersDistributionSorted.values.mkString("values <- c(", ",", ")"))
  }

  def distancesDistribution(examples: Examples): Unit = {
    val mentionExamples = new MentionExamplesBuilder(
      examples,
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()

    var distanceDistribution = CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(mentionExamples)
    println("All mention distance pairs num: %d".format(distanceDistribution.values.sum))
    println("Mention distance distribution all: \n\t%s".format(SortedMap[Int, Int]() ++ distanceDistribution))

    val distanceDistributionSorted = SortedMap[Int, Int]() ++ distanceDistribution
    println(distanceDistributionSorted.keys.mkString("keys <- c(", ",", ")"))
    println(distanceDistributionSorted.values.mkString("values <- c(", ",", ")"))
  }

  def checkCleannes(docClusters: ArrayBuffer[HashSet[Cluster]]): Boolean = {
    for {
      document <- docClusters
      cluster <- document
    } {
      val ints = cluster.map(_.get(Label.COREF).asInstanceOf[Int]).toSet
      if (ints.size != 1) {
        return false
      }
    }
    return true
  }

  def clusterResults(classifier: CorefClassifier, mentionExamples: Examples, realExamples: Examples, print: Boolean = true, analyseResults: Boolean = false) = {
    val allRealClusters = TaggedClusterer.doClustering(mentionExamples, ommitSingles = false)
    val allTaggedClusters = classifier.classifyClusters(mentionExamples)

    //printClusters(allRealClusters, allTaggedClusters)


    val F = new ClusterStatistics().stat(allRealClusters, allTaggedClusters)
    if (print)
      println("Results Pairwise: " + F)

    val MUC = MUCClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    if (print)
      println("Results MUC:" + MUC)

    val BCubed = BCubedClusterStatistics.scoreExamples(allRealClusters, allTaggedClusters)
    if (print)
      println("Results BCubed: " + BCubed)

    val CEAFe = CEAFClusterStatistics(CEAFStatisticsType.ENTITY_BASED).scoreExamples(allRealClusters, allTaggedClusters)
    if (print)
      println("Results CEAFe: " + CEAFe)

    val conll2012Official = CoNLL2012Score.scoreExamples(MUC._3, BCubed._3, CEAFe._3)
    if (print)
      println("Results CoNLL2012: " + conll2012Official)

    if (analyseResults) {
      println(s"BEFORE ANALYSIS: Number of real clusters: ${allRealClusters.map(_.size).sum}, number of tagged clusters: ${allTaggedClusters.map(_.size).sum}")
      //Razbijaj
      val documentClustersToAdd = new ArrayBuffer[HashSet[Cluster]]()
      var razbijajCount = 0
      for (documentClusters <- allTaggedClusters) {
        val documentClustersTemp = new mutable.HashSet[Cluster]()

        for (cluster <- documentClusters) {
          var docId = ""
          val adderMap = new AdderMap[Int, Constituent]
          for (constituent <- cluster) {
            adderMap.put(constituent.get(Label.COREF), constituent)
            docId = constituent.example.get(ExampleLabel.DOC_ID)
          }

          println(s" Razbit cluster (${docId}): ")
          //leave first cluster and create new clusters
          for (newConstituentsForCluster <- adderMap.map.values.toList.slice(1, adderMap.map.values.toList.size)) {
            val newCluster = new Cluster()
            newConstituentsForCluster.foreach(newCluster.add(_))
            newConstituentsForCluster.foreach(cluster.remove(_))

            documentClustersTemp.add(newCluster)
            razbijajCount = razbijajCount + 1
            println(s"\t${newCluster}")
          }
          println(s"\t${cluster}")

        }

        documentClustersToAdd.append(documentClustersTemp)
      }

      //Sanity check: if tagged and toAdd are clean
      if (! (checkCleannes(documentClustersToAdd) && checkCleannes(allTaggedClusters)) ) {
        println("ERROR: clusters should be clean!")
        System.exit(-1)
      }

      //Združuj
      var zdruzujCount = 0
      val allTaggedClustersTemp = new ArrayBuffer[HashSet[Cluster]]()
      for (docCls <- allTaggedClusters zip documentClustersToAdd) {
        val documentClusters = new HashSet[Cluster]
        documentClusters.addAll(docCls._1)
        documentClusters.addAll(docCls._2)

        val idToCluster = new HashMap[Int, Cluster]()

        for (cluster <- documentClusters) {
          if (idToCluster.contains(cluster.toList.get(0).get(Label.COREF).asInstanceOf[Int])) {
            val clusterToMerge = idToCluster.get(cluster.toList.get(0).get(Label.COREF).asInstanceOf[Int]).get
            cluster.foreach(clusterToMerge.add(_))
            zdruzujCount = zdruzujCount + 1
          } else {
            idToCluster.put(cluster.toList.get(0).get(Label.COREF).asInstanceOf[Int], cluster)
          }
        }

        val tempClusters = new mutable.HashSet[Cluster]()
        tempClusters.addAll(idToCluster.values)
        allTaggedClustersTemp.append(tempClusters)
      }
      allTaggedClusters.clear()
      allTaggedClusters.addAll(allTaggedClustersTemp)

      println(s"Razbij count: ${razbijajCount}, združuj count: ${zdruzujCount}")
      println(s"AFTER ANALYSIS: Number of real clusters: ${allRealClusters.map(_.size).sum}, number of tagged clusters: ${allTaggedClusters.map(_.size).sum}")
    }

    (MUC._3, BCubed._3, CEAFe._3)
  }

  def transformAndEval(evaluationName: String, featureFunctionSet: ArrayList[FeatureFunction], examples: Examples, examplesTest: Examples): Unit = {
    val mentionExamples = new MentionExamplesBuilder(
      examples,
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    println("TRAIN MENTION DATA:")
    mentionExamples.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)
    val mentionExamplesTest = new MentionExamplesBuilder(
      examplesTest,
      neTagsToGenerateConstituentsFrom = Set(),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()
    println("TEST MENTION DATA:")
    mentionExamplesTest.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.COREF), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true)

    //Removal of overlapping mentions of the same entity
    if (REMOVE_OVERLAPPING_MENTIONS) {
      val mentionsToRemove = new ArrayBuffer[Constituent]()
      for (example <- mentionExamples ++ mentionExamplesTest) {
        for (mention1 <- example) {
          for {
            mention2 <- example if mention1 != mention2 && mention1.asInstanceOf[Constituent].example == mention2.asInstanceOf[Constituent].example
          } {
            //Overlapping
            if (mention2.startIdx >= mention1.startIdx && mention2.startIdx < mention1.endIdx) {
              if (mention1.asInstanceOf[Constituent].get(Label.COREF).asInstanceOf[Int] == mention2.asInstanceOf[Constituent].get(Label.COREF).asInstanceOf[Int]) {
                mentionsToRemove.append(mention2.asInstanceOf[Constituent])
              }
            }
          }
        }
      }
      for (mention <- mentionsToRemove) {
        for (example <- mentionExamples ++ mentionExamplesTest) {
          example.asInstanceOf[Example].remove(mention)
        }
      }
      println(s"Overlapping mentions removed: ${mentionsToRemove.size}")
    }

    //Removal of verb-based mentions
    if (REMOVE_VERB_BASED_MENTIONS) {
      println(s"All mentions num before verb-based one removal: ${(mentionExamples ++ mentionExamplesTest).map(_.size).sum}")
      val mentionsToRemove = new ArrayBuffer[Constituent]()
      for (example <- mentionExamples ++ mentionExamplesTest) {
        for (mention1 <- example) {
          if (mention1.asInstanceOf[Constituent].get(Label.POS).asInstanceOf[String].equals("glagol")) {
            mentionsToRemove.append(mention1.asInstanceOf[Constituent])
          }
        }
      }
      for (mention <- mentionsToRemove) {
        for (example <- mentionExamples ++ mentionExamplesTest) {
          example.asInstanceOf[Example].remove(mention)
        }
      }
      println(s"All mentions num after verb-based one removal: ${(mentionExamples ++ mentionExamplesTest).map(_.size).sum}")
    }

    //Do runners
    println("*******")
    println("Doing runners package \"%s\"".format(evaluationName))
    println("*******")

    var classifier: CorefClassifier = null

    // Singleton classifier
    /*
    println("Singleton classifier")
    classifier = new CorefSingletonLearner().train()
    clusterResults(classifier, mentionExamplesTest, examplesTest)
    */

    // All in one classifier
    /*
    println("All in one classifier")
    classifier = new CorefAllInOneLearner().train()
    clusterResults(classifier, mentionExamplesTest, examplesTest)
    */

    // EXACT MATCH Classifier
    /*
    println("Exact match classifier")
    for (length <- 29 to 29) {
      //println("Doing length: " + length)
      classifier = new CorefPairwiseLearnerExactMatch(maxPairwiseDistance = length, learnLabelType = Label.COREF).train()
      val results = clusterResults(classifier, mentionExamplesTest, examplesTest, print = true)
      println(s"$length;${results._1};${results._2};${results._3}")
    }
    */

    // Pairwise CRF
    /*
    println("Pairwise CRF classifier")
    classifier = new CorefPairwiseLearner(
      mentionExamples,
      featureFunctions = featureFunctionSet,
      modelSaveFilename = evaluationName + "_PAIRWISE",
      maxPairwiseDistance = 29
    ).train()
    clusterResults(classifier, mentionExamplesTest, examplesTest)
    */


    // SkipCor algorithm
    println("SkipCor classifier")
    val distribution = CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(mentionExamples).keySet.toList.sorted
    println("Max distance: %d".format(distribution.last.toInt))

    val minSkipMentions = 29
    val maxSkipMentions = 29 //math.min(10, distribution.last) TODO: change for decided max value
    for (idx <- minSkipMentions to maxSkipMentions) {
      val classifier = new CorefMultipleLearner(
        mentionExamples,
        featureFunctions = featureFunctionSet,
        skipNumbers = distribution.take(idx).toArray,
        modelSaveFilename = evaluationName + "_MULTIPLE"
      ).train()

      println(idx)
      //clusterResults(classifier, mentionExamples, examples) // = train=test results
      clusterResults(classifier, mentionExamplesTest, examplesTest, analyseResults = true)
    }



  }

  def evaluate(evaluationName: String, dataProvider: Data, featureFunctionSet: ArrayList[FeatureFunction]): Unit = {
    val examples = dataProvider.importTrainData()
    println("TRAIN RAW DATA: ")
    //examples.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.PARSE_NODE, Label.REL, Label.COREF, Label.LEMMA), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true, ommitedMentionAttributes = Array(Label.OBS, Label.COREF, Label.EXTENT))
    val examplesTest = dataProvider.importTestData()
    println("TEST RAW DATA: ")
    //examplesTest.printStatistics(ommited = Array(Label.EXTENT, Label.OBS, Label.PARSE_NODE, Label.REL, Label.COREF, Label.LEMMA), ommitedExample = Array(ExampleLabel.DOC_ID), ommitMentions = true, ommitedMentionAttributes = Array(Label.OBS, Label.COREF, Label.EXTENT))

    transformAndEval(evaluationName, featureFunctionSet, examples, examplesTest)
  }

  def main(args: Array[String]): Unit = {

    SSJ500kData.reload()

    //val examples = SSJ500kData.importAllData()
    //examples.printStatistics(ommitMentions = false, ommitedExample = Array())
    //clustersDistribution(examples)
    //distancesDistribution(examples)

    // Dataset check - set exactmatch to see COREF label and go from 0 to 44
    //transformAndEval("TEST", new util.ArrayList[FeatureFunction](), examples, examples)


    //clustersDistribution(SSJ500kData.importAllData())
    //distancesDistribution(SSJ500kData.importAllData())

    //FULL EVALUATION
    SSJ500kData.reload()
    evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.allSloCorefFeatureFunctions)

    //SEPARATE FEATURE FUNCTIONS EVALUATION
    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.stringSloCorefFFunctions)

    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.lexicalSloCorefFFunctions)

    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.semanticSloCorefFFunctions)

    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.distanceSloCorefFFunctions)

    //SSJ500kData.reload()
    //var functions = SloCoreferenceFeatureFunctionPackages.stringSloCorefFFunctions
    //functions.addAll(SloCoreferenceFeatureFunctionPackages.lexicalSloCorefFFunctions)
    //evaluate("SSJ500K - Evaluations", SSJ500kData, functions)

    //SSJ500kData.reload()
    //functions = SloCoreferenceFeatureFunctionPackages.stringSloCorefFFunctions
    //functions.addAll(SloCoreferenceFeatureFunctionPackages.lexicalSloCorefFFunctions)
    //functions.addAll(SloCoreferenceFeatureFunctionPackages.semanticSloCorefFFunctions)
    //evaluate("SSJ500K - Evaluations", SSJ500kData, functions)

    //SSJ500kData.reload()
    //functions = SloCoreferenceFeatureFunctionPackages.stringSloCorefFFunctions
    //functions.addAll(SloCoreferenceFeatureFunctionPackages.lexicalSloCorefFFunctions)
    //functions.addAll(SloCoreferenceFeatureFunctionPackages.semanticSloCorefFFunctions)
    //functions.addAll(SloCoreferenceFeatureFunctionPackages.distanceSloCorefFFunctions)
    //evaluate("SSJ500K - Evaluations", SSJ500kData, functions)

    //NO OVERLAPPING ONES
    //REMOVE_OVERLAPPING_MENTIONS = true
    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.allSloCorefFeatureFunctions)
    //REMOVE_OVERLAPPING_MENTIONS = false

    //NO VERBS
    //REMOVE_VERB_BASED_MENTIONS = true
    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.allSloCorefFeatureFunctions)
    //REMOVE_VERB_BASED_MENTIONS = false

    //NO OVERLAPPING ONES AND NO VERBS
    //REMOVE_OVERLAPPING_MENTIONS = true
    //REMOVE_VERB_BASED_MENTIONS = true
    //SSJ500kData.reload()
    //evaluate("SSJ500K - Evaluations", SSJ500kData, SloCoreferenceFeatureFunctionPackages.allSloCorefFeatureFunctions)
    //REMOVE_VERB_BASED_MENTIONS = false
    //REMOVE_OVERLAPPING_MENTIONS = false

  }

}


object SSJ500kData extends Data {
  private val ssj500kFilename = "/Users/slavkoz/OneDrive - Univerza v Ljubljani/Datasets/ssj500k/ssj500k-sl.xml"
  private val sloveneCoreferencesFolder = "/Users/slavkoz/OneDrive - Univerza v Ljubljani/Datasets/ssj500k/ssj500-coreference-dataset"

  private var data: (Examples, Examples) = null
  private var allData: Examples = null

  def reload(): Unit = {
    allData = TEIP5Importer.getSloveneCoreferenceResolutionDataset(ssj500kFilename, sloveneCoreferencesFolder)

    data = RandomSampler.sample(allData, randomSeed = 42)
  }



  def importTrainData() = data._1
  def importTestData() = data._2
  def importAllData() = allData
}
