package si.zitnik.research.iobie.core.relationship.classifier.impl

import si.zitnik.research.iobie.algorithms.crf.{Classifier, Label}
import si.zitnik.research.iobie.core.relationship.classifier.abst.RelationshipClassifier
import si.zitnik.research.iobie.domain.{Example, Examples}
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import collection.mutable._
import si.zitnik.research.iobie.domain.relationship.Relationship
import si.zitnik.research.iobie.core.relationship.util.ExamplesToRelationshipExamplesTransformer
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics
import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/7/13
 * Time: 1:18 PM
 * To change this template use File | Settings | File Templates.
 */
class RelationshipMultipleClassifier(
                                      classifiers: Array[Classifier],
                                      skipNumbers: Array[Int],
                                      learnLabelType: Label.Value) extends RelationshipClassifier {

  def classifyRelationships(example: Example): HashSet[Relationship] = {
    val retVal = new HashSet[Relationship]()

    //create example set & classify
    for ((skipNumber, classifier) <- skipNumbers.zip(classifiers)) {
      val relationshipExamples = ExamplesToRelationshipExamplesTransformer.toRelationshipExamples(example.getAllMentions(), ArrayBuffer[Relationship](), skipNumber)
      val labelings = classifier.classify(relationshipExamples)

      for ((relationshipExample, labeling) <- relationshipExamples.zip(labelings._1)) {
        for (i <- 1 until labeling.size) {
          if (!labeling(i).equals("O")) {
            retVal.add(new Relationship(null, labeling(i), relationshipExample(i-1).asInstanceOf[Constituent].oldConstituent, relationshipExample(i).asInstanceOf[Constituent].oldConstituent))
          }
        }
      }
    }


    retVal
  }

  def classifyRelationships(testData: ArrayBuffer[Constituent]): HashSet[Relationship] = {
    classifyRelationships(testData, 0.0, 0.0)
  }

  def classifyRelationships(testData: ArrayBuffer[Constituent], tokenThreshold: Double, sequenceThreshold: Double) = {
    val retVal = new HashSet[Relationship]()

    //create example set & classify
    for ((skipNumber, classifier) <- skipNumbers.zip(classifiers)) {
      val relationshipExamples = ExamplesToRelationshipExamplesTransformer.toRelationshipExamples(testData, ArrayBuffer[Relationship](), skipNumber)
      val labelings = classifier.classify(relationshipExamples)

      for (idx <- 0 until relationshipExamples.size) {
        val (relationshipExample, labeling, tokenProbs, seqProb) = (relationshipExamples.get(idx), labelings._1.get(idx), labelings._2.get(idx), labelings._3.get(idx))
        for (i <- 1 until labeling.size) {
          if (!labeling(i).equals("O") && seqProb >= sequenceThreshold && tokenProbs(i) >= tokenThreshold) {
            retVal.add(new Relationship(null, labeling(i), relationshipExample(i-1).asInstanceOf[Constituent].oldConstituent, relationshipExample(i).asInstanceOf[Constituent].oldConstituent))
          }
        }
      }
    }


    retVal
  }

  def classifyRelationships(examples: Examples): ArrayBuffer[HashSet[Relationship]] = {
    new ArrayBuffer[HashSet[Relationship]]() ++ examples.map(v => classifyRelationships(v))
  }

  def classifyRelationships(testData: mutable.Buffer[ArrayBuffer[Constituent]], tokenThreshold: Double = 0.0, sequenceThreshold: Double = 0.0): ArrayBuffer[HashSet[Relationship]] = {
    //debug output
    //for (i <- 0 until testData.length) {
    //  if (testData(i)(0).example.get(ExampleLabel.DOC_ID).toString.equals("PMID-11069677-S3")) {
    //    val unclassified = testData
    //    val classified = testData.map(v => classifyRelationships(v))
    //    println()
    //  }
    //}
    //end

    new ArrayBuffer[HashSet[Relationship]]() ++ testData.map(v => classifyRelationships(v, tokenThreshold, sequenceThreshold))
  }



  def classify(example: Example, normalized: Boolean) = {
    throw new NotImplementedException()
  }

  override def classify(examples: Examples) = {
    throw new NotImplementedException()
  }

  def test(examples: Examples): Unit = {
    //create example sets & test classifiers
    for ((skipNumber, id) <- skipNumbers.zipWithIndex) {
      val relationshipExamples = new Examples()
      examples.foreach(v => relationshipExamples.add(ExamplesToRelationshipExamplesTransformer.toRelationshipExamples(v, skipNumber)))
      println("Skip number %d test:".format(skipNumber))
      new Statistics(classifiers(id), relationshipExamples).printAllStat(learnLabelType)
    }
  }

  def test(testData: mutable.Buffer[(ArrayBuffer[Constituent], ArrayBuffer[Relationship])]): Unit = {
    //create example sets & test classifiers
    for ((skipNumber, id) <- skipNumbers.zipWithIndex) {
      val relationshipExamples = new Examples()
      testData.foreach(v => relationshipExamples.add(ExamplesToRelationshipExamplesTransformer.toRelationshipExamples(v._1, v._2, skipNumber)))
      println("Skip number %d test:".format(skipNumber))
      new Statistics(classifiers(id), relationshipExamples).printAllStat(learnLabelType)
    }
  }


}
