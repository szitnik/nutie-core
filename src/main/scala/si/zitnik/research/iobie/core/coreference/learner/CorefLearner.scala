package si.zitnik.research.iobie.core.coreference.learner

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Classifier
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier
import si.zitnik.research.iobie.domain.Examples

abstract class CorefLearner(examples: Examples) extends StrictLogging {

  def train: CorefClassifier
}
