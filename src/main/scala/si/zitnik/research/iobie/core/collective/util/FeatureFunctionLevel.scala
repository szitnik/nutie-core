package si.zitnik.research.iobie.core.collective.util

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 6/15/12
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */

object FeatureFunctionLevel extends Enumeration {
  val LEVEL_1 = Value("L1")
  val LEVEL_2 = Value("L2")
}