package si.zitnik.research.iobie.core.coreference.clustering.impl

import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.{Examples, Example}
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.IOBIEConversions._
import scala.collection.JavaConversions._
import collection.mutable._

/**
 * This class clusters tagged mentions in mention examples type of Examples.
 */
object TaggedClusterer {


  /**
   * Example must be of type mentionExample and tokens (constituents) must have set COREF label,
   * which identifies cluster id.
   * @param mentionExample
   * @return
   */
  def doClustering(mentionExample: Example, ommitSingles: Boolean): HashSet[Cluster] = {
    val idToCluster = new HashMap[Int, Cluster]()
    val retVal = new HashSet[Cluster]()

    for (token <- mentionExample) {
      val id = token.get(Label.COREF)
      idToCluster.get(id) match {
        case Some(cluster) => {
          cluster.add(token)
        }
        case None => {
          val cluster = new Cluster(token)
          idToCluster.put(id, cluster)
          retVal.add(cluster)
        }
      }
    }

    if (ommitSingles) {
      retVal.filter(_.size > 1)
    } else {
      retVal
    }
  }

  def doClustering(mentionExamples: Examples, ommitSingles: Boolean = false): ArrayBuffer[HashSet[Cluster]] = {
    val retVal = new ArrayBuffer[HashSet[Cluster]]()

    for (mentionExample <- mentionExamples) {
      retVal += doClustering(mentionExample, ommitSingles)
    }

    retVal
  }
}
