package si.zitnik.research.iobie.core.coreference.test

import si.zitnik.research.iobie.datasets.conll2012._
import scala.Array
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesBuilder
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction, ExampleLabel}
import si.zitnik.research.iobie.core.coreference.learner.CorefMultipleLearner
import java.util
import util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.core.coreference.clustering.impl.TaggedClusterer

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/1/12
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */

object CorefMultipleLearnerTest {


  def main(args: Array[String]): Unit = {
    //STEP 1
    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2012/dataset/v1/conll-2012/v1/domain"
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = CoNLL2012ImporterDatasetTypeEnum.DEVELOPMENT,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS),
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    val rawExamples = conll2012Importer.importForIE()

    //STEP 2
    val mentionExamples = new MentionExamplesBuilder(
      rawExamples,
      neTagsToGenerateConstituentsFrom = Set("NE"),
      documentIdentifier = ExampleLabel.DOC_ID).buildFromTagged()


    //test just for fun :) :
    //mentionExamples.printStatistics()
    //val f = MentionStatistics.multiDocumentStat(rawExamples.getAllMentions(), mentionExamples.splitExamplesByDocuments())
    //println("Avg. Recall: %.2f, Avg. Precision: %.2f, F-micro: %.2f, F-macro: %.2f".format(f.averageRecall(), f.averagePrecision(), f.microAveragedF(), f.macroAveragedF()))

    //STEP 3
    val featureFunctions = defineFFunctions()


    //STEP 4
    val classifier = new CorefMultipleLearner(mentionExamples, featureFunctions).train();
    println(classifier.classifyClusters(mentionExamples)(0).mkString(" ||| "))
    println(TaggedClusterer.doClustering(mentionExamples(0), ommitSingles = true).mkString(" ||| "))
  }

  def defineFFunctions(): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -1 to 1).generate())


    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, -2 to 2, "UOBS").generate())

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, -1 to 2, "BOBS").generate())

    featureFunctions
  }
}
