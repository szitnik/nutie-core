package si.zitnik.research.iobie.core.coreference.clustering.impl

import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.core.coreference.clustering.abst.Clusterer
import collection.mutable._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 6/14/12
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * This clustering method merges all constituents into clusters, according to pairs.
 * When two pairs are in two different clusters, clusters are merged.
 */
object SimpleClusterer extends Clusterer {

  /**
   * There is no need to wory about mentionIds (when learning, they will be accessible in oldConstituent property of
   * returned constituents)
   * @param pairs
   * @return
   */
  def doClustering(pairs: HashSet[(Constituent, Constituent)]): HashSet[Cluster] = {
    var clusters = new HashSet[Cluster]()

    for ((mention1, mention2) <- pairs) {
      if (mention1.cluster == null && mention2.cluster == null) { //no cluster exists
        clusters.add(new Cluster(mention1, mention2))
      } else if (mention1.cluster == null){ //=> mention2.cluster != null
        mention2.cluster.add(mention1)
      } else if (mention2.cluster == null){ //=> mention1.cluster != null
        mention1.cluster.add(mention2)
      } else if (mention1.cluster != mention2.cluster) { //they are in different clusters
        //update hash codes, otherwise, some clusters will not be found to be deleted
        val tempClusters = new HashSet[Cluster]()
        clusters.map(tempClusters.add(_))
        clusters = tempClusters
        
        val clusterToDelete = mention2.cluster
        clusters.remove(clusterToDelete)
        mention1.cluster.addAll(clusterToDelete.toSeq: _*)
      } //else if: they are already in the same cluster
    }

    clusters
  }

}
