package si.zitnik.research.iobie.core.coreference.learner

import si.zitnik.research.iobie.domain.Examples

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf._
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.thirdparty.crfsuite.api.CRFSuiteLCCRFLearner
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesToCorefExamplesTransformer
import si.zitnik.research.iobie.core.coreference.classifier.impl.CorefMultipleClassifier

/**
 *
 * Input FeatureFunctions should not refer to neighbouring examples! That is because one example represents one
 * document and therefore coreferences are independent.
 *
 * The same must comply for input to classifier.
 *
 * -------------------
 *
 * Input examples should be built by MentionExamplesBuilder and should contain mentionId as Label.COREF key.
 * @param examples
 * @param featureFunctions
 * @param learnLabelType
 * @param skipNumbers
 */
class CorefMultipleLearner(
          examples: Examples,
          val featureFunctions: ArrayList[FeatureFunction],
          val learnLabelType: Label.Value = Label.COREF,
          val skipNumbers: Array[Int] = Array(0,1,2,3),
          val modelSaveFilename: String = "coref_model",
          val epochs: Option[Int] = None) extends CorefLearner(examples) with StrictLogging {


  def train() = {
    val classifiers = new Array[Classifier](skipNumbers.length)

    //create example sets & train classifiers
    for ((skipNumber, id) <- skipNumbers.zipWithIndex) {
      val mentionExamples = new Examples()
      examples.foreach(v => mentionExamples.add(MentionExamplesToCorefExamplesTransformer.toCorefExamples(v, skipNumber)))

      /*
      println("Skip number: %d\n\t".format(skipNumber))
      for (example <- mentionExamples) {
        example.subList(0, example.size()-1).zip(example.subList(1, example.size())).foreach{case (t1, t2) => {
          if (t2.get(Label.COREF).equals("C")) {
            println("\t(%s,%s), ".format(t1.toString, t2.toString))
          }
        }}
      }
      println()
      */

      classifiers(id) = new CRFSuiteLCCRFLearner(mentionExamples, learnLabelType, featureFunctions, modelSaveFilename+"_"+id+".obj", maxIterations = epochs).train()
    }

    new CorefMultipleClassifier(classifiers, skipNumbers, learnLabelType)
  }

  def trainAndTest(epochsBetweenTest: Int = 5, allEpochs: Int = 50, testMentionExamples: Examples = examples): CorefMultipleClassifier = {
    var classifier: CorefMultipleClassifier = null

    for (epoch <- 1 to math.max(allEpochs / epochsBetweenTest, 1)) {
      classifier = train()
      logger.info("Training perf:")
      classifier.test(testMentionExamples)

      if (testMentionExamples != examples) {
        logger.info("Testing perf:")
        classifier.test(testMentionExamples)
      }
    }

    classifier
  }

}
