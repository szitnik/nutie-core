package si.zitnik.research.iobie.core.coreference.classifier.impl

import si.zitnik.research.iobie.algorithms.crf.{Label, Classifier}
import si.zitnik.research.iobie.domain.{Example, Examples}
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesToCorefExamplesTransformer
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics
import scala.collection.JavaConversions._
import collection.mutable.{ArrayBuffer, HashSet}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import java.util
import si.zitnik.research.iobie.core.coreference.classifier.abst.CorefClassifier
import collection.mutable._


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/21/12
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
class CorefAllInOneClassifier(learnLabelType: Label.Value) extends CorefClassifier {

  /**
   * In the results there is every mention in its own cluster.
   * @param mentionExample
   * @return
   */
  def classifyCluster(mentionExample: Example): HashSet[Cluster] = {
    val cluster = new Cluster()
    mentionExample.foreach(c => cluster.add(c.asInstanceOf[Constituent]))
    HashSet[Cluster](cluster)
  }

  //only clusters containing more than 1 mention are returned
  def classifyClusters(mentionExamples: Examples): ArrayBuffer[HashSet[Cluster]] = {
    new ArrayBuffer[HashSet[Cluster]]() ++ mentionExamples.map(v => classifyCluster(v))
  }

  def classify(example: Example, normalized: Boolean) = {
    throw new NotImplementedException()
  }

  override def classify(examples: Examples) = {
    throw new NotImplementedException()
  }

  def test(examples: Examples): Unit = {
    //create example sets & test classifiers
    val mentionExamples = new Examples()
    examples.foreach(v => mentionExamples.add(MentionExamplesToCorefExamplesTransformer.toCorefExamples(v, 0)))
    new Statistics(new AllInOneClassifier(), mentionExamples).printStandardClassification(learnLabelType, "C")
  }
}

private class AllInOneClassifier extends Classifier {
  def classify(example: Example, normalized: Boolean): (util.ArrayList[String], util.ArrayList[Double], Double) = {
    val retVal = new util.ArrayList[String]()

    for (i <- 0 until example.size()) {
      if (i>0) {
        retVal.add("C")
      } else {
        retVal.add("O")
      }
    }

    (retVal, null, 0.0)
  }

  def test(data: Examples): Unit = {}
}
