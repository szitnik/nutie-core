package si.zitnik.research.iobie.runners

import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/29/12
 * Time: 12:55 AM
 * To change this template use File | Settings | File Templates.
 */

//TODO: obsolete, change or remove
object IterativeACE2004 extends StrictLogging {
  val rootPath = "/Users/slavkoz/Documents/DR_Research/Datasets/DataSet-IJCNLP2011/domain sets/"
  val dsNyt = "New York Times.txt"
  val dsWiki = "Wikipedia.txt"
  val numOfEachTaskIters = 500
  val numOfCollectiveIters = 5

  def generalFFunctions(): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -1 to 1).generate())


    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.POS, -2 to 2, "UPOS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, -2 to 2, "UOBS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, -2 to 2, "ULEM").generate())

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, -1 to 2, "BPOS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, -1 to 2, "BOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, -1 to 2, "BLEM").generate())

    featureFunctions
  }

  /*
  def main(args: Array[String]) {
    val mainFolder = "/Volumes/ace_tides_multling_train/domain/English"
    //val allFolders = List(mainFolder+"/bnews", mainFolder+"/nwire", mainFolder+"/fisher_transcripts")
    val allFolders = List(mainFolder + "/bnews")

    val examples = new Examples()
    val docMap = AllImporter.importAll(allFolders)
    for ((dName, (tDoc, annoDoc)) <- docMap) {
      val dsMan = new DatasetManager(tDoc, annoDoc)

      examples.addAll(dsMan.toExamples())
      dsMan.addNAMTagEntities(examples)
      dsMan.addRelTagRelations(examples)
    }

    examples.printLabelingDistribution(Label.NE)


    val posTagger = new PoSTagger()
    posTagger.tag(examples)
    val lemmaTagger = new LemmaTagger()
    lemmaTagger.tag(examples)
    val chunkTagger = new ChunkTagger()
    chunkTagger.tag(examples)


    println("ITERATIVE")
    val learner = new IterativeLearner(
      Map(Label.NE -> generalFFunctions(), Label.REL -> generalFFunctions(), Label.COREF -> generalFFunctions()),
      examples,
      numOfEachTaskIters,
      numOfCollectiveIters)
    val crfClassifier = learner.train()

    val os = new ObjectOutputStream(new FileOutputStream(new java.io.File("").getCanonicalPath() + "/CRF/out/IterativeACE.obj"))
    os.writeObject(crfClassifier)
    os.close()


  }
  */
}
