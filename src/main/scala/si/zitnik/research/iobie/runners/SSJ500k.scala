package si.zitnik.research.iobie.runners

import scala.Array
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.datasets.TEIP5.TEIP5Importer
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics
import si.zitnik.research.iobie.thirdparty.crfsuite.api.CRFSuiteLCCRFLearner

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/14/12
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
object SSJ500k {

  def importData() = {
    val dsSlo = "/Users/slavkoz/Documents/DR_Research/Datasets/ssj500k/ssj500kv1_1.xml"
    val examples = new TEIP5Importer(dsSlo).importForIE()
    //DatasetUtil.toBIO_Tags_Generic(examples1, Label.NE)
    examples
  }


  def createNEFFunctions(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    retVal.add(new BigramDistributionFeatureFunction())
    retVal.add(new UnigramDistributionFeatureFunction())
    retVal.add(new UnigramFeatureFunction(Label.NE, "U=NE"))

    retVal.add(new UnigramFeatureFunction(Label.POS, "U=POS"))
    retVal.add(new UnigramConsecutiveFeatureFunction(Label.POS, "UC=POS"))

    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=PER", Array(GazeteerFeatureFunction.PER)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=LOC", Array(GazeteerFeatureFunction.LOC)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=ORG", Array(GazeteerFeatureFunction.ORG)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=MON", Array(GazeteerFeatureFunction.MONEY)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=JOB", Array(GazeteerFeatureFunction.JOB)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=MEA", Array(GazeteerFeatureFunction.MEASURES)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=CARD", Array(GazeteerFeatureFunction.NUM + "/cardinalNumber.txt")))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=ORD", Array(GazeteerFeatureFunction.NUM + "/ordinalNumber.txt")))
    //retVal.add(new GazeteerFeatureFunction("UGZ=TEMP", Array(GazeteerFeatureFunction.TEMPORAL)))
    /*retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=NORP", Array(
      GazeteerFeatureFunction.gzPATH + "other/nationalities.txt",
      GazeteerFeatureFunction.gzPATH + "other/known_nationalities.lst"
    )))*/
    /*retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=WoA", Array(
      GazeteerFeatureFunction.gzPATH + "other/WikiArtWork.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiArtWorkRedirects.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiFilms.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiFilmsRedirects.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiSongs.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiSongsRedirects.lst"
    )))
    retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=EVE", Array(
      GazeteerFeatureFunction.gzPATH + "other/WikiCompetitionsBattlesEvents.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiCompetitionsBattlesEventsRedirects.lst"
    )))
    retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=PROD", Array(
      GazeteerFeatureFunction.gzPATH + "other/WikiManMadeObjectNames.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiManMadeObjectNamesRedirects.lst"
    )))     */


    retVal
  }

  def main(args: Array[String]): Unit = {
    val examples = importData()
    val iterScoring = 10
    val allIters = 50

    val neFFunctions = createNEFFunctions()

    val NElearnLabelType = Label.NE
    /*
    val NE1crfLearner = new LCCRFLearner(examples, NElearnLabelType, neFFunctions, featureThreshold = 3)
    val NE1crfClassifier = NE1crfLearner.trainAndTest(iterScoring, allIters, examples)
    */

    val NE1crfLearner = new CRFSuiteLCCRFLearner(examples, NElearnLabelType, neFFunctions, featureThreshold = 3, maxIterations = Some(allIters))
    val NE1crfClassifier = NE1crfLearner.train()

    new Statistics(NE1crfClassifier, examples).printAllStat(Label.NE)
  }

}
