package si.zitnik.research.iobie.runners

import si.zitnik.research.iobie.datasets.conll2012._
import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/23/12
 * Time: 11:39 AM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2012 {


  def importData() = {
    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2012/dataset/v1/conll-2012/v1/domain"
    val conll2012Importer = new CoNLL2012Importer(
      path,
      datasetType = CoNLL2012ImporterDatasetTypeEnum.DEVELOPMENT,
      language = CoNLL2012ImporterLanguageEnum.ENGLISH,
      sources = Array(CoNLL2012ImporterSourceTypeEnum.BROADCAST_NEWS),
      annoType = CoNLL2012ImporterAnnoTypeEnum.GOLD
    )
    //val conll2012Importer = new CoNLL2012Importer(path)

    val examples = conll2012Importer.importForIE()
    examples.printStatistics(ommited = Array(Label.OBS, Label.PARSE_NODE))
    examples
  }


  def createNEFFunctions(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    retVal.add(new BigramDistributionFeatureFunction())
    retVal.add(new UnigramDistributionFeatureFunction())
    retVal.add(new UnigramFeatureFunction(Label.NE, "U=NE"))

    retVal.add(new UnigramFeatureFunction(Label.POS, "U=POS"))
    retVal.add(new UnigramConsecutiveFeatureFunction(Label.POS, "UC=POS"))

    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=PER", Array(GazeteerFeatureFunction.PER)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=LOC", Array(GazeteerFeatureFunction.LOC)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=ORG", Array(GazeteerFeatureFunction.ORG)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=MON", Array(GazeteerFeatureFunction.MONEY)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=JOB", Array(GazeteerFeatureFunction.JOB)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=MEA", Array(GazeteerFeatureFunction.MEASURES)))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=CARD", Array(GazeteerFeatureFunction.NUM + "/cardinalNumber.txt")))
    //retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=ORD", Array(GazeteerFeatureFunction.NUM + "/ordinalNumber.txt")))
    retVal.add(new GazeteerFeatureFunction("UGZ=TEMP", Array(GazeteerFeatureFunction.TEMPORAL)))
    /*retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=NORP", Array(
      GazeteerFeatureFunction.gzPATH + "other/nationalities.txt",
      GazeteerFeatureFunction.gzPATH + "other/known_nationalities.lst"
    )))*/
    /*retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=WoA", Array(
      GazeteerFeatureFunction.gzPATH + "other/WikiArtWork.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiArtWorkRedirects.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiFilms.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiFilmsRedirects.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiSongs.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiSongsRedirects.lst"
    )))
    retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=EVE", Array(
      GazeteerFeatureFunction.gzPATH + "other/WikiCompetitionsBattlesEvents.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiCompetitionsBattlesEventsRedirects.lst"
    )))
    retVal.add(new GazeteerFeatureFunction(Label.NE, "UGZ=PROD", Array(
      GazeteerFeatureFunction.gzPATH + "other/WikiManMadeObjectNames.lst",
      GazeteerFeatureFunction.gzPATH + "other/WikiManMadeObjectNamesRedirects.lst"
    )))     */


    retVal
  }

  def main(args: Array[String]): Unit = {
    val examples = importData()
    val iterScoring = 10
    val allIters = 50

    val neFFunctions = createNEFFunctions()

    val NElearnLabelType = Label.NE
    val NE1crfLearner = new LCCRFLearner(examples, NElearnLabelType, neFFunctions, featureThreshold = 3)
    val NE1crfClassifier = NE1crfLearner.trainAndTest(iterScoring, allIters, examples)
    //printStat(NE1crfClassifier, examples, Label.NE)
  }


}
