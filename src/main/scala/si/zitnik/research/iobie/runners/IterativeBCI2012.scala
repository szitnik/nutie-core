package si.zitnik.research.iobie.runners

import scala.collection.JavaConversions._
import java.util.ArrayList
import si.zitnik.research.iobie.datasets.tab.TabImporter
import si.zitnik.research.iobie.algorithms.crf.stat.{Statistics, FMeasure}
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Classifier, Label}
import si.zitnik.research.iobie.algorithms.crf.feature._


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/14/12
 * Time: 10:22 PM
 * To change this template use File | Settings | File Templates.
 */
//TODO: obsolete, change or remove
object IterativeBCI2012 {
  val ds = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/rtvslo_dec2011_v1.tsv"
  var examples = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF, Label.LEMMA, Label.POS), ds).importForIE()

  var numOfCollectiveIters = 50
  var numOfEachTaskIters = 100

  examples.printStatistics()
  examples.printLabelingDistribution(Label.NE)
  examples.printLabelingDistribution(Label.REL)
  examples.printLabelingDistribution(Label.COREF)

  def printStat(crfClassifier: Classifier, testData: Examples, learnLabelType: Label.Value): Unit = {
    for (value <- testData.getAllLabelValues(learnLabelType)) {
      println("Table for value: %s".format(value))
      new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, value)
    }
    val fm = new FMeasure(crfClassifier, testData, learnLabelType)
    println(fm.macroAveragedF())
    println(fm.microAveragedF())
  }

  def generalFFunctions(): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -1 to 1).generate())


    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.POS, -2 to 2, "UPOS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, -2 to 2, "UOBS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, -2 to 2, "ULEM").generate())

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, -1 to 2, "BPOS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, -1 to 2, "BOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, -1 to 2, "BLEM").generate())

    featureFunctions
  }

  /*
  def main(args: Array[String]) {
    //ITERATIVE
    println("ITERATIVE")
    val learner = new IterativeLearner(
      Map(Label.NE -> generalFFunctions(), Label.REL -> generalFFunctions(), Label.COREF -> generalFFunctions()),
      examples,
      numOfEachTaskIters,
      numOfCollectiveIters)
    learner.train()
  }
  */
}
