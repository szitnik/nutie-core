package si.zitnik.research.iobie.runners

import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}


import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.feature._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/10/12
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */

object ACE2004 extends StrictLogging {
  val rootPath = "/Users/slavkoz/Documents/DR_Research/Datasets/DataSet-IJCNLP2011/domain sets/"
  val dsNyt = "New York Times.txt"
  val dsWiki = "Wikipedia.txt"

  def generalFFunctions(): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -1 to 1).generate())


    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.POS, -2 to 2, "UPOS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, -2 to 2, "UOBS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, -2 to 2, "ULEM").generate())

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, -1 to 2, "BPOS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, -1 to 2, "BOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, -1 to 2, "BLEM").generate())

    featureFunctions
  }
}
