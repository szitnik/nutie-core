package si.zitnik.research.iobie.runners

import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.stat.{FMeasure, Statistics}
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner
import si.zitnik.research.iobie.datasets.ijcnlp2011.IJCNLP2011Importer

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/22/12
 * Time: 2:46 PM
 * To change this template use File | Settings | File Templates.
 */

object IJCNLP2012 extends StrictLogging {
  val rootPath = "/Users/slavkoz/Documents/DR_Research/Datasets/DataSet-IJCNLP2011/domain sets/"
  val dsNyt = "New York Times.txt"
  val dsWiki = "Wikipedia.txt"


  def main(args: Array[String]): Unit = {
    val examples = new IJCNLP2011Importer(rootPath + dsNyt).importForIE
    val learnData = examples
    val testData = examples
    val learnLabelType = Label.REL

    //val posTagger = new PoSTagger() //already has pos tags
    //val chunkTagger = new ChunkTagger()
    //posTagger.tag(examples)
    //chunkTagger.tag(examples)

    val featureFunctions = new ArrayList[FeatureFunction]()
    /*featureFunctions.add( new BigramDistributionFeatureFunction() )
    featureFunctions.add( new UnigramFeatureFunction() )

    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.POS, -2 to 2).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.PHRB, -2 to 2).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -2 to 2).generate() )

    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.POS, -1 to 2).generate() )
    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.PHRB, -1 to 2).generate() )
    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.OBS, -1 to 2).generate() )
    */
    val crfLearner = new LCCRFLearner(learnData, learnLabelType, featureFunctions)
    var crfClassifier = crfLearner.trainAndTest(50, 500, testData)

    /*
    do serialization later...
    val os = new ObjectOutputStream(new FileOutputStream(new java.io.File(".").getCanonicalPath()+"/CRF/out/ijcnlp.obj"))
    os.writeObject(crfClassifier)
    os.close()

    val is = new ObjectInputStream(new FileInputStream(new java.io.File(".").getCanonicalPath()+"/CRF/out/ijcnlp.obj"))
    crfClassifier = is.readObject().asInstanceOf[LCCRFClassifier]
    is.close()
    Exception in thread "main" java.io.NotSerializableException: com.weiglewilczek.slf4s.DefaultLocationAwareLogger
    */


    examples.printStatistics()



    for (example <- testData) {
      val (classfiedLabeling, _, _) = crfClassifier.classify(example)

      if (classfiedLabeling.contains("B-R") || classfiedLabeling.contains("I-R")) {
        example.printLabeling(Label.OBS)
        example.printLabeling(learnLabelType)
        println(classfiedLabeling.mkString(" "))
      }

    }

    for (value <- examples.getAllLabelValues(learnLabelType)) {
      println("Table for value: %s".format(value))
      new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, value)
    }
    val fm = new FMeasure(crfClassifier, testData, learnLabelType)
    println(fm.macroAveragedF())
    println(fm.microAveragedF())
  }
}
