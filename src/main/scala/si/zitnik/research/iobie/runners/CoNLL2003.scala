package si.zitnik.research.iobie.runners

import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner
import si.zitnik.research.iobie.algorithms.crf.feature._
import si.zitnik.research.iobie.algorithms.crf.stat.{FMeasure, Statistics}
import si.zitnik.research.iobie.algorithms.crf.{Classifier, FeatureFunction, Label}
import si.zitnik.research.iobie.util.ObjectManager
import si.zitnik.research.iobie.datasets.conll2003.CoNLL2003Importer

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/29/12
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2003 {
  val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2003/eng.build.si.zitnik.research.iobie.datasets/"
  //val testData = new CoNLL2003Importer(path + "eng.testa") importForIE
  //val examples2 = new CoNLL2003Importer(path + "eng.testb") importForIE
  val learnData = new CoNLL2003Importer(path + "eng.train") importForIE
  val testData = learnData
  val learnLabelType = Label.NE

  def main(args: Array[String]): Unit = {
    learnData.printStatistics()


    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction("B"))

    featureFunctions.add(new UnigramFeatureFunction(Label.NE, "U=NE"))

    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "U=POS"))
    featureFunctions.add(new UnigramConsecutiveFeatureFunction(Label.POS, "UC=POS"))
    featureFunctions.add(new GazeteerFeatureFunction("GZ=PER", Array(GazeteerFeatureFunction.PER)))
    featureFunctions.add(new GazeteerFeatureFunction("GZ=LOC", Array(GazeteerFeatureFunction.LOC)))
    featureFunctions.add(new GazeteerFeatureFunction("GZ=ORG", Array(GazeteerFeatureFunction.ORG)))
    featureFunctions.add(new GazeteerFeatureFunction("GZ=MISC", Array(
      GazeteerFeatureFunction.gzPATH + "other/nationalities.txt",
      GazeteerFeatureFunction.gzPATH + "other/known_nationalities.lst"
    )))


    val crfLearner = new LCCRFLearner(learnData, learnLabelType, featureFunctions, featureThreshold = 3)
    var crfClassifier: Classifier = crfLearner.trainAndTest(1, 1, testData)


    //testis!!!
    new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, "I-PER")
    val filename = "/Users/slavkoz/temp/krneki.obj"
    ObjectManager.saveObject(crfClassifier, filename)
    crfClassifier = ObjectManager.readObject(filename).asInstanceOf[Classifier]
    new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, "I-PER")
    //


    testData.printStatistics()
    testData.printLabelingDistribution(Label.NE)
    new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, "I-PER")

    /*
    for (example <- testData) {
      val (classfiedLabeling, _) = crfClassifier.classify(example)

      if (classfiedLabeling.contains("I-PER") ) {
        example.printLabeling(Label.OBS)
        example.printLabeling(learnLabelType)
        println(classfiedLabeling.mkString(" "))
      }
    }*/


    val fm = new FMeasure(crfClassifier, testData, learnLabelType)
    println(fm.macroAveragedF())
    println(fm.microAveragedF())

  }
}
