package si.zitnik.research.iobie.runners

import scala.collection.JavaConversions._
import java.util.ArrayList
import java.io.FileWriter
import si.zitnik.research.iobie.datasets.tab.{TabExporter, TabImporter}
import si.zitnik.research.iobie.algorithms.crf.stat.{Statistics, FMeasure}
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Classifier, Label}
import si.zitnik.research.iobie.datasets.TEIP5.TEIP5Importer
import si.zitnik.research.iobie.domain.IOBIEConversions._


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/14/12
 * Time: 10:22 PM
 * To change this template use File | Settings | File Templates.
 */

object BCI2012SloIterative {
  val ds = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/rtvslo_dec2011_v1.tsv"
  var iterScoring = 600
  var allIters = 600
  var examples = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF, Label.LEMMA, Label.POS), ds).importForIE()
  examples.printStatistics()
  examples.printLabelingDistribution(Label.NE)
  examples.printLabelingDistribution(Label.REL)
  examples.printLabelingDistribution(Label.COREF)

  def reload(): Unit = {
    iterScoring = 600
    allIters = 600
    examples = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF, Label.LEMMA, Label.POS), ds).importForIE()
  }

  def exportForPosTagger(): Unit = {
    val ds = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1.tsv"
    val dsOut = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1_posInput.tsv"
    var examples = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF), ds).importForIE()

    val lines = new ArrayList[String]()
    for (example <- examples) {
      lines.add(example.getLabeling(Label.OBS).mkString(" "))
    }

    val fw = new FileWriter(dsOut)
    fw.write(lines.mkString("\r\n"))
    fw.close()
  }

  def normalizeTEIP5AndDataset(): Unit = {
    val ds1 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1.tsv"
    var examples1 = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF), ds1).importForIE()

    val ds2 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1_posLemmaTagged_TEIP5.xml"
    var examples2 = new TEIP5Importer(ds2).importForIE()

    examples1.printStatistics()
    examples2.printStatistics()

    val tokens1 = examples1.getLabeling(Label.OBS)
    val tokens2 = examples2.getLabeling(Label.OBS)

    for (i <- 0 until 6034) {
      if (!tokens1(i).equals(tokens2(i))) {
        println("idx: %d".format(i))
        println("ex1: %s".format(tokens1.subList(math.max(i - 5, 0), i + 5).mkString(" ")))
        println("ex2: %s\n--------".format(tokens2.subList(math.max(i - 5, 0), i + 5).mkString(" ")))
        System.exit(-1)
      }
    }

    println(tokens1.subList(6000, 6034).mkString(" "))
    println(tokens2.subList(6000, 6034).mkString(" "))
  }

  def mergeTEI5AndTabAndOutputToTab(): Unit = {
    val ds1 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1.tsv"
    var examples1 = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF), ds1).importForIE()

    val ds2 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1_posLemmaTagged_TEIP5.xml"
    var examples2 = new TEIP5Importer(ds2).importForIE()

    examples1.setLabeling(Label.LEMMA, examples2.getLabeling(Label.LEMMA))
    examples1.setLabeling(Label.POS, examples2.getLabeling(Label.POS))

    val labels = Array[Label.Value](Label.NE, Label.REL, Label.COREF, Label.LEMMA, Label.POS)
    val dsOut = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1_fullMerged.tsv"
    new TabExporter(labels, dsOut, examples1).exportForIE()
  }

  def testDatasets(): Unit = {
    val ds1 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1.tsv"
    var examples1 = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF), ds1).importForIE()
    val ds2 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1_posLemmaTagged_TEIP5.xml"
    var examples2 = new TEIP5Importer(ds2).importForIE()
    val ds3 = "/Users/slavkoz/Documents/DR_Research/Datasets/rtvslo_dec2011/articles_1_fullMerged.tsv"
    var examples3 = new TabImporter(Array[Label.Value](Label.NE, Label.REL, Label.COREF, Label.LEMMA, Label.POS), ds3).importForIE()

    examples1.printStatistics()
    examples2.printStatistics()
    examples3.printStatistics()
  }

  def printStat(crfClassifier: Classifier, testData: Examples, learnLabelType: Label.Value): Unit = {
    for (value <- testData.getAllLabelValues(learnLabelType)) {
      println("Table for value: %s".format(value))
      new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, value)
    }
    val fm = new FMeasure(crfClassifier, testData, learnLabelType)
    println(fm.macroAveragedF())
    println(fm.microAveragedF())
  }

  def NEFFunctions(learnData: Examples): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    /*featureFunctions.add( new BigramDistributionFeatureFunction() )
    featureFunctions.add( new UnigramFeatureFunction() )


    featureFunctions.add( new StartsUpperFeatureFunction(-1) )
    featureFunctions.add( new StartsUpperFeatureFunction() )
    featureFunctions.add( new StartsUpperTwiceFeatureFunction(-1) )
    featureFunctions.add( new StartsUpperTwiceFeatureFunction() )

    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, 2, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, 3, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, -3, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, -2, -1 to  1).generate() )


    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.POS, -2 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -2 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.LEMMA, -2 to 2).generate() )

    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.POS, -1 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -1 to 2, 3).generate() )
    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.LEMMA, -1 to 2).generate() )
     */
    featureFunctions
  }

  def RELFFunctions(learnData: Examples): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    /*featureFunctions.add( new BigramDistributionFeatureFunction() )
    featureFunctions.add( new UnigramFeatureFunction() )

    featureFunctions.add( new StartsUpperFeatureFunction(-1) )
    featureFunctions.add( new StartsUpperFeatureFunction() )
    featureFunctions.add( new StartsUpperTwiceFeatureFunction(-1) )
    featureFunctions.add( new StartsUpperTwiceFeatureFunction() )

    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, 2, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, 3, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, -3, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, -2, -1 to  1).generate() )


    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.POS, -2 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -2 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.LEMMA, -2 to 2).generate() )

    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.POS, -1 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -1 to 2, 3).generate() )
    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.LEMMA, -1 to 2).generate() )
    */
    featureFunctions
  }

  def independent(): Unit = {
    //LEARN NEs
    reload()
    val NElearnLabelType = Label.NE
    val NE1crfLearner = new LCCRFLearner(examples, NElearnLabelType, NEFFunctions(examples), featureThreshold = 3)
    val NE1crfClassifier = NE1crfLearner.trainAndTest(iterScoring, allIters, examples)
    printStat(NE1crfClassifier, examples, Label.NE)

    //LEARN RELs
    reload()
    val RELlearnLabelType = Label.REL
    val RELcrfLearner = new LCCRFLearner(examples, RELlearnLabelType, RELFFunctions(examples))
    var RELcrfClassifier = RELcrfLearner.trainAndTest(iterScoring, allIters, examples)
    printStat(RELcrfClassifier, examples, Label.REL)
  }

  def pipelineNE_REL(): Unit = {
    //LEARN  (pipeline fashion)
    reload()
    val NE1crfLearner = new LCCRFLearner(examples, Label.NE, NEFFunctions(examples), featureThreshold = 3)
    val NE1crfClassifier = NE1crfLearner.trainAndTest(iterScoring, allIters, examples)
    printStat(NE1crfClassifier, examples, Label.NE)

    for (example <- examples) {
      val classified = NE1crfClassifier.classify(example)
      example.setLabeling(Label.NE, classified._1.toArray[String](Array[String]()))
    }

    var fFunctions = RELFFunctions(examples)
    //fFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.NE, -2 to 2).generate() )
    //fFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.NE, -1 to 2).generate() )

    val REL2crfLearner = new LCCRFLearner(examples, Label.REL, fFunctions, featureThreshold = 3)
    val REL2crfClassifier = REL2crfLearner.trainAndTest(iterScoring, allIters, examples)
    printStat(REL2crfClassifier, examples, Label.NE)

  }

  def iterative(): Unit = {
    //LEARN NEs (collective fashion)
    //tag L1_NE, L1_REL tags & learn
    reload()
    val n = 2 //number of iters

    var nefFunctions = NEFFunctions(examples)
    var relfFunctions = RELFFunctions(examples)

    for (i <- 1 to n) {
      val NE3crfLearner = new LCCRFLearner(examples, Label.NE, nefFunctions)
      val NE3crfClassifier = NE3crfLearner.trainAndTest(iterScoring, allIters, examples)
      printStat(NE3crfClassifier, examples, Label.NE)

      val RELcrfLearner = new LCCRFLearner(examples, Label.REL, relfFunctions)
      var RELcrfClassifier = RELcrfLearner.trainAndTest(iterScoring, allIters, examples)
      printStat(RELcrfClassifier, examples, Label.REL)

      /*
      for (example <- examples) {
        val classifiedR = RELcrfClassifier.classify(example)
        example.setLabeling(Label.L1_REL, classifiedR._1.toArray[String](Array[String]())) //TODO: L1_REL removed later
        val classifiedNE = NE3crfClassifier.classify(example)
        example.setLabeling(Label.L1_NE, classifiedNE._1.toArray[String](Array[String]()))
      }
      */

      nefFunctions = NEFFunctions(examples)
      /*nefFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.L1_NE, -2 to 2).generate() )
      nefFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.L1_NE, -1 to 2).generate() )
      nefFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.L1_REL, -2 to 2).generate() )
      nefFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.L1_REL, -1 to 2).generate() )
      nefFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.REL, -2 to 2).generate() )
      nefFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.REL, -1 to 2).generate() )

      relfFunctions = RELFFunctions(examples)
      relfFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.L1_NE, -2 to 2).generate() )
      relfFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.L1_NE, -1 to 2).generate() )
      relfFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.L1_REL, -2 to 2).generate() )
      relfFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.L1_REL, -1 to 2).generate() )
      relfFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(examples, Label.NE, -2 to 2).generate() )
      relfFunctions.addAll( new LabelBigramFeatureFunctionGenerator(examples, Label.NE, -1 to 2).generate() )*/
    }

  }

  def main(args: Array[String]): Unit = {
    //used for slovene PoS tagger, written in .NET
    //exportForPosTagger()

    //normalizeTEIP5AndDataset()
    //mergeTEI5AndTabAndOutputToTab()
    //testDatasets()

    //INDEPENDENT
    println("INDEPENDENT")
    independent()

    //PIPELINE
    //println("PIPELINE")
    //pipelineNE_REL()

    //ITERATIVE
    //println("ITERATIVE")
    //collective()
  }
}
