package si.zitnik.research.iobie.runners

import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.{Classifier, FeatureFunction, Label}
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.feature.{UnigramFeatureFunction, BigramDistributionFeatureFunction}
import si.zitnik.research.iobie.datasets.conll2000.CoNLL2000Importer
import si.zitnik.research.iobie.util.ObjectManager
import java.io.File

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/29/12
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2000 {
  val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2000/"
  val objPath = "/Users/slavkoz/temp/IOBIEModels/CoNLL2012.obj"
  val testData = new CoNLL2000Importer(path + "test.txt") importForIE
  val learnData = new CoNLL2000Importer(path + "train.txt") importForIE
  val learnLabelType = Label.CHUNK


  def learnAndSaveModel(): Unit = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramFeatureFunction(Label.OBS))

    val crfLearner = new LCCRFLearner(learnData, learnLabelType, featureFunctions)
    val crfClassifier = crfLearner.trainAndTest(5, 5, testData)
    ObjectManager.saveObject(crfClassifier, objPath)
  }

  def main(args: Array[String]): Unit = {
    if (new File(objPath).exists()) {
      learnAndSaveModel()
    }
    val crfClassifier = ObjectManager.readObject(objPath).asInstanceOf[Classifier]



    new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, "I-NP")
    val (classfiedLabeling, _, _) = crfClassifier.classify(testData.get(0))
    testData.get(0).printLabeling(Label.OBS)
    testData.get(0).printLabeling(Label.CHUNK)
    println(classfiedLabeling.mkString(" "))
  }
}
