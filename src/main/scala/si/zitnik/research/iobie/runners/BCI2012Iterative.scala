package si.zitnik.research.iobie.runners

import java.util.ArrayList
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner
import si.zitnik.research.iobie.algorithms.crf.stat.{Statistics, FMeasure}
import si.zitnik.research.iobie.algorithms.crf.{Classifier, Label, FeatureFunction}
import si.zitnik.research.iobie.datasets.conll2003.CoNLL2003Importer
import si.zitnik.research.iobie.datasets.ijcnlp2011.IJCNLP2011Importer
import si.zitnik.research.iobie.thirdparty.opennlp.api.PoSTagger

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/11/12
 * Time: 10:18 PM
 * To change this template use File | Settings | File Templates.
 */

object BCI2012Iterative {

  def NEFFunctions(learnData: Examples): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    //featureFunctions.add( new BigramDistributionFeatureFunction() )
    /*featureFunctions.add( new UnigramFeatureFunction() )


    //featureFunctions.add( new StartsUpperFeatureFunction(-1) )
    featureFunctions.add( new StartsUpperFeatureFunction() )
    //featureFunctions.add( new StartsUpperTwiceFeatureFunction(-1) )
    featureFunctions.add( new StartsUpperTwiceFeatureFunction() )

    //featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, 2, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, 3, -1 to  1).generate() )
    featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, -3, -1 to  1).generate() )
    //featureFunctions.addAll( new XffixFeatureFunctionGenerator(learnData, Label.OBS, -2, -1 to  1).generate() )


    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.POS, -2 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -2 to 2, 3).generate() )

    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.POS, -1 to 2, 3).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -1 to 2, 3).generate() )
     */
    featureFunctions
  }

  def RELFFunctions(learnData: Examples): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    /*featureFunctions.add( new BigramDistributionFeatureFunction() )
    featureFunctions.add( new UnigramFeatureFunction() )

    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.POS, -2 to 2).generate() )
    //featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.PHRB, -2 to 2).generate() )
    featureFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(learnData, Label.OBS, -2 to 2).generate() )

    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.POS, -1 to 2).generate() )
    //featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.PHRB, -1 to 2).generate() )
    featureFunctions.addAll( new LabelBigramFeatureFunctionGenerator(learnData, Label.OBS, -1 to 2).generate() )
     */
    featureFunctions
  }


  def printStat(crfClassifier: Classifier, testData: Examples, learnLabelType: Label.Value): Unit = {
    for (value <- testData.getAllLabelValues(learnLabelType)) {
      println("Table for value: %s".format(value))
      new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, value)
    }
    val fm = new FMeasure(crfClassifier, testData, learnLabelType)
    println(fm.macroAveragedF())
    println(fm.microAveragedF())
  }

  def posTag(examples: Examples): Unit = {
    val posTagger = new PoSTagger()
    posTagger.tag(examples)
  }

  def main(args: Array[String]): Unit = {
    val iterScoring = 150 //100
    val allIters = 150 //500

    //DATASETS
    val NEpath = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2003/eng.build.si.zitnik.research.iobie.datasets/"
    val NEtestData = new CoNLL2003Importer(NEpath + "eng.testa") importForIE
    //val examples2 = new CoNLL2003Importer(path + "eng.testb") importForIE
    val NElearnData = new CoNLL2003Importer(NEpath + "eng.train") importForIE

    val RELrootPath = "/Users/slavkoz/Documents/DR_Research/Datasets/DataSet-IJCNLP2011/domain sets/"
    val RELdsNyt = "New York Times.txt"
    val RELdsWiki = "Wikipedia.txt"
    val RELexamples = new IJCNLP2011Importer(RELrootPath + RELdsNyt).importForIE
    RELexamples.addAll(new IJCNLP2011Importer(RELrootPath + RELdsWiki).importForIE)
    val RELlearnData = RELexamples
    val RELtestData = RELexamples

    posTag(NElearnData)
    posTag(NEtestData)
    posTag(RELexamples)

    //NElearnData.printStatistics()
    //RELexamples.printStatistics()


    //LEARN NEs
    val NElearnLabelType = Label.NE

    /*
val NE1crfLearner = new LCCRFLearner(NElearnData, NElearnLabelType, NEFFunctions(NElearnData), cleanFeatureFunctions=true, featureThreshold = 3)
val NE1crfClassifier = NE1crfLearner.trainAndTest(iterScoring, allIters, NEtestData)
printStat(NE1crfClassifier, NEtestData, Label.NE)
    */

    //LEARN RELs
    val RELlearnLabelType = Label.REL

    val RELcrfLearner = new LCCRFLearner(RELlearnData, RELlearnLabelType, RELFFunctions(RELlearnData))
    var RELcrfClassifier = RELcrfLearner.trainAndTest(iterScoring, allIters, RELtestData)
    printStat(RELcrfClassifier, RELtestData, Label.REL)

    //LEARN NEs (pipeline fashion)
    //tag REL tags & FFs & learn
    for (example <- NElearnData) {
      val classified = RELcrfClassifier.classify(example)
      example.setLabeling(Label.REL, classified._1.toArray[String](Array[String]()))
    }
    NElearnData.printLabelingDistribution(Label.REL)
    NEtestData.printLabelingDistribution(Label.REL)

    var fFunctions = NEFFunctions(NElearnData)
    //fFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(NElearnData, Label.REL, -2 to 2).generate() )
    //fFunctions.addAll( new LabelBigramFeatureFunctionGenerator(NElearnData, Label.REL, -1 to 2).generate() )

    val NE2crfLearner = new LCCRFLearner(NElearnData, NElearnLabelType, fFunctions, featureThreshold = 3)
    val NE2crfClassifier = NE2crfLearner.trainAndTest(iterScoring, allIters, NEtestData)
    printStat(NE2crfClassifier, NEtestData, Label.NE)

    //LEARN NEs (collective fashion)
    //tag L1_NE, L1_REL tags & learn
    /*
    for (example <- NElearnData) {
      val classifiedR = RELcrfClassifier.classify(example)
      example.setLabeling(Label.L1_REL, classifiedR._1.toArray[String](Array[String]()))
      val classifiedNE = NE1crfClassifier.classify(example)
      example.setLabeling(Label.L1_NE, classifiedNE._1.toArray[String](Array[String]()))
    }
    NElearnData.printLabelingDistribution(Label.L1_REL)
    NElearnData.printLabelingDistribution(Label.L1_NE)

    fFunctions = NEFFunctions(NElearnData)
    fFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(NElearnData, Label.L1_NE, -2 to 2).generate() )
    fFunctions.addAll( new LabelBigramFeatureFunctionGenerator(NElearnData, Label.L1_NE, -1 to 2).generate() )
    fFunctions.addAll( new LabelUnigramFeatureFunctionGenerator(NElearnData, Label.L1_REL, -2 to 2).generate() )
    fFunctions.addAll( new LabelBigramFeatureFunctionGenerator(NElearnData, Label.L1_REL, -1 to 2).generate() )

    val NE3crfLearner = new LCCRFLearner(NElearnData, NElearnLabelType, fFunctions, cleanFeatureFunctions=true, featureThreshold = 3)
    val NE3crfClassifier = NE3crfLearner.trainAndTest(iterScoring, allIters, NEtestData)
    printStat(NE3crfClassifier, NEtestData, Label.NE)
    */


  }

}
