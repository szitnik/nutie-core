package si.zitnik.research.iobie.algorithms.crf.stat

import si.zitnik.research.iobie.algorithms.crf.{Label, Classifier}
import si.zitnik.research.iobie.domain.{Example, Examples}
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConversions._
import java.util

/**
 * This is a standard calculation of F-Measure as proposed for keyword selection approaches
 * in the article:
 * Text Categorization with Class-Based and Corpus-Based Keyword Selection
 * Arzucan Ozgur, Levent Ozgur, Tunga Gungor
 * Computer and Information Sciences-ISCIS 2005
 */

class FMeasure(
                //these contain values for class i
                private var TPs: ArrayBuffer[Double] = new ArrayBuffer[Double](),
                private var FPs: ArrayBuffer[Double] = new ArrayBuffer[Double](),
                private var FNs: ArrayBuffer[Double] = new ArrayBuffer[Double]()) {


  def this(classifier: Classifier, examplesTest: Examples, labelType: Label.Value) {
    this()
    //calculate TPs, FPs, FNs
    val values = examplesTest.getAllLabelValues(labelType)
    val stat = new Statistics(classifier, examplesTest)

    values.foreach(v => {
      val (tp, tn, fp, fn) = stat.stat(labelType, v)
      TPs.append(tp)
      FPs.append(fp)
      FNs.append(fn)
    })
  }

  def microAveragedF(): Double = {
    val precision = (TPs.sum) / (TPs.sum + FPs.sum)
    val recall = (TPs.sum) / (TPs.sum + FNs.sum)
    if (!precision.isNaN && !recall.isNaN && precision + recall != 0)
      2 * precision * recall / (precision + recall)
    else
      0.0
  }

  def macroAveragedF(): Double = {
    var Fs = 0.0
    (0 until TPs.size).foreach(i => {
      val precision = TPs(i) / (TPs(i) + FPs(i))
      val recall = TPs(i) / (TPs(i) + FNs(i))
      if (!precision.isNaN && !recall.isNaN && (precision + recall) != 0) {
        Fs += 2 * precision * recall / (precision + recall)
      }
    })
    Fs / TPs.size
  }

  def averageRecall(): Double = {
    TPs.zip(FNs).map(_ match {case (tp, fn) => Statistics.recall(tp, fn)}).sum/TPs.size
  }

  def averagePrecision(): Double = {
    TPs.zip(FPs).map(_ match {case (tp, fp) => Statistics.precision(tp, fp)}).sum/TPs.size
  }

  override def toString =
    "MAF: %4.3f, MIF: %4.3f, AvgP: %4.3f, AvgR: %4.3f".format(
      this.macroAveragedF(),
      this.microAveragedF(),
      this.averagePrecision(),
      this.averageRecall())
}


object FMeasure {
  /**
   * Test with text from the MUC article (see class MUCStatistics):
   *
   * true:
   *
   * Unlike <ENAMEX TYPE="PERSON">Robert</ENAMEX>, <ENAMEX TYPE="PERSON">John
   * Briggs Jr</ENAMEX> contacted <ENAMEX TYPE="ORGANIZATION">Wonderful
   * Stockbrockers Inc</ENAMEX> in <ENAMEX TYPE="LOCATION">New York</ENAMEX>
   * and instructed them to sell all his shares in <ENAMEX
   * TYPE="ORGANIZATION">Acme</ENAMEX>.
   *
   * labelled:
   *
   * <ENAMEX TYPE="LOCATION">Unlike</ENAMEX> Robert, <ENAMEX
   * TYPE="ORGANIZATION">John Briggs Jr</ENAMEX> contacted Wonderful <ENAMEX
   * TYPE="ORGANIZATION">Stockbrockers</ENAMEX> Inc <ENAMEX TYPE="PERSON">in
   * New York</ENAMEX> and instructed them to sell all his shares in <ENAMEX
   * TYPE="ORGANIZATION">Acme</ENAMEX>.
   *
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val trueLabeling = Array[String]("O", "B-PER", "O", "B-PER", "I-PER", "I-PER", "O",
      "B-ORG", "I-ORG", "I-ORG", "O", "B-LOC", "I-LOC",
      "O", "O", "O", "O", "O", "O", "O", "O", "O", "B-ORG")
    val labelled = Array[String]("B-LOC", "O", "O", "B-ORG", "I-ORG", "I-ORG", "O", "O", "B-ORG",
      "O", "B-PER", "I-PER", "I-PER", "O", "O", "O", "O", "O", "O", "O", "O", "O", "B-ORG")

    /*
    //Jaz sem ti Đekson, Mirko San Jovo Dino.
    val trueLabeling = Array[String]("B-PER", "O", "B-PER", "B-PER", "O", "B-PER", "I-PER", "B-PER", "B-PER")
    //val labelled = Array[String]("B-PER", "O", "B-PER", "B-PER", "O", "B-PER", "I-PER", "B-PER", "B-PER")
    //val labelled = Array[String]("B-PER", "O", "B-PER", "I-PER", "O", "B-PER", "I-PER", "I-PER", "I-PER")
    */

    if (trueLabeling.size != labelled.size) {
      println("Test example not ok!")
      System.exit(-1)
    }

    val labelType = Label.NE
    val testExamples = new Examples(Array(new Example(trueLabeling, labelType, trueLabeling)))
    val classifier = new Classifier {
      override def classify(examplesTest: Examples): (ArrayBuffer[ArrayBuffer[String]], ArrayBuffer[ArrayBuffer[Double]], ArrayBuffer[Double]) = {
        val  a = new ArrayBuffer[ArrayBuffer[String]]()
        a.append(new ArrayBuffer[String]() ++ labelled.toSeq)
        (a, ArrayBuffer[ArrayBuffer[Double]](), ArrayBuffer[Double]())
      }


      def classify(example: Example, normalized: Boolean): (util.ArrayList[String], util.ArrayList[Double], Double) = throw new NotImplementedException()
      def test(data: Examples): Unit = {throw new NotImplementedException()}
    }

    val stat = new FMeasure(classifier, testExamples, labelType)
    println("Micro: " + stat.microAveragedF())
    println("Macro: " + stat.macroAveragedF())
  }
}
