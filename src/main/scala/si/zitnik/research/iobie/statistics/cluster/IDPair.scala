package si.zitnik.research.iobie.statistics.cluster

import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable.HashSet
import si.zitnik.research.iobie.domain.cluster.Cluster

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/21/12
 * Time: 10:57 AM
 * To change this template use File | Settings | File Templates.
 */
class IDPair {
  var a: Constituent = null
  var b: Constituent = null

  def this(a: Constituent, b: Constituent) {
    this()
    if (a < b) {
      this.a = a
      this.b = b
    } else {
      this.a = b
      this.b = a
    }
  }

  override def hashCode() = {
    //each constituent is exactly defined by example, startidx and endidx
    var h = 0

    h = 31*h+a.example.hashCode()
    h = 31*h+a.startIdx.hashCode()
    h = 31*h+a.endIdx.hashCode()

    h = 31*h+b.example.hashCode()
    h = 31*h+b.startIdx.hashCode()
    h = 31*h+b.endIdx.hashCode()

    h
  }


  override def equals(obj: Any) = obj.isInstanceOf[IDPair] && this.hashCode() == obj.hashCode()

  override def toString = "{%S,%S}".format(a.toString,b.toString)
}

object IDPair {

  def getIDPairs(clusters: HashSet[Cluster], includeReversePair: Boolean = false): HashSet[IDPair] = {
    val retVal = new HashSet[IDPair]()
    for (cluster <- clusters) {
      if (cluster.size == 1) {
        retVal.add(new IDPair(cluster.head, cluster.head))
      } else {
        cluster.toBuffer.combinations(2).foreach(v => {
          retVal.add(new IDPair(v(0), v(1)))
          if (includeReversePair) {
            retVal.add(new IDPair(v(1), v(0)))
          }
        })
      }
    }
    retVal
  }

  /**
   * Normally too expensive method to use
   * @param constituents
   * @return
   */
  def getNonCorefIDPairs(constituents: HashSet[Constituent]): HashSet[IDPair] = {
    val retVal = new HashSet[IDPair]()
    constituents.toBuffer.combinations(2).foreach(v => if (!v(0).cluster.equals(v(1))) retVal.add(new IDPair(v(0), v(1))))
    retVal
  }

}

