package si.zitnik.research.iobie.statistics

import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.Example

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.Label
import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.stat.FMeasure

import collection.mutable._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/27/12
 * Time: 1:24 PM
 * To change this template use File | Settings | File Templates.
 */
object MentionStatistics extends StrictLogging {

  /**
   * Method returns (tp, fp, fn) values that can be used to calculate Recall, Precision or F-score, ... by Statistic.scala
   * @param originalMentions
   * @param detectedMentions
   * @return
   */
  def singleDocumentStat(originalMentions: ArrayBuffer[Constituent], detectedMentions: ArrayBuffer[Example]): (Double, Double, Double) = {
    val originals = originalMentions.map(_.get(Label.OBS))
    if (detectedMentions == null) { //no mentions were detected in an example
      return (0.0, 0.0, originals.size)
    }
    if (detectedMentions.size > 1) {
      logger.warn("There is possible error. Arraylist should contain only one example")
    }
    val detecteds = detectedMentions(0).map(_.get(Label.OBS))


    var tp = 0
    var fn = 0
    var detectedsTemp = detectedMentions(0).map(_.get(Label.OBS))
    originals.foreach(original => {
      if (detectedsTemp.contains(original)) {
        detectedsTemp.remove(detectedsTemp.indexOf(original))
        tp+=1
      } else {
        fn+=1
      }
    }
    )
    val fp = detectedsTemp.size

    (tp, fp, fn)
  }

  /**
   * Returns F micro and macro
   * @param originalMentions
   * @param detectedMentions
   * @return
   */
  def multiDocumentStat(originalMentions: HashMap[String, ArrayBuffer[Constituent]], detectedMentions: scala.collection.mutable.HashMap[String, ArrayBuffer[Example]]): FMeasure = {
    if (originalMentions.size != detectedMentions.size) {
      logger.warn("Original and detected mention key sizes do not match (* may also appear if one of original documents does not contain mentions)!")
    }

    val TPs = new ArrayBuffer[Double]()
    val FPs = new ArrayBuffer[Double]()
    val FNs = new ArrayBuffer[Double]()


    for (docID <- (originalMentions.keySet ++ detectedMentions.keySet)) {
      val value = singleDocumentStat(originalMentions.get(docID).get, detectedMentions.get(docID).get)
      value match {
        case (0,0,0) => {} //no operation => to get exact result if one of the original documents does not contain mentions
        case (tp, fp, fn) => {
          TPs.add(tp)
          FPs.add(fp)
          FNs.add(fn)
        }
        case _ => logger.error("Wrong result type!")
      }
    }

    new FMeasure(TPs, FPs, FNs)
  }


}
