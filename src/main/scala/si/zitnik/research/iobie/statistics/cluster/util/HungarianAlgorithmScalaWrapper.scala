package si.zitnik.research.iobie.statistics.cluster.util

import com.typesafe.scalalogging.StrictLogging

import scala.collection.mutable._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/19/12
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
object HungarianAlgorithmScalaWrapper extends StrictLogging {

  def hgAlgorithm(array: Array[Array[Double]], sumType: HungarianSumType.Value = HungarianSumType.MAX): (HashMap[(Int, Int), Double], Double) = {
    val assignment = HungarianAlgorithm.hgAlgorithm(array, sumType.toString)

    //array must be rectangular!!!, simple check:
    if (array.length != array(0).length) {
      logger.error("There may be problem calculating KuhnMunkres algorithm, input array not square.")
      throw new Exception("There may be problem calculating KuhnMunkres algorithm, input array not square.")
    }

    var sum = 0.0
    val A = HashMap[(Int, Int), Double]()
    for (i <- 0 until assignment.length) {
      //print("array(%d,%d) = %.2f\n".format((assignment(i)(0)+1), (assignment(i)(1)+1), array(assignment(i)(0))(assignment(i)(1))))
      A.put((assignment(i)(0), assignment(i)(1)), array(assignment(i)(0))(assignment(i)(1)))
      sum = sum + array(assignment(i)(0))(assignment(i)(1))
    }

    (A, sum)
  }
}

object HungarianSumType extends Enumeration {
  val MAX = Value("max")
  val MIN = Value("min")
}
