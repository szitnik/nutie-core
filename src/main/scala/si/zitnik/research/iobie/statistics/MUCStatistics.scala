package si.zitnik.research.iobie.algorithms.crf.stat

import si.zitnik.research.iobie.algorithms.crf.{Label, Classifier}
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.domain.IOBIEConversions._
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import scala.collection.mutable.ArrayBuffer
import java.util

/**
 * MUC6 calculated as proposed in:
 * A survey of named entity recognition and classification
 * David Nadeau, Satoshi Sekine
 * National Research Council Canada, New York University
 *
 * This calculation currently supports only BIO notation. (e.g.: B-PER, I-PER, O, B-ORG, etc.)
 *
 * MUC score is actually the micro-averaged f-score (MAF).
 *
 */

class MUCStatistics(
                     val classifier: Classifier,
                     val examplesTest: Examples,
                     val labelType: Label.Value) {

  //type is ok if it is overlap with boundaries
  private var TYPE_COR: Double = 0
  //text is ok if entity boundaries are correct regardless of type
  private var TEXT_COR: Double = 0
  //POS same for TEXT and TYPE
  private var POS: Double = 0
  private var ACT: Double = 0

  init()

  private def init(): Unit = {
    //true annotations
    val trues = examplesTest.getLabeling(labelType)
    //labelled annotations
    val labelleds = classifier.classify(examplesTest)

    //get POS
    trues.foreach(v => if (v.startsWith("B-")) {
      POS += 1
    })
    //get ACT
    labelleds.foreach(v => if (v.startsWith("B-")) {
      ACT += 1
    })

    //get TYPEs (correct type at least overlap)
    var len = 1
    var i = 0
    while (i < trues.size) {
      len = 1
      if (trues(i).startsWith("B-")) {
        while (i + len < trues.size && trues(i + len).startsWith("I-")) len += 1
        //check part
        var typeOK = false
        for ((t, l) <- trues.subList(i, i + len).zip(labelleds._1.subList(i, i + len))) {
          if ((if (t.startsWith("B-") || t.startsWith("I-")) {
            t.substring(2)
          } else {
            t
          }).equals(
            if (l.startsWith("B-") || l.startsWith("I-")) {
              l.substring(2)
            } else {
              l
            })) {
            typeOK = true
          }
        }
        if (typeOK) TYPE_COR += 1
      }
      i += len
    }

    //get TEXTs (if boundaries correct regardless type)
    len = 1
    i = 0
    while (i < trues.size) {
      len = 1
      if (trues(i).startsWith("B-") && labelleds._1(i).startsWith("B-")) {
        while (i + len < trues.size && trues(i + len).startsWith("I-")) len += 1
        //check part
        var textOK = true
        for ((t, l) <- trues.subList(i + 1, i + len).zip(labelleds._1.subList(i + 1, i + len))) {
          if (t.startsWith("I-") != l.startsWith("I-")) textOK = false
        }
        //check end
        if (i + len < trues.size && labelleds._1(i + len).startsWith("I-")) textOK = false
        if (textOK) TEXT_COR += 1
      }
      i += len
    }
  }


  def textPrecision(): Double = {
    (TEXT_COR) / (ACT)
  }

  def textRecall(): Double = {
    (TEXT_COR) / (POS)
  }

  def typePrecision(): Double = {
    (TYPE_COR) / (ACT)
  }

  def typeRecall(): Double = {
    (TYPE_COR) / (POS)
  }

  def precision(): Double = {
    (TEXT_COR + TYPE_COR) / (2 * ACT)
  }

  def recall(): Double = {
    (TEXT_COR + TYPE_COR) / (2 * POS)
  }

  /**
   * aka. MUC score (micro averaged F)
   * @return
   */
  def fscore(): Double = {
    2 * precision() * recall() / (precision() + recall())
  }
}

object MUCStatistics {

  /**
   * Test with text from the article:
   *
   * true:
   *
   * Unlike <ENAMEX TYPE="PERSON">Robert</ENAMEX>, <ENAMEX TYPE="PERSON">John
   * Briggs Jr</ENAMEX> contacted <ENAMEX TYPE="ORGANIZATION">Wonderful
   * Stockbrockers Inc</ENAMEX> in <ENAMEX TYPE="LOCATION">New York</ENAMEX>
   * and instructed them to sell all his shares in <ENAMEX
   * TYPE="ORGANIZATION">Acme</ENAMEX>.
   *
   * labelled:
   *
   * <ENAMEX TYPE="LOCATION">Unlike</ENAMEX> Robert, <ENAMEX
   * TYPE="ORGANIZATION">John Briggs Jr</ENAMEX> contacted Wonderful <ENAMEX
   * TYPE="ORGANIZATION">Stockbrockers</ENAMEX> Inc <ENAMEX TYPE="PERSON">in
   * New York</ENAMEX> and instructed them to sell all his shares in <ENAMEX
   * TYPE="ORGANIZATION">Acme</ENAMEX>.
   *
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val trueLabeling = Array[String]("O", "B-PER", "O", "B-PER", "I-PER", "I-PER", "O",
      "B-ORG", "I-ORG", "I-ORG", "O", "B-LOC", "I-LOC",
      "O", "O", "O", "O", "O", "O", "O", "O", "O", "B-ORG")
    val labelled = Array[String]("B-LOC", "O", "O", "B-ORG", "I-ORG", "I-ORG", "O", "O", "B-ORG",
      "O", "B-PER", "I-PER", "I-PER", "O", "O", "O", "O", "O", "O", "O", "O", "O", "B-ORG")

    /*
    //Jaz sem ti Đekson, Mirko San Jovo Dino.
    val trueLabeling = Array[String]("B-PER", "O", "B-PER", "B-PER", "O", "B-PER", "I-PER", "B-PER", "B-PER")
    //val labelled = Array[String]("B-PER", "O", "B-PER", "B-PER", "O", "B-PER", "I-PER", "B-PER", "B-PER")
    //val labelled = Array[String]("B-PER", "O", "B-PER", "I-PER", "O", "B-PER", "I-PER", "I-PER", "I-PER")
    */

    if (trueLabeling.size != labelled.size) {
      println("Test example not ok!")
      System.exit(-1)
    }

    val labelType = Label.NE
    val testExamples = new Examples(Array(new Example(trueLabeling, labelType, trueLabeling)))
    val classifier = new Classifier {
      override def classify(examplesTest: Examples): (ArrayBuffer[ArrayBuffer[String]], ArrayBuffer[ArrayBuffer[Double]], ArrayBuffer[Double]) = {
        val  a = new ArrayBuffer[ArrayBuffer[String]]()
        a.append(new ArrayBuffer[String]() ++ labelled.toSeq)
        (a, ArrayBuffer[ArrayBuffer[Double]](), ArrayBuffer[Double]())
      }

      def classify(example: Example, normalized: Boolean): (util.ArrayList[String], util.ArrayList[Double], Double) = null
      def test(data: Examples): Unit = {throw new NotImplementedException()}
    }

    val stat = new MUCStatistics(classifier, testExamples, labelType)
    println(stat.fscore())
  }
}
