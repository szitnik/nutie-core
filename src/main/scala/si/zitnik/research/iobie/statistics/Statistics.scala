package si.zitnik.research.iobie.algorithms.crf.stat

import si.zitnik.research.iobie.algorithms.crf.{Classifier, Label}
import si.zitnik.research.iobie.domain.Examples

import scala.collection.JavaConversions._
import java.util

import com.typesafe.scalalogging.StrictLogging

import collection.mutable.ArrayBuffer

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/27/11
 * Time: 6:06 PM
 * To change this template use File | Settings | File Templates.
 */

class Statistics(
                  val classifier: Classifier,
                  val examplesTest: Examples) extends StrictLogging {



  def printStandardClassification(value: Label.Value, nullTrue: String): Unit = {
    val (tp, tn, fp, fn) = stat(value, nullTrue)
    Statistics.printClassificationTable((tp, tn, fp, fn))
    Statistics.printStandardClassification((tp, tn, fp, fn))
  }



  /**
   * Function returns output od (TP, TN, FP, FN)
   * for given dataset
   */
  def stat(labelType: Label.Value, nullTrue: String): (Double, Double, Double, Double) = {
    var TP, TN, FP, FN = 0.0

    //calculate scores
    for ((realTags, classifiedTags) <- examplesTest.getLabeling(labelType) zip classifier.classify(examplesTest)._1) {
      if (realTags.size() != classifiedTags.size) {
        logger.warn("Real and Classified tags size do not match!")
      }
      for ((realTag, classifiedTag) <- realTags zip classifiedTags) {
        if (realTag.equals(nullTrue) && realTag.equals(classifiedTag)) {
          TP += 1
        } else if (realTag.equals(nullTrue) && !realTag.equals(classifiedTag)) {
          FP += 1
        } else if (!realTag.equals(nullTrue) && realTag.equals(classifiedTag)) {
          TN += 1
        } else if (!realTag.equals(nullTrue) && !realTag.equals(classifiedTag)) {
          FN += 1
        }
      }
    }
    (TP, TN, FP, FN)
  }

  //TODO: this function does some standard testing
  def printAllStat(learnLabelType: Label.Value): Unit = {
    for (value <- examplesTest.getAllLabelValues(learnLabelType)) {
      println("Table for value: %s".format(value))
      new Statistics(classifier, examplesTest).printStandardClassification(learnLabelType, value)
    }
    val fm = new FMeasure(classifier, examplesTest, learnLabelType)
    println(fm.macroAveragedF())
    println(fm.microAveragedF())
  }
}

object Statistics extends StrictLogging {

  /**
   * Function returns output od (TP, TN, FP, FN)
   * for given dataset
   */
  def stat(labelType: Label.Value, nullTrue: String, realTagsAll: util.ArrayList[util.ArrayList[String]], classifiedTagsAll: ArrayBuffer[ArrayBuffer[String]]): (Double, Double, Double, Double) = {
    var TP, TN, FP, FN = 0.0

    //calculate scores
    for ((realTags, classifiedTags) <- realTagsAll zip classifiedTagsAll) {
      if (realTags.size() != classifiedTags.size) {
        logger.warn("Real and Classified tags size do not match!")
      }
      for ((realTag, classifiedTag) <- realTags zip classifiedTags) {
        if (realTag.equals(nullTrue) && realTag.equals(classifiedTag)) {
          TP += 1
        } else if (realTag.equals(nullTrue) && !realTag.equals(classifiedTag)) {
          FP += 1
        } else if (!realTag.equals(nullTrue) && realTag.equals(classifiedTag)) {
          TN += 1
        } else if (!realTag.equals(nullTrue) && !realTag.equals(classifiedTag)) {
          FN += 1
        }
      }
    }
    (TP, TN, FP, FN)
  }

  /**
   * value: (tp, tn, fp, fn)
   * @param value
   */
  def printStandardClassification(value: (Double, Double, Double, Double)): Unit = {
    value match {
      case (tp, tn, fp, fn) => {
        println("F-score:\n\t%3.2f".format(fScore(tp, fp, fn)))
        println("Precision:\n\t%3.2f".format(precision(tp, fp)))
        println("Recall:\n\t%3.2f".format(recall(tp, fn)))
        println("CA:\n\t%3.2f".format(CA(tp, tn, fp, fn)))
        println("Specificity:\n\t%3.2f".format(specificity(tn, fp)))
      }
      case _ => logger.error("Wrong parameters for method!")
    }

  }

  /**
   * value: (tp, tn, fp, fn)
   * @param value
   */
  def printClassificationTable(value: (Double, Double, Double, Double)): Unit = {
    value match {
      case (tp, tn, fp, fn) => {
        println(("Classification table:\n" +
          "\t            |  Null True  |  Null False  |\n" +
          "\t------------------------------------------\n" +
          "\tClass True  | %6.0f (TP) |  %6.0f (FN) |\n" +
          "\tClass False | %6.0f (FP) |  %6.0f (TN) |").format(tp, fn, fp, tn))
      }
      case _ => logger.error("Wrong parameters for method!")
    }


  }

  /**
   * beta == 1: E=F
   * beta < 1: weight precision more
   * beta > 1: weight recall more
   * @param beta
   * @param tp
   * @param fp
   * @param fn
   * @return
   */
  def eScore(beta: Double = 1, tp: Double, fp: Double, fn: Double) = {
    val prec = precision(tp, fp)
    val rec = recall(tp, fn)

    if ((prec + rec) == 0) {
      0.0
    } else {
      2 * prec * rec * (1 + beta*beta) / (beta * beta * prec + rec)
    }
  }

  def fScore(tp: Double, fp: Double, fn: Double) = {
    val prec = precision(tp, fp)
    val rec = recall(tp, fn)

    if ((prec + rec) == 0) {
      0.0
    } else {
      2 * prec * rec / (prec + rec)
    }
  }

  def CA(tp: Double, tn: Double, fp: Double, fn: Double) = {
    if ((tp + tn + fp + fn) == 0) {
      0.0
    } else {
      (tp + tn) / (tp + tn + fp + fn)
    }
  }

  /**
   * Specificity aka. True Negative Rate
   * @param tn
   * @param fp
   * @return
   */
  def specificity(tn: Double, fp: Double) = {
    if ((tn + fp) == 0) {
      0.0
    } else {
      tn / (tn + fp)
    }
  }

  /**
   * Precision aka. Positive predictive value
   * @param tp
   * @param fp
   * @return
   */
  def precision(tp: Double, fp: Double) = {
    if ((tp + fp) == 0) {
      0.0
    } else {
      tp / (tp + fp)
    }
  }

  /**
   * Recall aka. Hit rate aka. Sensitivity
   * @param tp
   * @param fn
   * @return
   */
  def recall(tp: Double, fn: Double) = {
    if ((tp + fn) == 0) {
      0.0
    } else {
      tp / (tp + fn)
    }
  }

}