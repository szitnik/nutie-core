package si.zitnik.research.iobie.statistics.cluster


/**
 * Class computes CoNLL 2012 official score: (MUC+BCubed+CEAFe)/3
 *
 * for more, see: Sameer Pradhan, Alessandro Moschitti, Nianwen Xue, Olga Uryupina, Yuchen Zhang:
 *                CoNLL-2012 Shared Task: Modeling Multilingual Unrestricted Coreference in OntoNotes
 */
object CoNLL2012Score {
  def scoreExamples(muc: Double, bCubed: Double, ceafe: Double): Double = {
    (muc+bCubed+ceafe) / 3.0
  }
}
