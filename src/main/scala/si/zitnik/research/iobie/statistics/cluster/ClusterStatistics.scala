package si.zitnik.research.iobie.statistics.cluster

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.algorithms.crf.stat.FMeasure

import scala.collection.mutable._

/**
 * Intuitive pairwise method (as applied at merging/entity resolution systems)
 */
class ClusterStatistics(val method: ClusterStatistics.Value = ClusterStatistics.STRICT) extends StrictLogging {
  /**
   * This method calculates statistics of identified clusters over ONE mention example! It calculates
   * pairwise results in the same way as we did at entity resolution.
   *
   * All possible pairs are are calculated from combinations of pair mentions over all mentions in realClusters.
   * Clusters contain at least two mentions, so single mentions are not considered in results.
   *
   * Method returns (TP, TN, FP, FN).
   *
   * @param realClusters
   * @param identifiedClusters
   * @return
   */
  def stat(realClusters: HashSet[Cluster], identifiedClusters: HashSet[Cluster]): (Double, Double, Double) = {
    val IDPairsResolved = IDPair.getIDPairs(identifiedClusters, includeReversePair = true)
    val IDPairsSolution = IDPair.getIDPairs(realClusters, includeReversePair = true)

    //logger.info("Solution pairs: %d, %s".format(IDPairsSolution.size(), IDPairsSolution.mkString(", ")))
    //logger.info("Resolved pairs: %d, %s".format(IDPairsResolved.size(), IDPairsResolved.mkString(", ")))

    val allPossiblePairsNum = getAllPossiblePairsNum(realClusters)

    var TPs = 0
    var FPs = 0
    var TNs = 0
    var FNs = 0

    for (pair <- IDPairsResolved) {
      if (IDPairsSolution.contains(pair)) {
        TPs+=1
      } else {
        FPs+=1
      }
    }

    IDPairsSolution --= IDPairsResolved
    FNs = IDPairsSolution.size

    //TNs are not needed - also incorrectly resolved, when singletons present
    //TNs = allPossiblePairsNum - TPs - FPs - FNs

    //logger.info("Positives: T %d, F %d".format(TPs, FPs))
    //logger.info("Negatives: T %d, F %d".format(TNs, FNs))

    (TPs, FPs, FNs)
  }

  private def getAllPossiblePairsNum(realClusters: HashSet[Cluster]) = {
    var allPossiblePairsNum = 0
    realClusters.foreach(v => allPossiblePairsNum += v.size)
    allPossiblePairsNum*(allPossiblePairsNum-1) / 2
  }

  def stat(allRealClusters: ArrayBuffer[HashSet[Cluster]], allIdentifiedClusters: ArrayBuffer[HashSet[Cluster]]): FMeasure = {

    val allTPs = new ArrayBuffer[Double]()
    val allFPs = new ArrayBuffer[Double]()
    val allFNs = new ArrayBuffer[Double]()

    for ((realClusters, identifiedClusters) <- allRealClusters.zip(allIdentifiedClusters)) {
      val result = stat(realClusters, identifiedClusters)
      allTPs.append(result._1)
      allFPs.append(result._2)
      allFNs.append(result._3)
    }

    new FMeasure(allTPs, allFPs, allFNs)
  }
}

object ClusterStatistics extends Enumeration {
  val STRICT = Value("STRICT") //mention start/end indexes must exactly match
  val OVERLAPPING = Value("OVERLAPPING") //enough mention start/end indexes to overlap
}



