package si.zitnik.research.iobie.statistics.cluster

import collection.mutable.{HashMap, HashSet, ArrayBuffer}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import util.{HungarianAlgorithmScalaWrapper}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/18/12
 * Time: 11:43 AM
 *
 * CEAFType defines:
 *  ORIGINAL = This is an implementation of CEAF coreference metric as proposed in "On Coreference Resolution Performance Metrics"
 * by Xiaoqiang Luo
 *  SYS = Evaluation metrics for End-to-End Coreference Resolution Systems, Cai & Strube
 *
 *
 */
case class CEAFClusterStatistics(val statType: CEAFStatisticsType.Value = CEAFStatisticsType.ENTITY_BASED,
                                 val ceafType: CEAFType.Value = CEAFType.ORIGINAL) {

  private val similarity = statType match {
    case CEAFStatisticsType.ENTITY_BASED => (gold: Cluster, sys: Cluster) => (gold intersect sys).size
    case CEAFStatisticsType.MENTION_BASED => (gold: Cluster, sys: Cluster) => 2 * (gold intersect sys).size / (gold.size + sys.size)
  }

  private def originalCEAF = (goldParam: HashSet[Cluster], guessParam: HashSet[Cluster]) => {
    val gold = goldParam.toArray
    val guess = guessParam.toArray

    val simDim = math.max(gold.size, guess.size)
    val similarities = Array.ofDim[Double](simDim, simDim)

    for (i <- 0 until gold.size) {
      for (j <- 0 until guess.size) {
        similarities(i)(j) = similarity(gold(i), guess(j))
      }
    }
    val (_, fiStar) = HungarianAlgorithmScalaWrapper.hgAlgorithm(similarities)

    val pn: Double = fiStar
    val pd: Double = guess.map(v => similarity(v,v)).sum
    val rn: Double = fiStar
    val rd: Double = gold.map(v => similarity(v,v)).sum


    (pn, pd, rn, rd)
  }

  private def sysCEAF = (goldParam: HashSet[Cluster], guessParam: HashSet[Cluster]) => {
    val allMentionsGold = new HashSet[Constituent]()
    var allMentionsGuess = new HashSet[Constituent]()

    val mentionToClusterGold = new HashMap[Constituent, Cluster]()
    for (g <- goldParam) {
      for (m <- g) {
        mentionToClusterGold.put(m, g)
        allMentionsGold.add(m)
      }
    }

    val mentionToClusterGuess = new HashMap[Constituent, Cluster]()
    for (g <- guessParam) {
      for (m <- g) {
        mentionToClusterGuess.put(m, g)
        allMentionsGuess.add(m)
      }
    }

    //discard singleton twinless mentions in response
    allMentionsGuess = allMentionsGuess.filter(m => mentionToClusterGuess.get(m).get.size > 1 || allMentionsGold.contains(m))
    //put all the twinless annotated mentions into response
    allMentionsGold.filter(!allMentionsGuess.contains(_)).foreach(m => {
      allMentionsGuess += m
    })

    val formClusters = (constituents: HashSet[Constituent], mapping: HashMap[Constituent, Cluster]) => {
      var retVal = HashSet[Cluster]()

      for (c <- constituents) {
        mapping.get(c) match {
          case Some(x) => retVal.add(x)
          case None => retVal.add(new Cluster(c))
        }
      }

      retVal
    }

    //init precision&recall sets
    val gold_p = formClusters(allMentionsGold ++ allMentionsGuess.filter(!allMentionsGold.contains(_)), mentionToClusterGold).toArray
    val guess_p = formClusters(allMentionsGuess, mentionToClusterGuess).toArray
    val gold_r = formClusters(allMentionsGold, mentionToClusterGold).toArray
    val guess_r = formClusters(allMentionsGuess.filter(allMentionsGold.contains(_)), mentionToClusterGuess).toArray

    //precision
    val (pd, pn) = {
      val simDim = math.max(gold_p.size, guess_p.size)
      val similarities = Array.ofDim[Double](simDim, simDim)

      for (i <- 0 until gold_p.size) {
        for (j <- 0 until guess_p.size) {
          similarities(i)(j) = similarity(gold_p(i), guess_p(j))
        }
      }
      val (_, fiStar) = HungarianAlgorithmScalaWrapper.hgAlgorithm(similarities)

      val pd: Double = guess_p.map(v => similarity(v,v)).sum
      val pn: Double = fiStar

      (pd, pn)
    }

    //recall
    val (rd, rn) = {
      val simDim = math.max(gold_r.size, guess_r.size)
      val similarities = Array.ofDim[Double](simDim, simDim)

      for (i <- 0 until gold_r.size) {
        for (j <- 0 until guess_r.size) {
          similarities(i)(j) = similarity(gold_r(i), guess_r(j))
        }
      }
      val (_, fiStar) = HungarianAlgorithmScalaWrapper.hgAlgorithm(similarities)

      val rd: Double = gold_r.map(v => similarity(v,v)).sum
      val rn: Double = fiStar

      (rd, rn)
    }

    (pn, pd, rn, rd)
  }


  /**
   * Scores the whole mention examples and returns CEAF scores (CEAF P, CEAF R, CEAF F1)
   * @param allRealClusters
   * @param allIdentifiedClusters
   */
  def scoreExamples(allRealClusters: ArrayBuffer[HashSet[Cluster]], allIdentifiedClusters: ArrayBuffer[HashSet[Cluster]]): (Double, Double, Double) = {

    var pnum = 0.0
    var pdenom = 0.0
    var rnum = 0.0
    var rdenom = 0.0

    val scoringFun = ceafType match {
      case CEAFType.ORIGINAL => originalCEAF
      case CEAFType.SYS => sysCEAF
      case _ => throw new Exception("Unsupported CEAF type!")
    }

    for ((realClusters, identifiedClusters) <- allRealClusters zip allIdentifiedClusters) {
      scoringFun(realClusters, identifiedClusters) match {
        case (pn, pd, rn, rd) => {
          pnum += pn
          pdenom += pd
          rnum += rn
          rdenom += rd
        }
      }
    }

    val CEAFP = pnum / pdenom
    val CEAFR = rnum / rdenom

    val CEAFF1 = if (CEAFP + CEAFR == 0) 0.0 else 2.0 * CEAFP * CEAFR / (CEAFP + CEAFR)

    (CEAFP, CEAFR, CEAFF1)
  }
}

object CEAFStatisticsType extends Enumeration {
  val MENTION_BASED = Value("m")
  val ENTITY_BASED = Value("e")
}

object CEAFType extends Enumeration {
  val ORIGINAL = Value("ORIGINAL")
  val SYS = Value("SYS")
}
