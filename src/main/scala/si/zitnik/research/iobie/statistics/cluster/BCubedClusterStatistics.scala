package si.zitnik.research.iobie.statistics.cluster

import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable._
import collection.mutable
import scala.Some

/**
 * Calculates BCubed score by:
 *  Amit Bagga and Breck Baldwin
 *  Algorithms for scoring coreference chains, 1998
 *
 *  ------------
 *
 *  B-cubed cluster scoring was defined as an alternative to the MUC scoring metric. There are two variants
 *  B-cubed cluster precision, both of which are weighted averages of a per-element precision score:
 *
 *   b3Precision(A,referencePartition,responsePartition)
 *   = |cluster(responsePartition,A) INTERSECT cluster(referencePartition,A)|
 *   / |cluster(responsePartition,A)|
 *   where cluster(partition,a) is the cluster in the partition partition containing the element a;
 *   in other words, this is A's equivalence class and contains the set of all elements equivalent to A in the partition.
 *
 *   For the uniform element method, each element a is weighted uniformly:
 *
 *   b3ElementPrecision(ReferencePartition,ResponsePartition)
 *   = Σa b3Precision(a,referencePartition,responsePartition) / numElements
 *   where numElements is the total number of elements in the partitions. For both B-cubed approaches,
 *   recall is defined dually by switching the roles of reference and response, and the F1-measure is defined in the usual way.
 */
object BCubedClusterStatistics {

  private def bSysScore = (gold: HashSet[Cluster], guess: HashSet[Cluster]) => {

    val allMentionsGold = new HashSet[Constituent]()
    var allMentionsGuess = new HashSet[Constituent]()

    val mentionToClusterGold = new HashMap[Constituent, Cluster]()
    for (g <- gold) {
      for (m <- g) {
        mentionToClusterGold.put(m, g)
        allMentionsGold.add(m)
      }
    }

    val mentionToClusterGuess = new HashMap[Constituent, HashSet[Constituent]]()
    for (g <- guess) {
      for (m <- g) {
        mentionToClusterGuess.put(m, g)
        allMentionsGuess.add(m)
      }
    }

    //discard singleton twinless mentions in response
    allMentionsGuess = allMentionsGuess.filter(m => mentionToClusterGuess.get(m).get.size > 1 || allMentionsGold.contains(m))
    //put all the twinless annotated mentions into response
    allMentionsGold.filter(!allMentionsGuess.contains(_)).foreach(m => {
      allMentionsGuess += m
    })


    //init precision&recall sets
    val allMentionsGold_p = allMentionsGold ++ allMentionsGuess.filter(!allMentionsGold.contains(_))
    val allMentionsGuess_p = allMentionsGuess
    val allMentionsGold_r = allMentionsGold
    val allMentionsGuess_r = allMentionsGuess.filter(allMentionsGold.contains(_))

    if (allMentionsGold_p.size != allMentionsGuess_p.size || allMentionsGold_r.size != allMentionsGuess_r.size) {
      throw new Exception("Wrong BCubed initialisation - gold and guess sets do not match")
    }

    //precision
    var p = 0.0
    for (m <- allMentionsGold_p){
      val mGuess = mentionToClusterGuess.get(m) match {
        case Some(x) => x
        case None => new Cluster(m)
      }
      val mGold = mentionToClusterGold.get(m) match {
        case Some(x) => x
        case None => new Cluster(m)
      }
      p += (mGuess intersect mGold).size * 1.0 / mGuess.size
    }

    //recall
    var r = 0.0
    for (m <- allMentionsGold_r){
      val mGuess = mentionToClusterGuess.get(m) match {
        case Some(x) => x
        case None => new Cluster(m)
      }
      val mGold = mentionToClusterGold.get(m) match {
        case Some(x) => x
        case None => new Cluster(m)
      }
      r += (mGuess intersect mGold).size * 1.0 / mGold.size
    }

    p = p / allMentionsGold_p.size
    r = r / allMentionsGold_r.size

    (p, r)
  }

  private def bRNScore = (gold: HashSet[Cluster], guess: HashSet[Cluster]) => {

    val allMentions = new HashSet[Constituent]()

    val mentionToClusterGold = new HashMap[Constituent, Cluster]()
    for (g <- gold) {
      for (m <- g) {
        mentionToClusterGold.put(m, g)
        allMentions.add(m)
      }
    }

    val mentionToClusterGuess = new HashMap[Constituent, HashSet[Constituent]]()
    for (g <- guess) {
      for (m <- g) {
        mentionToClusterGuess.put(m, g)
        if (g.size > 1 || allMentions.contains(m)) {
          allMentions.add(m)
        }
      }
    }


    var p = 0.0
    var r = 0.0
    var pCounter = 0.0
    var rCounter = 0.0
    for (m <- allMentions){
      val mGuess = mentionToClusterGuess.get(m) match {
        case Some(x) => x
        case None => new Cluster()
      }
      val mGold = mentionToClusterGold.get(m) match {
        case Some(x) => x
        case None => new Cluster()
      }

      if (mGuess.size > 0 && mGold.size > 0) {
        p += (mGuess intersect mGold).size * 1.0 / mGuess.size
        r += (mGuess intersect mGold).size * 1.0 / mGold.size
        pCounter += 1
        rCounter += 1
      } else if (mGold.size > 0) {
        r += 1.0 / mGold.size
        rCounter += 1
      } else if (mGuess.size > 0) {
        p += 1.0 / mGuess.size
        pCounter += 1
      }
    }

    p = p / pCounter
    r = r / rCounter

    (p, r)
  }

  private def bAllScore = (gold: HashSet[Cluster], guess: HashSet[Cluster]) => {

    val allMentions = new HashSet[Constituent]()

    val mentionToClusterGold = new HashMap[Constituent, Cluster]()
    for (g <- gold) {
      for (m <- g) {
        mentionToClusterGold.put(m, g)
        allMentions.add(m)
      }
    }

    val mentionToClusterGuess = new HashMap[Constituent, Cluster]()
    for (g <- guess) {
      for (m <- g) {
        mentionToClusterGuess.put(m, g)
        allMentions.add(m)
      }
    }



    var p = 0.0
    var r = 0.0
    var pCounter = 0.0
    var rCounter = 0.0
    for (m <- allMentions){
      val mGuess = mentionToClusterGuess.get(m) match {
        case Some(x) => x
        case None => new Cluster()
      }
      val mGold = mentionToClusterGold.get(m) match {
        case Some(x) => x
        case None => new Cluster()
      }

      if (mGuess.size > 0 && mGold.size > 0) {
        p += (mGuess intersect mGold).size * 1.0 / mGuess.size
        r += (mGuess intersect mGold).size * 1.0 / mGold.size
        pCounter += 1
        rCounter += 1
      } else if (mGold.size > 0) {
        r += 1.0 / mGold.size
        rCounter += 1
      } else if (mGuess.size > 0) {
        p += 1.0 / mGuess.size
        pCounter += 1
      }
    }

    p = p / pCounter
    r = r / rCounter

    (p, r)
  }

  private def b0Score = (gold: HashSet[Cluster], guess: HashSet[Cluster]) => {

    val allMentions = new HashSet[Constituent]()

    val mentionToClusterGold = new HashMap[Constituent, Cluster]()
    for (g <- gold) {
      for (m <- g) {
        mentionToClusterGold.put(m, g)
        allMentions.add(m)
      }
    }

    val mentionToClusterGuess = new HashMap[Constituent, mutable.HashSet[Constituent]]()
    for (g <- guess) {
      for (m <- g) {
        mentionToClusterGuess.put(m, g.filter(allMentions.contains(_))) //discard twinless
      }
    }



    var p = 0.0
    var r = 0.0
    var pCounter = 0.0
    var rCounter = 0.0
    for (m <- allMentions){
      val mGuess = mentionToClusterGuess.get(m) match {
        case Some(x) => x
        case None => new Cluster()
      }
      val mGold = mentionToClusterGold.get(m) match {
        case Some(x) => x
        case None => new Cluster()
      }

      if (mGuess.size > 0 && mGold.size > 0) {
        p += (mGuess intersect mGold).size * 1.0 / mGuess.size
        r += (mGuess intersect mGold).size * 1.0 / mGold.size
        pCounter += 1
        rCounter += 1
      } else if (mGold.size > 0) { //twinless keymention
        rCounter += 1
      }
    }

    p = if (pCounter == 0) 0 else p / pCounter
    r = if (rCounter == 0) 0 else r / rCounter

    (p, r)
  }

  /**
   * Scores the whole mention examples and returns BCubed scores (BCubed P, BCubed R, BCubed F1)
   * @param allRealClusters
   * @param allIdentifiedClusters
   */
  def scoreExamples(allRealClusters: ArrayBuffer[HashSet[Cluster]],
                    allIdentifiedClusters: ArrayBuffer[HashSet[Cluster]],
                    bType: BCubedType.Value = BCubedType.BCUBED_0): (Double, Double, Double) = {

    //score method type:
    //(HashSet[Cluster], HashSet[Cluster]) => (Double, Double)
    val pr = bType match {
      case BCubedType.BCUBED_0 => b0Score
      case BCubedType.BCUBED_ALL => bAllScore
      case BCubedType.BCUBED_R_AND_N => bRNScore
      case BCubedType.BCUBED_SYS => bSysScore
      case _ => throw new UnsupportedOperationException("BCubed type '%s' does not exist!".format(bType.toString))
    }

    var BCubedP = 0.0
    var BCubedR = 0.0

    for ((realClusters, identifiedClusters) <- allRealClusters zip allIdentifiedClusters) {
      pr(realClusters, identifiedClusters) match {
        case (p, r) => {
          BCubedP += p
          BCubedR += r
        }
      }
    }

    BCubedP /= allRealClusters.size //= number of mention examples
    BCubedR /= allRealClusters.size

    val BCubedF1 = if (BCubedP + BCubedR == 0) 0.0 else 2.0 * BCubedP * BCubedR / (BCubedP + BCubedR)

    (BCubedP, BCubedR, BCubedF1)
  }

}

/**
 * Classification aligned to:
 *  Cai & Strube: Evaluation Metrics For End-to-End Coreference Resolution Systems
 */
object BCubedType extends Enumeration {
  val BCUBED_0 = Value("B0")
  val BCUBED_ALL = Value("Ball")
  val BCUBED_R_AND_N = Value("Br&n")
  val BCUBED_SYS = Value("Bsys")
}
