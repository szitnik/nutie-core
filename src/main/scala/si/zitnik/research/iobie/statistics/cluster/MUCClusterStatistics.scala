package si.zitnik.research.iobie.statistics.cluster


import scala.collection.mutable._
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable

/**
 * Calculating MUC coreference score by:
 *  Marc Vilain, John Burger, John Aberdeen, Dennis Connoly, Lynette Hirschman
 *  A Model-Theoretic Coreference Scoring Scheme, 1995
 */
object MUCClusterStatistics {

  private def precision(gold: HashSet[Cluster], guess: HashSet[Cluster]): (Double, Double) = {
    var numP: Int = 0
    var denP: Int = 0

    val mentionToCluster = new mutable.HashMap[Constituent, Cluster]()
    for (g <- gold) {
      for (m <- g) {
        mentionToCluster.put(m, g)
      }
    }

    for (c <- guess if c.size > 0) {
      numP += c.size

      var pCounter = 0
      val partitions: Set[Cluster] = new HashSet[Cluster]
      for (m <- c) {
        mentionToCluster.get(m) match {
          case Some(x) => partitions.add(x)
          case None => pCounter += 1 //hack if singleton clusters are not included
        }
      }
      numP -= (partitions.size + pCounter)
      denP += c.size - 1
    }
    (1.0 * numP, 1.0 * denP)
  }

  private def recall(gold: HashSet[Cluster], guess: HashSet[Cluster]): (Double, Double) = {
    var numR: Int = 0
    var denR: Int = 0

    val mentionToCluster = new mutable.HashMap[Constituent, Cluster]()
    for (g <- guess) {
      for (m <- g) {
        mentionToCluster.put(m, g)
      }
    }

    for (c <- gold if c.size > 0) {
      numR += c.size

      var pCounter = 0
      val partitions: Set[Cluster] = new HashSet[Cluster]
      for (m <- c) {
        mentionToCluster.get(m) match {
          case Some(x) => partitions.add(x)
          case None => pCounter += 1 //hack if singleton clusters are not included
        }
      }
      numR -= (partitions.size + pCounter)
      denR += c.size - 1
    }
    (1.0 * numR, 1.0 * denR)
  }

  /**
   * Scores the whole mention examples and returns MUC scores (MUC P, MUC R, MUC F1)
   * @param allRealClusters
   * @param allIdentifiedClusters
   */
  def scoreExamples(allRealClusters: ArrayBuffer[HashSet[Cluster]], allIdentifiedClusters: ArrayBuffer[HashSet[Cluster]]): (Double, Double, Double) = {

    var precisionNumSum = 0.0
    var recallNumSum = 0.0
    var precisionDenSum = 0.0
    var recallDenSum = 0.0

    for ((realClusters, identifiedClusters) <- allRealClusters zip allIdentifiedClusters) {
      precision(realClusters, identifiedClusters) match {
        case (num, den) => precisionNumSum += num; precisionDenSum += den
      }

      recall(realClusters, identifiedClusters) match {
        case (num, den) => recallNumSum += num; recallDenSum += den
      }
    }

    val MUCP = if (precisionDenSum == 0)  0.0 else precisionNumSum / precisionDenSum
    val MUCR = if (recallDenSum == 0) 0.0 else recallNumSum / recallDenSum
    val MUCF1 = if (MUCP+MUCR == 0) 0.0 else 2.0 * MUCP * MUCR / (MUCP + MUCR)

    (MUCP, MUCR, MUCF1)
  }

}


