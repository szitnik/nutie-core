package si.zitnik.research.iobie.statistics.cluster

import com.typesafe.scalalogging.StrictLogging

import collection.mutable._
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent

import collection.mutable

/**
 * BLANC measure implemented by "BLANC: Implementing the Rand Index for Coreference Evaluation"
 * Marta Recasens, Eduard Hovy, 14.8.2010
 */
object BLANCClusterStatistics extends StrictLogging {

  private def getCorrectNonCorefIDPairsNum(goldClusters: HashSet[Cluster], sysClusters: HashSet[Cluster]): Double = {
    var retVal = 0.0

    val mentionToCluster = new mutable.HashMap[Constituent, Cluster]()
    for (g <- sysClusters) {
      for (m <- g) {
        mentionToCluster.put(m, g)
      }
    }

    for {
      g1 <- goldClusters
      g2 <- goldClusters if (g1 != g2)
      c1 <- g1
      c2 <- g2 if (!mentionToCluster.get(c1).get.equals(mentionToCluster.get(c2).get))
    } {
      retVal += 1
    }

    retVal /= 2.0

    retVal
  }

  private def getNonCorefIDPairsNum(clusters: HashSet[Cluster], allMentionsSize: Int): Double = {
    var retVal = 0.0
    for (c <- clusters) {
      retVal += c.size * (allMentionsSize-c.size)
    }
    retVal /= 2.0
    retVal
  }

  /**
   * Method returns (BLANC-P, BLANC-R, BLANC-F)
   * @param gold
   * @param sys
   * @return
   */
  private def documentBlanc(gold: HashSet[Cluster], sys: HashSet[Cluster]): (Double, Double, Double) = {
    val allMentionsGold = gold.flatMap(v1 => v1.map(v2 => v2))
    val allMentionsSys = sys.flatMap(v1 => v1.map(v2 => v2))

    val goldHasBothTypeLinks = gold.size > 1 && gold.size < allMentionsGold.size
    val sysHasBothTypeLinks = sys.size > 1 && sys.size < allMentionsSys.size

    val N = allMentionsGold.size

    val goldenLinksC = IDPair.getIDPairs(gold)
    val systemLinksC = IDPair.getIDPairs(sys)


    val systemLinksNsize = getNonCorefIDPairsNum(sys, allMentionsSys.size)

    //right coreferent (=TP)
    var rc: Double = (goldenLinksC intersect systemLinksC).size
    //wrong coreferent (=FP)
    var wc: Double = systemLinksC.size - rc
    //right not coreferent (=TN)
    var rn: Double = getCorrectNonCorefIDPairsNum(gold, sys)
    //wrong not coreferent (=FN)
    var wn: Double = systemLinksNsize - rn


    val L = N*(N-1)/2.0
    val SL = rc + wc + rn + wn

    if (L != SL) {
      logger.error("Problem calculating BLANC measure! Values L (%.2f) and SL (%.2f) do not match!".format(L, SL))
    }

    var Pc = rc / (rc + wc)
    var Rc = rc / (rc + wn)
    var Fc = 2 * Pc * Rc / (Pc + Rc)

    var Pn = rn / (rn + wn)
    var Rn = rn / (rn + wc)
    var Fn = 2 * Pn * Rn / (Pn + Rn)

    //Boundary cases:
    //Case 1
    if (sys.size == 1) {
      if (gold.size == 1) {
        return (100.0, 100.0, 100.0)
      }
      if (gold.size == allMentionsGold.size){
        return (0.0, 0.0, 0.0)
      }
      if (goldHasBothTypeLinks) {
        Pn = 0.0
        Rn = 0.0
        Fn = 0.0
      }
    }
    //Case 2:
    if (allMentionsSys.size == sys.size) {
      if (gold.size == sys.size) {
        return (100.0, 100.0, 100.0)
      }
      if (gold.size == 1) {
        return (0.0, 0.0, 0.0)
      }
      if (goldHasBothTypeLinks) {
        Pc = 0.0
        Rc = 0.0
        Fc = 0.0
      }
    }
    //Case 3:
    if (goldHasBothTypeLinks) {
      if (rc == 0) {
        Pc = 0.0
        Rc = 0.0
        Fc = 0.0
      }
      if (rn == 0) {
        Pn = 0.0
        Rn = 0.0
        Fn = 0.0
      }
    }
    //Case 4:
    if (sysHasBothTypeLinks) {
      if (gold.size == 1) {
        return (Pc, Rc, Fc)
      }
      if (allMentionsGold.size == gold.size) {
        return (Pn, Rn, Fn)
      }
    }


    val BLANC_P = (Pc + Pn) / 2.0
    val BLANC_R = (Rc + Rn) / 2.0
    val BLANC_F = (Fc + Fn) / 2.0

    (BLANC_P, BLANC_R, BLANC_F)
  }

  /**
   * Scores the whole mention examples and returns BLANC scores (BLANC P, BLANC R, BLANC F1)
    *
    * TODO: repair scoring!
   * @param allRealClusters
   * @param allIdentifiedClusters
   */
  def scoreExamples(allRealClusters: ArrayBuffer[HashSet[Cluster]], allIdentifiedClusters: ArrayBuffer[HashSet[Cluster]]): (Double, Double, Double) = {

    var BLANC_P = 0.0
    var BLANC_R = 0.0
    var BLANC_F = 0.0

    for ((realClusters, identifiedClusters) <- allRealClusters zip allIdentifiedClusters) {
      documentBlanc(realClusters, identifiedClusters) match {
        case (p, r, f) => {
          BLANC_P += p
          BLANC_R += r
          BLANC_F += f
        }
      }
    }

    BLANC_P /= allRealClusters.size
    BLANC_R /= allRealClusters.size
    BLANC_F /= allRealClusters.size

    (BLANC_P, BLANC_R, BLANC_F)
  }
}
