package si.zitnik.research.iobie.statistics.test

import org.apache.commons.math3.stat.inference.TestUtils
import org.apache.commons.math3.stat.descriptive.SummaryStatistics

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 5/6/14
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */
class TTest(mean: Double, sample: Array[Double], alpha: Double, tableTValue: Double = -1) {
  val sampleStats = new SummaryStatistics()
  for (i <- 0 until sample.length) {
    sampleStats.addValue(sample(i))
  }

  def tStatistic() = {
    TestUtils.t(mean, sample)
  }

  def pValue() = {
    TestUtils.tTest(mean, sample)
  }

  def rejected() = {
    TestUtils.tTest(mean, sample, alpha)
  }

  def confidenceInterval() = {
    val stdDev = sampleStats.getStandardDeviation()
    val denom = math.sqrt(sample.length)

    tableTValue*stdDev/denom
  }

  def toDetailString() = {
    "T-test:\n\tdata: %s\n\tmu: %.2f\n\tt-statistic: %.4f\n\tp-value: %.4f\n\trejected with confidence level %.2f: %s\n\tconfidence interval: %.2f +- %.2f".format(
      sample.mkString(","),
      mean,
      tStatistic(),
      pValue(),
      (1-alpha),
      rejected()+"",
      sampleStats.getMean,
      confidenceInterval()
    )
  }

}
