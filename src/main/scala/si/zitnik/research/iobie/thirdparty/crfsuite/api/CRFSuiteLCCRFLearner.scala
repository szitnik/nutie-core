package si.zitnik.research.iobie.thirdparty.crfsuite.api

import si.zitnik.research.iobie.algorithms.crf.{Classifier, FeatureFunction, Label, Learner}
import si.zitnik.research.iobie.domain.Examples
import java.util.ArrayList

import si.zitnik.research.iobie.algorithms.crf.linearchain.FeatureDict
import java.io.File

import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.util.StreamGobbler
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/9/12
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */

class CRFSuiteLCCRFLearner(val examples: Examples,
                           val learnLabelType: Label.Value,
                           val featureFunctions: ArrayList[FeatureFunction],
                           val modelSaveFilename: String = "model.obj",
                           val featureThreshold: Int = 5,
                           val printCRFSuiteOutput: Boolean = false,
                           val maxIterations: Option[Int] = None,
                           val removePreTrainedModels: Boolean = true) extends Learner(examples) with StrictLogging {

  val featureDict = new FeatureDict(learnLabelType, featureFunctions, examples, featureThreshold)
  val command = List(
    IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_CMD),
    "learn",
    "-m",
    IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_MODEL_FOLDER) + "/" + modelSaveFilename) :::
    { if (maxIterations.isDefined) List("max_iterations=%d".format(maxIterations.get)) else List() } :::
    List("-")

  def train(): Classifier = {
    if (new File(IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_MODEL_FOLDER) + "/" + modelSaveFilename).exists()) {
      if (removePreTrainedModels) {
        new File(IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_MODEL_FOLDER) + "/" + modelSaveFilename).delete()
      } else {
        return new CRFSuiteLCCRFClassifier(learnLabelType, modelSaveFilename, featureDict, printCRFSuiteOutput)
      }
    }

    //START CRFSuite
    val pb = new ProcessBuilder(command)
    val p = pb.start()

    //INPUT DATA
    logger.info("Adding data to CRFSuite input ...")
    CRFSuiteUtil.writeExamples(examples, learnLabelType, featureDict, p.getOutputStream())
    p.getOutputStream().close()
    logger.info("... DONE. Running CRFSuite ...")


    //LOG CRFSuite's output
    val errorGobbler = new StreamGobbler(p.getErrorStream(), printCRFSuiteOutput)
    val outputGobbler = new StreamGobbler(p.getInputStream(), printCRFSuiteOutput)
    errorGobbler.start()
    outputGobbler.start()
    val exitStatus = p.waitFor()
    if (exitStatus != 0) {
      logger.error("Exit status: %d".format(exitStatus))
      throw new RuntimeException("Failed to run CRFSuite")
    }
    //read everything from gobblers
    errorGobbler.join()
    outputGobbler.join()
    logger.info("... CRFSuite run finished.")

    new CRFSuiteLCCRFClassifier(learnLabelType, modelSaveFilename, featureDict, printCRFSuiteOutput)
  }


}
