package si.zitnik.research.iobie.thirdparty.genderdata.api

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/12/12
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */
object NumberAndGenderDataFactory {
  private var nagd: NumberAndGenderData = null

  def instance() = {
    if (nagd == null) {
      nagd = new NumberAndGenderData()
    }
    nagd
  }
}
