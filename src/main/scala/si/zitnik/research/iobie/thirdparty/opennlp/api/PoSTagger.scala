package si.zitnik.research.iobie.thirdparty.opennlp.api


import com.typesafe.scalalogging.StrictLogging
import opennlp.tools.postag.{POSModel, POSTaggerME}

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.{Example, Examples}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/7/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */

class PoSTagger extends Tagger with StrictLogging {
  var posTagger: POSTaggerME = null

  init()
  private def init(): Unit = {
    val model = this.getClass.getClassLoader.getResourceAsStream("apache-opennlp-1.5.2-incubating/models/en-pos-maxent.bin")
    val posModel = new POSModel(model)
    model.close()
    posTagger = new POSTaggerME(posModel)
  }

  def tag(examples: Examples): Unit = {
    for (example <- examples) {
      tag(example)
    }
  }

  def tag(example: Example): Unit = {
    //warn if tag already exists
    //if (example.getLabeling(Label.POS).size() != 0) {
    //  logger.warn("There exist an example already having POS tags! Tags will be overwritten!")
    //}
    val tags = posTagger.tag(example.getLabeling(Label.OBS).toArray(Array[String]()))
    example.setLabeling(Label.POS, tags)
  }
}
