package si.zitnik.research.iobie.thirdparty.genderdata.api

import java.util.HashMap

import com.typesafe.scalalogging.StrictLogging

import io.Source

/**
 * 1) all the words in the phrase,
 * 2) with the first word in the phrase followed by the "!" character, and
 * 3) with "!" followed by the last word in the phrase.
 * For example, we store "dr. martin luther king jr. boulevard" as
 * "dr. martin luther king jr. boulevard", "dr. !" and "! boulevard".
 *
 * noun phrase [TAB] Masculine_Count [SPACE] Feminine_Count [SPACE] Neutral_Count [SPACE] Plural_Count
 */

class NumberAndGenderData extends StrictLogging {
  val data = new HashMap[String, NaGCounts]()

  init()

  private def init(): Unit = {
    logger.info("Starting to read gender domain.")

    for (line <- Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("genderdata/gender.data"), "iso-8859-1").getLines()) {
      val lineTab = line.split("\t")

      val counts = new NaGCounts(lineTab(1).split(" "))
      val nounPhrase = lineTab(0)

      data.put(nounPhrase, counts)
    }
    logger.info("Gender domain read.")
  }

  /**
   * First transforms numbers, than returns max count (GENDER, COUNT) if exists in order:
   * 1)
   * 2)
   * 3)
   *
   *
   * see explanation above
   *
   * If the search term is not found, null is returned.
   */
  def getMaxCountsResult(np: String): (GenderData.Value, NumberData.Value) = {
    val n = np.replaceAll("[0-9]+", "#")
    val nsplit = n.split(" ")

    var counts: NaGCounts = null
    if (counts == null) {
      counts = data.get(n)
    }
    if (counts == null) {
      counts = data.get(nsplit(0) + " !")
    }
    if (counts == null) {
      counts = data.get("! " + nsplit(nsplit.length - 1))
    }
    if (counts != null) {
      val gen = if (counts.masculine > counts.feminine) {
        if (counts.masculine > counts.neutral) GenderData.MALE else GenderData.NEUTRAL
      } else {
        if (counts.feminine > counts.neutral) GenderData.FEMALE else GenderData.NEUTRAL
      }
      val num = if (counts.plural > 0) NumberData.PLURAL else NumberData.SINGULAR

      (gen, num)
    } else {
      null
    }
  }
}

object NumberData extends Enumeration {
  val SINGULAR = Value("SI")
  val PLURAL = Value("PL")
}

object GenderData extends Enumeration {
  val MALE = Value("M")
  val FEMALE = Value("F")
  val NEUTRAL = Value("N")
}

class NaGCounts(counts: Array[String]) {
  val masculine = counts(0).toInt
  val feminine = counts(1).toInt
  val neutral = counts(2).toInt
  val plural = counts(3).toInt
}