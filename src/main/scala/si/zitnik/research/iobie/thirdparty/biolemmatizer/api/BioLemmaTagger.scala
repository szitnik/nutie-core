package si.zitnik.research.iobie.thirdparty.biolemmatizer.api

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.thirdparty.opennlp.api.Tagger
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.Label
import edu.ucdenver.ccp.nlp.biolemmatizer.BioLemmatizer

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 16/01/14
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
class BioLemmaTagger extends Tagger with StrictLogging {
  var bioLemmatizer: BioLemmatizer = null

  init()

  def init(): Unit = {
    bioLemmatizer = new BioLemmatizer()
  }

  def tag(examples: Examples): Unit = {
    for (example <- examples) {
      tag(example)
    }
  }

  def tag(example: Example): Unit = {
    for (token <- example) {
      if (token.contains(Label.LEMMA)) {
        logger.warn("Data has already been lemmatized! Overwriting lemmas...")
      }
      if (!token.contains(Label.POS)) {
        logger.warn("Data does not contain POS tags! Results may be bad...")
      }
      token.put(Label.LEMMA, bioLemmatizer.lemmatizeByLexiconAndRules(token.get(Label.OBS), token.get(Label.POS)).getLemmas.toList.get(0).getLemma)
    }
  }

}
