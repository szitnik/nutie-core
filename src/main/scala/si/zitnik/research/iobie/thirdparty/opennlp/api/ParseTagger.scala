package si.zitnik.research.iobie.thirdparty.opennlp.api

import si.zitnik.research.iobie.domain.{Example, Examples}

import opennlp.tools.parser._

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.Label

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.IOBIEConversions._
import opennlp.tools.util.Span
import si.zitnik.research.iobie.domain.parse.ParseTree

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/8/12
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */

class ParseTagger(val escapeBrackets: Boolean = false) extends Tagger with StrictLogging {
  var parser: Parser = null

  val leftBracketEscaper = "X_LEVI_OKLEPAJ_X"
  val rightBracketEscaper = "X_DESNI_OKLEPAJ_X"

  init()
  private def init(): Unit = {
    val modelIn = this.getClass.getClassLoader.getResourceAsStream("apache-opennlp-1.5.2-incubating/models/en-parser-chunking.bin")
    val model = new ParserModel(modelIn)
    modelIn.close()
    parser = ParserFactory.create(model)
  }

  def tag(examples: Examples): Unit = {
    for (example <- examples) {
      tag(example)
    }
  }

  def tag(example: Example): Unit = {
    val sentence = if (escapeBrackets) {
      example.map(_.get(Label.OBS).asInstanceOf[String].replaceAll("\\(", leftBracketEscaper).replaceAll("\\)", rightBracketEscaper)).toArray
    } else {
      example.map(_.get(Label.OBS).asInstanceOf[String]).toArray
    }
    val parse = getParseString(sentence)
    ParseTree.setParseTree(example, parse)
  }

  def getParseString(sentence: Array[String]) = {
    //build parse input
    val parse = buildParseInput(sentence)
    val sb = new StringBuffer()
    parser.parse(parse).show(sb)
    sb.toString.replaceAll(" \\(", "(")
  }

  private def buildParseInput(sentence: Array[String]) = {
    val text = sentence.mkString(" ")
    val p = new Parse(text,
      // a new span covering the entire text
      new Span(0, text.length()),
      // the label for the top if an incomplete node
      AbstractBottomUpParser.INC_NODE,
      // the probability of this parse...uhhh...?
      1,
      // the token index of the head of this parse
      0);

    var start = 0
    for (idx <- 0 until sentence.length) {
      val tok: String = sentence(idx)
      // flesh out the parse with token sub-parses
      p.insert(new Parse(text,
        new Span(start, start + tok.length()),
        AbstractBottomUpParser.TOK_NODE,
        0,
        idx))
      // find the index of the next token, +1 is whitespace sign
      start += tok.length + 1
    }

    p
  }

}
