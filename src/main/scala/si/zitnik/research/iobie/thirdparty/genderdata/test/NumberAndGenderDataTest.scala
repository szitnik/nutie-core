package si.zitnik.research.iobie.thirdparty.genderdata.test

import si.zitnik.research.iobie.thirdparty.genderdata.api.NumberAndGenderData

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/23/12
 * Time: 3:52 PM
 * To change this template use File | Settings | File Templates.
 */

object NumberAndGenderDataTest {

  def main(args: Array[String]): Unit = {
    val ngd = new NumberAndGenderData()
    println(ngd.getMaxCountsResult("jacksonsXXXXXX"))
    println(ngd.getMaxCountsResult("jacksons"))
  }

}
