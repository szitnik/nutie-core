package si.zitnik.research.iobie.thirdparty.opennlp.api

import opennlp.tools.sentdetect.{SentenceDetectorME, SentenceModel}
import scala.collection.mutable.ArrayBuffer

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/21/13
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class SentenceDetector {
  var sentDetector: SentenceDetectorME = null

  init()
  private def init(): Unit = {
    val model = this.getClass.getClassLoader.getResourceAsStream("apache-opennlp-1.5.2-incubating/models/en-sent.bin")
    val sentModel = new SentenceModel(model)
    model.close()
    sentDetector = new SentenceDetectorME(sentModel)
  }


  def inbetween(first: String, second: String, text: String) = {
    var idx1 = text.indexOf(first) + first.size
    var idx2 = text.indexOf(second, idx1)

    while (!text.substring(idx1, idx2).trim.isEmpty) {
      idx1 = text.indexOf(first, idx1-first.size+1) + first.size
      idx2 = text.indexOf(second, idx1)

    }

    try {
      text.substring(idx1, idx2)
    } catch {
      case e: Exception => {
        println("First id: %d: '%s'\nSecond id %d: '%s'\nText: '%s'".format(idx1, first, idx2, second, text))
        System.exit(-1)
      }
    }
  }

  def detectSentences(text: String, exceptionsWhereNotToSplit: Array[String] = Array()) = {
    var sentences = new ArrayBuffer[String]()
    sentences.appendAll(sentDetector.sentDetect(text))

    var tempSentences = new ArrayBuffer[String]()
    for (notToSplit <- exceptionsWhereNotToSplit) {
      var firstSentence = sentences(0)
      for (secondSentence <- sentences.slice(1, sentences.size)) {
        if (firstSentence.endsWith(notToSplit)) {
          firstSentence += inbetween(firstSentence, secondSentence, text) + secondSentence
          if (!text.contains(firstSentence)) {
            println("The text between sentences found was wrong!")
            System.exit(-1)
          }
        } else {
          tempSentences.append(firstSentence)
          firstSentence = secondSentence
        }
      }
      tempSentences.append(firstSentence)
      sentences = tempSentences
      tempSentences = new ArrayBuffer[String]()
    }

    sentences
  }
}
