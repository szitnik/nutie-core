package si.zitnik.research.iobie.thirdparty.pronoun

import com.typesafe.scalalogging.StrictLogging

import io.Source

import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/24/12
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
class PronounData(language: String) extends StrictLogging {
  val data = new mutable.HashMap[String, String]()

  init()

  private def init(): Unit = {
    logger.info("Starting to read pronoun domain.")
    val lineTab = " "
    var counter = 0
    for (line <- Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("pronouns/"+language+"_pronouns.txt")).getLines()) {
      if (!line.startsWith("#")) {
        val idx = line.lastIndexOf(lineTab)
        data.put(line.substring(0, idx), line.substring(idx+1, line.size))
        counter += 1
      }
    }
    logger.info("Pronoun domain read. %d pronoun types added.".format(counter))
  }
}
