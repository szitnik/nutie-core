package si.zitnik.research.iobie.thirdparty.slopostags

import si.zitnik.research.iobie.algorithms.crf.Label

import scala.collection.immutable.HashMap
import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/24/12
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
object SloPosTagsDataFactory {
  private var data = new mutable.HashMap[String, HashMap[Label.Value, String]]()

  def instance() = {
    if (data.isEmpty) {
      data = new SloPosTagsData().data
    }
    data
  }
}
