package si.zitnik.research.iobie.thirdparty.crfsuite.test

import io.Source
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/9/12
 * Time: 11:44 AM
 * To change this template use File | Settings | File Templates.
 */

object StreamTest {
  def main(args: Array[String]): Unit = {
    //val p = Runtime.getRuntime().exec("crfsuite learn - << \"EOF\"")

    val pb = new ProcessBuilder(IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_CMD), "learn", "-m", "model.obj", "-")
    val p = pb.start()

    val outS = p.getOutputStream();
    outS.write("a a\n".getBytes("utf-8"))
    outS.close()

    val inS = p.getInputStream()
    val exitStatus = p.waitFor()
    println(exitStatus)

    val r = Source.fromInputStream(inS).getLines()

    while (r.hasNext) {
      println(r.next())
    }

  }
}
