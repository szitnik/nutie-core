package si.zitnik.research.iobie.thirdparty.opennlp.api

import opennlp.tools.tokenize.{TokenizerModel, TokenizerME}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/21/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
class Tokenizer {
  var tokenizer: TokenizerME = null

  init()
  private def init(): Unit = {
    val model = this.getClass.getClassLoader.getResourceAsStream("apache-opennlp-1.5.2-incubating/models/en-token.bin")
    val tokenModel = new TokenizerModel(model)
    model.close()
    tokenizer = new TokenizerME(tokenModel)
  }

  def tokenize(sentence: String): Array[String] = tokenizer.tokenize(sentence)

  def tokenize(sentences: String*): Array[Array[String]] = sentences.map(tokenize(_)).toArray
}
