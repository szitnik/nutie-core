package si.zitnik.research.iobie.thirdparty.opennlp.api

import si.zitnik.research.iobie.domain.{Example, Examples}


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/7/12
 * Time: 11:21 AM
 * To change this template use File | Settings | File Templates.
 */

abstract class Tagger {
  def tag(examples: Examples)

  def tag(example: Example)
}
