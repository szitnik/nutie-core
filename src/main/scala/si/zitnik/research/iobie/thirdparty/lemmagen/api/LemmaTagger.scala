package si.zitnik.research.iobie.thirdparty.lemmagen.api

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.{Example, Examples}

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.thirdparty.opennlp.api.Tagger
import si.zitnik.research.lemmagen.impl.Lemmatizer
import si.zitnik.research.lemmagen.LemmagenFactory

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/9/12
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */

class LemmaTagger(lowercase: Boolean = false) extends Tagger with StrictLogging {
  var lemmatizer: Lemmatizer = null

  init()

  def init(): Unit = {
    lemmatizer = LemmagenFactory.readObject(this.getClass.getClassLoader.getResourceAsStream("lemmagen4j/lemmagenENModel.obj")).asInstanceOf[Lemmatizer]
  }

  def tag(examples: Examples): Unit = {
    for (example <- examples) {
      tag(example)
    }
  }

  def tag(example: Example): Unit = {
    for (token <- example) {
      if (token.contains(Label.LEMMA)) {
        logger.warn("Data has already been lemmatized! Overwriting lemmas...")
      }
      if (lowercase) {
        token.put(Label.LEMMA, lemmatizer.Lemmatize(token.get(Label.OBS).toLowerCase()).toLowerCase)
      } else {
        token.put(Label.LEMMA, lemmatizer.Lemmatize(token.get(Label.OBS)))
      }
    }
  }

}
