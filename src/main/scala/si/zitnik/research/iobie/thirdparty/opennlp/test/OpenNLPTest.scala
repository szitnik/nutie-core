package si.zitnik.research.iobie.thirdparty.opennlp.test

import si.zitnik.research.iobie.thirdparty.opennlp.api.{PoSTagger, ChunkTagger}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/7/12
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */

object OpenNLPTest {

  def main(args: Array[String]): Unit = {
    val sentence = Array[String]("I", "am", "jonny", "from", "Slovenia", ".", "This", "is", "a", "very", "beautiful", "country", "!")
    val example = new Example(sentence)

    val chunkTagger = new ChunkTagger()
    val posTagger = new PoSTagger()
    posTagger.tag(example)
    chunkTagger.tag(example)

    example.printLabeling(Label.OBS)
    example.printLabeling(Label.POS)
    example.printLabeling(Label.CHUNK)
  }
}
