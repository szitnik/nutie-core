package si.zitnik.research.iobie.thirdparty.reverb.test

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.thirdparty.reverb.api.ReVerbExtractor

import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/25/12
 * Time: 12:36 PM
 * To change this template use File | Settings | File Templates.
 */

object ReVerbExtractorTest extends StrictLogging {
  def main(args: Array[String]): Unit = {
    val example = new Example("Slavko i Mirko is the best movie ever played!".split(" "))
    val examples = new Examples(Array[Example](example))


    logger.info("Extracted domain (subject, relation, object, confidence):")
    for (data <- new ReVerbExtractor().extract(examples)) {
      logger.info(data.toString())
    }
  }
}
