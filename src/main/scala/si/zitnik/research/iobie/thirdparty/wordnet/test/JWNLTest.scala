package si.zitnik.research.iobie.thirdparty.wordnet.test

import net.didion.jwnl.data.POS
import si.zitnik.research.iobie.thirdparty.wordnet.api.WordNetHelper
import scala.collection.JavaConversions._


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/29/12
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */

object JWNLTest {
  def main(args: Array[String]): Unit = {
    val wnHelper = WordNetHelper


    println(wnHelper.getSameIndexWords("have", POS.VERB).mkString(" "))

    println(wnHelper.findRelationship("south", POS.NOUN, "turkey", POS.NOUN))
    println(wnHelper.findRelationship("test", POS.NOUN, "trial", POS.NOUN))

    println(wnHelper.findRelationship("animal", POS.NOUN, "cow", POS.NOUN))
    println(wnHelper.findRelationship("cow", POS.NOUN, "animal", POS.NOUN))
    println(wnHelper.findRelationship("pavement", POS.NOUN, "sidewalk", POS.NOUN))
    println(wnHelper.findRelationship("sidewalk", POS.NOUN, "pavement", POS.NOUN))
    println(wnHelper.findRelationship("good", POS.NOUN, "bad", POS.NOUN))
  }
}
