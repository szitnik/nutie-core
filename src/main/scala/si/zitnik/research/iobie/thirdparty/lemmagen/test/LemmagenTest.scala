package si.zitnik.research.iobie.thirdparty.lemmagen.test

import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.thirdparty.lemmagen.api.LemmaTagger
import scala.collection.JavaConversions._


/**
 * Lemmagen4J is our implementation of open-source Lemmagen, written in C# (v 3.0)
 * for more info see: http://lemmatise.ijs.si/Software
 */

object LemmagenTest {
  def main(args: Array[String]): Unit = {

    val examples = new Examples(Array[Example](new Example(
      Label.OBS,
      //Array[String]("I", "am", "jonny", "from", "Slovenia", ".", "This", "is", "a", "very", "beautifully", "countries", "!"))))
      Array[String]("Jaz", "bom", "skrit", "pomen", "prevedel", "in", "testerisal", "!"))))

    val lemmaTagger = new LemmaTagger()
    lemmaTagger.tag(examples)

    println(examples.getLabeling(Label.LEMMA).mkString(" "))

  }
}
