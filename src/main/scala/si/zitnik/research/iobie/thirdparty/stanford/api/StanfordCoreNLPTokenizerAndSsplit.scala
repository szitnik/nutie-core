package si.zitnik.research.iobie.thirdparty.stanford.api

import java.util.Properties
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import java.lang.String
import edu.stanford.nlp.ling.CoreAnnotations
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.{Examples, Example, Token}
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import scala.Some
import scala.Some
import collection.mutable
import collection.mutable.ArrayBuffer
import scala.Predef._
import scala.Some

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/4/13
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
class StanfordCoreNLPTokenizerAndSsplit {
  private val props: Properties = new Properties()
  props.setProperty("annotators", "tokenize, ssplit")
  props.setProperty("tokenize.options", "ptb3Escaping=false")
  private val pipeline: StanfordCoreNLP = new StanfordCoreNLP(props)

  private val defaultTokenProcessor = (startIdx: Int, text: String) => {
    val retVal = new ArrayBuffer[Token]()

    val token = new Token()
    token.put(Label.START_IDX, startIdx)
    token.put(Label.OBS, text)
    retVal.add(token)

    retVal
  }

  def tokenizeAndSsplit(text: String, docId: Option[String] = None, defaultTokenProcessor: (Int, String) => mutable.Buffer[_ <: Token] = defaultTokenProcessor) = {

    var annotation = new Annotation(text)

    pipeline.annotate(annotation)

    val retVal = new Examples()

    //for each sentence
    for (sentence <- annotation.get(classOf[CoreAnnotations.SentencesAnnotation])) {
      val sentenceExample = new Example()

      //for each token
      for (token <- sentence.get(classOf[CoreAnnotations.TokensAnnotation])) {
        val tokenExample = new Token()

        val startIdx: Integer = token.get(classOf[CoreAnnotations.CharacterOffsetBeginAnnotation])
        var text: String = token.get(classOf[CoreAnnotations.TextAnnotation])


        sentenceExample.addAll(defaultTokenProcessor(startIdx, text))
      }

      docId match {
        case Some(value) => sentenceExample.put(ExampleLabel.DOC_ID, value)
        case None => {}
      }

      retVal.add(sentenceExample)
    }
    retVal
  }

  /*
  Returns an array of sentences
   */
  def detectSentences(text: String) = {
    val examples = tokenizeAndSsplit(text)

    val retVal = new ArrayBuffer[String]()
    var startIdx = 0
    for (example <- examples) {
      val endIdx = example.last.get(Label.START_IDX).asInstanceOf[Int] + example.last.get(Label.OBS).asInstanceOf[String].size
      retVal.append(text.substring(startIdx, endIdx))

      startIdx = endIdx
    }
    retVal
  }
}

object StanfordCoreNLPTokenizerAndSsplit {

  def main(args: Array[String]): Unit = {
    val stanfordTokenAndSsplit = new StanfordCoreNLPTokenizerAndSsplit()
    val examples = stanfordTokenAndSsplit.tokenizeAndSsplit("Kosgi Santosh sent an email to Stanford University. He didn't get a reply.", Some("ONE_DOCUMENT"))

    examples.printStatistics(ommited = Array(), ommitedExample = Array())
  }
}
