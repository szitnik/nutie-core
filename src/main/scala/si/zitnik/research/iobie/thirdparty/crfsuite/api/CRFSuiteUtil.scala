package si.zitnik.research.iobie.thirdparty.crfsuite.api

import java.io._
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.Label
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.linearchain.FeatureDict

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/9/12
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */

object CRFSuiteUtil {

  def writeExamples(examples: Examples, learnLabelType: Label.Value, featureDict: FeatureDict, out: OutputStream): Unit = {
    for (example <- examples) {
      writeExample(example, learnLabelType, featureDict, out)
    }
  }

  def writeExample(example: Example, learnLabelType: Label.Value, featureDict: FeatureDict, out: OutputStream): Unit = {

      for (token <- example) {
        val sb = new StringBuilder()
        token.ufeatures.activeIterator.foreach{case (key, value) => sb.append(featureDict.idxToFeature.get(key) + "\t")}
        token.bfeatures.activeIterator.foreach{case (key, value) => sb.append(featureDict.idxToFeature.get(key) + "\t")}
        sb.replaceAllLiterally("\\", "\\\\")
        sb.replaceAllLiterally(":", "\\:")

        //when inferencing from non-tagged domain, token.get(learnLabelType) is normally null - no problem with that
        out.write("%s\t%s\n".format(token.get(learnLabelType), sb.toString()).getBytes("utf-8"))
      }
    out.write("\n".getBytes("utf-8"))
  }
}
