package si.zitnik.research.iobie.thirdparty.biolemmatizer.test

import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.thirdparty.biolemmatizer.api.BioLemmaTagger
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.thirdparty.opennlp.api.PoSTagger

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 16/01/14
 * Time: 12:16
 * To change this template use File | Settings | File Templates.
 */
object BioLemmaTaggerTest {
  def main(args: Array[String]): Unit = {

    val examples = new Examples(Array[Example](new Example(
      Label.OBS,
      Array[String]("I", "am", "jonny", "from", "Slovenia", ".", "This", "is", "a", "very", "beautifully", "countries", "!"))))

    //TEST 1
    val lemmaTagger = new BioLemmaTagger()
    lemmaTagger.tag(examples)
    println(examples.getLabeling(Label.LEMMA).mkString(" "))

    //TEST 2
    val posTagger = new PoSTagger()
    posTagger.tag(examples)
    lemmaTagger.tag(examples)
    println(examples.getLabeling(Label.LEMMA).mkString(" "))

  }
}
