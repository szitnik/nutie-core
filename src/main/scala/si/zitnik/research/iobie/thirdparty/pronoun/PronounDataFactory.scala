package si.zitnik.research.iobie.thirdparty.pronoun

import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/24/12
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
object PronounDataFactory {
  private val prns = new mutable.HashMap[String, mutable.HashMap[String, String]]()

  def instance(language: String = "en") = {
    if (!prns.contains(language)) {
      prns.put(language, new PronounData(language).data)
    }
    prns.get(language).get
  }
}
