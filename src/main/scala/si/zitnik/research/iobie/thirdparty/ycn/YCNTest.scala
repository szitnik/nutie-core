package si.zitnik.research.iobie.thirdparty.ycn

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.datasets.ace2004.{ACE2004Language, ACE2004DocumentType}
import java.util.{HashMap, Random, HashSet}
import si.zitnik.research.iobie.datasets.ace2004.doc.{TextDocument, SGMLImporter}
import si.zitnik.research.iobie.datasets.ace2004.annotations.{AnnotationsDocument, XMLImporter}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/13/12
 * Time: 3:12 PM
 *
 * info: ACE2004 SoA = 0.8
 */

object YCNTest {

  def main(args: Array[String]): Unit = {
    val allFolders = ACE2004DocumentType.values.map(dType => "/Volumes/ace_tides_multling_train/domain/"+ACE2004Language.ENGLISH+"/"+dType).toList
    val sgmls = SGMLImporter.importAll(allFolders)
    val xmls = XMLImporter.importAll(allFolders)

    val docMap = new HashMap[String, (TextDocument, AnnotationsDocument)]()
    if ((sgmls zip xmls).forall(p => p._1.docno == p._2.documentId))
      (sgmls zip xmls).foreach(p => docMap.put(p._1.docno, p))

    var y = 0
    var a = 0
    var i = 0
    var docCounter = 0

    val ycn = new YCNEndpoint()
    for ((dsName, (textDoc, annoDoc)) <- docMap) {
      println("----")
      println("Input text:\n\t%s\n".format(textDoc.text))
      val analyzedTtext = ycn.doContentAnalysis(textDoc.text)
      println("YCN output text:\n\t%s\n".format(analyzedTtext))
      val ycnEntities = new HashSet[String](ycn.getEntities(analyzedTtext))
      y += ycnEntities.size
      val annEntities = new HashSet[String](annoDoc.getEntities(textDoc))
      a += annEntities.size
      val intersect = ycnEntities.intersect(annEntities)
      i += intersect.size
      docCounter += 1

      println("Doc number: %d".format(docCounter))
      println("YCN entities: %d".format(ycnEntities.size()))
      println("ANN entities: %d".format(annEntities.size()))
      println("INTERSECT entities: %d = %.3f".format(intersect.size, 1.0 * intersect.size / annEntities.size))

      println("----")

      Thread.sleep(1000 + new Random().nextInt(5000))


      println("FINAL results:")
      println("YCN entities: %d".format(y))
      println("ANN entities: %d".format(a))
      println("INTERSECT entities: %d = %.3f".format(i, 1.0 * i / a))
    }

  }

}