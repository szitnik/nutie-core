package si.zitnik.research.iobie.thirdparty.crfsuite.api

import si.zitnik.research.iobie.algorithms.crf.{Classifier, Label}
import si.zitnik.research.iobie.domain.{Example, Examples}
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import si.zitnik.research.iobie.algorithms.crf.linearchain.FeatureDict

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.util.{StreamGobbler, StreamGobblerExample, StreamGobblerExamples}
import java.util

import com.typesafe.scalalogging.StrictLogging

import collection.mutable.ArrayBuffer
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/9/12
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */

class CRFSuiteLCCRFClassifier(
                        val learnLabelType: Label.Value,
                        val modelSaveFilename: String,
                        val featureDict: FeatureDict,
                        val printCRFSuiteOutput: Boolean,
                        val outputMarginals: Boolean = true,
                        val outputSequenceProbabilities: Boolean = true) extends Classifier with StrictLogging {
  val command =
    List(IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_CMD), "tag") :::
    { if (outputSequenceProbabilities) List("-p") else List() } ::: //sequence probability
    { if (outputMarginals) List("-i") else List() } ::: //marginal probability
    List("-m", IOBIEPropertiesUtil.getProperty(IOBIEProperties.CRFSUITE_MODEL_FOLDER) + "/" + modelSaveFilename)

  def classify(example: Example, normalized: Boolean): (util.ArrayList[String], util.ArrayList[Double], Double) = {
    featureDict.initExample(example)

    //START CRFSuite
    val pb = new ProcessBuilder(command)
    val p = pb.start()

    //INPUT DATA
    CRFSuiteUtil.writeExample(example, learnLabelType, featureDict, p.getOutputStream())
    p.getOutputStream().close()


    //LOG CRFSuite's output
    val errorGobbler = new StreamGobbler(p.getErrorStream(), printCRFSuiteOutput)
    val outputGobbler = new StreamGobblerExample(p.getInputStream())
    errorGobbler.start()
    outputGobbler.start()
    val exitStatus = p.waitFor()
    if (exitStatus != 0) {
      logger.error("Exit status: %d".format(exitStatus))
      throw new RuntimeException("Failed to run CRFSuite")
    }
    //read everything from gobblers
    errorGobbler.join()
    outputGobbler.join()

    (outputGobbler.data, outputGobbler.marginalProbabilities, outputGobbler.sequenceProbability)
  }

  private def processExamples(examples: Examples): (ArrayBuffer[ArrayBuffer[String]], ArrayBuffer[ArrayBuffer[Double]], ArrayBuffer[Double]) = {
    //START CRFSuite
    val pb = new ProcessBuilder(command)
    val p = pb.start()

    //INPUT DATA
    CRFSuiteUtil.writeExamples(examples, learnLabelType, featureDict, p.getOutputStream())
    p.getOutputStream().close()


    //LOG CRFSuite's output
    val errorGobbler = new StreamGobbler(p.getErrorStream(), printCRFSuiteOutput)
    val outputGobbler = new StreamGobblerExamples(p.getInputStream())
    errorGobbler.start()
    outputGobbler.start()
    val exitStatus = p.waitFor()
    if (exitStatus != 0) {
      logger.error("Exit status: %d".format(exitStatus))
      throw new RuntimeException("Failed to run CRFSuite")
    }
    //read everything from gobblers
    errorGobbler.join()
    outputGobbler.join()


    (outputGobbler.data, outputGobbler.marginalProbabilities, outputGobbler.sequenceProbabilities)
  }

  override def classify(examplesTest: Examples): (ArrayBuffer[ArrayBuffer[String]], ArrayBuffer[ArrayBuffer[Double]], ArrayBuffer[Double]) = {
    featureDict.initDataset(examplesTest)

    var retVal_1 = new ArrayBuffer[ArrayBuffer[String]]()
    var retVal_2 = new ArrayBuffer[ArrayBuffer[Double]]()
    var retVal_3 = new ArrayBuffer[Double]()


    val chunkSize = 1000
    for (startIdx <- 0 until examplesTest.size by chunkSize) {
      val endIdx = math.min(startIdx + chunkSize, examplesTest.size())
      val res = processExamples(examplesTest.subExamples(startIdx, endIdx))
      retVal_1 ++= res._1
      retVal_2 ++= res._2
      retVal_3 ++= res._3
    }

    (retVal_1, retVal_2, retVal_3)
  }



  def test(data: Examples): Unit = {throw new NotImplementedException()}
}
