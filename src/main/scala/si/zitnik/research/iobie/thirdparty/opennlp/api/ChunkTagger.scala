package si.zitnik.research.iobie.thirdparty.opennlp.api

import si.zitnik.research.iobie.domain.{Example, Examples}

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label
import opennlp.tools.chunker.{ChunkerME, ChunkerModel}

import scala.collection.JavaConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/7/12
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */

class ChunkTagger extends Tagger with StrictLogging {
  var chunkTagger: ChunkerME = null

  init()
  private def init(): Unit = {
    val model = this.getClass.getClassLoader.getResourceAsStream("apache-opennlp-1.5.2-incubating/models/en-chunker.bin")
    val chunkModel = new ChunkerModel(model)
    model.close()
    chunkTagger = new ChunkerME(chunkModel)
  }

  def tag(examples: Examples): Unit = {
    for (example <- examples) {
      tag(example)
    }
  }

  def tag(example: Example): Unit = {
    //warn if tag already exists
    //if (example.getLabeling(Label.CHUNK).size() != 0) {
    //  logger.warn("There exist an example already having CHUNK tags! Tags will be overwritten!")
    //}

    if (!example(0).keySet().contains(Label.POS)) {
      logger.warn("The examples must have POS tags before chunking! POS tagging example...")
      new PoSTagger().tag(example)
    }

    val tags = chunkTagger.chunk(
      example.getLabeling(Label.OBS).toArray(Array[String]()),
      example.getLabeling(Label.POS).toArray(Array[String]()))

    example.setLabeling(Label.CHUNK, tags)
  }
}
