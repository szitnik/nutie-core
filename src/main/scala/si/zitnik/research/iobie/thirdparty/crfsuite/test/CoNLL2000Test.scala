package si.zitnik.research.iobie.thirdparty.crfsuite.test

import si.zitnik.research.iobie.datasets.conll2000.CoNLL2000Importer
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.feature._

import si.zitnik.research.iobie.thirdparty.crfsuite.api.CRFSuiteLCCRFLearner

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/9/12
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2000Test {
  val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2000/"
  val objPath = "/Users/slavkoz/temp/IOBIEModels/CoNLL2012.obj"
  val testData = new CoNLL2000Importer(path + "test.txt") importForIE
  val learnData = new CoNLL2000Importer(path + "train.txt") importForIE
  val learnLabelType = Label.CHUNK




  def main(args: Array[String]): Unit = {
    val crfLearner = new CRFSuiteLCCRFLearner(learnData, learnLabelType, generalFFunctions())
    val crfClassifier = crfLearner.train()


    println(crfClassifier.classify(learnData)._1.size == learnData.size())
    /*
    new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, "I-NP")
    val (classfiedLabeling, _) = crfClassifier.classify(testData.get(0))
    testData.get(0).printLabeling(Label.OBS)
    testData.get(0).printLabeling(Label.CHUNK)
    println(classfiedLabeling.mkString(" "))*/
  }

  def generalFFunctions(): ArrayList[FeatureFunction] = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -1 to 1).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -1 to 1).generate())


    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.POS, -2 to 2, "UPOS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, -2 to 2, "UOBS").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, -2 to 2, "ULEM").generate())

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, -1 to 2, "BPOS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, -1 to 2, "BOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, -1 to 2, "BLEM").generate())

    featureFunctions
  }
}
