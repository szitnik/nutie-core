package si.zitnik.research.iobie.thirdparty.reverb.api

import si.zitnik.research.iobie.domain.Examples
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.thirdparty.opennlp.api.{ChunkTagger, PoSTagger}
import edu.washington.cs.knowitall.nlp.ChunkedSentence

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 7/25/12
 * Time: 12:13 PM
 * To change this template use File | Settings | File Templates.
 */

class ReVerbExtractor extends StrictLogging {

  def extract(examples: Examples): ArrayList[(String, String, String)] = {
    //1. check POS, Chunk tag existence
    if (!examples(0)(0).keySet().contains(Label.POS)) {
      logger.info("There were no POS tags in the examples, tagging ...")
      new PoSTagger().tag(examples);
      logger.info("POS tagging finished.")
    }
    if (!examples(0)(0).keySet().contains(Label.CHUNK)) {
      logger.info("There were no CHUNK tags in the examples, tagging ...")
      new ChunkTagger().tag(examples);
      logger.info("CHUNK tagging finished.")
    }

    logger.info("Extracting ...")
    //2. extract
    val retVal = new ArrayList[(String, String, String)]()
    val extractor = new edu.washington.cs.knowitall.extractor.ReVerbExtractor()
    for (example <- examples) {
      //2a. transform sentence
      val sentence = new ChunkedSentence(
        example.getLabeling(Label.OBS).toArray(Array[String]()),
        example.getLabeling(Label.POS).toArray(Array[String]()),
        example.getLabeling(Label.CHUNK).toArray(Array[String]())
      )

      //2b. extract relations
      for (extr <- extractor.extract(sentence)) {
        retVal.add(
          (extr.getArgument1().toString,
           extr.getRelation().toString,
           extr.getArgument2().toString)
        )
      }
    }
    logger.info("Extraction finished.")

    retVal
  }

}
