package si.zitnik.research.iobie.thirdparty.wordnet.api

import net.didion.jwnl.JWNL
import net.didion.jwnl.dictionary.Dictionary
import java.util.HashSet
import net.didion.jwnl.data._
import net.didion.jwnl.data.relationship.RelationshipFinder
import scala.Some

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/5/12
 * Time: 11:02 AM
 * To change this template use File | Settings | File Templates.
 */

object WordNetHelper {
  private val propsFile = this.getClass.getClassLoader.getResourceAsStream("wordnet/jwnl_properties.xml")
  JWNL.initialize(propsFile)
  private val wordnet: Dictionary = Dictionary.getInstance()

  private val MAX_RELATIONSHIP_DEPTH = 5

  private val pointerTypes = Array(PointerType.ANTONYM,
  PointerType.HYPERNYM,
  PointerType.HYPONYM,
  PointerType.SEE_ALSO,
  PointerType.SIMILAR_TO,
  PointerType.ATTRIBUTE,
  PointerType.CATEGORY,
  PointerType.CATEGORY_MEMBER,
  PointerType.CAUSE,
  PointerType.DERIVED,
  PointerType.ENTAILED_BY,
  PointerType.ENTAILMENT,
  PointerType.MEMBER_HOLONYM,
  PointerType.MEMBER_MERONYM,
  PointerType.NOMINALIZATION,
  PointerType.PART_HOLONYM,
  PointerType.PART_MERONYM,
  PointerType.PARTICIPLE_OF,
  PointerType.PERTAINYM,
  PointerType.REGION,
  PointerType.REGION_MEMBER,
  PointerType.SUBSTANCE_HOLONYM,
  PointerType.SUBSTANCE_MERONYM,
  PointerType.USAGE,
  PointerType.USAGE_MEMBER,
  PointerType.VERB_GROUP)


  def getSameIndexWords(word: String, posType: POS): HashSet[String] = {
    val retVal = new HashSet[String]()

    val iword: IndexWord = wordnet.getIndexWord(posType, word)
    for (sense: Synset <- iword.getSenses()) {
      for (similarWord <- sense.getWords()) {
        retVal.add(similarWord.getLemma())
      }
    }

    retVal
  }

  /**
   * Find a relationship, how word1 is related to word2.
   * @param word1
   * @param pos1
   * @param word2
   * @param pos2
   * @return
   */
  def findRelationship(word1: String, pos1: POS, word2: String, pos2: POS): Option[String] = {
    val iword1 = wordnet.getIndexWord(pos1, word1.toLowerCase())
    val iword2 = wordnet.getIndexWord(pos2, word2.toLowerCase())

    if (iword1 == null || iword2 == null) {
      return None
    } else if (iword1.equals(iword2)) {
      return Some("Same")
    }

    var retVal: Option[String] = None
    var curDepth = Int.MaxValue
    for (pointerType <- pointerTypes) {
      try {
        val relationships = RelationshipFinder.getInstance().findRelationships(iword1.getSense(1), iword2.getSense(1), pointerType, MAX_RELATIONSHIP_DEPTH)

        if (relationships.size() > 0 && relationships.getShallowest.getDepth < curDepth) {
          curDepth  = relationships.getShallowest.getDepth
          retVal = Some(pointerType.getLabel)
        }
      } catch {
        case e: Exception => {  }
      }
    }

    retVal
  }





}
