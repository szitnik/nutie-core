package si.zitnik.research.iobie.thirdparty.ycn

import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.conn.scheme.{PlainSocketFactory, Scheme, SchemeRegistry}
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.client.methods.HttpPost
import com.google.common.io.ByteStreams
import org.apache.http.protocol.HTTP
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.message.BasicNameValuePair
import org.apache.http.{HttpEntity, NameValuePair}
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging

import org.json.{JSONArray, JSONObject}
import org.apache.http.params.HttpConnectionParams

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/13/12
 * Time: 10:41 AM
 *
 * Read documentation on: http://developer.yahoo.com/search/content/V2/contentAnalysis.html
 */

class YCNEndpoint(
                   val format: String = "json",
                   val related_entities: String = "true",
                   val show_metadata: String = "true",
                   val enable_categorizer: String = "true",
                   val unique: String = "false",
                   val max: String = "10000") extends StrictLogging {

  private var httpClient: DefaultHttpClient = null
  private val url = "http://query.yahooapis.com/v1/public/yql"
  init()

  private def init(): Unit = {
    val schemeRegistry = new SchemeRegistry();
    schemeRegistry.register(
      new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
    schemeRegistry.register(
      new Scheme("https", 443, SSLSocketFactory.getSocketFactory()));

    val cm = new ThreadSafeClientConnManager(schemeRegistry);
    // Increase max total connection to 200
    cm.setMaxTotal(200);
    // Increase default max connection per route to 20
    cm.setDefaultMaxPerRoute(100);

    httpClient = new DefaultHttpClient(cm)
  }

  private def createHttpPost(arg: String, isUrl: Boolean = false) = {
    val httpPost = new HttpPost(url);
    val nvps = new ArrayList[NameValuePair]()

    nvps.add(new BasicNameValuePair("format", format));
    nvps.add(new BasicNameValuePair("related_entities", related_entities));
    nvps.add(new BasicNameValuePair("show_metadata", show_metadata));
    nvps.add(new BasicNameValuePair("enable_categorizer", enable_categorizer));
    nvps.add(new BasicNameValuePair("unique", unique));
    nvps.add(new BasicNameValuePair("max", max));

    if (isUrl) {
      nvps.add(new BasicNameValuePair("q", "SELECT * FROM contentanalysis.analyze WHERE url=\"%s\"".format(arg)));
    } else {
      nvps.add(new BasicNameValuePair("q", "SELECT * FROM contentanalysis.analyze WHERE text=\"%s\"".format(arg)));
    }

    httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

    val httpParams = httpClient.getParams();
    HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
    HttpConnectionParams.setSoTimeout(httpParams, 5000);

    httpPost
  }

  /**
   * arg: text or url
   * isUrl: indicates if arg is url of the page to be analyzed
   */
  def doContentAnalysis(arg: String, isUrl: Boolean = false) = {
    var retVal = ""

    logger.info("Posting request ...")
    val response = httpClient.execute(createHttpPost(
      arg.replace("\"", "&quot;"),
      isUrl))
    if (response.getStatusLine().getStatusCode() != 200) {
      logger.error("Failed to fetch results.")
      throw new RuntimeException("Failed to fetch results.")
    }
    val entity: HttpEntity = response.getEntity();
    if (entity != null) {
      retVal = new String(ByteStreams.toByteArray(entity.getContent()));
      logger.trace(retVal);
    } else {
      logger.error("Failed to read response.");
      throw new RuntimeException("Failed to read response.");
    }

    retVal
  }


  def getEntities(caResult: String): ArrayList[String] = {
    val retVal = new ArrayList[String]()
    println("Entities:")

    val json = new JSONObject(caResult)

    var entities = new JSONObject()
    try {
      entities = json.getJSONObject("query").getJSONObject("results").getJSONObject("entities")
    } catch {
      case e: Throwable => return new ArrayList[String]()
    }


    var array = new JSONArray()
    try {
      array = entities.getJSONArray("entity")
    } catch {
      case e: Throwable => try {
        entities.getJSONObject("entity").toJSONArray(array)
      } catch {
        case e: Throwable => return new ArrayList[String]()
      }
    }

    for (i <- 0 to array.length() - 1) {
      val content = array.get(i).asInstanceOf[JSONObject].getJSONObject("text").getString("content")
      println("\t-%s".format(content))
      retVal.add(content)
    }
    retVal
  }

}

object YCNEndpoint {
  def main(args: Array[String]): Unit = {
    val ycn = new YCNEndpoint()

    var result = ycn.doContentAnalysis("Slavko is the president ot the United States of America.")
    println(result)

    result = ycn.doContentAnalysis("http://www.nytimes.com/2012/01/13/us/politics/pacs-aid-allows-mitt-romneys-rivals-to-extend-race.html?_r=1&hp", true)
    println(result)

    result = ycn.doContentAnalysis("Italian sculptors and painters of the renaissance favored the Virgin Mary for inspiration.")
    println(result)

    //Example1 full URL:
    //http://query.yahooapis.com/v1/public/yql?format=json&related_entities=true&show_metadata=true&enable_categorizer=true&unique=false&max=10000&q=SELECT * FROM contentanalysis.analyze WHERE text="like many heartland states, iowa has had trouble keeping young people down on the farm or anywhere within state lines. with population waning, the state is looking beyond its borders for newcomers. as abc's jim sciutto reports, one little town may provide a big lesson.    on homecoming night postville feels like hometown, usa, but a look around this town of 2,000 shows it's become a miniature ellis island.    this was an all-white, all-christian community that all the sudden was taken over -- not taken over, that's a very bad choice of words, but invaded by, perhaps different groups.    it began when a hasidic jewish family bought one of the town's two meat-packing plants 13 years ago. first they brought in other hasidic jews, then mexicans, palestinians, ukrainians. the plant and another next store changed the face of postville.    i think postville might be catching up with the rest of america. if you look all over america, if you did a study of any great city, you would see the same thing.    the plants helped spur economic development in a town that had long been stagnant. stores are expanding. new apartments and houses are being built. and there are hundreds of new jobs, making unemployment almost nonexistent.    what else would you like?    postville now has 22 different nationalities, but some resident haven't welcomed the newcomers.    they have no insurance on their cars. they don't stop at stop signs. they do not put their children in a cash -- they're never heard of a car seat.    you know we've been here a long time and i think most people would meet them halfway, but that doesn't seem to be what they want.    for those who prefer the old postville, mayor john hyman has a simple answer.    i just tell them, you've got to change. they're here to stay. they're not gonna leave. and you're going to have to adapt to these people.    iowa's governor is now looking to follow postville's example, relaxing immigration quoteios. it's a goal that seems possible when on homecoming night the starting kicker is 14-year-old nikita kargalskiy, who may be 5,000 miles from his hometown in russia. jim sciutto, abc news, postville iowa."

    //Example2 full URL:
    //http://query.yahooapis.com/v1/public/yql?format=json&related_entities=true&show_metadata=true&enable_categorizer=true&unique=false&max=10000&q=SELECT * FROM contentanalysis.analyze WHERE text="Slavko is the president ot the United States of America."

    //Example3 full URL:
    //http://query.yahooapis.com/v1/public/yql?format=json&related_entities=true&show_metadata=true&enable_categorizer=true&unique=false&max=10000&q=SELECT * FROM contentanalysis.analyze WHERE text="Italian sculptors and painters of the renaissance favored the Virgin Mary for inspiration."
  }
}