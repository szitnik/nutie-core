package si.zitnik.research.iobie.thirdparty.slopostags

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

import scala.collection.immutable.HashMap
import scala.collection.mutable
import scala.io.Source

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/24/12
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
protected class SloPosTagsData() extends StrictLogging {
  val data = new mutable.HashMap[String, HashMap[Label.Value, String]]()

  init()



  private def init(): Unit = {
    logger.info("Starting to read SLO POS TAGS.")
    var counter = 0
    for (line <- Source.fromFile(IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/slo-pos-tags/josMSD-attval-sl.tbl").getLines()) {
      val lineData = line.split("\t")
      var attributes = SloPosTagsData.customSloPOSTag(lineData(1).split(" ")(0))

      for (attr <- lineData(1).split(" ").drop(1)) {
        val attrData = attr.split("=")
        val label = Label.withName("POS_" + attrData(0).toUpperCase)
        attributes += (label -> attrData(1))
      }
      data.put(lineData(0), attributes)
      counter += 1
    }
    logger.info("SLO POS TAGS read. %d tags added.".format(counter))
  }
}

object SloPosTagsData {
  def customSloPOSTag(value: String) = {
    var attributes = HashMap[Label.Value, String]()

    //pos type
    attributes += (Label.POS -> value)
    attributes += (Label.POS_TYPE -> "O")
    attributes += (Label.POS_GENDER -> "O")
    attributes += (Label.POS_NUMBER -> "O")
    attributes += (Label.POS_CASE -> "O")
    attributes += (Label.POS_ASPECT -> "O")
    attributes += (Label.POS_VFORM -> "O")
    attributes += (Label.POS_PERSON -> "O")
    attributes += (Label.POS_NEGATIVE -> "O")
    attributes += (Label.POS_CLITIC -> "O")
    attributes += (Label.POS_OWNER_NUMBER -> "O")
    attributes += (Label.POS_OWNER_GENDER -> "O")
    attributes += (Label.POS_ANIMATE -> "O")
    attributes += (Label.POS_DEGREE -> "O")
    attributes += (Label.POS_DEFINITENESS -> "O")
    attributes += (Label.POS_FORM -> "O")

    attributes
  }
}
