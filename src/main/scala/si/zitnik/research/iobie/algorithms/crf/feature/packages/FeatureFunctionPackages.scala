package si.zitnik.research.iobie.algorithms.crf.feature.packages

import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.algorithms.crf.feature._
import coreference._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/12/12
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
object FeatureFunctionPackages {
  /**
   * This is standard feature functions set.
   */
  def standardFFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -5 to 5).generate())


    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "UOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "BOBS").generate())

    featureFunctions
  }

  def standardNerFFunctions() = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(FeatureFunctionPackages.standardFFunctions)

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzJOB", fileNames = Array("iobie-resources/gazeteers/JOB")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzLOC", fileNames = Array("iobie-resources/gazeteers/LOC")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzMEAS", fileNames = Array("iobie-resources/gazeteers/MEASURES")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzMON", fileNames = Array("iobie-resources/gazeteers/MONEY")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzNUM", fileNames = Array("iobie-resources/gazeteers/NUM")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzORG", fileNames = Array("iobie-resources/gazeteers/ORG")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzOTH", fileNames = Array("iobie-resources/gazeteers/other")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzPER", fileNames = Array("iobie-resources/gazeteers/PER")))
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzTEMP", fileNames = Array("iobie-resources/gazeteers/TEMPORAL")))

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, userPredicate = "UPOS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, userPredicate = "BPOS").generate())

    featureFunctions.add(new GenderFeatureFunction())


    featureFunctions.add(new WordNetFeatureFunction())
    featureFunctions.add(new WordNetFeatureFunction(labelType = Label.LEMMA, userPredicate = "UWNL"))
    /*
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT", maxPathFromOneNode = 1))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT1", maxPathFromOneNode = 2))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT2", maxPathFromOneNode = 3))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT3", maxPathFromOneNode = 4))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT4", maxPathFromOneNode = 5))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT5"))
    */
    featureFunctions.add(new ParseTreeMentionDepthFeatureFunction(userPredicate = "UPMD2"))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction())
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT2", parentLength = 2))

    featureFunctions
  }

  def standardChemdner2013FFunctions(ffLevel: Int) = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    //part A - level 0
    featureFunctions.addAll(FeatureFunctionPackages.standardFFunctions)

    //part B - level 1
    if (ffLevel <= 1) {
    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzG", fileNames = Array("iobie-resources/gazeteers/GREEK")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzG", fileNames = Array("iobie-resources/gazeteers/GREEK")))

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzP", fileNames = Array("iobie-resources/gazeteers/PERIODICTABLE")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzP", fileNames = Array("iobie-resources/gazeteers/PERIODICTABLE")))

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzB", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/brands.txt")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzB", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/brands.txt")))

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzI", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/iupacNames.txt")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzI", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/iupacNames.txt")))

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzN", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/names.txt")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzN", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/names.txt")))

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzS", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/synonyms.txt")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzS", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/synonyms.txt")))

    featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "UGzC", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/CTDBase.txt")))
    //featureFunctions.add(new GazeteerFeatureFunction(userPredicate = "BGzC", fileNames = Array("iobie-resources/gazeteers/CHEMICALS/CTDBase.txt")))
    }

    //part C - level 2
    if (ffLevel <= 2) {
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, range = -1 to 1, userPredicate = "UBLF").generate())
    //featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, range = -1 to 1, userPredicate = "BBLF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, range = -1 to 1, userPredicate = "UULF").generate())
    //featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, range = -1 to 1, userPredicate = "BULF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.CHUNK, range = -1 to 1, userPredicate = "UBCF").generate())
    //featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.CHUNK, range = -1 to 1, userPredicate = "BBCF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.CHUNK, range = -1 to 1, userPredicate = "UUCF").generate())
    //featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.CHUNK, range = -1 to 1, userPredicate = "BUCF").generate())

    //featureFunctions.addAll(new OffsetFeatureFunctionGenerator(Label.POS, -2 to 2, userPredicate = "BOff").generate())
    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "U=POS"))
    //featureFunctions.add(new UnigramFeatureFunction(Label.POS, "B=POS"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, userPredicate = "UPOS").generate())
    }

    //part D
    if (ffLevel <= 3) {
    featureFunctions.add(new ContainsTwoDigitsFeatureFunction(userPredicate = "UTD"))
    featureFunctions.add(new ContainsTwoCapsFeatureFunction(userPredicate = "UTC"))
    featureFunctions.add(new ContainsDashFeatureFunction(userPredicate = "UD"))
    featureFunctions.add(new ContainsDotFeatureFunction(userPredicate = "UDo"))
    featureFunctions.add(new ContainsCommaFeatureFunction(userPredicate = "UC"))
    //featureFunctions.add(new StartsUpperFeatureFunction(-1, userPredicate = "BSUm1"))
    //featureFunctions.add(new StartsUpperFeatureFunction(userPredicate = "BSUm1"))
    //featureFunctions.add(new ContainsTwoDigitsFeatureFunction(userPredicate = "BTD"))
    //featureFunctions.add(new ContainsTwoCapsFeatureFunction(userPredicate = "BTC"))
    //featureFunctions.add(new ContainsDashFeatureFunction(userPredicate = "BD"))
    //featureFunctions.add(new ContainsDotFeatureFunction(userPredicate = "BDo"))
    //featureFunctions.add(new ContainsCommaFeatureFunction(userPredicate = "BC"))

    //featureFunctions.addAll(new CharacterNGramFeatureFunctionGenerator(userPredicate = "BCNG").generate())
    featureFunctions.addAll(new CharacterNGramFeatureFunctionGenerator(userPredicate = "UCNG").generate())
    }

    //part E
    if (ffLevel <= 4) {
    //featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "BPN", distanceFromMention = Array(1,2,3), mergeTokensToConstituent = true).generate())
    featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "UPN", distanceFromMention = Array(1,2,3), mergeTokensToConstituent = true).generate())
    //featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "BPNP", labelType = Label.POS, distanceFromMention = Array(1,2,3), mergeTokensToConstituent = true).generate())
    featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "UPNP", labelType = Label.POS, distanceFromMention = Array(1,2,3), mergeTokensToConstituent = true).generate())

    //featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "BPN1", distanceFromMention = Array(1,2,3), mergeTokensToConstituent = false).generate())
    featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "UPN1", distanceFromMention = Array(1,2,3), mergeTokensToConstituent = false).generate())
    //featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "BPNP1", labelType = Label.POS, distanceFromMention = Array(1,2,3), mergeTokensToConstituent = false).generate())
    featureFunctions.addAll(new PrevNextWordsTokenFeatureFunctionGenerator(userPredicate = "UPNP1", labelType = Label.POS, distanceFromMention = Array(1,2,3), mergeTokensToConstituent = false).generate())
    }

    featureFunctions
  }

  def standardCorefFFunctions() = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(FeatureFunctionPackages.standardFFunctions)
    featureFunctions.add(new UnigramFeatureFunction(Label.NE, "U=NE"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "U=POS"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "B=POS"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, userPredicate = "UPOS").generate())

    featureFunctions.add(new BigramFeatureFunctionMatch(Label.OBS, userPredicate = "UMO")) //string match  (TANL-1 Same feature)
    featureFunctions.add(new BigramFeatureFunctionMatch(Label.NE, userPredicate = "UMN")) //semantic match (TANL-1 Type feature)
    featureFunctions.add(new BigramGenderMatchFeatureFunction(userPredicate = "UGM")) //gender match (TANL-1 Gender)
    featureFunctions.add(new BigramAppositiveFeatureFunction()) //is it appositive
    featureFunctions.add(new BigramAliasFeatureFunction(userPredicate = "UAL")) //one is alias, abbreviation of another (TANL-1 Acronym)
    featureFunctions.add(new BigramIsPrefixFeatureFunction(userPredicate = "UPF")) //one mention is a prefix of another (TANL-1 Prefix)
    featureFunctions.add(new BigramIsSuffixFeatureFunction(userPredicate = "USF")) //one mention is a suffix of another (TANL-1 Suffix)
    featureFunctions.add(new BigramSimilarityFeatureFunction(userPredicate = "USim")) //mentions are similar to each other (TANL-1 Edit distance)
    featureFunctions.add(new UnigramPronounTypeFeatureFunction()) //pronoun type (TANL-1 Pronoun)
    featureFunctions.add(new BigramIsInTheSameSentenceFeatureFunction())
    featureFunctions.add(new HearstCooccurrenceFeatureFunction())
    featureFunctions.add(new HearstCooccurrenceFeatureFunction(userPredicate = "UHCOC"))

    //featureFunctions.add(new BigramConsecutiveFeatureFunction(Label.POS)) //POS pair (TANL-1 Head POS)



    //CoNLL2012 Fernandes: Distance between mi and mj in sentences;
    //sentence distance (TANL-1 Sentence distance)
    featureFunctions.add(new SentenceDistanceFeatureFunction(userPredicate = "USD"))

    //CoNLL2012 Fernandes: Distance in number of mentions;
    //mention distance (TANL-1 Mention distance)
    // NO POINT for this function because of skip mentions
    //featureFunctions.add(new MentionDistanceFeatureFunction())

    //token distance (TANL-1 Token distance)
    //featureFunctions.add(new TokenDistanceFeatureFunction(userPredicate = "UTD"))

    //animacy agreement (BART animacy)
    featureFunctions.add(new GenderFeatureFunction())

    //is in quoted speech (BART inquotedspeech)
    featureFunctions.add(new IsQuotedFeatureFunction(userPredicate = "BQuoted"))

    featureFunctions
  }

  def bestACE2004CorefFeatureFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(FeatureFunctionPackages.standardCorefFFunctions())

    /*featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.NE).generate())
    featureFunctions.add(new BigramConsecutiveFeatureFunction(Label.POS)) //POS pair (TANL-1 Head POS)
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "UNM")) //number match (TANL-1 Number)
    */
    //newly designed feature functions
    featureFunctions.addAll(new SubstringMatchFeatureFunctionGenerator(Label.OBS, Label.EXTENT).generate())
    featureFunctions.addAll(new StartsWithFeatureFunctionGenerator(Label.OBS).generate())
    featureFunctions.addAll(new EndsWithFeatureFunctionGenerator(Label.OBS).generate())

    //cullota07
//    We follow Soon et al. (2001) and Ng and Cardie (2002) to generate most of our features for the Pair- wise Model. These include:

//    • Match features - Check whether
//        gender,

     //done - also TANL-1 FF
//        number,
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "UNM")) //number match (TANL-1 Number)
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "BNM")) //number match (TANL-1 Number)
//        head text, or
    //done, OBS is already head text
//        entire phrase matches
    featureFunctions.add(new BigramFeatureFunctionMatch(Label.EXTENT, userPredicate = "UEM")) // entire phrase ==? extent

//    • Mention type (pronoun, name, nominal)
    featureFunctions.add(new UnigramFeatureFunction(Label.MENTION_TYPE, "BMTYPE"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.MENTION_TYPE, userPredicate = "UMTYPEP").generate())
    featureFunctions.add(new UnigramFeatureFunction(Label.LDC_MENTION_TYPE, "BLMTYPE"))

//    • Aliases - Heuristically decide if one noun is the acronym of the other
    //done
//    • Apposition - Heuristically decide if one noun is in apposition to the other
    featureFunctions.addAll(new HeuristicTextAppositionFeatureFunctionGenerator(userPredicate = "BAPP").generate())

//    • Relative Pronoun - Heuristically decide if one noun is a relative pronoun referring to the other.
    //featureFunctions.add(new BigramLabelFeatureFunction(l1 = Label.OBS, l2 = Label.MENTION_TYPE))
    featureFunctions.add(new BigramLabelFeatureFunction(l2 = Label.OBS, l1 = Label.MENTION_TYPE))

//    • Wordnet features - Use Wordnet to decide if one noun is a hypernym, synonym, or antonym of another, or if they share a hypernym.
    featureFunctions.add(new WordNetFeatureFunction()) //-better results
    featureFunctions.add(new WordNetFeatureFunction(labelType = Label.LEMMA, userPredicate = "UWNL")) //-worse Bcubed, better overall

//    • Both speak - True if both contain an adjacent context word that is a synonym of “said.” This is a domain-specific feature that helps for many newswire articles.
    //featureFunctions.addAll(new ContextFeatureFunctionGenerator().generate())
    //featureFunctions.addAll(new ContextFeatureFunctionGenerator(ngramSize = 4, userPredicate = "BNGR4").generate())

    //P1
    featureFunctions.add(new UnigramFeatureFunction(labelType = Label.ENTITY_TYPE, userPredicate = "UET"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.ENTITY_TYPE, userPredicate = "UETB").generate())
    featureFunctions.add(new UnigramFeatureFunction(labelType = Label.ENTITY_SUBTYPE, userPredicate = "BEST"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.ENTITY_SUBTYPE, userPredicate = "UESTB").generate())

    //P2
    /*
    featureFunctions.add(new UnigramFeatureFunction(labelType = Label.ENTITY_TYPE, userPredicate = "BET"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.ENTITY_TYPE, userPredicate = "BETB").generate())
    featureFunctions.add(new UnigramFeatureFunction(labelType = Label.ENTITY_SUBTYPE, userPredicate = "UEST"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.ENTITY_SUBTYPE, userPredicate = "BESTB").generate())
     */

//    • Modifiers Match - for example, in the phrase “President Clinton”, “President” is a modifier of “Clinton”. This feature indicates if one noun is a modifier of the other, or they share a modifier.
    //done, substrinMatchFeatureFunction EXTENT, OBS

//    • Substring - True if one noun is a substring of the other (e.g. “Egypt” and “Egyptian”).
    //done

//    The First-Order Model includes the following features:
//    • Enumerate each pair of noun phrases and compute the features listed above. All-X is true if all pairs share a feature X, Most-True-X is true if the majority of pairs share a feature X, and Most-False-X is true if most of the pairs do not share feature X .
       //TODO: implement

//TODO: good for iterative model
//    • Use the output of the Pairwise Model for each pair of nouns. All-True is true if all pairs are predicted to be coreferent, Most-True is true if most pairs are predicted to be coreferent, and Most-False is true if most pairs are predicted to not be coreferent. Additionally, Max-True is true if the maximum pairwise score is above threshold, and Min-True if the minimum pair- wise score is above threshold.
//    • Cluster Size indicates the size of the cluster.
//    • Count how many phrases in the cluster are of each mention type (name, pronoun, nom- inal), number (singular/plural) and gender (male/female). The features All-X and Most- True-X indicate how frequent each feature is in the cluster. This feature can capture the soft constraint such that no cluster consists only of pronouns.
//      In addition to the listed features, we also include conjunctions of size 2, for example “Genders match AND numbers match”.


    //bengston and roth, 2008
    //TODO: haghigi&klein feature functions

    //CoNLL2012 - Fernandes et al
//    Lexical:
//  head word of mi j;
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "UOBS1", range = -2 to 2).generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "BOBS1", range = -2 to 2).generate())

//    String matching of (head word of) mi and mj (y/n);
    //done

//    Both are pronouns and their strings match (y/n);
    //featureFunctions.add(new TwoLabelMatchFeatureFunction(userPredicate = "UTLM"))


//    Previous/Next two words of mi j ;
    //featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(distanceFromMention = Array(1), userPredicate = "BPN").generate())

//    Length of mi j ;
    featureFunctions.add(new LengthDifferenceFeatureFunction(userPredicate = "ULDF"))

//    Edit distance of head words;
    //done, similarity feature function

//    mi j is a definitive NP (y/n);
    //featureFunctions.add(new EnglishDefinitiveNounFeatureFunction(userPredicate = "UEDN"))

//    mi j is a demonstrative NP (y/n);
    featureFunctions.add(new EnglishDemonstrativeFeatureFunction())


//    Both are proper names and their strings match (y/n).
    featureFunctions.add(new TwoLabelMatchFeatureFunction(userPredicate = "UTLM", l1 = Label.NE))

//     Syntactic:
//    POS tag of the mi j head word;
    //done

//    Previous/Next two POS tags of mi j;
    featureFunctions.addAll(new OffsetFeatureFunctionGenerator(Label.POS, -2 to 2, userPredicate = "BOff").generate())
    //featureFunctions.addAll(new OffsetRealExampleTokenFeatureFunctionGenerator(Label.POS, -2 to 2).generate())

//    mi and mj are both pronouns / proper names (y/n);
//    Previous/Next predicate of mi j;
//    Compatible pronouns, which checks whether two pronouns agree in number, gender and person (y/n);
//    NP embedding level;
//    Number of embedded NPs in mi j .

//      Semantic:
//    sense of the mi j head word;
//    Named entity type of mi j;
//    mi and mj have the same named entity;
//    Semantic role of mi j for the prev/next predicate;
//    Concatenation of semantic roles of mi and mj for the same predicate (if they are in the same sentence);
//    Same speaker (y/n);
//    mj is an alias of mi (y/n).

//      Distance and Position:
//    Distance in number of person names (applies only for the cases where mi and mj are both pronouns or one of them is a person name);
//    One mention is in apposition to the other (y/n).

    //ParseTree feature functions

    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT", maxPathFromOneNode = 1))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT1", maxPathFromOneNode = 2))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT2", maxPathFromOneNode = 3))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT3", maxPathFromOneNode = 4))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT4", maxPathFromOneNode = 5))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT5"))


    //new 9.2.2013
    featureFunctions.add(new ParseTreeMentionDepthFeatureFunction(userPredicate = "UPMD2"))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction())
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT2", parentLength = 2))



    //ITERATIVE


//CURRENTLY UNAPPLICABLE TODO: try to find a solution
    //the result of a baseline system (CoNLL 2012 - Fernandes)
    //is first mention (BART first mention)
    //mention count (TANL-1 Count)



    featureFunctions
  }

  def bestCoNLL2012CorefFeatureFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(FeatureFunctionPackages.standardCorefFFunctions())

    featureFunctions.addAll(new StartsWithFeatureFunctionGenerator(Label.OBS).generate())
    featureFunctions.addAll(new EndsWithFeatureFunctionGenerator(Label.OBS).generate())
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "UNM")) //number match (TANL-1 Number)
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "BNM")) //number match (TANL-1 Number)
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "UOBS1", range = -2 to 2).generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "BOBS1", range = -2 to 2).generate())
    featureFunctions.add(new LengthDifferenceFeatureFunction(userPredicate = "ULDF"))
    featureFunctions.add(new EnglishDemonstrativeFeatureFunction())
    featureFunctions.add(new TwoLabelMatchFeatureFunction(userPredicate = "UTLM", l1 = Label.NE))

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.W_SENSE, userPredicate = "UWS", range = -1 to 1).generate())
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT", maxPathFromOneNode = 1))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT1", maxPathFromOneNode = 2))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT2", maxPathFromOneNode = 3))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT3", maxPathFromOneNode = 4))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT4", maxPathFromOneNode = 5))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT5"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.PROPBANK_FID, userPredicate = "UPBF", range = -1 to 1).generate())

    //new 9.2.2013
    featureFunctions.add(new ParseTreeMentionDepthFeatureFunction(userPredicate = "UPMD2"))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction())
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT2", parentLength = 2))
    featureFunctions.addAll(new LabelUnigramArrayFeatureFunctionGenerator(labelType = Label.REL, range = -2 to 2).generate())
    featureFunctions.addAll(new LabelUnigramArrayFeatureFunctionGenerator(labelType = Label.REL, range = -2 to 2, userPredicate = "BAr").generate())

    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(labelType = Label.SPEAKER, range = -2 to 2, userPredicate = "UUSA").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(labelType = Label.SPEAKER, range = -2 to 2, userPredicate = "UUSB").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(labelType = Label.SPEAKER, range = -2 to 2, userPredicate = "BUSA").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(labelType = Label.SPEAKER, range = -2 to 2, userPredicate = "BUSB").generate())


    featureFunctions
  }

  def bestSemEval2010CorefFeatureFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(FeatureFunctionPackages.standardCorefFFunctions())

    featureFunctions.addAll(new StartsWithFeatureFunctionGenerator(Label.OBS).generate())
    featureFunctions.addAll(new EndsWithFeatureFunctionGenerator(Label.OBS).generate())
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "UNM")) //number match (TANL-1 Number)
    featureFunctions.add(new BigramNumberMatchFeatureFunction(userPredicate = "BNM")) //number match (TANL-1 Number)
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "UOBS1", range = -2 to 2).generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "BOBS1", range = -2 to 2).generate())
    featureFunctions.add(new LengthDifferenceFeatureFunction(userPredicate = "ULDF"))
    featureFunctions.add(new EnglishDemonstrativeFeatureFunction())
    featureFunctions.add(new TwoLabelMatchFeatureFunction(userPredicate = "UTLM", l1 = Label.NE))


    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.W_SENSE, userPredicate = "UWS", range = -1 to 1).generate())
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT", maxPathFromOneNode = 1))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT1", maxPathFromOneNode = 2))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT2", maxPathFromOneNode = 3))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT3", maxPathFromOneNode = 4))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT4", maxPathFromOneNode = 5))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT5"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.PROPBANK_FID, userPredicate = "UPBF", range = -1 to 1).generate())

    //new 9.2.2013
    featureFunctions.add(new ParseTreeMentionDepthFeatureFunction(userPredicate = "UPMD2"))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction())
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT2", parentLength = 2))

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.REL, userPredicate = "UOBSRel", range = -2 to 2).generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.REL, userPredicate = "BOBSRel1", range = -2 to 2).generate())



    featureFunctions
  }


  def standardRelFFPackages() = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    //part A

    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.add(new HearstCooccurrenceFeatureFunction(userPredicate = "BHCOC"))
    featureFunctions.add(new HearstCooccurrenceFeatureFunction(userPredicate = "UHCOC"))

    featureFunctions.add(new MentionTokenDistanceFeatureFunction(userPredicate = "BMD"))
    featureFunctions.add(new MentionTokenDistanceFeatureFunction(userPredicate = "UMD"))

/*
    //part B

    featureFunctions.add(new ParseTreeMentionDepthFeatureFunction(userPredicate = "BPMD"))
    featureFunctions.add(new ParseTreeMentionDepthFeatureFunction(userPredicate = "UPMD"))

    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT1", parentLength = 1))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "BPPT1", parentLength = 1))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT2", parentLength = 2))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "BPPT2", parentLength = 2))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "UPPT3", parentLength = 3))
    featureFunctions.add(new ParseTreeParentValueFeatureFunction(userPredicate = "BPPT3", parentLength = 3))

    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "BPT", maxPathFromOneNode = 3))
    featureFunctions.add(new ParseTreePathFeatureFunction(userPredicate = "UPT", maxPathFromOneNode = 3))


    //part C

    featureFunctions.add(new BSubtilisFeatureFunction(userPredicate = "USub"))
    featureFunctions.add(new BSubtilisFeatureFunction(userPredicate = "BSub"))

    featureFunctions.add(new IsBSubtilisFeatureFunction(userPredicate = "BIsSub"))
    featureFunctions.add(new IsBSubtilisFeatureFunction(userPredicate = "UIsSub"))

    featureFunctions.add(new IsBSubtilisPairFeatureFunction(userPredicate = "BIsSubP"))
    featureFunctions.add(new IsBSubtilisPairFeatureFunction(userPredicate = "UIsSubP"))


    //generators

    //part D

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -5 to 5).generate())


    //part E

    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "UOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "BOBS").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.ENTITY_TYPE, range = -4 to 4, userPredicate = "UBEF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.ENTITY_TYPE, range = -4 to 4, userPredicate = "BBEF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, range = -4 to 4, userPredicate = "UBOF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, range = -4 to 4, userPredicate = "BBOF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, range = -4 to 4, userPredicate = "UBPF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, range = -4 to 4, userPredicate = "BBPF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, range = -4 to 4, userPredicate = "UBLF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.LEMMA, range = -4 to 4, userPredicate = "BBLF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.COREF, userPredicate = "UBCF").generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.COREF, userPredicate = "BBCF").generate())

    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.ENTITY_TYPE, range = -4 to 4, userPredicate = "UUEF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.ENTITY_TYPE, range = -4 to 4, userPredicate = "BUEF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, range = -4 to 4, userPredicate = "UUOF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.OBS, range = -4 to 4, userPredicate = "BUOF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.POS, range = -4 to 4, userPredicate = "UUPF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.POS, range = -4 to 4, userPredicate = "BUPF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, range = -4 to 4, userPredicate = "UULF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.LEMMA, range = -4 to 4, userPredicate = "BULF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.COREF, userPredicate = "UUCF").generate())
    featureFunctions.addAll(new LabelUnigramFeatureFunctionGenerator(Label.COREF, userPredicate = "BUCF").generate())


    //part F

    featureFunctions.addAll(new ContextFeatureFunctionGenerator(userPredicate = "BContxt").generate())
    featureFunctions.addAll(new ContextFeatureFunctionGenerator(userPredicate = "UContxt").generate())


    //part G

    featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(userPredicate = "BPN").generate())
    featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(userPredicate = "UPN").generate())
    featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(userPredicate = "BPNL", labelType = Label.LEMMA).generate())
    featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(userPredicate = "UPNL", labelType = Label.LEMMA).generate())
    featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(userPredicate = "BPNP", labelType = Label.POS).generate())
    featureFunctions.addAll(new PrevNextWordsFeatureFunctionGenerator(userPredicate = "UPNP", labelType = Label.POS).generate())

    featureFunctions.addAll(new PrevNextBetweenWordsFeatureFunctionGenerator(userPredicate = "BBPN").generate())
    featureFunctions.addAll(new PrevNextBetweenWordsFeatureFunctionGenerator(userPredicate = "UBPN").generate())
    featureFunctions.addAll(new PrevNextBetweenWordsFeatureFunctionGenerator(labelType = Label.LEMMA, userPredicate = "BBPNL").generate())
    featureFunctions.addAll(new PrevNextBetweenWordsFeatureFunctionGenerator(labelType = Label.LEMMA, userPredicate = "UBPNL").generate())
    featureFunctions.addAll(new PrevNextBetweenWordsFeatureFunctionGenerator(labelType = Label.POS, userPredicate = "BBPNLP").generate())
    featureFunctions.addAll(new PrevNextBetweenWordsFeatureFunctionGenerator(labelType = Label.POS, userPredicate = "UBPNLP").generate())


    //part H

    featureFunctions.addAll(new SplitByDelimiterAndGenerateFirstNFeatureFunctionGenerator(labelType = Label.OBS, userPredicate = "BSBDaG").generate())
    featureFunctions.addAll(new SplitByDelimiterAndGenerateFirstNFeatureFunctionGenerator(labelType = Label.OBS, userPredicate = "USBDaG").generate())
    featureFunctions.addAll(new SplitByDelimiterAndGenerateFirstNFeatureFunctionGenerator(labelType = Label.LEMMA, userPredicate = "BSBDaGL").generate())
    featureFunctions.addAll(new SplitByDelimiterAndGenerateFirstNFeatureFunctionGenerator(labelType = Label.LEMMA, userPredicate = "USBDaGL").generate())
*/

    featureFunctions
  }

  }
