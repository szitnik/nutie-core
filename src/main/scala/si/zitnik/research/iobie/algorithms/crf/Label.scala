package si.zitnik.research.iobie.algorithms.crf

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/8/11
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */

object Label extends Enumeration {
  //Speaker
  val SPEAKER = Value("SPEAKER")
  //Word sense
  val W_SENSE = Value("WORD_SENSE")
  //Propbank Frameset ID
  val PROPBANK_FID = Value("PROPBANK_FID")
  //Parse tree representation (should be link to corresponding parse node leaf in parse tree)
  val PARSE_NODE = Value("PARSE_NODE")

  //Dummy start/end identifiers for CRF helper
  val START = Value("START")
  val END = Value("END")

  //observable
  val OBS = Value("OBS")
  //named entity
  val NE = Value("NE")
  //part of speech
  val POS = Value("POS")
  val POS_TYPE = Value("POS_VRSTA")
  val POS_GENDER = Value("POS_SPOL")
  val POS_NUMBER = Value("POS_ŠTEVILO")
  val POS_CASE = Value("POS_SKLON")
  val POS_ASPECT = Value("POS_VID")
  val POS_VFORM = Value("POS_OBLIKA")
  val POS_PERSON = Value("POS_OSEBA")
  val POS_NEGATIVE = Value("POS_NIKALNOST")
  val POS_CLITIC = Value("POS_NASLONSKOST")
  val POS_OWNER_NUMBER = Value("POS_ŠTEVILO_SVOJINE")
  val POS_OWNER_GENDER = Value("POS_SPOL_SVOJINE")
  val POS_ANIMATE = Value("POS_ŽIVOST")
  val POS_DEGREE = Value("POS_STOPNJA")
  val POS_DEFINITENESS = Value("POS_DOLOČNOST")
  val POS_FORM = Value("POS_ZAPIS")


  //chunk
  val CHUNK = Value("CHUNK")
  //relation
  val REL = Value("REL")
  //lemma
  val LEMMA = Value("LEMMA")

  //phrase boundary
  val PHRB = Value("PHRB")
  //article id (for CoNLL dataset)
  val AID = Value("AID")

  //COREFERENCE-SPECIFIC LABELTYPES
  //usually for extension around constituent (originates from ACE2004 dataset)
  val EXTENT = Value("EXTENT")
  //coreference
  val COREF = Value("COREF")
  //mention type
  val MENTION_TYPE = Value("MENTION_TYPE")
  //ldc mention type
  val LDC_MENTION_TYPE = Value("LDC_MENTION_TYPE")
  //metonymy mention
  val METONYMY_MENTION = Value("METONYMY_MENTION")
  //reference
  val REFERENCE = Value("REFERENCE")
  //entity subtype
  val ENTITY_SUBTYPE = Value("ENTITY_SUBTYPE")
  //entity type
  val ENTITY_TYPE = Value("ENTITY_TYPE")
  //entity class
  val ENTITY_CLASS = Value("ENTITY_CLASS")


  //ITERATIVE LABELTYPES
  //previous NE labeling
  val L1_NE = Value("L1_NE")



  //OTHER
  val START_IDX = Value("STARTIDX")
  val ID  = Value("ID")
  //val TYPE = Value("TYPE")
  val VALUE = Value("VALUE")
  val ATTRIBUTE_TYPE = Value("ATTRIBUTE_TYPE")
  val SUBJECT_TYPE = Value("SUBJECT_TYPE")
  val OBJECT_TYPE = Value("OBJECT_TYPE")
  val MARGINAL_PROB = Value("MARGINAL_PROBABILITY")

  //relation subtype
  val RELATION_SUBTYPE = Value("RELATION_SUBTYPE")
  val LEXICAL_CONDITION = Value("LEXICAL_CONDITION")
}
