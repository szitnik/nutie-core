package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * This feature function supports Hearst's feature function, as presented by Bansal and Klein, 2012
 * (Mohit Bansal, Dan Klein: Coreference Semantics from Web Features).
 *
 * Marti Hearst. 1992. Automatic acquisition of hyponyms from large text corpora. In Proceedings of COLING.
 */
class HearstCooccurrenceFeatureFunction(userPredicate: String = "BHCOC") extends FeatureFunction(userPredicate+"=") {

  def score = (example: Example, i: Int) => {
    if (i > 0 && i < example.size()) {
      var cons1 = example.get(i-1).asInstanceOf[Constituent]
      val cons2 = example.get(i).asInstanceOf[Constituent]

      if (cons1.example == cons2.example && cons2.startIdx > cons1.endIdx) {
        val betweenText = cons1.example.subLabeling(Label.OBS, cons1.endIdx, cons2.startIdx).mkString(" ").toLowerCase()
        matchText(betweenText)
      } else {
        null
      }
    } else {
      null
    }
  }

  def matchText(betweenText: String): String = {
    if (betweenText.matches("(is|are|was|were)( (a|an|the))?")) {
      predicate+"A"
    } else if (betweenText.matches("(and|or) (other|the other|another)")) {
      predicate+"B"
    } else if (betweenText.matches("other than( (a|an|the))?")) {
      predicate+"C"
    } else if (betweenText.matches("such as( (a|an|the))?")) {
      predicate+"D"
    } else if (betweenText.matches("including( (a|an|the))?")) {
      predicate+"E"
    } else if (betweenText.matches("especially( (a|an|the))?")) {
      predicate+"F"
    } else if (betweenText.matches("of( (the|all))?")) {
      predicate+"G"
    } else {
      null
    }
  }


}