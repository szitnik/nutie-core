package si.zitnik.research.iobie.algorithms.crf.linearchain

import si.zitnik.research.iobie.domain.{Examples, Example}
import si.zitnik.research.iobie.algorithms.crf.{Label, Classifier}
import scala.collection.JavaConversions._
import java.util.ArrayList
import breeze.linalg.SparseVector
import java.util

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/29/12
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */

class LCCRFClassifier(
                       private val learnLabelType: Label.Value,
                       private val dict: FeatureDict,
                       private val w: SparseVector[Double],
                       private val wscale: Double,
                       private val wnorm: Double,
                       private val lambda: Double
                       ) extends Classifier with Serializable {

  //TODO: check for normalized
  def classify(example: Example, normalized: Boolean = false): (util.ArrayList[String], util.ArrayList[Double], Double) = {
    dict.initExample(example)
    val scorer = new Scorer(learnLabelType, example, dict, w, wscale)
    val labelingIds = new ArrayList[Int]()

    val score = scorer.viterbi(labelingIds)
    val labeling = new ArrayList[String]()
    for (labelId <- labelingIds) {
      labeling.add(dict.labelName(labelId))
    }

    (labeling, null, score)
  }

  def test(data: Examples): Unit = {
    dict.initDataset(data)

    println("sentences=%d".format(data.size))
    var obj: Double = 0;
    var errors: Int = 0;
    var total: Int = 0;
    for (sentence: Example <- data) {
      val scorer = new Scorer(learnLabelType, sentence, dict, w, wscale)
      obj += scorer.scoreForward() - scorer.scoreCorrect()
      errors += scorer.test()
      total += sentence.size()
    }
    obj = obj * 1.0 / data.size
    println(" loss=%.3f".format(obj))
    obj += 0.5 * wnorm * lambda;
    val misrate: Double = (errors * 100.0) / (if (total != 0) {
      total
    } else {
      1
    })
    println(" obj=%.3f err=%d (%.3f%%)".format(obj, errors, misrate))
  }
}
