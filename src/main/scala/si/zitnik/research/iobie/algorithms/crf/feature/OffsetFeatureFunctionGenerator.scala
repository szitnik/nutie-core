package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import si.zitnik.research.iobie.domain.Example
import java.util.ArrayList
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * if (substringLen positive) => use prefix
 * else => use suffix
 */
class OffsetFeatureFunctionGenerator(
                                     val labelType: Label.Value,
                                     val offset: Range = 0 to 0,
                                     userPredicate: String = "UOff") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (off <- offset) {
      val ff = score(off, userPredicate + "+" + off + "=")
      retVal.add(new FeatureFunction(userPredicate + "+" + off + "=") {
        def score = ff
      })
    }

    retVal
  }

  def score(offset: Int, predicate: String) = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx >= 0 &&
      idx < example.size()) {
        predicate + example.get(idx, labelType)
    } else {
      null
    }
  }
}