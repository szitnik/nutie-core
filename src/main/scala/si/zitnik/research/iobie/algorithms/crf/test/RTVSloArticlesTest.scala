package si.zitnik.research.iobie.algorithms.crf.test

import si.zitnik.research.iobie.datasets.tab.TabImporter
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/28/11
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */

object RTVSloArticlesTest extends StrictLogging {

  def main(args: Array[String]): Unit = {
    val rootPath = "/Users/slavkoz/IdeaProjects/IOBIE/Datasets/rtvslo_dec2011/"
    val examplesTrain = new TabImporter(Array[Label.Value](Label.NE), rootPath + "articles_1.tsv").
      importForIE()
    val examplesTest = new TabImporter(Array[Label.Value](Label.NE), rootPath + "articles_2.tsv").
      importForIE()

    val genFeatures = new ArrayList[FeatureFunction]()
    //genFeatures.addAll( new CurrLabelAndUpperStartFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new ClassFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new WordFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new OnlineGazeteerFeatureFactory(examplesTrain, Label.NE, "PER").generateFeatures() )

    logger.info("FeatureFunction functions size: %d.".format(genFeatures.size))

    val features = genFeatures.toArray(Array[FeatureFunction]())

    //val classifier = new Learner(Label.NE, examplesTrain, features).train()

    //new Statistics(classifier, examplesTrain).printStandardClassification(Label.NE, "PER")

  }

}