package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 11:57 AM
 * To change this template use File | Settings | File Templates.
 */
class EnglishDemonstrativeFeatureFunction(
                                           labelType: Label.Value = Label.OBS,
                                           userPredicate: String = "BEDeN") extends FeatureFunction(userPredicate + "=") {

  private val demonstrativeNouns = Set("this", "that", "these", "those", "yonder", "yon", "this one", "that one")

  def score = (example: Example, i: Int) => {
    val cons1 = example.get(i).asInstanceOf[Constituent]

    if (demonstrativeNouns.contains(cons1.get(labelType).toLowerCase()) ||
      (cons1.startIdx > 0 && demonstrativeNouns.contains(cons1.example.get(cons1.startIdx-1, labelType).toLowerCase()))) {
      predicate + "D"
    } else {
      predicate + "N"
    }
  }

}
