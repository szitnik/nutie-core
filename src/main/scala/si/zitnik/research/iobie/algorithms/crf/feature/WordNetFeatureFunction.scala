package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.thirdparty.wordnet.api.WordNetHelper
import net.didion.jwnl.data.POS

/**
 * IMPORTANT !!!!:
 * Needs to have POS tags and sufficient mapping. Default mapping is:
 *
      Number Tag Description
      1.	CC	Coordinating conjunction
      2.	CD	Cardinal number
      3.	DT	Determiner
      4.	EX	Existential there
      5.	FW	Foreign word
      6.	IN	Preposition or subordinating conjunction
      7.	JJ	Adjective
      8.	JJR	Adjective, comparative
      9.	JJS	Adjective, superlative
      10.	LS	List item marker
      11.	MD	Modal
      12.	NN	Noun, singular or mass
      13.	NNS	Noun, plural
      14.	NNP	Proper noun, singular
      15.	NNPS	Proper noun, plural
      16.	PDT	Predeterminer
      17.	POS	Possessive ending
      18.	PRP	Personal pronoun
      19.	PRP$	Possessive pronoun
      20.	RB	Adverb
      21.	RBR	Adverb, comparative
      22.	RBS	Adverb, superlative
      23.	RP	Particle
      24.	SYM	Symbol
      25.	TO	to
      26.	UH	Interjection
      27.	VB	Verb, base form
      28.	VBD	Verb, past tense
      29.	VBG	Verb, gerund or present participle
      30.	VBN	Verb, past participle
      31.	VBP	Verb, non-3rd person singular present
      32.	VBZ	Verb, 3rd person singular present
      33.	WDT	Wh-determiner
      34.	WP	Wh-pronoun
      35.	WP$	Possessive wh-pronoun
      36.	WRB	Wh-adverb
 */
class WordNetFeatureFunction(labelType: Label.Value = Label.OBS,
                             userPredicate: String = "UWN") extends FeatureFunction(userPredicate + "=") {
  private val posTagMapping = Map(
                                  //adjectives
                                  "JJ" -> POS.ADJECTIVE,
                                  "JJS" -> POS.ADJECTIVE,
                                  "JJR" -> POS.ADJECTIVE,

                                  //nouns
                                  "NN" -> POS.NOUN,
                                  "NNPS" -> POS.NOUN,
                                  "NNS" -> POS.NOUN,
                                  "NNP" -> POS.NOUN,

                                  //verbs
                                  "VBN" -> POS.VERB,
                                  "VB" -> POS.VERB,
                                  "VBP" -> POS.VERB,
                                  "VBZ" -> POS.VERB,
                                  "VBD" -> POS.VERB,
                                  "MD" -> POS.VERB,
                                  "VBG" -> POS.VERB,

                                  //adverbs
                                  "RB" -> POS.ADVERB,
                                  "RBR" -> POS.ADVERB,
                                  "RBS" -> POS.ADVERB,
                                  "WRB" -> POS.ADVERB/*,

                                  "TO" -> POS.NOUN,
                                  "DT" -> POS.NOUN,
                                  "RP" -> POS.NOUN,
                                  "LS" -> POS.NOUN,
                                  "FW" -> POS.NOUN,
                                  "PDT" -> POS.NOUN,
                                  "``" -> POS.NOUN,
                                  "WP$" -> POS.NOUN,
                                  "PRP" -> POS.NOUN,
                                  "WDT" -> POS.NOUN,
                                  "WP" -> POS.NOUN,
                                  "IN" -> POS.NOUN,
                                  "EX" -> POS.NOUN,
                                  "POS" -> POS.NOUN,
                                  "UH" -> POS.NOUN,
                                  "PRP$" -> POS.NOUN,
                                  "CC" -> POS.NOUN,
                                  "CD" -> POS.NOUN,

                                  "$" -> POS.NOUN,
                                  "''" -> POS.NOUN,
                                  "-LRB-" -> POS.NOUN,
                                  "." -> POS.NOUN,
                                  "," -> POS.NOUN,
                                  ":" -> POS.NOUN,
                                  "-RRB-" -> POS.NOUN*/)

  def score = (example: Example, i: Int) => {
    if (i>0) {

      val pos1 = posTagMapping.get(example.get(i-1, Label.POS).toString())
      val pos2 = posTagMapping.get(example.get(i, Label.POS).toString())

      if (pos1.isDefined && pos2.isDefined) {
        WordNetHelper.findRelationship(
          example.get(i-1, labelType).toString(),
          pos1.get,
          example.get(i, labelType).toString(),
          pos2.get
        ) match {
          case Some(x) => predicate+x
          case None => null
        }
      } else {
        null
      }
    } else {
      null
    }
  }
}
