package si.zitnik.research.iobie.algorithms.crf.sgd

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/2/12
 * Time: 11:30 PM
 * To change this template use File | Settings | File Templates.
 */

import java.util.ArrayList

import scala.collection.JavaConversions._
import breeze.linalg.{DenseMatrix, DenseVector, SparseVector}
import com.typesafe.scalalogging.StrictLogging


class Scorer(
              val s: Sentence,
              val d: FeatureDict,
              val w: DenseVector[Double],
              val wscale: Double,
              val eta: Double = 0) extends StrictLogging {


  var scoresOk: Boolean = false
  var uScores = new ArrayList[DenseVector[Double]]()
  var bScores = new ArrayList[DenseMatrix[Double]]()

  def computeScores(): Unit = {
    if (!scoresOk) {
      val nout: Int = d.nOutputs()
      val npos: Int = s.size


      // compute uScores
      uScores = new ArrayList();
      for ( pos <- 0 to npos - 1) {
        val u = DenseVector.zeros[Double](nout)
        uScores.add(u)

        val x: SparseVector[Double] = s.u(pos)
        x.activeIterator.foreach{case (i, v) => {
          for (j <- 0 to nout - 1) {
            u(j) += w(i + j) * v
          }
        }}

        for (j <- 0 to nout - 1) {
          u(j) *= wscale;

        }

        //if (u.sum != 0)
        //  logger.info("b-sum: %3f".format(u.sum))
      }
      // compute bScores
      bScores = new ArrayList()
      for (pos <- 0 to npos - 2) {
        val b = DenseMatrix.zeros[Double](nout, nout)
        bScores.add(b)

        val x: SparseVector[Double] = s.b(pos)
        x.activeIterator.foreach{case (pi, pv) => {
          //println("%d - %.3f".format(i, v))

          var k: Int = 0;
          for (i <- 0 to nout - 1) {
            val bi = b(::, i)
            for (j <- 0 to nout - 1) {
              bi(j) += w(pi + k) * pv
              k += 1
            }
          }
        }}

        for (i <- 0 to nout - 1) {
          val bi = b(::, i)
          for (j <- 0 to nout - 1) {
            bi(j) *= wscale;

          }
        }
      }
    }
    scoresOk = true;
  }

  def uGradients(g: DenseVector[Double], pos: Int, fy: Int, ny: Int): Unit = {
    val off = fy
    val x: SparseVector[Double] = s.u(pos)
    val gain: Double = eta / wscale
    x.activeIterator.foreach{case (i, v) => {
      for (j <- 0 to ny - 1) {
        w(i + off + j) += g(j) * v * gain;
      }
    }}
  }

  def bGradients(g: DenseVector[Double], pos: Int, fy: Int, ny: Int, y: Int): Unit = {
    val n = d.nOutputs()
    val off = y * n + fy
    val x: SparseVector[Double] = s.b(pos)

    val gain: Double = eta / wscale
    x.activeIterator.foreach{case (i, v) => {
      for (j <- 0 to ny - 1) {
        w(i + off + j) += g(j) * v * gain;
      }
    }}
  }

  def viterbi(path: ArrayList[Int]): Double = {
    computeScores();
    val npos: Int = s.size();
    val nout: Int = d.nOutputs();
    var pos, i, j: Int = 0

    // allocate backpointer array
    val pointers = new Array[Array[Int]](npos)
    for (i <- 0 to npos - 1)
      pointers(i) = new Array[Int](nout)

    // process scores
    var scores: DenseVector[Double] = uScores.get(0);
    for (pos <- 1 to npos - 1) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores.get(pos)

      for (i <- 0 to nout - 1) {
        val bs = DenseVector.zeros[Double](nout)
        bs := bScores.get(pos - 1)(::, i)
        bs += scores

        var bestj: Int = 0
        var bests: Double = bs(0)
        for (j <- 1 to nout - 1)
          if (bs(j) > bests) {
            bests = bs(j)
            bestj = j
          }
        pointers(pos)(i) = bestj
        us(i) += bests
      }
      scores = us
    }
    // find best final score
    var bestj: Int = 0
    var bests: Double = scores(0)
    for (j <- 1 to nout - 1)
      if (scores(j) > bests) {
        bests = scores(j)
        bestj = j
      }
    // backtrack
    path.clear()
    for (i <- 1 to npos) {
      path.add(0)
    }
    for (pos <- (0 to npos - 1).reverse) {
      path.set(pos, bestj)
      bestj = pointers(pos)(bestj)
    }
    return bests;

  }

  def test(): Int = {
    val path = new ArrayList[Int]()
    val npos = s.size()
    var errors: Int = 0
    viterbi(path);
    for (pos <- 0 to npos - 1)
      if (path.get(pos) != s.y(pos))
        errors += 1;
    return errors;
  }

  def scoreCorrect(): Double = {
    computeScores();
    val npos: Int = s.size();
    var y: Int = s.y(0);
    var sum: Double = uScores.get(0)(y)

    for (pos <- 1 to npos - 1) {
      val fy = y
      y = s.y(pos)
      if (y >= 0 && fy >= 0)
        sum += bScores.get(pos - 1)(fy, y)
      if (y >= 0)
        sum += uScores.get(pos)(y)
    }
    return sum;
  }

  def gradCorrect(g: Double): Double = {
    computeScores()
    val npos: Int = s.size()
    var y: Int = s.y(0)
    val vf = DenseVector[Double](g);

    uGradients(vf, 0, y, 1);
    var sum: Double = uScores.get(0)(y)

    for (pos <- 1 to npos - 1) {
      val fy: Int = y
      y = s.y(pos)
      if (y >= 0 && fy >= 0)
        sum += bScores.get(pos - 1)(fy, y)
      if (y >= 0)
        sum += uScores.get(pos)(y)
      if (y >= 0 && fy >= 0)
        bGradients(vf, pos - 1, fy, 1, y)
      if (y >= 0)
        uGradients(vf, pos, y, 1)
    }
    sum
  }

  def scoreForward(): Double = {
    computeScores()
    val npos: Int = s.size()
    val nout: Int = d.nOutputs()

    var scores: DenseVector[Double] = uScores.get(0)
    for (pos <- 1 to npos - 1) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores.get(pos)

      for (i <- 0 to nout - 1) {
        var bs = DenseVector.zeros[Double](nout)
        bs := bScores.get(pos - 1)(::, i)
        bs += scores
        us(i) += SGDCRF.logSum(bs)
      }
      scores = us;

    }
    SGDCRF.logSum(scores)
  }

  def gradForward(g: Double): Double = {
    computeScores()
    val npos: Int = s.size()
    val nout: Int = d.nOutputs()

    val alpha = DenseMatrix.zeros[Double](nout, npos)
    val beta = DenseMatrix.zeros[Double](nout, npos)


    // forward
    alpha(::, 0) := uScores(0);
    for ( pos <- 1 to npos - 1) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores(pos);
      for ( i <- 0 to nout - 1) {
        val bs = DenseVector.zeros[Double](nout)
        bs := bScores(pos - 1)(::, i)
        bs += alpha(::, pos - 1)
        us(i) += SGDCRF.logSum(bs);
      }
      alpha(::, pos) := us;
    }


    // backward
    beta(::, npos - 1) := uScores(npos - 1)
    for ( pos <- (0 to npos - 2).reverse) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores(pos)
      for ( i <- 0 to nout - 1) {
        val bs = DenseVector.zeros[Double](nout)
        val bsc = bScores(pos)
        for ( j <- 0 to nout - 1)
          bs(j) = bsc(i, j)
        bs += beta(::, pos + 1)
        us(i) += SGDCRF.logSum(bs)
      }
      beta(::, pos) := us
    }
    // score
    val score = SGDCRF.logSum(beta(::, 0))

    // collect gradients
    for ( pos <- 0 to npos - 1) {
      val b = beta(::, pos)
      if (pos > 0) {
        val a = alpha(::, pos - 1)
        val bsc = bScores(pos - 1)
        for ( j <- 0 to nout - 1) {
          val bs = bsc(::, j)
          val bgrad = DenseVector.zeros[Double](nout)
          for ( i <- 0 to nout - 1)
            bgrad(i) = g * SGDCRF.expmx(math.max(0.0, score - bs(i) - a(i) - b(j)))
          bGradients(bgrad, pos - 1, 0, nout, j)
        }
      }
      val a = alpha(::, pos)
      val us = uScores(pos)
      val ugrad = DenseVector.zeros[Double](nout)
      for ( i <- 0 to nout - 1)
        ugrad(i) = g * SGDCRF.expmx(math.max(0.0, score + us(i) - a(i) - b(i)))

      uGradients(ugrad, pos, 0, nout)
    }
    score
  }
}