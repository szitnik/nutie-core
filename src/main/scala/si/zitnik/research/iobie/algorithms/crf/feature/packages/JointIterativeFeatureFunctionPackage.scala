package si.zitnik.research.iobie.algorithms.crf.feature.packages;

import si.zitnik.research.iobie.domain.Examples
import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import java.util
import si.zitnik.research.iobie.algorithms.crf.feature.{UnigramConsecutiveFeatureFunction, UnigramPreviousFeatureFunction, UnigramFeatureFunction}
import scala.collection.mutable
import si.zitnik.research.iobie.domain.relationship.Relationship
import si.zitnik.research.iobie.domain.constituent.Constituent
import scala.collection.mutable.{HashSet, ArrayBuffer}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.util.AdderMap
import si.zitnik.research.iobie.algorithms.crf.feature.iterative.{CoreferentMentionLabelFeatureFunctionGenerator, RelationAndOtherConstituentLabelPairFeatureFunctionGenerator, HasASpecificRelationFeatureFunctionGenerator}


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 04/04/14
 * Time: 17:53
 * To change this template use File | Settings | File Templates.
 */
object JointIterativeFeatureFunctionPackage {

  val mentionToRelations = new AdderMap[Constituent, Relationship]()
  val mentionToCorefCluster = new mutable.HashMap[Constituent, Cluster]()

  def reindex(curRels: ArrayBuffer[HashSet[Relationship]], curCorefClusters: ArrayBuffer[HashSet[Cluster]]): Unit = {
    mentionToRelations.map.clear()
    mentionToCorefCluster.clear()

    curRels.foreach(_.foreach(r => {
      mentionToRelations.put(r.subj, r)
      mentionToRelations.put(r.obj, r)
    }))

    curCorefClusters.foreach(set => {
      set.foreach(c => {
        c.foreach(m => {
          mentionToCorefCluster.put(m.oldConstituent, c)
        })
      })
    })
  }

  def create(examples: Examples): ArrayList[FeatureFunction] = {

    val featureFunctions = new util.ArrayList[FeatureFunction]()

    //NE
    featureFunctions.add(new UnigramFeatureFunction(Label.L1_NE, "UL1NE"))
    featureFunctions.add(new UnigramFeatureFunction(Label.L1_NE, "BL1NE"))

    featureFunctions.add(new UnigramPreviousFeatureFunction(Label.L1_NE, "UPL1NE"))
    featureFunctions.add(new UnigramPreviousFeatureFunction(Label.L1_NE, "BPL1NE"))

    featureFunctions.add(new UnigramConsecutiveFeatureFunction(Label.L1_NE, "UCL1NE"))
    featureFunctions.add(new UnigramConsecutiveFeatureFunction(Label.L1_NE, "BCL1NE"))

    //REL
    featureFunctions.addAll(new HasASpecificRelationFeatureFunctionGenerator(examples, "UHACR").generate())
    featureFunctions.addAll(new HasASpecificRelationFeatureFunctionGenerator(examples, "BHACR").generate())

    featureFunctions.addAll(new RelationAndOtherConstituentLabelPairFeatureFunctionGenerator(examples, Label.NE, "URaOCN").generate())
    featureFunctions.addAll(new RelationAndOtherConstituentLabelPairFeatureFunctionGenerator(examples, Label.NE, "BRaOCN").generate())

    featureFunctions.addAll(new RelationAndOtherConstituentLabelPairFeatureFunctionGenerator(examples, Label.OBS, "URaOCO").generate())
    featureFunctions.addAll(new RelationAndOtherConstituentLabelPairFeatureFunctionGenerator(examples, Label.OBS, "BRaOCO").generate())


    //COREF
    featureFunctions.addAll(new CoreferentMentionLabelFeatureFunctionGenerator(examples, Label.NE, "UCoM").generate())
    featureFunctions.addAll(new CoreferentMentionLabelFeatureFunctionGenerator(examples, Label.NE, "BCoM").generate())

    featureFunctions
  }

}
