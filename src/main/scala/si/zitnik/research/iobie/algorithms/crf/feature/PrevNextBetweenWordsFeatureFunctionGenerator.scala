package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.{Token, Example}
import si.zitnik.research.iobie.domain.constituent.Constituent
import scala.collection.JavaConversions._
import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/24/13
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
class PrevNextBetweenWordsFeatureFunctionGenerator(
                                                    distanceFromMention: Seq[Int] = 1 to 5,
                                                    labelType: Label.Value = Label.OBS,
                                                    userPredicate: String = "BBPN") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (distance <- distanceFromMention) {
      retVal.add(ffLeft(distance))
      retVal.add(ffRight(distance))
      retVal.add(ffBetweenL(distance))
      retVal.add(ffBetweenR(distance))
    }


    retVal
  }

  //TODO: optimize
  private def ffBetweenL(distance: Int) = new FeatureFunction(userPredicate + distance + "BL=") {
    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val tokens: mutable.Buffer[Token] = if (cons1.example == cons2.example) {
          if (cons1.endIdx < cons2.startIdx) {
            cons1.example.subList(cons1.endIdx, cons2.startIdx).toBuffer
          } else {
            mutable.Buffer[Token]()
          }
        } else {
          cons1.example.subList(math.min(cons1.endIdx, cons1.example.size()-1), cons1.example.size()) ++
            cons1.example.examples.subList(
              cons1.example.examples.indexOf(cons1.example)+1,
              cons1.example.examples.indexOf(cons2.example)
            ).flatten ++
            cons2.example.subList(0, cons2.startIdx)
        }

        if (tokens.size >= distance) {
          predicate + tokens.get(distance-1).get(labelType)
        } else {
          null
        }
      } else {
        null
      }
    }
  }

  private def ffBetweenR(distance: Int) = new FeatureFunction(userPredicate + distance + "BR=") {
    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val tokens: mutable.Buffer[Token] = if (cons1.example == cons2.example) {
          if (cons1.endIdx < cons2.startIdx) {
            cons1.example.subList(cons1.endIdx, cons2.startIdx).toBuffer
          } else {
            mutable.Buffer[Token]()
          }
        } else {
          cons1.example.subList(math.min(cons1.endIdx, cons1.example.size()-1), cons1.example.size()) ++
            cons1.example.examples.subList(
              cons1.example.examples.indexOf(cons1.example)+1,
              cons1.example.examples.indexOf(cons2.example)
            ).flatten ++
            cons2.example.subList(0, cons2.startIdx)
        }

        if (tokens.size >= distance) {
          predicate + tokens.get(tokens.size -1 - distance + 1).get(labelType)
        } else {
          null
        }
      } else {
        null
      }
    }
  }

  private def ffLeft(distance: Int) = new FeatureFunction(userPredicate + distance + "L=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val idx1 = cons1.startIdx-distance

        if (idx1 >= 0) {
          predicate + cons1.example.get(idx1, labelType)
        } else {
          null
        }
      } else {
        null
      }
    }
  }

  private def ffRight(distance: Int) = new FeatureFunction(userPredicate + distance + "R=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val cons2 = example.get(i).asInstanceOf[Constituent]
        var idx2 = cons2.endIdx+distance

        if (idx2 < cons2.example.size()) {
          predicate + cons2.example.get(idx2, labelType)
        } else {
          null
        }
      } else {
        null
      }
    }
  }

}
