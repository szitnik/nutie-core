package si.zitnik.research.iobie.algorithms.crf.test

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.{Examples, Example}
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.algorithms.crf.linearchain.LCCRFLearner
import si.zitnik.research.iobie.algorithms.crf.stat.Statistics
import si.zitnik.research.iobie.algorithms.crf.feature.{OnlineGazeteerFeatureFunction, UnigramFeatureFunction, BigramDistributionFeatureFunction}
import java.util.{HashSet, ArrayList}


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/12/11
 * Time: 11:23 AM
 * To change this template use File | Settings | File Templates.
 */

object SimpleCRFTest {
  def main(args: Array[String]): Unit = {
    //example 1
    val example1 = new Example(
      Array("Slavko", "i", "Mirko", "je", "hud", "film", ""),
      Label.NE,
      Array("PER", "O", "PER", "O", "O", "O", "O"))


    val example2 = new Example(
      Array("Mirko", "pravi", "spasio", "si", "mi", "život", "Slavko", "!"),
      Label.NE,
      Array("PER", "O", "O", "O", "O", "O", "PER", "O"))

    val learnData = new Examples(Array[Example](example1, example2))
    val testData = new Examples(Array[Example](example2))
    val learnLabelType = Label.NE

    val featureFunctions = new ArrayList[FeatureFunction]()
    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramFeatureFunction(Label.OBS))
    val h = new HashSet[String]()
    h.add("Slavko")
    h.add("Mirko")
    featureFunctions.add(new OnlineGazeteerFeatureFunction(Label.OBS, h))

    val crfLearner = new LCCRFLearner(learnData, learnLabelType, featureFunctions)
    val crfClassifier = crfLearner.trainAndTest(5, 15, testData)

    new Statistics(crfClassifier, testData).printStandardClassification(learnLabelType, "PER")
    val (classfiedLabeling, _, _) = crfClassifier.classify(testData.get(0))

    testData.get(0).printLabeling(learnLabelType)
    println(classfiedLabeling.mkString(" "))
  }
}

