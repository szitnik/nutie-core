package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/24/12
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */

class UnigramPreviousFeatureFunction(labelType: Label.Value, userPredicate: String = "UP") extends FeatureFunction(userPredicate + "=") {
  def score = (example: Example, i: Int) =>
    if (i > 0)
      predicate + example.get(i - 1, labelType)
    else
      predicate + Label.START.toString
}