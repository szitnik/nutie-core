package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example

/**
 * This feature function returns userPredicate iff two consecutive labels of type labelType have
 * the same value.
 */
class BigramFeatureFunctionMatch(labelType: Label.Value, userPredicate: String = "BM") extends FeatureFunction(userPredicate) {
  def score = (example: Example, i: Int) => {
    if (i > 0 && example.get(i-1, labelType).equals(example.get(i, labelType))) {
      predicate
    } else {
      null
    }
  }
}
