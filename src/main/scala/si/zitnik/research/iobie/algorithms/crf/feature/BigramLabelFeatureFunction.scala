package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/16/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramLabelFeatureFunction(l1: Label.Value = Label.OBS, l2: Label.Value = Label.OBS, userPredicate: String = "BLFF") extends FeatureFunction(userPredicate+"=") {

  def score = (example: Example, i: Int) => {
    val val1 = if (i == 0) {Label.START.toString} else {example.get(i-1, l1).toLowerCase()}
    val val2 = example.get(i, l2).toLowerCase()

    predicate+val1+"/"+val2
  }

}