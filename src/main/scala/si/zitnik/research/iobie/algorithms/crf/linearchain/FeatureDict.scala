package si.zitnik.research.iobie.algorithms.crf.linearchain


import scala.collection.JavaConversions._
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import java.util.{ArrayList, HashMap, HashSet}

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.util.CounterMap


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/3/12
 * Time: 11:05 AM
 * To change this template use File | Settings | File Templates.
 */

class FeatureDict extends Serializable with StrictLogging {
  var learnLabelType: Label.Value = null
  var featureFunctions: ArrayList[FeatureFunction] = null
  var featureThreshold: Int = 0

  val labelToIdx = new HashMap[String, Int]()
  val labelNames = new ArrayList[String]()

  val featuresToIdx = new HashMap[String, Int]()
  val idxToFeature = new HashMap[Int, String]()
  var features: scala.collection.mutable.HashSet[String] = null

  var nextFeatureIndex: Int = 0 //dimension of feature function vectors


  def this(learnLabelType: Label.Value, featureFunctions: ArrayList[FeatureFunction], learnExamples: Examples, featureThreshold: Int) {
    this()

    //check feature function duplicates
    val predicates = new HashSet[String]()
    featureFunctions.foreach(f => {
      if (predicates.contains(f.predicate)) {
        logger.error("There exist feature functions using same predicate: %s!".format(f.predicate))
        System.exit(-1)
      } else {
        predicates.add(f.predicate)
      }
    })

    this.learnLabelType = learnLabelType
    this.featureFunctions = featureFunctions
    this.featureThreshold = featureThreshold

    labelNames.addAll(learnExamples.getAllLabelValues(learnLabelType))
    for ((l, i) <- labelNames.zipWithIndex) {
      labelToIdx.put(l, i)
    }
    logger.debug("  sentences: %d  outputs: %d".format(learnExamples.size, labelNames.size()))

    logger.debug("Cleaning feature functions ...")
    cleanFeatures(learnExamples)
    logger.debug("Feature functions cleaned.")

    nextFeatureIndex = initFeatureVectorIndices()
    //init learn domain
    logger.debug("Initializing dataset ...")
    initDataset(learnExamples)
    logger.debug("Dataset initialized.")
    logger.debug("  feature functions: %d, features: %d,  parameters: %d".format(featureFunctions.size(), features.size, nextFeatureIndex))
  }

  def initFeatureVectorIndices() = {
    var featuresNumber = 0
    for (feature <- features) {
      featuresToIdx.put(feature, featuresNumber)
      idxToFeature.put(featuresNumber, feature)

      if (feature.charAt(0) == 'B') //IS BIGRAM FEATURE
        featuresNumber += labelNames.size * labelNames.size
      else
        featuresNumber += labelNames.size
    }
    featuresNumber
  }

  def nLabels() = labelNames.size()

  def nFeatures() = features.size

  def nParams() = nextFeatureIndex

  def labelName(i: Int) = labelNames.get(i)

  def labelToIdx(s: String): Int = labelToIdx.get(s)


  //COUNT FEATURES DELETE UNDER THRESHOLD

  /**
   * Deletes feature functions which are triggered less than featureThreshold
   *
   */
  def cleanFeatures(learnExamples: Examples): Unit = {
    //1. Init
    val featureCounter = new CounterMap[String]()

    //2. Count
    for (example <- learnExamples) {
      for (i <- 0 until example.size) {
        for (featureFunction <- featureFunctions) {
          val expanded = featureFunction.score(example, i)

          if (expanded != null)
            featureCounter.put(expanded)
        }
      }
    }




    //3. Remove underthreshold ffs
    var diffSize = featureCounter.map.keySet.size
    features = featureCounter.toSet(featureThreshold)
    diffSize -= features.size
    logger.debug("%d feature functions removed due to low threshold.".format(diffSize))
  }

  //INIT FEATURE VECTORS

  /**
   * This function must be called:
   * - before learning on learn dataset
   * - before tagging on si.zitnik.research.iobie.test dataset
   *
   * @param examples
   */
  def initDataset(examples: Examples): Unit = {
    for (example <- examples) {
      initExample(example)
    }
  }

  def initExample(example: Example): Unit = {
    for ( pos <- 0 to example.size - 1) {
      example.get(pos).initFeatures(nextFeatureIndex)
      for (featureFunction <- featureFunctions) {

        val score = featureFunction.score(example, pos)
        if (score != null && featuresToIdx.containsKey(score)) {
          val expanded = featuresToIdx.get(score)
          score.charAt(0) match {
            case 'B' => if (pos > 0) example.get(pos - 1).bfeatures(expanded) = 1
            case 'U' => example.get(pos).ufeatures(expanded) = 1
            case _ => logger.warn("An unsupported feature function was given: %s".format(score))
          }
        }
      }
    }
  }
}