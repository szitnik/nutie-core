package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.FeatureFunction

/**
 * Bigram feature function.
 *
 * In all "normal-length" si.zitnik.research.iobie.datasets this feature function should be above featureFunctionThreshold
 */
class BigramDistributionFeatureFunction(userPredicate: String = "B") extends FeatureFunction(userPredicate) {
  def score = (example: Example, i: Int) => predicate
}
