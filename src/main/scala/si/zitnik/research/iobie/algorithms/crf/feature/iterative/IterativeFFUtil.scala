package si.zitnik.research.iobie.algorithms.crf.feature.iterative

import si.zitnik.research.iobie.domain.{Token, Example}
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 07/04/14
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
object IterativeFFUtil {

  def getConstituent(example: Example, i: Int): Option[Constituent] = {
    example.get(i) match {
      case x: Constituent => {
        var y = x
        while (y.oldConstituent != null) y = y.oldConstituent
        Some(y)
      }
      case t: Token => {
        val mentionsIntersect = example.getAllMentions().filter(m => {
          m.startIdx <= i && i < m.endIdx
        })
        if (mentionsIntersect.size > 0) {
          Some(mentionsIntersect(0))
        } else {
          None
        }
      }
      case _ => {
        throw new Exception("Error!")
        System.exit(-1)
        None
      }
    }
  }

}
