package si.zitnik.research.iobie.algorithms.crf.feature

import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import si.zitnik.research.iobie.domain.Example

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/8/12
 * Time: 12:30 PM
 * To change this template use File | Settings | File Templates.
 */
class LabelUnigramArrayFeatureFunctionGenerator(
                                            val labelType: Label.Value,
                                            val range: Range = 0 to 0,
                                            val maxArrayLen: Int = 5,
                                            userPredicate: String = "UAr") extends FeatureFunctionGenerator with StrictLogging {


  def generate() = {
    val retVal = new ArrayList[FeatureFunction]()

    for (offset <- range) {
      for (arrayIdx <- 0 to maxArrayLen) {
        val ff = makeff(labelType, offset, arrayIdx, userPredicate + offset + "-" + arrayIdx + "=")
        retVal.add(new FeatureFunction(userPredicate + offset + "-" + arrayIdx + "=") {
          def score = ff
        })
      }
    }

    retVal
  }

  def makeff(labelType: Label.Value, offset: Int, arrayIdx: Int, predicate: String) = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx >= 0 &&
      idx < example.size()) {

      val array = example.get(idx, labelType).asInstanceOf[Array[String]]
      if (array.size > arrayIdx) {
        predicate + array(arrayIdx)
      } else {
        null
      }
    } else {
      null
    }
  }

}
