package si.zitnik.research.iobie.algorithms.crf.test

import si.zitnik.research.iobie.datasets.conll2000.CoNLL2000Importer
import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.FeatureFunction

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/12
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */

object CoNLL2000Test extends StrictLogging {

  def main(args: Array[String]): Unit = {
    val rootPath = "/Users/slavkoz/Documents/DR_Research/Datasets/conll2000/"
    val examplesTrain = new CoNLL2000Importer(rootPath + "train.txt").
      importForIE()
    val examplesTest = new CoNLL2000Importer(rootPath + "si.zitnik.research.iobie.test.txt").
      importForIE()

    val genFeatures = new ArrayList[FeatureFunction]()
    //genFeatures.addAll( new CurrLabelAndUpperStartFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new ClassFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new WordFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new OnlineGazeteerFeatureFactoryAll(examplesTrain, Label.CHUNK).generateFeatures() )

    logger.info("FeatureFunction functions size: %d.".format(genFeatures.size))

    val features = genFeatures.toArray(Array[FeatureFunction]())

    //val classifier = new Learner(Label.CHUNK, examplesTrain, features).train()

    //Statistics.printStandardClassification(classifier, examplesTrain, Label.NE, "PER")

  }

}