package si.zitnik.research.iobie.algorithms.crf.feature.iterative

import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util
import si.zitnik.research.iobie.algorithms.crf.feature.packages.JointIterativeFeatureFunctionPackage
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.cluster.Cluster

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 08/04/14
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
class CoreferentMentionLabelFeatureFunctionGenerator(
                                                      examples: Examples,
                                                      otherConsLabelType: Label.Value = Label.OBS,
                                                      userPredicate: String = "BCoM") extends FeatureFunctionGenerator {

  def generate(): util.ArrayList[FeatureFunction] = {
    val retVal = new util.ArrayList[FeatureFunction]()

    val labelValues = examples.map(_.getAllMentions().map(_.get(otherConsLabelType).asInstanceOf[String]).toSet).flatten.toSet
    for (labelValue <- labelValues) {
      retVal.add(ff(labelValue))
    }

    retVal
  }

  private def ff(labelValue: String) = new FeatureFunction(userPredicate + "=" + labelValue) {
    def score = (example: Example, i: Int) => {
      IterativeFFUtil.getConstituent(example, i) match {
        case Some(cons) => {
          val set = JointIterativeFeatureFunctionPackage.mentionToCorefCluster.getOrElse(cons, new Cluster()).filter(_ != cons).map(_.get(otherConsLabelType).asInstanceOf[String]).toSet
          //TODO: does it really remove cons from cluster

          if (set.contains(labelValue))  {
            predicate
          } else {
            null
          }
        }

        case None => {
          null
        }
      }
    }
  }

  }