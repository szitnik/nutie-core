package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/9/12
 * Time: 3:47 PM
 * To change this template use File | Settings | File Templates.
 */
class StartsUpperFeatureFunction(val offset: Int = 0, userPredicate: String = "USU") extends FeatureFunction(userPredicate + offset) {

  def score = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx >= 0 &&
      idx < example.get(i, Label.OBS).asInstanceOf[String].length() &&
      example.get(i, Label.OBS).charAt(idx).isUpper) {
      predicate+"=T"
    } else {
      null
    }
  }
}
