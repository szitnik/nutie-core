package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/9/13
 * Time: 8:14 PM
 * To change this template use File | Settings | File Templates.
 */
class ParseTreeParentValueFeatureFunction(
                                           userPredicate: String = "UPPT",
                                           parentLength: Int = 1 //1 = direct parent
                                         ) extends FeatureFunction(userPredicate + "=") {
  def score = (example: Example, i: Int) => {
    var parseNode = example.get(i, Label.PARSE_NODE)

    var length = parentLength
    while (parseNode.parent != null && parentLength > 0) {
      parseNode = parseNode.parent
      length -= 1
    }

    if (length == 0) {
      predicate + parseNode.value
    } else {
      null
    }
  }

}
