package si.zitnik.research.iobie.algorithms.crf.linearchain

import breeze.linalg.DenseVector
import com.typesafe.scalalogging.StrictLogging

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/10/12
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */

object Util extends StrictLogging {

  def expmx(x: Double) = {
    //calculates approx
    //exp(-x)

    if (x < 0) {
      logger.error("Calculating exp(NEGATIVE)!")
    }

    // fast approximation of exp(-x) for x positive
    if (x < 13.0) {
      var y =
        1.0 + x *
          (0.125 + x *
            (0.0078125 + x *
              (0.00032552083 + x *
                1.0172526e-5)))
      y *= y
      y *= y
      y *= y
      1 / y
    } else {
      0.0
    }
  }

  def dLogSum(g: Double, v: DenseVector[Double], r: DenseVector[Double]): Unit = {
    if (v.size > r.size) {
      logger.error("Vector sizes at dLogSum do not match correctly!")
    }
    dLogSum(g, v, r, v.size);
  }
  /*
  TODO: is this ok transformed to scala 2.10.0??
  def dLogSum(g: Double, v: DenseVector[Double], r: DenseVector[Double]) {
    if (v.size > r.size) {
      logger.error("Vector sizes at dLogSum do not match correctly!")
    }
    dLogSum(g, v, r, v.size);
  } */

  def dLogSum(g: Double, v: DenseVector[Double], r: DenseVector[Double], n: Int): Unit = {
    var m: Double = v(0)
    for ( i <- 0 to n - 1)
      m = math.max(m, v(i));

    var z: Double = 0;
    for ( i <- 0 to n - 1) {
      val e: Double = expmx(m - v(i));
      r(i) = e;
      z += e;
    }
    for ( i <- 0 to n - 1)
      r(i) = 1.0 * g * r(i) / z;
  }

  def logSum(v: DenseVector[Double]): Double = {
    logSum(v, v.size)
  }

  def logSum(v: DenseVector[Double], n: Int): Double = {
    var m: Double = v(0);
    for ( i <- 0 to n - 1)
      m = math.max(m, v(i))
    var s: Double = 0;
    for ( i <- 0 to n - 1)
      s += expmx(m - v(i));
    m + math.log(s);
  }
}
