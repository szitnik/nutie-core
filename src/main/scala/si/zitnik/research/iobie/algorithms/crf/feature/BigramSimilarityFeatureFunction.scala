package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import com.wcohen.ss.{JaroWinklerTFIDF, AbstractStringDistance}
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 12:43 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramSimilarityFeatureFunction(
                                       labelType: Label.Value = Label.OBS,
                                       similarity: AbstractStringDistance = new JaroWinklerTFIDF(),
                                       thresholds: (Double, Double, Double) = (0.5, 0.8, 0.95),
                                       userPredicate: String = "BSim") extends FeatureFunction(userPredicate+"=") {

  def score = (example: Example, i: Int) => {
    if (i > 0) {
      val val1 = example.get(i-1, labelType).toLowerCase
      val val2 = example.get(i, labelType).toLowerCase
      val sim = similarity.score(val1, val2)
      if (sim < thresholds._1) {
        predicate+"1"
      } else if (sim < thresholds._2) {
        predicate+"2"
      } else if (sim < thresholds._3) {
        predicate+"3"
      } else {
        predicate+"4"
      }
    } else {
      null
    }
  }
}