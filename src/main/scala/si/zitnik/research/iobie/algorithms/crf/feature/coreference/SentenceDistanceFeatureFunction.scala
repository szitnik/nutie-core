package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.FeatureFunction
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
class SentenceDistanceFeatureFunction(
                                       userPredicate: String = "BSD") extends FeatureFunction(userPredicate + "=") with StrictLogging {

  def score = (example: Example, i: Int) => {
    if (i>0) {
      val rawExamples = example.get(i-1).asInstanceOf[Constituent].example.examples
      val e1 = rawExamples.indexOf(example.get(i-1).asInstanceOf[Constituent].example)
      val e2 = rawExamples.indexOf(example.get(i).asInstanceOf[Constituent].example)

      if (e1 == -1 || e2 == -1) {
        logger.warn("Sentence detector feature function could not detect an example!")
      }

      predicate + (e2-e1)
    } else {
      null
    }
  }

}
