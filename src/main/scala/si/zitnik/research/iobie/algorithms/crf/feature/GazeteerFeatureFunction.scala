package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example

import io.Source
import si.zitnik.research.iobie.domain.IOBIEConversions._
import java.util.HashSet

import java.io.File

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, FeatureFunction, Label}

/**
 * fileName parameters can be folder or direct filename. This gazeteer also supports matching over multiple words.
 *
 * IMPORTANT: To identify multiword mentions, PARSE TREE must be calculated.
 *
 *
 */
//TODO: maybe too intensive (getting NPs from tree many times!!!)
class GazeteerFeatureFunction(
                               userPredicate: String,
                               fileNames: Array[String]) extends FeatureFunction(userPredicate) with StrictLogging {
  val gazeteer = new HashSet[String]()

  private val commentChar = "#"

  init()

  private def init(): Unit = {
    //1. init set
    for (fileName: String <- fileNames) {
      try {
        val source = new File(fileName)
        if (source.isDirectory) {
          for (file: File <- source.listFiles()) {
            logger.debug("Reading gazeteer file %s".format(file.getName))
            readData(file)
          }
        } else {
          readData(source)
        }

      } catch {
        case e: Throwable => logger.info("There was error reading file %s".format(fileName))
        e.printStackTrace()
      }
    }

  }

  def readData(file: File): Unit = {
    Source.fromFile(file, "utf-8").getLines().foreach(line =>
      if (!line.startsWith(commentChar) && line.length() > 0) {
        gazeteer.add(line)
      }
    )
  }

  /**
   * Checks all constituents that contain current word
   * @param example
   * @param i
   * @return
   */
  def isMultiwordMention(example: Example, i: Int): Boolean = {
    if (example.containsLabel(ExampleLabel.PARSE_TREE)) {
      val constituents = example.get(ExampleLabel.PARSE_TREE).getConstituents(example, "NP")

      var idx = 0
      //go to effective constituents
      while (idx < constituents.size() && !(constituents.get(idx).startIdx <= i && i <= constituents.get(idx).endIdx))
        idx += 1

      //check if any constituent matches the gazeteer
      while (idx < constituents.size() && (constituents.get(idx).startIdx <= i && i <= constituents.get(idx).endIdx)) {
        if (gazeteer.contains(constituents.get(idx).get(Label.OBS))) {
          return true
        }
        idx += 1
      }
    }

    false
  }

  def score = (example: Example, i: Int) => {
    if (gazeteer.contains(example.get(i, Label.OBS)) || isMultiwordMention(example, i))
      predicate
    else
      null
  }


}

object GazeteerFeatureFunction {
  //standard gazeteer paths
  val gzPATH = "iobie-resources/gazeteers/"

  val JOB = gzPATH + "JOB"
  val LOC = gzPATH + "LOC"
  val MEASURES = gzPATH + "MEASURES"
  val MONEY = gzPATH + "MONEY"
  val NUM = gzPATH + "NUM"
  val ORG = gzPATH + "ORG"
  val PER = gzPATH + "PER"
  val TEMPORAL = gzPATH + "TEMPORAL"
}