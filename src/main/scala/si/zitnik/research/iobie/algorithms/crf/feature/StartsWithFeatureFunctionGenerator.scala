package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example

/**
 * Generates two feature functions, depending if i-th starts with (i-1)-th or vice versa.
 */
class StartsWithFeatureFunctionGenerator(
                                          labelType1: Label.Value = Label.OBS,
                                          userPredicate: String = "USt") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()
    retVal.add(ff1)
    retVal.add(ff2)
    retVal
  }

  private def ff1 = new FeatureFunction(userPredicate + "1") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i-1, labelType1).asInstanceOf[String].toLowerCase().startsWith(example.get(i, labelType1).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff2 = new FeatureFunction(userPredicate + "2") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i, labelType1).asInstanceOf[String].toLowerCase().startsWith(example.get(i-1, labelType1).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

}
