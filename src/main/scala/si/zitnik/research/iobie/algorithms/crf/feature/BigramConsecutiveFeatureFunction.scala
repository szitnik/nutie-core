package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}

/**
 *
 */
//TODO: this gives awesome results!!!!!


class BigramConsecutiveFeatureFunction(labelType: Label.Value, userPredicate: String = "BC") extends FeatureFunction(userPredicate + "=") {
  def score = (example: Example, i: Int) =>
    if (i > 0)
      predicate + example.get(i - 1, labelType) + "/" + example.get(i, labelType)
    else
      predicate + Label.START.toString + "/" + example.get(i, labelType)
}
