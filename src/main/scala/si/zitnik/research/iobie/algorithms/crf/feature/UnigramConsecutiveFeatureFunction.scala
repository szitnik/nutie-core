package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/19/12
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */

class UnigramConsecutiveFeatureFunction(labelType: Label.Value, userPredicate: String = "UC") extends FeatureFunction(userPredicate + "=") {
  def score = (example: Example, i: Int) =>
    if (i > 0)
      predicate + example.get(i - 1, labelType) + "/" + example.get(i, labelType)
    else
      predicate + Label.START.toString + "/" + example.get(i, labelType)
}
