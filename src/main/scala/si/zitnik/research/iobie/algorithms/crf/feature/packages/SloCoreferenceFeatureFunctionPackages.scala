package si.zitnik.research.iobie.algorithms.crf.feature.packages

import java.util.ArrayList
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.algorithms.crf.feature._
import coreference._

object SloCoreferenceFeatureFunctionPackages {

  // FEATURE FUNCTION GROUP: String-shape
  def stringSloCorefFFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.add(new BigramDistributionFeatureFunction())
    featureFunctions.add(new UnigramDistributionFeatureFunction())

    featureFunctions.add(new UnigramFeatureFunction(Label.LEMMA, "U=LEMMA"))
    featureFunctions.add(new UnigramFeatureFunction(Label.LEMMA, "B=LEMMA"))

    featureFunctions.add(new StartsUpperFeatureFunction(-1))
    featureFunctions.add(new StartsUpperFeatureFunction())
    featureFunctions.add(new StartsUpperTwiceFeatureFunction(-1))
    featureFunctions.add(new StartsUpperTwiceFeatureFunction())

    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 2, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, 3, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -3, -5 to 5).generate())
    featureFunctions.addAll(new UnigramXffixFeatureFunctionGenerator(Label.OBS, -2, -5 to 5).generate())

    //is in quoted speech (BART inquotedspeech)
    featureFunctions.add(new IsQuotedFeatureFunction(userPredicate = "BQuoted"))

    //newly designed feature functions
    featureFunctions.addAll(new SubstringMatchFeatureFunctionGenerator().generate())
    featureFunctions.addAll(new StartsWithFeatureFunctionGenerator(Label.OBS).generate())
    featureFunctions.addAll(new EndsWithFeatureFunctionGenerator(Label.OBS).generate())

    //  head word of mi j;
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "UOBS1", range = -2 to 2).generate())
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.OBS, userPredicate = "BOBS1", range = -2 to 2).generate())

    //    Length of mi j ;
    featureFunctions.add(new LengthDifferenceFeatureFunction(userPredicate = "ULDF"))

    featureFunctions.add(new BigramAliasFeatureFunction(userPredicate = "UAL")) //one is alias, abbreviation of another (TANL-1 Acronym)
    featureFunctions.add(new BigramIsPrefixFeatureFunction(userPredicate = "UPF")) //one mention is a prefix of another (TANL-1 Prefix)
    featureFunctions.add(new BigramIsSuffixFeatureFunction(userPredicate = "USF")) //one mention is a suffix of another (TANL-1 Suffix)

    featureFunctions.add(new BigramSimilarityFeatureFunction(userPredicate = "USim")) //mentions are similar to each other (TANL-1 Edit distance)
    featureFunctions.add(new BigramFeatureFunctionMatch(Label.OBS, userPredicate = "UMO")) //string match  (TANL-1 Same feature)

    featureFunctions.add(new BigramAppositiveFeatureFunction(possiblePOSTags = Set("samostalnik"))) //is it appositive

    featureFunctions
  }

  // FEATURE FUNCTION GROUP: Lexical
  def lexicalSloCorefFFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    //    Previous/Next two POS tags of mi j;
    featureFunctions.addAll(new OffsetFeatureFunctionGenerator(Label.POS, -2 to 2, userPredicate = "BOff").generate())
    featureFunctions.add(new BigramConsecutiveFeatureFunction(Label.POS)) //POS pair (TANL-1 Head POS)
    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "U=POS"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS, "B=POS"))
    featureFunctions.addAll(new LabelBigramFeatureFunctionGenerator(Label.POS, userPredicate = "UPOS").generate())

    featureFunctions.add(new UnigramFeatureFunction(Label.POS_TYPE, "U=POS_VRSTA"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_NUMBER, "U=POS_ŠTEVILO"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_CASE, "U=POS_SKLON"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_PERSON, "U=POS_OSEBA"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_OWNER_NUMBER, "U=POS_ŠTEVILO_SVOJINE"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_FORM, "U=POS_ZAPIS"))

    featureFunctions
  }

  // FEATURE FUNCTION GROUP: Semantic
  def semanticSloCorefFFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    //    Both are proper names and their strings match (y/n).
    featureFunctions.add(new TwoLabelMatchFeatureFunction(userPredicate = "UTLM", l1 = Label.NE))
    featureFunctions.add(new BigramFeatureFunctionMatch(Label.NE, userPredicate = "UMN")) //semantic match (TANL-1 Type feature)
    featureFunctions.add(new UnigramFeatureFunction(Label.NE, "U=NE"))

    featureFunctions.add(new UnigramFeatureFunction(Label.POS_ANIMATE, "U=POS_ŽIVOST"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_OWNER_GENDER, "U=POS_SPOL_SVOJINE"))
    featureFunctions.add(new UnigramFeatureFunction(Label.POS_GENDER, "U=POS_SPOL"))

    featureFunctions
  }

  // FEATURE FUNCTION GROUP: Distance
  def distanceSloCorefFFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    //CoNLL2012 Fernandes: Distance between mi and mj in sentences;
    //sentence distance (TANL-1 Sentence distance)
    featureFunctions.add(new SentenceDistanceFeatureFunction(userPredicate = "USD"))
    //token distance (TANL-1 Token distance)
    featureFunctions.add(new TokenDistanceFeatureFunction(userPredicate = "UTD"))
    featureFunctions.add(new BigramIsInTheSameSentenceFeatureFunction())

    featureFunctions
  }

  def allSloCorefFeatureFunctions = {
    val featureFunctions = new ArrayList[FeatureFunction]()

    featureFunctions.addAll(stringSloCorefFFunctions)
    featureFunctions.addAll(lexicalSloCorefFFunctions)
    featureFunctions.addAll(semanticSloCorefFFunctions)
    featureFunctions.addAll(distanceSloCorefFFunctions)

    featureFunctions
  }

}
