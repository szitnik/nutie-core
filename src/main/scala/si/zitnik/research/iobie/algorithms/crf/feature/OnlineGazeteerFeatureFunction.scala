package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import java.util.HashSet

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/18/12
 * Time: 12:47 PM
 * To change this template use File | Settings | File Templates.
 */
class OnlineGazeteerFeatureFunction(labelType: Label.Value, set: HashSet[String], userPredicate: String = "UOG") extends FeatureFunction(userPredicate) {
  def score = (example: Example, i: Int) =>
    if (set.contains(example.get(i, labelType)))
      predicate
    else
      null
}
