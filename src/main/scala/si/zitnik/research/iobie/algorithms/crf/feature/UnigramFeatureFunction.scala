package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}

/**
 * Unigram feature function.
 *
 * In all "normal-length" si.zitnik.research.iobie.datasets this feature function should be above featureFunctionThreshold
 */

class UnigramFeatureFunction(labelType: Label.Value, userPredicate: String = "U") extends FeatureFunction(userPredicate + "=") {
  def score = (example: Example, i: Int) => predicate + example.get(i, labelType)
}
