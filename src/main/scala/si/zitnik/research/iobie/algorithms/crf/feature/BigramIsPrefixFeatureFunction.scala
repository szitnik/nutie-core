package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 4:31 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramIsPrefixFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "BPF") extends FeatureFunction(userPredicate+"=") {
  def score = (example: Example, i: Int) => {
    if (i > 0) {
      val val1 = example.get(i-1, labelType).toLowerCase()
      val val2 = example.get(i, labelType).toLowerCase()

      if (val2.startsWith(val1)) {
        predicate+"T"
      } else {
        predicate+"F"
      }
    } else {
      null
    }
  }

}
