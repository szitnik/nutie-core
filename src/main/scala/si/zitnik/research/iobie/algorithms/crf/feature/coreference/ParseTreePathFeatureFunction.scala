package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.parse.{NodePath, ParseNode}
import si.zitnik.research.iobie.domain.IOBIEConversions._
import collection.mutable.ArrayBuffer

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/22/13
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
class ParseTreePathFeatureFunction(
                                    userPredicate: String = "BPT",
                                    maxPathFromOneNode: Int = Int.MaxValue,
                                    includeLeafInFeature: NodePath.Value = NodePath.NON_INCLUSIVE) extends FeatureFunction(userPredicate + "=") {
  def score = (example: Example, i: Int) => {
    if (i>0) {
      val cons1 = example.get(i-1, Label.PARSE_NODE)
      val cons2 = example.get(i, Label.PARSE_NODE)

      if (example.get(i-1).example == example.get(i).example) {
        var nodePath = ParseNode.getNodePath(cons1, cons2, includeLeafInFeature)
        if (nodePath.size >= 2*maxPathFromOneNode) {
          nodePath = nodePath.take(maxPathFromOneNode) ++ ArrayBuffer(new ParseNode("...")) ++ nodePath.takeRight(maxPathFromOneNode)
        }
        predicate + nodePath.map(_.value).mkString("/")
      } else {
        null
      }
    } else {
      null
    }
  }

}
