package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import si.zitnik.research.iobie.domain.Example
import java.util.ArrayList
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * if (substringLen positive) => use prefix
 * else => use suffix
 */
class OffsetRealExampleTokenFeatureFunctionGenerator(
                                     val labelType: Label.Value,
                                     val offset: Range = 0 to 0,
                                     userPredicate: String = "UROff") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (off <- offset) {
      val ff = score(off, userPredicate + "+" + off + "=")
      retVal.add(new FeatureFunction(userPredicate + "+" + off + "=") {
        def score = ff
      })
    }

    retVal
  }

  def score(offset: Int, predicate: String) = (example: Example, i: Int) => {
    val realExample = example.get(i).asInstanceOf[Constituent].example
    val idx = example.get(i).asInstanceOf[Constituent].startIdx + offset
    if (idx >= 0 &&
      idx < realExample.size()) {
        predicate + realExample.get(idx, labelType)
    } else {
      null
    }
  }
}