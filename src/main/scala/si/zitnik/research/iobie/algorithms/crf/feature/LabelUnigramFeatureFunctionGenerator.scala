package si.zitnik.research.iobie.algorithms.crf.feature

import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import si.zitnik.research.iobie.domain.Example


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/8/12
 * Time: 12:30 PM
 * To change this template use File | Settings | File Templates.
 */
class LabelUnigramFeatureFunctionGenerator(
                                            val labelType: Label.Value,
                                            val range: Range = 0 to 0,
                                            userPredicate: String = "U") extends FeatureFunctionGenerator with StrictLogging {


  def generate() = {
    val retVal = new ArrayList[FeatureFunction]()

    for (offset <- range) {
      val ff = makeff(labelType, offset, userPredicate + offset + "=")
      retVal.add(new FeatureFunction(userPredicate + offset + "=") {
        def score = ff
      })
    }

    retVal
  }

  def makeff(labelType: Label.Value, offset: Int, predicate: String) = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx >= 0 &&
      idx < example.size()) {
      predicate + example.get(idx, labelType)
    } else {
      null
    }
  }

}
