package si.zitnik.research.iobie.algorithms.crf.sgd

import java.util.{ArrayList, HashMap}

import com.typesafe.scalalogging.StrictLogging

import io.Source
import scala.collection.JavaConversions._


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/3/12
 * Time: 11:05 AM
 * To change this template use File | Settings | File Templates.
 */

class FeatureDict(val columns: Int) extends StrictLogging {
  val outputs = new HashMap[String, Int]()
  val features = new HashMap[String, Int]()
  val templates = new ArrayList[String]()
  val outputnames = new ArrayList[String]()
  val internedStrings = new HashMap[String, Int]()
  var index: Int = 0

  def nOutputs() = {
    outputs.size()
  }

  def nFeatures() = {
    features.size()
  }

  def nTemplates() = {
    templates.size()
  }

  def nParams() = {
    index
  }

  def output(s: String): Int = {
    if (outputs.containsKey(s)) {
      outputs.get(s)
    } else {
      -1
    }
  }

  def feature(s: String): Int = {
    if (features.containsKey(s)) {
      features.get(s)
    } else {
      -1
    }
  }

  def outputString(i: Int) = {
    outputnames.get(i)
  }

  def templateString(i: Int) = {
    templates.get(i)
  }

  def internString(s: String) = {
    if (internedStrings.containsKey(s)) {
      s //TODO: no memory for string storing is saved
    } else {
      internedStrings.put(s, 1)
      s
    }
  }

  def initFromData(tFile: String, dFile: String, cutoff: Int = 1): Int = {
    // clear all
    templates.clear();
    outputs.clear();
    features.clear();
    index = 0;

    // read templates
    logger.info("Reading template file %s.".format(tFile))
    Util.readTemplateFile(tFile, templates)
    var nu: Int = 0
    var nb: Int = 0
    for (template: String <- templates) {
      if (template.charAt(0) == 'U')
        nu += 1;
      else if (template.charAt(0) == 'B')
        nb += 1;
    }

    logger.info("  u-templates: %d,  b-templates: %d".format(nu, nb))
    if (nu + nb != templates.size()) {
      logger.info("ERROR (building dictionary): Problem counting templates")
      System.exit(10)
    }

    // process compressed datafile
    logger.info("Scanning %s to build dictionary.".format(dFile))

    val fcount = new HashMap[String, Int]()
    var oindex: Int = 0
    var sentences: Int = 0
    val s = new ArrayList[String]()

    val it = Source.fromFile(dFile).getLines();
    while (Util.readDataSentence(it, s, columns) != 0) {
      sentences += 1;
      // intern strings to save memory - //TODO: deleted

      // expand features and count them
      val rows: Int = s.size() / columns;
      for (pos <- 0 to rows - 1) {
        // check output keyword
        val y: String = s.get(pos * columns + columns - 1)
        if (!outputs.containsKey(y)) {
          outputs.put(y, oindex)
          oindex += 1
        }
        // expand templates
        for (t <- 0 to templates.size() - 1) {
          val x: String = Util.expandTemplate(templates.get(t), s, columns, pos)
          if (fcount.containsKey(x)) {
            fcount.put(x, fcount.get(x) + 1)
          } else {
            fcount.put(x, 1)
          }
        }
      }
    }
    outputnames.clear()
    for (i <- 1 to oindex)
      outputnames.add("")

    for ((k, v) <- outputs) {
      outputnames.set(v, k)
    }

    logger.info("  sentences: %d  outputs: %d".format(sentences, oindex))

    // sorting in frequency order
    var fRemoved = 0
    val keys = new ArrayList[SiPair]()
    for ((k, v: Int) <- fcount) {
      if (v >= cutoff) {
        keys.add(new SiPair(k, v))
      } else {
        fRemoved += 1
      }
    }
    logger.info("%d features were removed due to low frequency!".format(fRemoved))
    if (keys.size() <= 0) {
      logger.error("ERROR (building dictionary): No features satisfy the cutoff frequency")
      System.exit(10)
    }
    java.util.Collections.sort(keys)

    // allocating parameters
    for (j <- 0 to keys.size() - 1) {
      val k: String = keys.get(j).s
      features.put(k, index)
      if (k.charAt(0) == 'B')
        index += oindex * oindex;
      else
        index += oindex;
    }
    logger.info("  cutoff: %d  features: %d  parameters: %d".format(cutoff, features.size(), index))

    return sentences;
  }

}

class SiPair(
              val s: String,
              val i: Int) extends Comparable[SiPair] {

  def compareTo(p2: SiPair) = {
    if (i > p2.i)
      0
    else if (i < p2.i)
      1
    else
    if (s.compareTo(p2.s) < 0)
      0
    else
      1

  }
}