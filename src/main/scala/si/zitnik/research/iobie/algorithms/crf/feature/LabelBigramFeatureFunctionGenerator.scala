package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunctionGenerator, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import java.util.ArrayList

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/8/12
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
class LabelBigramFeatureFunctionGenerator(
                                           val labelType: Label.Value,
                                           val range: Range = 0 to 0,
                                           userPredicate: String = "B") extends FeatureFunctionGenerator {
  def generate() = {
    val retVal = new ArrayList[FeatureFunction]()


    for (offset <- range) {
      val ff = makeff(labelType, offset, userPredicate + offset + "=")
      retVal.add(new FeatureFunction(userPredicate + offset + "=") {
        def score = ff
      })
    }

    retVal
  }

  def makeff(labelType: Label.Value, offset: Int, predicate: String) = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx - 1 >= 0 && idx < example.size()) {
      predicate + example.get(idx - 1, labelType) + "/" + example.get(idx, labelType)
    } else if (idx >= 0 && idx < example.size()) {
      predicate + Label.START.toString + "/" + example.get(idx, labelType)
    } else {
      predicate + Label.START.toString
    }
  }
}