package si.zitnik.research.iobie.algorithms.crf.sgd



import util.Random
import io.Source
import java.util.ArrayList

import scala.collection.JavaConversions._
import breeze.linalg.DenseVector
import com.typesafe.scalalogging.StrictLogging

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/2/12
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */

class SGDCRF extends StrictLogging {
  var w = DenseVector.zeros[Double](1)
  //initialized in code
  var wscale: Double = 1
  var lambda: Double = 0
  var wnorm: Double = 0
  var t: Double = 0

  val startMillis: Double = System.currentTimeMillis()
  val dict = new FeatureDict(3)

  var epoch: Int = 0

  def initialize(tFile: String, dFile: String, c: Double, cutoff: Int): Unit = {
    t = 0;
    epoch = 0;
    val n: Int = dict.initFromData(tFile, dFile, cutoff)
    lambda = 1./ (c * n);
    logger.info("Using c=%.3f, i.e. lambda=%.9f".format(c, lambda))

    w = DenseVector.zeros[Double](dict.nParams())
    wscale = 1.0;
    wnorm = 0;
  }

  def rescale(): Unit = {
    if (wscale != 1.0) {
      w = w.map(v => v * wscale)
      wscale = 1;
    }
  }


  def findObjBySampling(data: ArrayList[Sentence], sample: ArrayList[Int]) = {
    var loss: Double = 0
    val n: Int = sample.size()
    for ( sIdx <- sample) {
      val scorer: Scorer = new Scorer(data.get(sIdx), dict, w, wscale);
      loss += scorer.scoreForward()
      loss -= scorer.scoreCorrect()
    }
    println("final loss: %.15f".format(loss));
    loss / n + 0.5 * wnorm * lambda
  }

  def tryEtaBySampling(data: ArrayList[Sentence], sample: ArrayList[Int], eta: Double) = {
    val savedW = DenseVector.zeros[Double](w.size)
    savedW := w
    val savedWScale = wscale
    val savedWNorm = wnorm

    for ( sIdx <- sample) {
      val scorer: Scorer = new Scorer(data.get(sIdx), dict, w, wscale, eta)
      scorer.gradCorrect(+1)
      //scorer.gradForward(-1)
      wscale *= (1 - eta * lambda)
    }
    wnorm = w.dot(w) * wscale * wscale
    val obj: Double = findObjBySampling(data, sample)
    println("wnorm: %.15f".format(wnorm))
    println("wscale: %.15f".format(wscale))
    println("obj: %.15f".format(obj))
    //System.exit(0);


    w = savedW
    wscale = savedWScale
    wnorm = savedWNorm
    obj
  }

  def adjustEta(eta: Double): Unit = {
    t = 1 / (eta * lambda);
    logger.info(" taking eta=%.3f  t0=%.3f".format(eta, t))
  }

  /**
   * In seconds
   */
  def getRunningTime() = {
    (System.currentTimeMillis() - startMillis) / 1000.0
  }

  def adjustEta(dataset: ArrayList[Sentence], samples: Int = 500, seta: Double = 1): Unit = {
    val sample = new ArrayList[Int]()
    logger.info("[Calibrating] -- %d samples".format(samples))

    // choose sample
    val rand = new Random()
    if (samples < dataset.size) {
      for ( i <- 0 to samples - 1) {
        sample.add(i)
        //sample.put(rand.nextInt(dataset.size)) //TODO: change to random
      }
    } else {
      for ( i <- 0 to dataset.size - 1)
        sample.add(i)
    }

    // initial obj
    val sobj: Double = findObjBySampling(dataset, sample);
    logger.info(" initial objective= %.15f".format(sobj))
    // empirically find eta that works best
    var besteta: Double = 1;
    var bestobj: Double = sobj;
    var eta: Double = seta;
    var totest: Int = 10;
    val factor: Double = 2;
    var phase2: Boolean = false;
    while (totest > 0 || !phase2) {
      val obj: Double = tryEtaBySampling(dataset, sample, eta);
      val okay: Boolean = (obj < sobj);
      logger.info(" trying eta=%.5f  obj=%.5f".format(eta, obj))
      if (okay) {
        logger.info("(possible)")
      } else {
        logger.info("(too large)")
      }

      if (okay) {
        totest -= 1;
        if (obj < bestobj) {
          bestobj = obj;
          besteta = eta;
        }
      }
      if (!phase2) {
        if (okay)
          eta = eta * factor;
        else {
          phase2 = true;
          eta = seta;
        }
      }
      if (phase2)
        eta = eta / factor;
    }
    // take it on the safe side (implicit regularization)
    besteta /= factor;
    // set initial t
    adjustEta(besteta);
    // message
    logger.info(" time=%.3fs.".format(getRunningTime()))
  }

  def train(dataset: ArrayList[Sentence], epochs: Int): Unit = {
    if (t <= 0) {
      logger.error("(train) Must call adjustEta() before train().")
      System.exit(10);
    }

    var shuffle = (0 to dataset.size - 1).map(v => v)

    for ( j <- 0 to epochs - 1) {
      epoch += 1;
      // shuffle examples
      shuffle = Random.shuffle(shuffle)
      logger.info("[Epoch %d] --".format(epoch))
      // perform epoch
      for ( exampleIdx <- shuffle) {
        var eta: Double = 1 / (lambda * t);
        // train
        val scorer = new Scorer(dataset.get(exampleIdx), dict, w, wscale, eta);
        scorer.gradCorrect(+1);
        scorer.gradForward(-1);
        // weight decay
        wscale *= (1 - eta * lambda);
        // iteration done
        t += 1;
      }
      // epoch done
      if (wscale < 1e-5)
        rescale()
      wnorm = w.dot(w) * wscale * wscale;
      logger.info(" wnorm=%.3f".format(wnorm))
      logger.info(" time=%.2f".format(getRunningTime()))
    }
    // this never hurts
    rescale();
  }

  def test(data: ArrayList[Sentence]): Unit = {
    if (dict.nOutputs() <= 0) {
      logger.error(" (si.zitnik.research.iobie.test): Must call load() or initialize() before si.zitnik.research.iobie.test().")
      System.exit(10)
    }
    logger.info("sentences=%d".format(data.size))
    var obj: Double = 0;
    var errors: Int = 0;
    var total: Int = 0;
    for (sentence: Sentence <- data) {
      val scorer = new Scorer(sentence, dict, w, wscale)
      obj += scorer.scoreForward() - scorer.scoreCorrect()
      errors += scorer.test()
      total += sentence.size()
    }
    obj = obj * 1.0 / data.size
    logger.info(" loss=%.3f".format(obj))
    obj += 0.5 * wnorm * lambda;
    val misrate: Double = (errors * 100.0) / (if (total != 0) {
      total
    } else {
      1
    })
    logger.info(" obj=%.3f err=%d (%.3f%%) time=%.3fs".format(obj, errors, misrate, getRunningTime()))
  }

  def getEpoch(): Int = {
    epoch
  }

  def getDict(): FeatureDict = {
    dict
  }

  def getLambda() = {
    lambda
  }

  def getEta() = {
    1 / (t * lambda)
  }

  def getW() = {
    rescale()
    w
  }

}

object SGDCRF extends StrictLogging {
  val c = 1.0;
  val eta = 0.0;
  val cutoff = 3;
  val epochs = 50;
  val cepochs = 5;
  val tag = false;
  val train = new ArrayList[Sentence]()
  val test = new ArrayList[Sentence]()

  def expmx(x: Double) = {
    //calculates approx
    //exp(-x)

    if (x < 0) {
      logger.error("Calculating exp(NEGATIVE)!")
    }

    // fast approximation of exp(-x) for x positive
    if (x < 13.0) {
      var y =
        1.0 + x *
          (0.125 + x *
            (0.0078125 + x *
              (0.00032552083 + x *
                1.0172526e-5)))
      y *= y
      y *= y
      y *= y
      1 / y
    } else {
      0.0
    }
  }

  def dLogSum(g: Double, v: DenseVector[Double], r: DenseVector[Double]): Unit = {
    if (v.size > r.size) {
      logger.error("Vector sizes at dLogSum do not match correctly!")
    }
    dLogSum(g, v, r, v.size);
  }
  /*
  TODO: is this ok transformed for scala 2.10.0???
  def dLogSum(g: Double, v: DenseVector[Double], r: DenseVector[Double]) {
    if (v.size > r.size) {
      logger.error("Vector sizes at dLogSum do not match correctly!")
    }
    dLogSum(g, v, r, v.size);
  } */

  def dLogSum(g: Double, v: DenseVector[Double], r: DenseVector[Double], n: Int): Unit = {
    var m: Double = v(0)
    for ( i <- 0 to n - 1)
      m = math.max(m, v(i));

    var z: Double = 0;
    for ( i <- 0 to n - 1) {
      val e: Double = expmx(m - v(i));
      r(i) = e;
      z += e;
    }
    for ( i <- 0 to n - 1)
      r(i) = 1.0 * g * r(i) / z;
  }

  def logSum(v: DenseVector[Double]): Double = {
    logSum(v, v.size)
  }

  def logSum(v: DenseVector[Double], n: Int): Double = {
    var m: Double = v(0);
    for ( i <- 0 to n - 1)
      m = math.max(m, v(i))
    var s: Double = 0;
    for ( i <- 0 to n - 1)
      s += expmx(m - v(i));
    m + math.log(s);
  }

  def loadSentences(filename: String, dict: FeatureDict, data: ArrayList[Sentence], columns: Int): Unit = {

    logger.info("Reading and preprocessing %s.".format(filename))

    var sentences: Int = 0
    val s = new ArrayList[String]()

    val it = Source.fromFile(filename).getLines();
    while (Util.readDataSentence(it, s, columns) != 0) {
      val ps = new Sentence()
      ps.init(dict, s, columns);
      data.add(ps);
      sentences += 1;
    }
    logger.info("  processed: %d sentences".format(sentences))
  }


  def main(args: Array[String]): Unit = {
    val templateFile = "/Users/slavkoz/Downloads/sgd-2.0/crf/template"
    val trainFile = "/Users/slavkoz/Documents/DR_Research/Datasets/CONLL2000/train.txt"
    val testFile = "/Users/slavkoz/Documents/DR_Research/Datasets/CONLL2000/si.zitnik.research.iobie.test.txt"

    val crf: SGDCRF = new SGDCRF()
    crf.initialize(templateFile, trainFile, c, cutoff);
    loadSentences(trainFile, crf.getDict(), train, 3);
    loadSentences(testFile, crf.getDict(), test, 3);
    // training
    if (eta > 0)
      crf.adjustEta(eta)
    else
      crf.adjustEta(train, 1000, 0.1)
    while (crf.getEpoch() < epochs) {
      val ce = cepochs // (crf.getEpoch() < cepochs) ? 1 : cepochs;
      crf.train(train, ce)
      logger.info("Training perf:")
      crf.test(train)
      logger.info("Testing perf:")
      crf.test(test)
    }
    logger.info("Done!  %.3f seconds.".format(crf.getRunningTime()))
  }
}