package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunctionGenerator, FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import java.util.ArrayList
import java.util.regex.Pattern

/**
 * ACE2004 specific feature function (Constituent must contain OBS (=HEAD) and EXTENT label)
 */
class HeuristicTextAppositionFeatureFunctionGenerator(
                                              head: Label.Value = Label.OBS,
                                              extent: Label.Value = Label.EXTENT,
                                              userPredicate: String = "BAPP") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()
    retVal.add(ff1)
    retVal.add(ff2)
    retVal
  }

  private def ff1 = new FeatureFunction(userPredicate + "1=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val headText = example.get(i-1, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")
        val extentText = example.get(i-1, extent).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        val appositionText = extentText.replaceFirst(Pattern.quote(headText), "")
        val currentText = example.get(i, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        if (currentText.equals(appositionText)) {
          predicate+currentText
        } else {
          null
        }
      } else {
        null
      }
    }
  }

  private def ff2 = new FeatureFunction(userPredicate + "2=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val headText = example.get(i, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")
        val extentText = example.get(i, extent).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        val appositionText = extentText.replaceFirst(Pattern.quote(headText), "")
        val currentText = example.get(i-1, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        if (currentText.equals(appositionText)) {
          predicate+currentText
        } else {
          null
        }
      } else {
        null
      }
    }
  }



}
