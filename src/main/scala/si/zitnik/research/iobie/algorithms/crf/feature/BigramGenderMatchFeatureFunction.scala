package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.thirdparty.genderdata.api.NumberAndGenderDataFactory
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/12/12
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramGenderMatchFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "BGM") extends FeatureFunction(userPredicate + "=") {
  private val nagd = NumberAndGenderDataFactory.instance()

  def score = (example: Example, i: Int) => {
    if (i > 0) {
      val val1 = nagd.getMaxCountsResult(example.get(i-1, labelType).toLowerCase)
      val val2 = nagd.getMaxCountsResult(example.get(i, labelType).toLowerCase)
      if (val1 != null && val2 != null && val1._1.equals(val2._1)) {
        predicate+val1._1
      } else {
        predicate+"UNK"
      }
    } else {
      null
    }
  }
}