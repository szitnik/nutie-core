package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 11:44 AM
 * To change this template use File | Settings | File Templates.
 */
class EnglishDefinitiveNounFeatureFunction(
                                            labelType: Label.Value = Label.OBS,
                                            userPredicate: String = "BEDN") extends FeatureFunction(userPredicate + "=") {



  def score = (example: Example, i: Int) => {
    val cons1 = example.get(i).asInstanceOf[Constituent]

    if (cons1.get(labelType).toLowerCase().startsWith("the") ||
        (cons1.startIdx > 0 && cons1.example.get(cons1.startIdx-1, labelType).toLowerCase().startsWith("the"))) {
      predicate + "D"
    } else {
      predicate + "N"
    }
  }

}
