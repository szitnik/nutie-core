package si.zitnik.research.iobie.algorithms.crf.sgd

import java.util.ArrayList

import io.Source
import java.lang.String

import com.typesafe.scalalogging.StrictLogging

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/3/12
 * Time: 10:17 PM
 * To change this template use File | Settings | File Templates.
 */

object Util extends StrictLogging {

  def readTemplateFile(filename: String, templates: ArrayList[String]): Unit = {
    for (line <- Source.fromFile(filename).getLines()) {
      if (!line.startsWith("#") && !line.isEmpty) {
        checkTemplate(line)
        templates.add(line)
      }
    }
  }

  def checkTemplate(tpl: String) = {
    //TODO: implement for security reasons
    /*
    if (tpl.charAt(0)!='U' && tpl.charAt(0)!='B')
    {
      logger.error("ERROR: Unrecognized template type (neither U nor B.) \n" +
                  "       Template was %s".format(tpl))
      System.exit(10)
    }
    while (p[0])
    {
      if (p[0]=='%' && p[1]=='x')
      {
        bool okay = false;
        char *n = const_cast<char*>(p);
        long junk;
        if (n[2]=='[') {
        junk = strtol(n+3,&n, 10);
        while (isspace(n[0]))
        n += 1;
        if (n[0] == ',') {
          junk = strtol(n+1, &n, 10);
          while (isspace(n[0]))
          n += 1;
          if (n[0] == ']')
          okay = true;
        }
      }
        if (okay)
          p = n;
        else {
          logger.error("ERROR: Syntax error in %x[.,,] expression." +
                       "       Template was %s".format(tpl))
          System.exit(10)
        }
      }
      p += 1;
    }
    */
  }


  def expandTemplate(tpl: String, s: ArrayList[String], columns: Int, pos: Int) = {
    var e: String = ""
    val rows: Int = s.size() / columns;

    val BOS = Array[String]("_B-1", "_B-2", "_B-3", "_B-4")
    val EOS = Array[String]("_B+1", "_B+2", "_B+3", "_B+4")


    var pi: Int = 0
    var ti: Int = 0
    while (pi < tpl.length) {
      if (tpl.substring(pi).startsWith("%x[")) {
        if (pi > ti)
          e += tpl.substring(ti, ti + pi - ti);
        // parse %x[A,B] assuming syntax has been verified
        val tab = tpl.substring(pi).split("\\[|,|\\]")
        var a: Int = Integer.parseInt(tab(1))
        val b: Int = Integer.parseInt(tab(2))

        pi = tpl.indexOf("]", pi)
        ti = pi + 1;
        // catenate
        a += pos;
        if (b >= 0 && b < columns)
        {
          if (a >= 0 && a < rows)
            e += s.get(a*columns+b);
        else if (a < 0)
          e += BOS(math.min(3,-a-1))
        else if (a>=rows)
          e += EOS(math.min(3,a-rows))
        }
      }
      pi += 1
    }
    if (pi > ti)
      e += tpl.substring(ti, ti+pi-ti);
    e
  }

  def readDataLine(f: String, line: ArrayList[String], expected: Int): Unit =
  {for (value <- f.split(" ")) {
    line.add(value)
  }}

  def readDataSentence(fLines: Iterator[String], s: ArrayList[String], expected: Int): Int =
  {s.clear()

    var isEmpty = false
    while (fLines.hasNext && !isEmpty) {
      val line = fLines.next()
      if (line.isEmpty) {
        isEmpty = true
      } else {
        readDataLine(line, s, expected)
      }
    }

    if (expected != 0) {
      s.size() / expected
    } else {
      0
    }}
}