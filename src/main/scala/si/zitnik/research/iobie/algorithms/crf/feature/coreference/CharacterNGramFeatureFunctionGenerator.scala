package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/24/13
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
class CharacterNGramFeatureFunctionGenerator(
                                             labelType: Label.Value = Label.OBS,
                                             ngramSize: Int = 2,
                                             numberOfNgrams: Int = 10,
                                             userPredicate: String = "BCNG") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (ngramIdx <- 0 until numberOfNgrams) {
      retVal.add(ff(ngramIdx))
    }
    retVal
  }

  private def ff(ngramIdx: Int) = new FeatureFunction(userPredicate + ngramIdx + "=") {
    def score = (example: Example, i: Int) => {
      val obs = example.get(i, labelType).asInstanceOf[String]

      if (obs.length() >= ngramIdx+ngramSize) {
        predicate + obs.substring(ngramIdx, ngramIdx+ngramSize)
      } else {
        null
      }
    }
  }

}
