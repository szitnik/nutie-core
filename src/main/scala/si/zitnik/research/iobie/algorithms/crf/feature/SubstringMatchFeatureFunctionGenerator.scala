package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunctionGenerator, FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import java.util.ArrayList

/**
 * These feature functions checks for two labelTypes if one is a substring of another (there
 * are 8 possibilities ).
 */
class SubstringMatchFeatureFunctionGenerator(
                                     labelType1: Label.Value = Label.OBS,
                                     labelType2: Label.Value = Label.OBS,
                                     userPredicate: String = "BSMatch") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()
    retVal.add(ff1)
    retVal.add(ff2)
    retVal.add(ff3)
    retVal.add(ff4)
    retVal.add(ff5)
    retVal.add(ff6)
    retVal.add(ff7)
    retVal.add(ff8)
    retVal
  }

  private def ff1 = new FeatureFunction(userPredicate + "1") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i-1, labelType1).asInstanceOf[String].toLowerCase().contains(example.get(i, labelType1).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff2 = new FeatureFunction(userPredicate + "2") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i-1, labelType2).asInstanceOf[String].toLowerCase().contains(example.get(i, labelType2).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff3 = new FeatureFunction(userPredicate + "3") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i-1, labelType1).asInstanceOf[String].toLowerCase().contains(example.get(i, labelType2).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff4 = new FeatureFunction(userPredicate + "4") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i-1, labelType2).asInstanceOf[String].toLowerCase().contains(example.get(i, labelType1).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff5 = new FeatureFunction(userPredicate + "5") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i, labelType1).asInstanceOf[String].toLowerCase().contains(example.get(i-1, labelType1).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff6 = new FeatureFunction(userPredicate + "6") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i, labelType2).asInstanceOf[String].toLowerCase().contains(example.get(i-1, labelType2).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff7 = new FeatureFunction(userPredicate + "7") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i, labelType1).asInstanceOf[String].toLowerCase().contains(example.get(i-1, labelType2).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }

  private def ff8 = new FeatureFunction(userPredicate + "8") { def score = (example: Example, i: Int) => {
    if (i > 0) {
      if (example.get(i, labelType2).asInstanceOf[String].toLowerCase().contains(example.get(i-1, labelType1).asInstanceOf[String].toLowerCase())) {
        predicate
      } else {
        null
      }
    } else {
      null
    }
  }
  }
}
