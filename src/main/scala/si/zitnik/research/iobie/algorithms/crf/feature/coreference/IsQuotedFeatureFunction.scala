package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.IOBIEConversions._

import collection.immutable.StringOps

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
class IsQuotedFeatureFunction(
                              labelType: Label.Value = Label.OBS,
                              userPredicate: String = "UQuoted") extends FeatureFunction(userPredicate) with StrictLogging {

  private val quotes = Set('"', '\'', '`', '´')

  def score = (example: Example, i: Int) => {
    val curCons = example.get(i).asInstanceOf[Constituent]

      val strMinus1 = if (curCons.startIdx > 0) curCons.example.get(curCons.startIdx-1, labelType).asInstanceOf[String] else null
      val str = curCons.get(labelType).asInstanceOf[String]
      val strPlus1 = if (curCons.endIdx<curCons.example.size()) curCons.example.get(curCons.endIdx, labelType).asInstanceOf[String] else null

      if ((quotes.contains(str.charAt(0)) || (strMinus1 != null && quotes.contains(new StringOps(strMinus1).last))) &&
      (quotes.contains(new StringOps(str).last) || (strPlus1 != null && quotes.contains(strPlus1.charAt(0))))) {
        predicate
      } else {
        null
      }
  }
}
