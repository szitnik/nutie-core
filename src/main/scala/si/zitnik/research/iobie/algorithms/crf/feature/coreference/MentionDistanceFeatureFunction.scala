package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.FeatureFunction
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
class MentionTokenDistanceFeatureFunction(
                                       userPredicate: String = "BMD") extends FeatureFunction(userPredicate + "=") with StrictLogging {

  def score = (example: Example, i: Int) => {
    if (i>0) {
      val cons1 = example.get(i-1).asInstanceOf[Constituent]
      val cons2 = example.get(i).asInstanceOf[Constituent]

      val distance = if (cons1.example == cons2.example) {
        cons2.startIdx - cons1.startIdx
      } else {
        (cons1.example.size() - cons1.startIdx - 1) +
        {
          cons1.example.examples.subList(
            cons1.example.examples.indexOf(cons1.example)+1,
            cons1.example.examples.indexOf(cons2.example)
          ).map(_.size).sum
        } +
        (cons2.startIdx + 1)
      }

      if (distance < 0) {
        logger.warn("Mention detector feature function could not detect a mention!")
      }

      predicate + distance
    } else {
      null
    }
  }

}
