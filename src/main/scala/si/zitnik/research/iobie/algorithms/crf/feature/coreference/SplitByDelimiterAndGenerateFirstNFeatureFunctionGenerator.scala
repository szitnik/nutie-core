package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/24/13
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
class SplitByDelimiterAndGenerateFirstNFeatureFunctionGenerator(
                                             labelType: Label.Value = Label.OBS,
                                             delimiter: String = " ",
                                             noOfFirstTokensToGenerate: Int = 2,
                                             userPredicate: String = "BSBDaG") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (tokenIdx <- 0 until noOfFirstTokensToGenerate) {
      retVal.add(ff(tokenIdx))
    }
    retVal
  }

  private def ff(tokenIdx: Int) = new FeatureFunction(userPredicate + tokenIdx + "=") {
    def score = (example: Example, i: Int) => {
      val delimited = example.get(i, labelType).asInstanceOf[String].split(delimiter)
      if (delimited.size > tokenIdx) {
        predicate + delimited(tokenIdx)
      } else {
        null
      }
    }
  }

}
