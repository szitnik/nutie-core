package si.zitnik.research.iobie.algorithms.crf

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/28/12
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */

object ExampleLabel extends Enumeration {
  val ID = Value("ID")
  val TYPE = Value("TYPE")
  val TEXT = Value("TEXT")
  val DOC_ID = Value("DOCUMENT_ID") //Document ID
  val PARSE_TREE = Value("PARSE_TREE") //Parse tree
  val EXAMPLE_PROB = Value("EXAMPLE_PROB")
  val METADATA = Value("METADATA")
  val PART_NO = Value("PART_NO")
}
