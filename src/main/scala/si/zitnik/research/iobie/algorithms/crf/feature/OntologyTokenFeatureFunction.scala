package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.ontology.OntologyManager

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 14/03/14
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
class OntologyTokenFeatureFunction(labelType: Label.Value,
                                   modelFile: String,
                                   queryFile: String,
                                   userPredicate: String = "UOTFF") extends FeatureFunction(userPredicate + "=") {
  val ontoManager = new OntologyManager(modelFile)

  def score = (example: Example, i: Int) => {
    try {
      val res = ontoManager.getResults(queryFile, Array(example.get(i, labelType).asInstanceOf[String]))

      if (res.size > 0) {
        predicate + res(0)
      } else {
        null
      }
    } catch {
      case e: Exception => {
        throw e
      }
    }

  }

}
