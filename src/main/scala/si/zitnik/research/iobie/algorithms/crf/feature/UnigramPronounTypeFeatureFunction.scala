package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.thirdparty.pronoun.PronounDataFactory
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/24/12
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */
class UnigramPronounTypeFeatureFunction(language: String = "en", userPredicate: String = "UPR") extends FeatureFunction(userPredicate + "=") {
  private val map = PronounDataFactory.instance(language)

  def score = (example: Example, i: Int) => {
    val key = example.get(i, Label.OBS)
    if (map.contains(key)) {
      predicate+map(key)
    } else {
      null
    }
  }
}
