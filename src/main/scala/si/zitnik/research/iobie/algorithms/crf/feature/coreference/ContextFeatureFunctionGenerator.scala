package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 *
 */
//TODO try to implement this faster
class ContextFeatureFunctionGenerator(
                                       contextLabelType: Label.Value = Label.OBS,
                                       neighbourTokensToCheck: Int = 5,
                                       ngramSize: Int = 3,
                                       userPredicate: String = "BContxt") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()
    retVal.add(ffLeftLeft)
    retVal.add(ffLeftRight)
    retVal.add(ffRightLeft)
    retVal.add(ffRightRight)
    retVal.add(ffFull)
    retVal
  }

  private def classify(set1: Set[String], set2: Set[String]): String = {
    val jaccard = set1.intersect(set2).size*1.0 / (set1.union(set2).size)
    if (jaccard == 0) {
      "0"
    } else if (jaccard < 0.25) {
      "1"
    } else if (jaccard >= 0.25 && jaccard <= 0.5) {
      "2"
    } else if (jaccard > 0.5 && jaccard <= 0.75) {
      "3"
    } else if (jaccard > 0.75 && jaccard <= 0.9) {
      "4"
    } else if (jaccard > 0.9 && jaccard <= 1.0) {
      "5"
    } else if (jaccard == 1.0) {
      "6"
    } else {
      "7"
    }
  }

  private def ffLeftLeft = new FeatureFunction(userPredicate + "LL=") {

    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val left1 = cons1.example.subLabeling(contextLabelType, math.max(0, cons1.startIdx-neighbourTokensToCheck), cons1.startIdx).mkString(" ")
          .sliding(ngramSize).toSet
        val left2 = cons2.example.subLabeling(contextLabelType, math.max(0, cons2.startIdx-neighbourTokensToCheck), cons2.startIdx).mkString(" ")
          .sliding(ngramSize).toSet

        predicate+classify(left1, left2)
      } else {
        null
      }
    }
  }

  private def ffLeftRight = new FeatureFunction(userPredicate + "LR=") {

    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val left1 = cons1.example.subLabeling(contextLabelType, math.max(0, cons1.startIdx-neighbourTokensToCheck), cons1.startIdx).mkString(" ")
          .sliding(ngramSize).toSet
        val right2 = cons2.example.subLabeling(contextLabelType, cons2.endIdx, math.min(cons2.example.size(), cons2.endIdx+neighbourTokensToCheck)).mkString(" ")
          .sliding(ngramSize).toSet

        predicate+classify(left1, right2)
      } else {
        null
      }
    }
  }

  private def ffRightLeft = new FeatureFunction(userPredicate + "RL=") {

    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val left2 = cons2.example.subLabeling(contextLabelType, math.max(0, cons2.startIdx-neighbourTokensToCheck), cons2.startIdx).mkString(" ")
          .sliding(ngramSize).toSet
        val right1 = cons1.example.subLabeling(contextLabelType, cons1.endIdx, math.min(cons1.example.size(), cons1.endIdx+neighbourTokensToCheck)).mkString(" ")
          .sliding(ngramSize).toSet

        predicate+classify(right1, left2)
      } else {
        null
      }
    }
  }

  private def ffRightRight = new FeatureFunction(userPredicate + "RR=") {

    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val right1 = cons1.example.subLabeling(contextLabelType, cons1.endIdx, math.min(cons1.example.size(), cons1.endIdx+neighbourTokensToCheck)).mkString(" ")
          .sliding(ngramSize).toSet
        val right2 = cons2.example.subLabeling(contextLabelType, cons2.endIdx, math.min(cons2.example.size(), cons2.endIdx+neighbourTokensToCheck)).mkString(" ")
          .sliding(ngramSize).toSet

        predicate+classify(right1, right2)
      } else {
        null
      }
    }
  }

  private def ffFull = new FeatureFunction(userPredicate + "F=") {

    def score = (example: Example, i: Int) => {
      if (i > 0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val left1 = cons1.example.subLabeling(contextLabelType, math.max(0, cons1.startIdx-neighbourTokensToCheck), cons1.startIdx).mkString(" ")
          .sliding(ngramSize).toSet
        val left2 = cons2.example.subLabeling(contextLabelType, math.max(0, cons2.startIdx-neighbourTokensToCheck), cons2.startIdx).mkString(" ")
          .sliding(ngramSize).toSet

        val right1 = cons1.example.subLabeling(contextLabelType, cons1.endIdx, math.min(cons1.example.size(), cons1.endIdx+neighbourTokensToCheck)).mkString(" ")
          .sliding(ngramSize).toSet
        val right2 = cons2.example.subLabeling(contextLabelType, cons2.endIdx, math.min(cons2.example.size(), cons2.endIdx+neighbourTokensToCheck)).mkString(" ")
          .sliding(ngramSize).toSet

        predicate+classify(left1.union(right1), left2.union(right2))
      } else {
        null
      }
    }
  }

}
