package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/24/13
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
class PrevNextWordsTokenFeatureFunctionGenerator( labelType: Label.Value = Label.OBS,
                                             distanceFromMention: Array[Int] = Array(2),
                                             mergeTokensToConstituent: Boolean = true,
                                             userPredicate: String = "BPN") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (distance <- distanceFromMention) {
      retVal.add(ffRight(distance, mergeTokensToConstituent))
      retVal.add(ffLeft(distance, mergeTokensToConstituent))
    }
    retVal
  }

  private def ffLeft(distance: Int, mergeTokensToConstituent: Boolean) = new FeatureFunction(userPredicate + "D"+distance + "L=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val cons1 = example.get(i-1)
        val cons2 = example.get(i)

        val idx1 = math.max(example.indexOf(cons1)-distance, 0)
        val idx2 = math.max(example.indexOf(cons2)-distance, 0)

        if (mergeTokensToConstituent) {
          val mergeTokens1 = example.subLabeling(labelType, idx1, example.indexOf(cons1)).mkString("+")
          val mergeTokens2 = example.subLabeling(labelType, idx2, example.indexOf(cons2)).mkString("+")
          predicate + mergeTokens1 + "/" + mergeTokens2
        } else {
          predicate + example.get(idx1, labelType) + "/" + example.get(idx2, labelType)
        }
      } else {
        null
      }
    }
  }

  private def ffRight(distance: Int, mergeTokensToConstituent: Boolean) = new FeatureFunction(userPredicate + "D"+distance + "R=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val cons1 = example.get(i-1)
        val cons2 = example.get(i)

        var idx1 = math.min(example.indexOf(cons1)+1+distance, example.size())
        var idx2 = math.min(example.indexOf(cons2)+1+distance, example.size())

        if (mergeTokensToConstituent && example.indexOf(cons1)+1 <= idx1 && example.indexOf(cons2)+1 <= idx2) {
          val mergeTokens1 = example.subLabeling(labelType, example.indexOf(cons1)+1, idx1).mkString("+")
          val mergeTokens2 = example.subLabeling(labelType, example.indexOf(cons2)+1, idx2).mkString("+")
          predicate + mergeTokens1 + "/" + mergeTokens2
        } else {
          idx1 = math.min(idx1, example.size()-1)
          idx2 = math.min(idx2, example.size()-1)
          predicate + example.get(idx1, labelType) + "/" + example.get(idx2, labelType)
        }
      } else {
        null
      }
    }
  }

}
