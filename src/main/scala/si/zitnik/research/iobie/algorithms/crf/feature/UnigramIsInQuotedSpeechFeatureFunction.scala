package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 4:11 PM
 * To change this template use File | Settings | File Templates.
 */
class UnigramIsInQuotedSpeechFeatureFunction(labelType: Label.Value, userPredicate: String = "UQS") extends FeatureFunction(userPredicate) {
  def score = (example: Example, i: Int) => {
    //before
    val beforeTokenEndsWithQuote = if (i>0 && (example.get(i-1, labelType).endsWith("\"") || example.get(i-1, labelType).endsWith("'"))) {true} else {false}
    val tokenStartsWith = example.get(i, labelType).startsWith("\"") || example.get(i, labelType).startsWith("'")
    //after
    val afterTokenStartsWithQuote = if (i+1<example.size() && (example.get(i+1, labelType).startsWith("\"") || example.get(i+1, labelType).startsWith("'"))) {true} else {false}
    val tokenEndsWith = example.get(i, labelType).endsWith("\"") || example.get(i, labelType).endsWith("'")

    if ( example.get(i, labelType).asInstanceOf[String].length > 1 && ((beforeTokenEndsWithQuote && afterTokenStartsWithQuote) || (tokenStartsWith && tokenEndsWith))) {
      predicate
    } else {
      null
    }
  }
}
