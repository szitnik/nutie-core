package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 10:31 AM
 * To change this template use File | Settings | File Templates.
 */
class LengthDifferenceFeatureFunction(
                                       labelType: Label.Value = Label.OBS,
                                       userPredicate: String = "BLDF") extends FeatureFunction(userPredicate + "=") {

  def score = (example: Example, i: Int) => {
    if (i > 0) {
      val str1len = example.get(i-1, Label.OBS).asInstanceOf[String].length
      val str2len = example.get(i, Label.OBS).asInstanceOf[String].length

      val diff = str2len - str1len
      val minLen = math.min(str1len, str2len)
      val maxLen = math.max(str1len, str2len)

      if (diff == 0) {
        predicate + "Same"
      } else if (math.abs(diff) == minLen) { //one of strings is empty
        predicate + "Empty"
      } else if (diff < 0) {
        if (math.abs(diff) <= 0.33*maxLen) {
          predicate + "N3"
        } else if (math.abs(diff) <= 0.67*maxLen) {
          predicate + "N6"
        } else {
          predicate + "NSim"
        }
      } else { //diff > 0
        if (diff <= 0.33*maxLen) {
          predicate + "3"
        } else if (diff <= 0.67*maxLen) {
          predicate + "6"
        } else {
          predicate + "Sim"
        }
      }


    } else {
      null
    }
  }

}
