package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.algorithms.crf.FeatureFunction
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/25/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
class TokenDistanceFeatureFunction(
                                       userPredicate: String = "BTD") extends FeatureFunction(userPredicate + "=") with StrictLogging {

  def score = (example: Example, i: Int) => {
    if (i>0) {
      val cons1 = example.get(i-1).asInstanceOf[Constituent]
      val cons2 = example.get(i).asInstanceOf[Constituent]

      val distance =
      if (cons1.example != cons2.example) {
        //beginning
        (cons1.example.size() - cons1.startIdx) +
        //middle
        (cons2.example.examples.indexOf(cons1.example) until cons2.example.examples.indexOf(cons1.example)).map(cons2.example.examples.get(_).size()).sum +
        //end
        cons2.startIdx
      } else { //same example
        cons2.startIdx-cons1.startIdx
      }

      predicate + distance
    } else {
      null
    }
  }

}
