package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * xffixLength: if positive it is prefix, otherwise it is suffix
 */
class BigramXffixMatchFeatureFunction(xffixLength: Int = 3, labelType: Label.Value = Label.OBS, userPredicate: String = "BPX") extends FeatureFunction(userPredicate+"=") {
  def score = (example: Example, i: Int) => {
    if (i > 0) {
      val val1 = xffix(example.get(i-1, labelType))
      val val2 = xffix(example.get(i, labelType))

      if (val1.toLowerCase.equals(val2.toLowerCase)) {
        predicate+"T"
      } else {
        predicate+"F"
      }
    } else {
      null
    }
  }

  private def xffix(str: String) = {
    val fixlen = math.min(math.abs(xffixLength), str.length)
    if (xffixLength > 0) {
      str.substring(0, fixlen)
    } else {
      str.substring(str.length-fixlen, str.length)
    }
  }
}
