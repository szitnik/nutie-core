package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/24/13
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
class PrevNextWordsFeatureFunctionGenerator( labelType: Label.Value = Label.OBS,
                                             distanceFromMention: Array[Int] = Array(2),
                                             mergeTokensToConstituent: Boolean = true,
                                             userPredicate: String = "BPN") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (distance <- distanceFromMention) {
      retVal.add(ffRight(distance, mergeTokensToConstituent))
      retVal.add(ffLeft(distance, mergeTokensToConstituent))
    }
    retVal
  }

  private def ffLeft(distance: Int, mergeTokensToConstituent: Boolean) = new FeatureFunction(userPredicate + "L=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        val idx1 = math.max(cons1.startIdx-distance, 0)
        val idx2 = math.max(cons2.startIdx-distance, 0)

        if (mergeTokensToConstituent) {
          val mergeTokens1 = cons1.example.subLabeling(labelType, idx1, cons1.startIdx).mkString("+")
          val mergeTokens2 = cons2.example.subLabeling(labelType, idx2, cons2.startIdx).mkString("+")
          predicate + mergeTokens1 + "/" + mergeTokens2
        } else {
          predicate + cons1.example.get(idx1, labelType) + "/" + cons2.example.get(idx2, labelType)
        }
      } else {
        null
      }
    }
  }

  private def ffRight(distance: Int, mergeTokensToConstituent: Boolean) = new FeatureFunction(userPredicate + "R=") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val cons1 = example.get(i-1).asInstanceOf[Constituent]
        val cons2 = example.get(i).asInstanceOf[Constituent]

        var idx1 = math.min(cons1.endIdx+distance, cons1.example.size())
        var idx2 = math.min(cons2.endIdx+distance, cons1.example.size())

        if (mergeTokensToConstituent && cons1.endIdx <= idx1 && cons2.endIdx <= idx2) {
          val mergeTokens1 = cons1.example.subLabeling(labelType, cons1.endIdx, idx1).mkString("+")
          val mergeTokens2 = cons2.example.subLabeling(labelType, cons2.endIdx, idx2).mkString("+")
          predicate + mergeTokens1 + "/" + mergeTokens2
        } else {
          idx1 = math.min(idx1, cons1.example.size()-1)
          idx2 = math.min(idx2, cons1.example.size()-1)
          predicate + cons1.example.get(idx1, labelType) + "/" + cons2.example.get(idx2, labelType)
        }
      } else {
        null
      }
    }
  }

}
