package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/24/13
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
class TwoLabelMatchFeatureFunction(
                                    userPredicate: String = "BTLM",
                                    l1: Label.Value = Label.POS,
                                    l2: Label.Value = Label.OBS) extends FeatureFunction(userPredicate + "=") {

  def score = (example: Example, i: Int) => {
    if (i>0) {
      val l11 = example.get(i-1, l1).asInstanceOf[String]
      val l12 = example.get(i, l1).asInstanceOf[String]

      val l21 = example.get(i-1, l2).asInstanceOf[String].toLowerCase()
      val l22 = example.get(i, l2).asInstanceOf[String].toLowerCase()

      if (l11.equals(l12) && l21.equals(l22)) {
        predicate + l11 + "T"
      } else {
        null
      }
    } else {
      null
    }
  }

}
