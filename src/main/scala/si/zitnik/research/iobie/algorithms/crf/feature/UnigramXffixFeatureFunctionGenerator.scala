package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import si.zitnik.research.iobie.domain.Example
import java.util.ArrayList
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * if (substringLen positive) => use prefix
 * else => use suffix
 */
class UnigramXffixFeatureFunctionGenerator(
                                     val labelType: Label.Value,
                                     val substringLen: Int = 3,
                                     val offset: Range = 0 to 0,
                                     userPredicate: String = "UXF") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()

    for (off <- offset) {
      val ff = score(off, userPredicate + substringLen + "+" + off + "=")
      retVal.add(new FeatureFunction(userPredicate + substringLen + "+" + off + "=") {
        def score = ff
      })
    }

    retVal
  }

  def score(offset: Int, predicate: String) = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx >= 0 &&
      idx < example.size() &&
      example.get(idx, labelType).asInstanceOf[String].length() >= math.abs(substringLen)) {

      if (substringLen > 0) {
        predicate + example.get(idx, labelType).substring(0, substringLen)
      } else {
        val s = example.get(idx, labelType).asInstanceOf[String]
        predicate + s.substring(s.length() + substringLen, s.length())
      }

    } else {
      null
    }
  }
}