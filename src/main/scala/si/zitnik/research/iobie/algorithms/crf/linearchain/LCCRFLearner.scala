package si.zitnik.research.iobie.algorithms.crf.linearchain

import si.zitnik.research.iobie.domain.Examples
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label, Learner}

import util.Random
import scala.collection.JavaConversions._
import java.util.{ArrayList, HashSet}

import breeze.linalg.SparseVector
import com.typesafe.scalalogging.StrictLogging

/**
 * Liner-chain CRF learner
 */
class LCCRFLearner(
                    val examples: Examples,
                    val learnLabelType: Label.Value,
                    val featureFunctions: ArrayList[FeatureFunction],
                    val c: Double = 1.0,
                    val featureThreshold: Int = 3) extends Learner(examples) with StrictLogging {

  init()
  private def init(): Unit = {
    //check feature function duplicates
    val predicates = new HashSet[String]()
    featureFunctions.foreach(f => {
      if (predicates.contains(f.predicate)) {
        logger.error("There exist feature functions using same predicate: %s!".format(f.predicate))
        System.exit(-1)
      } else {
        predicates.add(f.predicate)
      }
    })

    //remove zero-length examples
    var i = 0
    var removed = 0
    while (i<examples.size()) {
      if (examples(i).size() == 0) {
        examples.remove(i)
        removed += 1
      } else {
        i += 1
      }
    }
    if (removed > 0) {
      logger.warn("There were %d zero-length examples removed!".format(removed))
    }
  }


  private val dict = new FeatureDict(learnLabelType, featureFunctions, examples, featureThreshold)
  private var w = SparseVector.zeros[Double](dict.nParams())
  private var wscale: Double = 1.0
  private var lambda: Double = 1.0/ (c * examples.size)
  private var wnorm: Double = 0.0
  private var t: Double = 0.0
  logger.trace("Using c=%.3f, i.e. lambda=%.9f".format(c, lambda))


  private def rescale(): Unit = {
    if (wscale != 1.0) {
      w.activeIterator.foreach{case (i, v) => {
        w(i) *= wscale
      }}
      wscale = 1;
    }
  }


  private def findObjBySampling(data: Examples, sample: ArrayList[Int]) = {
    var loss: Double = 0
    val n: Int = sample.size()
    for ( sIdx <- sample) {
      val scorer: Scorer = new Scorer(learnLabelType, data.get(sIdx), dict, w, wscale);
      loss += scorer.scoreForward()
      loss -= scorer.scoreCorrect()
    }
    logger.trace("final loss: %.15f".format(loss));
    loss / n + 0.5 * wnorm * lambda
  }

  private def tryEtaBySampling(data: Examples, sample: ArrayList[Int], eta: Double) = {
    val savedW = SparseVector.zeros[Double](w.size)
    w.activeIterator.foreach{case (i, v) => savedW(i) = v}
    val savedWScale = wscale
    val savedWNorm = wnorm

    for ( sIdx <- sample) {
      val scorer: Scorer = new Scorer(learnLabelType, data.get(sIdx), dict, w, wscale, eta)
      scorer.gradCorrect(+1)
      //scorer.gradForward(-1)
      wscale *= (1 - eta * lambda)
    }
    wnorm = w.dot(w) * wscale * wscale
    val obj: Double = findObjBySampling(data, sample)
    logger.trace("wnorm: %.15f".format(wnorm))
    logger.trace("wscale: %.15f".format(wscale))
    logger.trace("obj: %.15f".format(obj))

    w = savedW
    wscale = savedWScale
    wnorm = savedWNorm
    obj
  }

  private def adjustEta(eta: Double): Unit = {
    t = 1 / (eta * lambda);
    logger.trace(" taking eta=%.3f  t0=%.3f".format(eta, t))
  }

  private def adjustEta(dataset: Examples, samples: Int = 500, seta: Double = 1): Unit = {
    val sample = new ArrayList[Int]()
    logger.info("[Calibrating] -- %d samples".format(samples))

    // choose sample
    val rand = new Random()
    if (samples < dataset.size) {
      for ( i <- 0 to samples - 1) {
        sample.add(rand.nextInt(dataset.size))
      }
    } else {
      for ( i <- 0 to dataset.size - 1)
        sample.add(i)
    }

    // initial obj
    val sobj: Double = findObjBySampling(dataset, sample);
    logger.info(" initial objective= %.15f".format(sobj))
    // empirically find eta that works best
    var besteta: Double = 1;
    var bestobj: Double = sobj;
    var eta: Double = seta;
    var totest: Int = 10;
    val factor: Double = 2;
    var phase2: Boolean = false;
    while (totest > 0 || !phase2) {
      val obj: Double = tryEtaBySampling(dataset, sample, eta);
      val okay: Boolean = (obj < sobj);
      logger.info(" trying eta=%.5f  obj=%.5f".format(eta, obj))
      if (okay) {
        logger.info("(possible)")
      } else {
        logger.info("(too large)")
      }

      if (okay) {
        totest -= 1;
        if (obj < bestobj) {
          bestobj = obj;
          besteta = eta;
        }
      }
      if (!phase2) {
        if (okay)
          eta = eta * factor;
        else {
          phase2 = true;
          eta = seta;
        }
      }
      if (phase2)
        eta = eta / factor;
    }
    // take it on the safe side (implicit regularization)
    besteta /= factor;
    // set initial t
    adjustEta(besteta);
  }

  def train() = {
    train(50)
  }

  def train(epochs: Int): LCCRFClassifier = {
    if (t <= 0) {
      logger.info("(train) adjusting eta.")
      adjustEta(examples, 1000, 0.1)
    }

    var shuffle = (0 to examples.size - 1).map(v => v)

    var epoch: Int = 0
    for ( j <- 0 to epochs - 1) {
      epoch += 1;
      // shuffle examples
      shuffle = Random.shuffle(shuffle)
      logger.info("[Epoch %d] --".format(epoch))
      // perform epoch
      for ( exampleIdx <- shuffle) {
        var eta: Double = 1 / (lambda * t);
        // train
        val scorer = new Scorer(learnLabelType, examples.get(exampleIdx), dict, w, wscale, eta);
        scorer.gradCorrect(+1);
        scorer.gradForward(-1);
        // weight decay
        wscale *= (1 - eta * lambda);
        // iteration done
        t += 1;
      }
      // epoch done
      if (wscale < 1e-5)
        rescale()
      wnorm = w.dot(w) * wscale * wscale;
      logger.trace(" wnorm=%.3f".format(wnorm))
    }
    // this never hurts
    rescale()

    new LCCRFClassifier(learnLabelType, dict, w, wscale, wnorm, lambda)
  }

  def trainAndTest(epochsBetweenTest: Int = 5, allEpochs: Int = 50, testExamples: Examples = examples): LCCRFClassifier = {
    adjustEta(examples, 1000, 0.1)
    var classifier: LCCRFClassifier = null

    for (epoch <- 1 to math.max(allEpochs / epochsBetweenTest, 1)) {
      classifier = train(epochsBetweenTest)
      logger.info("Training perf:")
      classifier.test(examples)

      if (testExamples != examples) {
        logger.info("Testing perf:")
        classifier.test(testExamples)
      }
    }

    classifier
  }
}
