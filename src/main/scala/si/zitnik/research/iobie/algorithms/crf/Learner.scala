package si.zitnik.research.iobie.algorithms.crf

import si.zitnik.research.iobie.domain.Examples

import com.typesafe.scalalogging.StrictLogging



/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/6/11
 * Time: 7:44 PM
 * To change this template use File | Settings | File Templates.
 */

abstract
class Learner(examples: Examples) extends StrictLogging {

  def train: Classifier
}

