package si.zitnik.research.iobie.algorithms.crf.feature.relation

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.core.relationship.test.BioNLP2013Rules
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 21/01/14
 * Time: 20:20
 * To change this template use File | Settings | File Templates.
 */
class IsBSubtilisPairFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "BIsSubP") extends FeatureFunction(userPredicate + "=") {

  //(example: Example, i: Int): Double
  def score: (Example, Int) => String = (example: Example, i: Int) => {
    if (i==0) {
      null
    } else {
      val left = BioNLP2013Rules.isBacillusSubtilisGene(example.get(i-1).asInstanceOf[Constituent])
      val right = BioNLP2013Rules.isBacillusSubtilisGene(example.get(i).asInstanceOf[Constituent])

      if (left && right) {
        predicate + "Both"
      } else if (left) {
        predicate + "Left"
      } else if (right) {
        predicate + "Right"
      } else {
        null
      }
    }
  }
}

