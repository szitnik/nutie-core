package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/9/13
 * Time: 8:21 PM
 * To change this template use File | Settings | File Templates.
 */
class ParseTreeMentionDepthFeatureFunction(userPredicate: String = "UPMD") extends FeatureFunction(userPredicate + "=") {

  def score = (example: Example, i: Int) => {
    var parseNode = example.get(i, Label.PARSE_NODE)

    var counter = 0

    while (parseNode.parent != null) {
      parseNode = parseNode.parent
      counter += 1
    }

    predicate + counter
  }

}
