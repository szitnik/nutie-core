package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, FeatureFunctionGenerator, Label}
import java.util.ArrayList
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/15/13
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
class HeuristicAppositionFeatureFunctionGenerator(
                                                   head: Label.Value = Label.OBS,
                                                   extent: Label.Value = Label.EXTENT,
                                                   userPredicate: String = "BAPP") extends FeatureFunctionGenerator {

  def generate(): ArrayList[FeatureFunction] = {
    val retVal = new ArrayList[FeatureFunction]()
    retVal.add(ff1)
    retVal.add(ff2)
    retVal
  }

  private def ff1 = new FeatureFunction(userPredicate + "1") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val headText = example.get(i-1, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")
        val extentText = example.get(i-1, extent).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        val appositionText = extentText.replaceFirst(headText, "")
        val currentText = example.get(i, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        if (currentText.equals(appositionText)) {
          predicate
        } else {
          null
        }
      } else {
        null
      }
    }
  }

  private def ff2 = new FeatureFunction(userPredicate + "2") {
    def score = (example: Example, i: Int) => {
      if (i>0) {
        val headText = example.get(i, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")
        val extentText = example.get(i, extent).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        val appositionText = extentText.replaceFirst(headText, "")
        val currentText = example.get(i-1, head).asInstanceOf[String].toLowerCase().replaceAll(" ", "").replaceAll(",", "")

        if (currentText.equals(appositionText)) {
          predicate
        } else {
          null
        }
      } else {
        null
      }
    }
  }



}
