package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/11/12
 * Time: 9:07 PM
 * To change this template use File | Settings | File Templates.
 */
class StartsUpperTwiceFeatureFunction(val offset: Int = 0, userPredicate: String = "USUT") extends FeatureFunction(userPredicate + offset) {
  def score = (example: Example, i: Int) => {
    val idx = i + offset
    if (idx >= 0 &&
      idx + 1 < example.get(i, Label.OBS).asInstanceOf[String].length() &&
      example.get(i, Label.OBS).charAt(idx).isUpper &&
      example.get(i, Label.OBS).charAt(idx + 1).isUpper) {
      predicate
    } else {
      null
    }
  }
}