package si.zitnik.research.iobie.algorithms.crf

import si.zitnik.research.iobie.domain.Example

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/7/11
 * Time: 9:48 AM
 * To change this template use File | Settings | File Templates.
 */

abstract class FeatureFunction(val predicate: String) extends Serializable {

  //(example: Example, i: Int): Double
  def score: (Example, Int) => String
}