package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.thirdparty.genderdata.api.NumberAndGenderDataFactory
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/12/12
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
class GenderFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "UGen") extends FeatureFunction(userPredicate + "=") {
  private val nagd = NumberAndGenderDataFactory.instance()

  def score = (example: Example, i: Int) => {
    val result = nagd.getMaxCountsResult(example.get(i, labelType).toLowerCase)
    if (result != null) {
      predicate + result._1.toString
    } else {
      null
    }
  }
}