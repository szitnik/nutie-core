package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import scala.collection.immutable._
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Two tokens are asumed appositive if:
 *  - they are of NP, NN POS tag or other noun-related tag
 *  - previous token is followed by comma (ends with or next in real example starts with)
 */
class BigramAppositiveFeatureFunction(labelType: Label.Value = Label.OBS, possiblePOSTags: Set[String] = Set("NN", "NP"), userPredicate: String = "BA") extends FeatureFunction(userPredicate) {
  def score = (example: Example, i: Int) => {
    var retVal: String = null
    if (i > 0) {
      //they are NN, NP
      if (//possiblePOSTags.contains(example.get(i-1, Label.POS)) && - not useful, especially if previous token is a comma
        possiblePOSTags.contains(example.get(i, Label.POS))) {
        //previous token followed by comma
        val previousConstituent: Constituent = example.get(i-1)
        if (previousConstituent.example.get(previousConstituent.endIdx-1, Label.OBS).endsWith(",") ||
          (previousConstituent.example.size() > previousConstituent.endIdx && previousConstituent.example.get(previousConstituent.endIdx, Label.OBS).startsWith(","))) {
          retVal = predicate
        }
      }
    }
    retVal
  }
}