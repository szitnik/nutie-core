package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import si.zitnik.research.iobie.algorithms.crf.FeatureFunction
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.IOBIEConversions._
/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 3:11 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramIsInTheSameSentenceFeatureFunction(userPredicate: String = "BIssS") extends FeatureFunction(userPredicate+"=") {

  def score = (example: Example, i: Int) => {
    if (i > 0) {
      var sentence1: Example = example.get(i-1).example
      val sentence2: Example = example.get(i).example
      if (sentence1 == sentence2) {
        predicate+"T"
      } else {
        predicate+"F"
      }
    } else {
      null
    }
  }
}