package si.zitnik.research.iobie.algorithms.crf.feature.iterative

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunctionGenerator, Label, FeatureFunction}
import si.zitnik.research.iobie.domain.{Examples, Example}
import java.util
import si.zitnik.research.iobie.algorithms.crf.feature.packages.JointIterativeFeatureFunctionPackage
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 07/04/14
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
class RelationAndOtherConstituentLabelPairFeatureFunctionGenerator(
                                                           examples: Examples,
                                                           otherConsLabelType: Label.Value = Label.OBS,
                                                           userPredicate: String = "BRaOCL") extends FeatureFunctionGenerator {

  def generate(): util.ArrayList[FeatureFunction] = {
    val retVal = new util.ArrayList[FeatureFunction]()

    val pairs = examples.map(_.getAllRelationships()).flatten.map(r => Array(r.subj.get(otherConsLabelType)+"/"+r.relationshipName, r.obj.get(otherConsLabelType)+"/"+r.relationshipName)).flatten.toSet
    for (pair <- pairs) {
      retVal.add(ff(pair))
    }

    retVal
  }

  private def ff(pair: String) = new FeatureFunction(userPredicate + "=" + pair) {
    def score = (example: Example, i: Int) => {
      IterativeFFUtil.getConstituent(example, i) match {
        case Some(cons) => {
          JointIterativeFeatureFunctionPackage.mentionToRelations.map.get(cons) match {
            case Some(rels) => {

              val contains = rels.map(r => {
                val tCons = if (cons == r.subj) r.subj else r.obj
                tCons.get(otherConsLabelType)+"/"+r.relationshipName
              }).toSet.contains(pair)

              if (contains) {
                predicate
              } else {
                null
              }
            }
            case None => {
              null
            }
          }
        }

        case None => {
          null
        }
      }
    }
  }



}
