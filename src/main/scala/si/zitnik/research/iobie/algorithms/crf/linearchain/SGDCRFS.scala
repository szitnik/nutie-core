package si.zitnik.research.iobie.algorithms.crf.linearchain


import util.Random
import java.util.ArrayList

import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.datasets.conll2000.CoNLL2000Importer
import breeze.linalg.SparseVector
import com.typesafe.scalalogging.StrictLogging

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/2/12
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */

class SGDCRFS(
               val learnLabelType: Label.Value,
               val featureFunctions: ArrayList[FeatureFunction],
               val learnData: Examples,
               val c: Double,
               val featureThreshold: Int = 3) extends StrictLogging {

  val dict = new FeatureDict(learnLabelType, featureFunctions, learnData, featureThreshold)
  var w = SparseVector.zeros[Double](dict.nParams())
  var wscale: Double = 1.0
  var lambda: Double = 1.0/ (c * learnData.size)
  var wnorm: Double = 0.0
  var t: Double = 0.0

  val startMillis: Double = System.currentTimeMillis()
  var epoch: Int = 0

  logger.info("Using c=%.3f, i.e. lambda=%.9f".format(c, lambda))


  def rescale(): Unit = {
    if (wscale != 1.0) {
      w.activeIterator.foreach{case (i, v) => {
        w(i) *= wscale
      }}
      wscale = 1;
    }
  }

  /**
   * To be called on si.zitnik.research.iobie.test set before inference/training
   * @param examples
   */
  def initDataset(examples: Examples): Unit = {
    dict.initDataset(examples)
  }


  def findObjBySampling(data: Examples, sample: ArrayList[Int]) = {
    var loss: Double = 0
    val n: Int = sample.size()
    for ( sIdx <- sample) {
      val scorer: Scorer = new Scorer(learnLabelType, data.get(sIdx), dict, w, wscale);
      loss += scorer.scoreForward()
      loss -= scorer.scoreCorrect()
    }
    println("final loss: %.15f".format(loss));
    loss / n + 0.5 * wnorm * lambda
  }

  def tryEtaBySampling(data: Examples, sample: ArrayList[Int], eta: Double) = {
    val savedW = SparseVector.zeros[Double](w.size)
    w.activeIterator.foreach{case (i, v) => savedW(i) = v}
    val savedWScale = wscale
    val savedWNorm = wnorm

    for ( sIdx <- sample) {
      val scorer: Scorer = new Scorer(learnLabelType, data.get(sIdx), dict, w, wscale, eta)
      scorer.gradCorrect(+1)
      //scorer.gradForward(-1)
      wscale *= (1 - eta * lambda)
    }
    wnorm = w.dot(w) * wscale * wscale
    val obj: Double = findObjBySampling(data, sample)
    println("wnorm: %.15f".format(wnorm))
    println("wscale: %.15f".format(wscale))
    println("obj: %.15f".format(obj))
    //System.exit(0);


    w = savedW
    wscale = savedWScale
    wnorm = savedWNorm
    obj
  }

  def adjustEta(eta: Double): Unit = {
    t = 1 / (eta * lambda);
    logger.info(" taking eta=%.3f  t0=%.3f".format(eta, t))
  }

  /**
   * In seconds
   */
  def getRunningTime() = {
    (System.currentTimeMillis() - startMillis) / 1000.0
  }

  def adjustEta(dataset: Examples, samples: Int = 500, seta: Double = 1): Unit = {
    val sample = new ArrayList[Int]()
    logger.info("[Calibrating] -- %d samples".format(samples))

    // choose sample
    val rand = new Random()
    if (samples < dataset.size) {
      for ( i <- 0 to samples - 1) {
        sample.add(rand.nextInt(dataset.size))
      }
    } else {
      for ( i <- 0 to dataset.size - 1)
        sample.add(i)
    }

    // initial obj
    val sobj: Double = findObjBySampling(dataset, sample);
    logger.info(" initial objective= %.15f".format(sobj))
    // empirically find eta that works best
    var besteta: Double = 1;
    var bestobj: Double = sobj;
    var eta: Double = seta;
    var totest: Int = 10;
    val factor: Double = 2;
    var phase2: Boolean = false;
    while (totest > 0 || !phase2) {
      val obj: Double = tryEtaBySampling(dataset, sample, eta);
      val okay: Boolean = (obj < sobj);
      logger.info(" trying eta=%.5f  obj=%.5f".format(eta, obj))
      if (okay) {
        logger.info("(possible)")
      } else {
        logger.info("(too large)")
      }

      if (okay) {
        totest -= 1;
        if (obj < bestobj) {
          bestobj = obj;
          besteta = eta;
        }
      }
      if (!phase2) {
        if (okay)
          eta = eta * factor;
        else {
          phase2 = true;
          eta = seta;
        }
      }
      if (phase2)
        eta = eta / factor;
    }
    // take it on the safe side (implicit regularization)
    besteta /= factor;
    // set initial t
    adjustEta(besteta);
    // message
    logger.info(" time=%.3fs.".format(getRunningTime()))
  }

  def train(dataset: Examples, epochs: Int): Unit = {
    if (t <= 0) {
      logger.error("(train) Must call adjustEta() before train().")
      System.exit(10);
    }

    var shuffle = (0 to dataset.size - 1).map(v => v)

    for ( j <- 0 to epochs - 1) {
      epoch += 1;
      // shuffle examples
      shuffle = Random.shuffle(shuffle)
      logger.info("[Epoch %d] --".format(epoch))
      // perform epoch
      for ( exampleIdx <- shuffle) {
        var eta: Double = 1 / (lambda * t);
        // train
        val scorer = new Scorer(learnLabelType, dataset.get(exampleIdx), dict, w, wscale, eta);
        scorer.gradCorrect(+1);
        scorer.gradForward(-1);
        // weight decay
        wscale *= (1 - eta * lambda);
        // iteration done
        t += 1;
      }
      // epoch done
      if (wscale < 1e-5)
        rescale()
      wnorm = w.dot(w) * wscale * wscale;
      logger.info(" wnorm=%.3f".format(wnorm))
      logger.info(" time=%.2f".format(getRunningTime()))
    }
    // this never hurts
    rescale();
  }

  def test(data: Examples): Unit = {
    initDataset(data)
    if (dict.nLabels() <= 0) {
      logger.error(" (si.zitnik.research.iobie.test): Must call load() or initialize() before si.zitnik.research.iobie.test().")
      System.exit(10)
    }
    logger.info("sentences=%d".format(data.size))
    var obj: Double = 0;
    var errors: Int = 0;
    var total: Int = 0;
    for (sentence: Example <- data) {
      val scorer = new Scorer(learnLabelType, sentence, dict, w, wscale)
      obj += scorer.scoreForward() - scorer.scoreCorrect()
      errors += scorer.test()
      total += sentence.size()
    }
    obj = obj * 1.0 / data.size
    logger.info(" loss=%.3f".format(obj))
    obj += 0.5 * wnorm * lambda;
    val misrate: Double = (errors * 100.0) / (if (total != 0) {
      total
    } else {
      1
    })
    logger.info(" obj=%.3f err=%d (%.3f%%) time=%.3fs".format(obj, errors, misrate, getRunningTime()))
  }

  def getEpoch(): Int = {
    epoch
  }

  def getDict(): FeatureDict = {
    dict
  }

  def getLambda() = {
    lambda
  }

  def getEta() = {
    1 / (t * lambda)
  }

  def getW() = {
    rescale()
    w
  }

}

object SGDCRFS extends StrictLogging {
  val c = 1.0;
  val eta = 0.0;
  val cutoff = 3;
  val epochs = 50;
  val cepochs = 5;
  val tag = false;

  def main(args: Array[String]): Unit = {
    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2000/"
    val testData = new CoNLL2000Importer(path + "si.zitnik.research.iobie.test.txt") importForIE
    val learnData = new CoNLL2000Importer(path + "train.txt") importForIE
    val learnLabelType = Label.CHUNK


    /*
    val path = "/Users/slavkoz/Documents/DR_Research/Datasets/CoNLL2003/eng.build.si.zitnik.research.iobie.datasets/"
    val testData = new CoNLL2003Importer(path + "eng.testa") importForIE
    //val examples2 = new CoNLL2003Importer(path + "eng.testb") importForIE
    val learnData = new CoNLL2003Importer(path + "eng.train") importForIE
    val learnLabelType = Label.NE
    */

    val featureFunctions = new ArrayList[FeatureFunction]()
    //featureFunctions.add( new BigramDistributionFeatureFunction() )
    //featureFunctions.put( new OnlineGazeteerFunction(learnData, learnLabelType) )
    //featureFunctions.addAll( new OnlineGazeteerFeatureFunctionGenerator(learnData, cutoff, learnLabelType).generate() )

    val crf: SGDCRFS = new SGDCRFS(learnLabelType, featureFunctions, learnData, c, cutoff)
    // training
    if (eta > 0)
      crf.adjustEta(eta);
    else
      crf.adjustEta(learnData, 1000, 0.1)
    while (crf.getEpoch() < epochs) {
      val ce = cepochs; // (crf.getEpoch() < cepochs) ? 1 : cepochs;
      crf.train(learnData, ce)
      logger.info("Training perf:")
      crf.test(learnData)
      logger.info("Testing perf:")
      crf.test(testData)
    }
    logger.info("Done!  %.3f seconds.".format(crf.getRunningTime()))
  }
}