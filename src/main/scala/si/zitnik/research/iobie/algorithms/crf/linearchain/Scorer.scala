package si.zitnik.research.iobie.algorithms.crf.linearchain

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/2/12
 * Time: 11:30 PM
 * To change this template use File | Settings | File Templates.
 */


import scala.collection.JavaConversions._
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.Label
import java.util.ArrayList

import breeze.linalg.{DenseMatrix, DenseVector, SparseVector}
import com.typesafe.scalalogging.StrictLogging


class Scorer(
              val learnLabelType: Label.Value,
              val s: Example,
              val d: FeatureDict,
              val w: SparseVector[Double],
              val wscale: Double,
              val eta: Double = 0) extends StrictLogging {


  var scoresOk: Boolean = false
  var uScores = new ArrayList[DenseVector[Double]]()
  var bScores = new ArrayList[DenseMatrix[Double]]()

  def computeScores(): Unit = {
    if (!scoresOk) {
      val nout: Int = d.nLabels()
      val npos: Int = s.size


      // compute u/b-Scores
      uScores = new ArrayList()
      for ( pos <- 0 to npos - 1) {
        val u = DenseVector.zeros[Double](nout)
        uScores.add(u)

        val x: SparseVector[Double] = s.ufeatures(pos)
        x.activeIterator.foreach{case (fi, fv) => {
          //do unigram stuff
          for (idx <- 0 until d.nLabels()) {
            u(idx) += w(fi + idx) * fv
          }
        }}
        //scale unigram
        u := u * wscale
      }

      bScores = new ArrayList()
      for ( pos <- 0 to npos - 2) {
        val b = DenseMatrix.zeros[Double](nout, nout)
        bScores.add(b)


        val x: SparseVector[Double] = s.bfeatures(pos)
        x.activeIterator.foreach{case (fi, fv) => {
          //do bigram stuff
          for (i <- 0 until d.nLabels()) {
            val off = fi + d.nLabels() * i
            for (j <- 0 until d.nLabels()) {
              b(j, i) += w(off + j) * fv
            }
          }
        }}
        //scale bigram
        b := b * wscale
      }
    }
    scoresOk = true;
  }

  def uGradients(g: DenseVector[Double], position: Int, yi: Int): Unit = {
    val off = yi
    val x: SparseVector[Double] = s.ufeatures(position)
    val gain: Double = eta / wscale

    x.activeIterator.foreach{case (i, v) => {
      for (idx <- 0 until g.size) {
        w(i + off + idx) += g(idx) * (v * gain)
      }
    }}
  }

  def bGradients(g: DenseVector[Double], pos: Int, yiMinus1: Int, yi: Int): Unit = {
    val off = yi * d.nLabels() + yiMinus1
    val x: SparseVector[Double] = s.bfeatures(pos)
    val gain: Double = eta / wscale

    x.activeIterator.foreach{case (i, v) => {
      for (idx <- 0 until g.size) {
        w(i + off + idx) += g(idx) * (v * gain)
      }
    }}
  }

  def viterbi(path: ArrayList[Int]): Double = {
    computeScores();
    val npos: Int = s.size();
    val nout: Int = d.nLabels();

    // allocate backpointer array
    val pointers = new Array[Array[Int]](npos)
    for (i <- 0 to npos - 1)
      pointers(i) = new Array[Int](nout)

    // process scores
    var scores: DenseVector[Double] = uScores.get(0);
    for (pos <- 1 to npos - 1) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores.get(pos)

      for (i <- 0 to nout - 1) {
        val bs = DenseVector.zeros[Double](nout)
        bs := bScores.get(pos - 1)(::, i)
        bs += scores

        var bestj: Int = 0
        var bests: Double = bs(0)
        for (j <- 1 to nout - 1)
          if (bs(j) > bests) {
            bests = bs(j)
            bestj = j
          }
        pointers(pos)(i) = bestj
        us(i) += bests
      }
      scores = us
    }
    // find best final score
    var bestj: Int = 0
    var bests: Double = scores(0)
    for (j <- 1 to nout - 1)
      if (scores(j) > bests) {
        bests = scores(j)
        bestj = j
      }
    // backtrack
    path.clear()
    for (i <- 1 to npos) {
      path.add(0)
    }
    for (pos <- (0 to npos - 1).reverse) {
      path.set(pos, bestj)
      bestj = pointers(pos)(bestj)
    }
    return bests;

  }

  def test(): Int = {
    val path = new ArrayList[Int]()
    val npos = s.size()
    var errors: Int = 0
    viterbi(path);
    for (pos <- 0 to npos - 1)
      if (path.get(pos) != d.labelToIdx(s.get(pos, learnLabelType).asInstanceOf[String]))
        errors += 1;
    return errors;
  }

  def scoreCorrect(): Double = {
    computeScores();
    val npos: Int = s.size();
    var y: Int = d.labelToIdx(s.get(0, learnLabelType).asInstanceOf[String])
    var sum: Double = uScores.get(0)(y)

    for (pos <- 1 to npos - 1) {
      val fy = y
      y = d.labelToIdx(s.get(pos, learnLabelType).asInstanceOf[String])
      if (y >= 0 && fy >= 0)
        sum += bScores.get(pos - 1)(fy, y)
      if (y >= 0)
        sum += uScores.get(pos)(y)
    }
    return sum;
  }

  def gradCorrect(g: Double): Double = {
    computeScores()
    val npos: Int = s.size()
    var y: Int = d.labelToIdx(s.get(0, learnLabelType).asInstanceOf[String])
    val vf = DenseVector[Double](g);

    uGradients(vf, 0, y);
    var sum: Double = uScores.get(0)(y)

    for (pos <- 1 to npos - 1) {
      val fy: Int = y
      y = d.labelToIdx(s.get(pos, learnLabelType).asInstanceOf[String])
      if (y >= 0 && fy >= 0)
        sum += bScores.get(pos - 1)(fy, y)
      if (y >= 0)
        sum += uScores.get(pos)(y)
      if (y >= 0 && fy >= 0)
        bGradients(vf, pos - 1, fy, y)
      if (y >= 0)
        uGradients(vf, pos, y)
    }
    sum
  }

  def scoreForward(): Double = {
    computeScores()
    val npos: Int = s.size()
    val nout: Int = d.nLabels()

    var scores: DenseVector[Double] = uScores.get(0)
    for (pos <- 1 to npos - 1) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores.get(pos)

      for (i <- 0 to nout - 1) {
        var bs = DenseVector.zeros[Double](nout)
        bs := bScores.get(pos - 1)(::, i)
        bs += scores
        us(i) += Util.logSum(bs)
      }
      scores = us;

    }
    Util.logSum(scores)
  }

  def gradForward(g: Double): Double = {
    computeScores()
    val npos: Int = s.size()
    val nout: Int = d.nLabels()

    val alpha = DenseMatrix.zeros[Double](nout, npos)
    val beta = DenseMatrix.zeros[Double](nout, npos)


    // forward
    alpha(::, 0) := uScores(0);
    for ( pos <- 1 to npos - 1) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores(pos);
      for ( i <- 0 to nout - 1) {
        val bs = DenseVector.zeros[Double](nout)
        bs := bScores(pos - 1)(::, i)
        bs += alpha(::, pos - 1)
        us(i) += Util.logSum(bs);
      }
      alpha(::, pos) := us;
    }


    // backward
    beta(::, npos - 1) := uScores(npos - 1)
    for ( pos <- (0 to npos - 2).reverse) {
      val us = DenseVector.zeros[Double](nout)
      us := uScores(pos)
      for ( i <- 0 to nout - 1) {
        val bs = DenseVector.zeros[Double](nout)
        val bsc = bScores(pos)
        for ( j <- 0 to nout - 1)
          bs(j) = bsc(i, j)
        bs += beta(::, pos + 1)
        us(i) += Util.logSum(bs)
      }
      beta(::, pos) := us
    }
    // score
    val score = Util.logSum(beta(::, 0))

    // collect gradients
    for ( pos <- 0 to npos - 1) {
      val b = beta(::, pos)
      if (pos > 0) {
        val a = alpha(::, pos - 1)
        val bsc = bScores(pos - 1)
        for ( j <- 0 to nout - 1) {
          val bs = bsc(::, j)
          val bgrad = DenseVector.zeros[Double](nout)
          for ( i <- 0 to nout - 1)
            bgrad(i) = g * Util.expmx(math.max(0.0, score - bs(i) - a(i) - b(j)))
          bGradients(bgrad, pos - 1, 0, j)
        }
      }
      val a = alpha(::, pos)
      val us = uScores(pos)
      val ugrad = DenseVector.zeros[Double](nout)
      for ( i <- 0 to nout - 1)
        ugrad(i) = g * Util.expmx(math.max(0.0, score + us(i) - a(i) - b(i)))

      uGradients(ugrad, pos, 0)
    }
    score
  }
}