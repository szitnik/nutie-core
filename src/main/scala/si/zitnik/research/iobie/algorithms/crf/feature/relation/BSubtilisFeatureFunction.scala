package si.zitnik.research.iobie.algorithms.crf.feature.relation

import scala.io.Source
import scala.collection.mutable
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 17/01/14
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 */
class BSubtilisFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "BSub") extends FeatureFunction(userPredicate + "=") {

  def loadGeneTagsToNames() = {
    val retVal = new mutable.HashMap[String, String]()
    for (line <- Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("specific/Bacillus-subtilis.genes")).getLines()) {
      val geneName = line.split(" ")(1).replaceAll("\\[gene=", "").replaceAll("\\]", "")
      val geneTag = line.split(" ")(2).replaceAll("\\[locus_tag=", "").replaceAll("\\]", "")
      retVal.put(geneTag, geneName)
    }
    retVal
  }

  def loadBacillusSubtilisPPI() = {
    val ppiMap = new mutable.HashMap[(String, String), Int]()
    val tagToName = loadGeneTagsToNames()

    for (line <- Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("specific/b_subtilis-ppi.txt")).getLines()) {
      val splitLine = line.split(" ")

      val name1 = tagToName.get(splitLine(0).split("\\.")(1)) match {
        case Some(x) => x
        case None => null
      }
      val name2 = tagToName.get(splitLine(1).split("\\.")(1)) match {
        case Some(x) => x
        case None => null
      }
      val prob = splitLine(2).toInt

      if (name1 != null && name2 != null) {
        ppiMap.put((name1, name2), prob)
        ppiMap.put((name2, name1), prob)
      }

    }

    ppiMap
  }

  val bsPPIMap = loadBacillusSubtilisPPI()

  //(example: Example, i: Int): Double
  def score = (example: Example, i: Int) => {
    if (i > 0) {
      val prev = example.get(i-1, labelType).toString
      val cur = example.get(i, labelType).toString
      bsPPIMap.get((cur, prev)) match {
        case Some(x) => {
          if (x < 300) {
            predicate + "VLow"
          } else if (x < 500) {
            predicate + "Low"
          } else if (x < 700) {
            predicate + "Med"
          } else if (x < 850) {
            predicate + "High"
          } else {
            predicate + "Exc"
          }
        }
        case None => { null }
      }
    } else {
      null
    }
  }
}
