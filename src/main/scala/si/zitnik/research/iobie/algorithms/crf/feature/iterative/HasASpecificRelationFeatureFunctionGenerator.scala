package si.zitnik.research.iobie.algorithms.crf.feature.iterative

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label, FeatureFunctionGenerator}
import java.util
import si.zitnik.research.iobie.domain.{Example, Examples}
import si.zitnik.research.iobie.algorithms.crf.feature.packages.JointIterativeFeatureFunctionPackage
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 07/04/14
 * Time: 13:26
 * To change this template use File | Settings | File Templates.
 */
class HasASpecificRelationFeatureFunctionGenerator(examples: Examples, userPredicate: String = "BHACR") extends FeatureFunctionGenerator {

  def generate(): util.ArrayList[FeatureFunction] = {
    val retVal = new util.ArrayList[FeatureFunction]()

    val relNames = examples.getAllRelationshipLabelValues(Label.REL)
    for (relName <- relNames) {
      retVal.add(ff(relName))
    }

    retVal
  }

  private def ff(relName: String) = new FeatureFunction(userPredicate + "=" + relName) {
    def score = (example: Example, i: Int) => {
      IterativeFFUtil.getConstituent(example, i) match {
        case Some(cons) => {
          JointIterativeFeatureFunctionPackage.mentionToRelations.map.get(cons) match {
            case Some(rels) => {
              if (rels.map(_.get(Label.REL).asInstanceOf[String]).toSet.contains(relName)) {
                predicate
              } else {
                null
              }
            }
            case None => {
              null
            }
          }
        }

        case None => {
          null
        }
      }
  }
  }

}
