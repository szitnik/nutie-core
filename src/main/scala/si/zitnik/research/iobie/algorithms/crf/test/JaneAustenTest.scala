package si.zitnik.research.iobie.algorithms.crf.test

import si.zitnik.research.iobie.datasets.tab.TabImporter
import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}

import java.util.ArrayList

import com.typesafe.scalalogging.StrictLogging

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/26/11
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */

object JaneAustenTest extends StrictLogging {

  def main(args: Array[String]): Unit = {
    val rootPath = "/Users/slavkoz/Documents/EclipseWorkspaces/ResearchWorkspace/StanfordNER/domain/"
    val examplesTrain = new TabImporter(Array[Label.Value](Label.NE), rootPath + "jane-austen-emma-ch1.tsv").
      importForIE()
    val examplesTest = new TabImporter(Array[Label.Value](Label.NE), rootPath + "jane-austen-emma-ch2.tsv").
      importForIE()

    val genFeatures = new ArrayList[FeatureFunction]()
    //genFeatures.addAll( new CurrLabelAndUpperStartFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new ClassFeatureFactory(examplesTrain, Label.NE).generateFeatures() )
    //genFeatures.addAll( new WordFeatureFactory(examplesTrain, Label.NE).generateFeatures() )

    logger.info("FeatureFunction functions size: %d.".format(genFeatures.size))

    val features = genFeatures.toArray(Array[FeatureFunction]())

    //val classifier = new Learner(Label.NE, examplesTrain, features).train()

    //Statistics.printStandardClassification(classifier, examples, Label.NE, "PERS")

  }

}