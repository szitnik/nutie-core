package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.algorithms.crf.{FeatureFunction, Label}
import si.zitnik.research.iobie.domain.Example
import scala.collection.immutable._
import si.zitnik.research.iobie.domain.IOBIEConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 9/12/12
 * Time: 1:12 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramAliasFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "BAL") extends FeatureFunction(userPredicate + "=") {
  val splitRegex = "\\.|,| |/|-"

  def initials(word: String) = word.split(splitRegex).filter(_.length > 0).map(_.charAt(0)).mkString("")

  def score = (example: Example, i: Int) => score1(example, i)

  private def score1(example: Example, i: Int): String = {
    if (i > 0) {
      val word1 = example.get(i-1, Label.OBS).toLowerCase()
      val word2 = example.get(i  , Label.OBS).toLowerCase()

      //word or initials match
      val initials1 = initials(word1)
      val initials2 = initials(word2)
      if (word1.equals(word2) || initials1.equals(initials2) || initials1.equals(word2) || word1.equals(initials2)) {
        return predicate + "F"
      }

      //partly matching
      val words1: Set[String] = word1.split(splitRegex).toSet
      val words2 = word2.split(splitRegex).toSet
      val min = math.min(words1.size, words2.size)

      //one set is the real subset of other
      if (words1.intersect(words2).size >= min) {
        return predicate + "F"
      }

      //if 50% or more words of the smallest words match, it is part match
      if (words1.intersect(words2).size >= min*0.5) {
        return predicate + "P"
      }
      return predicate + "N"
    }
    null
  }


}
