package si.zitnik.research.iobie.algorithms.crf.sgd

import java.util.ArrayList
import scala.collection.JavaConversions._
import breeze.linalg.SparseVector

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/3/12
 * Time: 10:54 AM
 * To change this template use File | Settings | File Templates.
 */

class Sentence {
  var refcount: Int = 0
  var columns: Int = 0
  var data: ArrayList[String] = new ArrayList[String]()
  var yLabels: ArrayList[Int] = new ArrayList[Int]()
  val uFeatures: ArrayList[SparseVector[Double]] = new ArrayList[SparseVector[Double]]()
  val bFeatures: ArrayList[SparseVector[Double]] = new ArrayList[SparseVector[Double]]()

  def init(dict: FeatureDict, s: ArrayList[String], columns: Int): Unit = {
    val maxcol: Int = columns - 1
    val maxpos: Int = s.size() / columns - 1
    val ntemplat: Int = dict.nTemplates()
    uFeatures.clear()
    bFeatures.clear()
    yLabels.clear()
    this.columns = columns
    // intern strings to save memory
    for (str: String <- s) {
      data.add(dict.internString(str))
    }
    // expand features
    for (pos <- 0 to maxpos) {
      // labels
      val y: String = s.get(pos * columns + maxcol)
      val yindex: Int = dict.output(y)
      yLabels.add(yindex)
      // features
      val u = SparseVector.zeros[Double](dict.nParams())
      val b = SparseVector.zeros[Double](dict.nParams())

      for (t <- 0 to ntemplat - 1) {
        val tpl: String = dict.templateString(t);
        //println(Util.expandTemplate(tpl, s, columns, position))
        val findex: Int = dict.feature(Util.expandTemplate(tpl, s, columns, pos));
        if (findex >= 0) {
          if (tpl.charAt(0) == 'U')
            u(findex) = 1
          else if (tpl.charAt(0) == 'B')
            b(findex) = 1
        }
      }
      uFeatures.add(u);
      if (pos < maxpos)
        bFeatures.add(b);
    }
  }

  def size(): Int = {
    uFeatures.size()
  }

  def u(i: Int) = {
    uFeatures.get(i)
  }

  def b(i: Int) = {
    bFeatures.get(i)
  }

  def y(i: Int) = {
    yLabels.get(i);
  }


  def data(pos: Int, col: Int): String = {
    if (pos >= 0 && pos < size())
      if (col>=0 && col < columns)
        data.get(pos * columns + col)
    ""
  }

}