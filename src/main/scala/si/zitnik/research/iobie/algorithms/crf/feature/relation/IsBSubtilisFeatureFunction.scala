package si.zitnik.research.iobie.algorithms.crf.feature.relation

import si.zitnik.research.iobie.algorithms.crf.{Label, FeatureFunction}
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.core.relationship.test.BioNLP2013Rules
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 21/01/14
 * Time: 20:15
 * To change this template use File | Settings | File Templates.
 */
class IsBSubtilisFeatureFunction(labelType: Label.Value = Label.OBS, userPredicate: String = "BIsSub") extends FeatureFunction(userPredicate + "=") {

  //(example: Example, i: Int): Double
  def score = (example: Example, i: Int) => {
    if (BioNLP2013Rules.isBacillusSubtilisGene(example.get(i).asInstanceOf[Constituent])) {
      predicate + "T"
    } else {
      null
    }
  }
}
