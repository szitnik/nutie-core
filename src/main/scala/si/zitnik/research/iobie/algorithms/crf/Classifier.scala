package si.zitnik.research.iobie.algorithms.crf

import si.zitnik.research.iobie.domain.{Examples, Example}
import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer
import java.util

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/6/11
 * Time: 6:52 PM
 * To change this template use File | Settings | File Templates.
 */

abstract
class Classifier {
  def classify(example: Example, normalized: Boolean = false): (util.ArrayList[String], util.ArrayList[Double], Double)

  def classify(examplesTest: Examples): (ArrayBuffer[ArrayBuffer[String]], ArrayBuffer[ArrayBuffer[Double]], ArrayBuffer[Double]) = {
    val retVal = new ArrayBuffer[ArrayBuffer[String]]()
    val retVal1 = new ArrayBuffer[ArrayBuffer[Double]]()
    val retVal2 = new ArrayBuffer[Double]()

    for (example <- examplesTest) {
      val classified = this.classify(example)
      retVal.add(new ArrayBuffer[String]() ++ classified._1) //TODO: optimize, transforma to ArrayBuffer for all
      retVal1.add(new ArrayBuffer[Double]() ++ classified._2)
      retVal2.add(classified._3)
    }

    (retVal, retVal1, retVal2)
  }

  def test(data: Examples)
}