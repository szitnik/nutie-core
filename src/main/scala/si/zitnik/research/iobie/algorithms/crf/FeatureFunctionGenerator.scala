package si.zitnik.research.iobie.algorithms.crf

import java.util.ArrayList

/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/16/12
 * Time: 4:13 PM
 * To change this template use File | Settings | File Templates.
 */

abstract
class FeatureFunctionGenerator {
  def generate(): ArrayList[FeatureFunction]
}
