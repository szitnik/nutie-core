package si.zitnik.research.iobie.ontology

import com.hp.hpl.jena.rdf.model.Model
import com.hp.hpl.jena.util.FileManager
import scala.io.Source
import com.hp.hpl.jena.vocabulary.RDF
import com.hp.hpl.jena.rdf.model.ResourceFactory
import com.hp.hpl.jena.query.QueryExecution
import com.hp.hpl.jena.query.QueryExecutionFactory
import com.hp.hpl.jena.query.ResultSet
import com.hp.hpl.jena.query.QuerySolution
import com.hp.hpl.jena.rdf.model.Resource
import java.io.FileWriter
import collection.mutable.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 14/03/14
 * Time: 11:02
 * To change this template use File | Settings | File Templates.
 */
class OntologyManager(modelFile: String) {
  val iePrefix = "http://zitnik.si/ontos/owl/iobie.owl#"

  val nameProperty = iePrefix + "name"

  private val model: Model = OntologyManager.getModel(modelFile)



  def writeTTL(): Unit = {
    model.write(System.out, "TURTLE")
  }

  def writeToFile(filename: String): Unit = {
    val fw = new FileWriter(filename)
    model.write(fw, "TURTLE")
    fw.close()
  }

  def addEntity(`type`: String, name: String): Unit = {
    model.createResource(iePrefix + resourceNameCleaner(name)).
      addProperty(RDF.`type`, model.createResource(iePrefix + resourceNameCleaner(`type`)) ).
      addLiteral(ResourceFactory.createProperty(nameProperty), name)
  }

  def addRelation(relation: String, name1: String, name2: String): Unit = {
    model.getResource(iePrefix + resourceNameCleaner(name1)).
      addProperty(model.getProperty(iePrefix, relation), model.getResource(iePrefix + resourceNameCleaner(name2)))
  }

  def resourceNameCleaner(name: String) = {
    name.replaceAll("%", "__").replaceAll(" ", "_")
  }

  def getResults(queryFile: String, parameters: Array[String]) = {
    var retVal = List[String]()
    val query = OntologyManager.getQuery(queryFile).format(parameters.map(_.replaceAll("\n", "").replaceAll("\"", "")): _*)



    val qexec: QueryExecution = QueryExecutionFactory.create(query, model)
    try {
      val results: ResultSet = qexec.execSelect
      while (results.hasNext) {
        val soln: QuerySolution = results.nextSolution
        val result: Resource = soln.getResource("result")
        retVal +:= result.getLocalName
      }
    }
    finally {
      qexec.close
    }

    retVal
  }

}

object OntologyManager {
  private val nameToModel = new HashMap[String, Model]()
  private val nameToQuery = new HashMap[String, String]()

  def getModel(modelFile: String) = {
    if (!nameToModel.contains(modelFile)) {
      nameToModel.put(modelFile, FileManager.get.loadModel(modelFile))
    }
    nameToModel(modelFile)

  }

  def getQuery(queryFile: String) = {
    if (!nameToQuery.contains(queryFile)) {
      nameToQuery.put(queryFile, Source.fromFile(queryFile).getLines().mkString("\n"))
    }
    nameToQuery(queryFile)
  }

}