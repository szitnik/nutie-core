package si.zitnik.research.iobie.ontology.test

import si.zitnik.research.iobie.ontology.OntologyManager
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 14/03/14
 * Time: 11:02
 * To change this template use File | Settings | File Templates.
 */
object OntologyManagerTest {

  def generalTest(): Unit = {
    val modelFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie.ttl"
    val ontoManager = new OntologyManager(modelFile)
    //ontoManager.writeTTL()

    //selects
    println("Test filled selects")

    var queryNameQ: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getTypesByName.txt"
    val superNameQ: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getSupertypesByName.txt"
    val betweenQ = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getRelationBetweenTheTwo.txt"

    println(ontoManager.getResults(queryNameQ, Array("John Doe")))
    println(ontoManager.getResults(queryNameQ, Array("Ljubljana")))
    println(ontoManager.getResults(superNameQ, Array("Ljubljana")))
    println(ontoManager.getResults(betweenQ, Array("John Doe", "Ljubljana")))
    println(ontoManager.getResults(betweenQ, Array("Ljubljana", "John Doe")))


    //add new data
    println("Test new data")
    ontoManager.addEntity("Person", "Slavko")
    ontoManager.addEntity("Address", "Vrbičje 4")
    ontoManager.addRelation("Located", "Slavko", "Vrbičje 4")
    //ontoManager.writeTTL()
    ontoManager.addEntity("Other", "Neka zadeva")

    println(ontoManager.getResults(queryNameQ, Array("Slavko")))
    println(ontoManager.getResults(queryNameQ, Array("Vrbičje 4")))
    println(ontoManager.getResults(superNameQ, Array("Vrbičje 4")))
    println(ontoManager.getResults(betweenQ, Array("Vrbičje 4", "Slavko")))
    println(ontoManager.getResults(betweenQ, Array("Slavko", "Vrbičje 4")))

    println(ontoManager.getResults(queryNameQ, Array("Neka zadeva")))
    println(ontoManager.getResults(superNameQ, Array("Neka zadeva")))

  }

  def ACE2004OntoTest(): Unit = {
    val modelFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie_ACE2004.ttl"
    val ontoManager = new OntologyManager(modelFile)
    //ontoManager.writeTTL()

    //selects
    println("Test filled selects")

    var queryNameQ: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getTypesByName.txt"
    val superNameQ: String = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getSupertypesByName.txt"
    val betweenQ = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getRelationBetweenTheTwo.txt"

    println(ontoManager.getResults(queryNameQ, Array("postville")))
    println(ontoManager.getResults(superNameQ, Array("postville")))
    println(ontoManager.getResults(queryNameQ, Array("iowa")))
    println(ontoManager.getResults(superNameQ, Array("iowa")))

    println(ontoManager.getResults(betweenQ, Array("iowa", "postville")))
    println(ontoManager.getResults(betweenQ, Array("postville", "iowa")))
  }

  def main(args: Array[String]): Unit = {
    ACE2004OntoTest()
  }
}
