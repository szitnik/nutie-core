package si.zitnik.research.iobie.util.properties

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/28/12
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
object IOBIEProperties extends Enumeration {
  val PROPERTIES_FILENAME = Value("conf/iobie.properties")

  //CRFSUITE
  val CRFSUITE_CMD = Value("crfsuitecmd")
  val CRFSUITE_MODEL_FOLDER = Value("crfsuitemodelfolder")

  //DATASETS
  val ACE2004_PATH = Value("ace2004path")
  val CONLL2012_PATH = Value("conll2012path")
  val SEMEVAL2010_PATH = Value("semeval2010path")
  val BIONLP2013_PATH = Value("bionlp2013path")
  val CHEMDNER2013_PATH = Value("chemdner2013path")

  //RESOURCES
  val IOBIE_RESOURCES = Value("iobie_resources")
}
