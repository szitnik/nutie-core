package si.zitnik.research.iobie.util

import java.io.{BufferedReader, IOException, InputStream, InputStreamReader}

import java.util.Scanner

import com.typesafe.scalalogging.StrictLogging

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/10/12
 * Time: 1:29 PM
 * To change this template use File | Settings | File Templates.
 */
class StreamGobbler(private val is: InputStream, private val printOutput: Boolean) extends Thread with StrictLogging {

  override
  def run(): Unit = {
    try {
      val br = new BufferedReader(new InputStreamReader(is))
      val sc = new Scanner(br)
      while (sc.hasNextLine()) {
        if (printOutput) logger.info(sc.nextLine()) else sc.nextLine()
      }
    } catch {
      case ioe: IOException => throw new RuntimeException(ioe)
    }
  }
}