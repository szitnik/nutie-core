package si.zitnik.research.iobie.util

import java.io.{BufferedReader, IOException, InputStream, InputStreamReader}

import java.util.Scanner
import java.util

import com.typesafe.scalalogging.StrictLogging

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/10/12
 * Time: 1:29 PM
 * To change this template use File | Settings | File Templates.
 */
class StreamGobblerExample(private val is: InputStream) extends Thread with StrictLogging {
  @volatile var data = new util.ArrayList[String]()
  @volatile var marginalProbabilities = new util.ArrayList[Double]()
  @volatile var sequenceProbability = 0.0

  override
  def run(): Unit = {
    try {
      val br = new BufferedReader(new InputStreamReader(is))
      val sc = new Scanner(br)
      while (sc.hasNextLine()) {
        val label = sc.nextLine()
        if (!label.isEmpty) {
          if (label.startsWith("@probability")) {
            sequenceProbability = label.split("\t")(1).toDouble
          } else {
            val splitIndex = label.lastIndexOf(':')
            if (splitIndex == -1) {
              data.add(label)
            } else {
              data.add(label.substring(0, splitIndex))
              marginalProbabilities.add(label.substring(splitIndex+1).toDouble)
            }
          }
        }
      }
    } catch {
      case ioe: IOException => throw new RuntimeException(ioe)
    }
  }
}