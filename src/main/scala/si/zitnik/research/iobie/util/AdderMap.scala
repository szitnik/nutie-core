package si.zitnik.research.iobie.util

import scala.collection.mutable._


/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 5/30/12
 * Time: 2:36 PM
 * To change this template use File | Settings | File Templates.
 */

class AdderMap[K, V] {
  val map = new HashMap[K, ArrayBuffer[V]]()

  def put(k: K, v: V): Unit = {
    map.get(k) match {
      case Some(b) => b.append(v)
      case None => {
        map.put(k, ArrayBuffer[V](v))
      }
    }
  }

  def put(k: K, v: TraversableOnce[V]): Unit = {
    map.get(k) match {
      case Some(b) => b.appendAll(v)
      case None => {
        val b = new ArrayBuffer[V]()
        b.appendAll(v)
        map.put(k, b)
      }
    }
  }

  override def toString = map.toString()

}
