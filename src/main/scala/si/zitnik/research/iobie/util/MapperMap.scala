package si.zitnik.research.iobie.util

import scala.collection.mutable._


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/9/12
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */

class MapperMap[K] {
  val map = new HashMap[K, Int]()
  private var curIdx = 0

  def put(k: K): Int = {
    map.get(k) match {
      case Some(v) => {}
      case None => {
        map.put(k, curIdx)
        curIdx += 1
      }
    }
    map.get(k).get
  }

  override def toString = map.toString()
}
