package si.zitnik.research.iobie.util

import scala.collection.mutable._


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/9/12
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */

class CounterMap[K] {
  val map = new HashMap[K, Int]()

  def put(k: K): Int = {
    map.get(k) match {
      case Some(v) => map.put(k, v + 1);v+1
      case None => map.put(k, 1);1
    }
  }

  /**
   * Returns a set of key which have counter values at least 3
   * @param cutoff
   * @return
   */
  def toSet(cutoff: Int = 3): HashSet[K] = {
    val retVal = new HashSet[K]()
    for ((k, v) <- map) {
      if (v >= cutoff) {
        retVal.add(k)
      }
    }
    retVal
  }

  override def toString = map.toString()


}
