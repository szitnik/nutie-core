package si.zitnik.research.iobie.util

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import com.typesafe.scalalogging.StrictLogging


/**
 * Created by IntelliJ IDEA.
 * User: slavkoz
 * Date: 4/26/12
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */

object ObjectManager extends StrictLogging {

  def saveObject(obj: Object, filename: String): Unit = {
    logger.info("Serializing object to: %s".format(filename))
    val time = System.currentTimeMillis()
    val bw = new ObjectOutputStream(new FileOutputStream(filename))
    bw.writeObject(obj)
    bw.close()
    logger.info("Object serialized in %.2fs.".format((System.currentTimeMillis() - time) / 1000.0))
  }


  def readObject(filename: String): Any = {
    logger.info("Deserializing object from: %s".format(filename))
    val time = System.currentTimeMillis()
    val rw = new ObjectInputStream(new FileInputStream(filename))
    val obj = rw.readObject()
    rw.close()
    logger.info("Object deserialized in %.2fs.".format((System.currentTimeMillis() - time) / 1000.0))
    obj
  }


}
