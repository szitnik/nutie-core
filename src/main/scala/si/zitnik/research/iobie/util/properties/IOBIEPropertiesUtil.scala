package si.zitnik.research.iobie.util.properties

import java.util.Properties
import java.io.FileInputStream

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/28/12
 * Time: 5:52 PM
 * To change this template use File | Settings | File Templates.
 */
object IOBIEPropertiesUtil {
  private val props = new Properties()
  props.load(new FileInputStream(IOBIEProperties.PROPERTIES_FILENAME.toString))

  def getProps() = props

  def getProperty(value: IOBIEProperties.Value): String = {
    props.getProperty(value.toString)
  }
}
