package si.zitnik.research.iobie.util

import java.io.{BufferedReader, IOException, InputStream, InputStreamReader}

import java.util.Scanner

import collection.mutable.ArrayBuffer

import com.typesafe.scalalogging.StrictLogging

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 8/10/12
 * Time: 1:29 PM
 * To change this template use File | Settings | File Templates.
 */
class StreamGobblerExamples(private val is: InputStream) extends Thread with StrictLogging {
  @volatile var data = new ArrayBuffer[ArrayBuffer[String]]()
  @volatile var marginalProbabilities = new ArrayBuffer[ArrayBuffer[Double]]()
  @volatile var sequenceProbabilities = new ArrayBuffer[Double]()

  override
  def run(): Unit = {
    try {
      val br = new BufferedReader(new InputStreamReader(is))
      val sc = new Scanner(br)
      var tempList = new ArrayBuffer[String]()
      var tempMarginalList = new ArrayBuffer[Double]()
      while (sc.hasNextLine()) {
        val label = sc.nextLine()
        if (label.isEmpty) {
          data.append(tempList)
          tempList = new ArrayBuffer[String]()
          marginalProbabilities.append(tempMarginalList)
          tempMarginalList = new ArrayBuffer[Double]()
        } else {
          if (label.startsWith("@probability")) {
            sequenceProbabilities.append(label.split("\t")(1).toDouble)
          } else {
            val splitIndex = label.lastIndexOf(':')
            if (splitIndex == -1) {
              tempList.append(label)
            } else {
              tempList.append(label.substring(0, splitIndex))
              tempMarginalList.append(label.substring(splitIndex+1).toDouble)
            }
          }
        }
      }
    } catch {
      case ioe: IOException => throw new RuntimeException(ioe)
    }
  }
}