package si.zitnik.research.iobie.preprocessing.tokenization.impl

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.preprocessing.tokenization.abst.Tokenizer
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.IOBIEConversions._

import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 3:20 PM
 *
 * The class tokenizes the input examples.
 * Each example must have only one token!
 * After tokenization each token also holds startIdx of the first character in the token.
 */
object RuleTokenizer extends Tokenizer with StrictLogging {

  private def split(tokenText: String, splitIndexes: Array[Int]): Array[String] = {
    if (splitIndexes.length == 0) {
      return Array(tokenText)
    }

    var splits = Array[String]()
    var startIdx = 0
    for (idx <- splitIndexes) {
      if (startIdx != idx) {
        splits :+= tokenText.substring(startIdx, idx)
      }

      startIdx +=  idx - startIdx
      splits :+= tokenText.substring(startIdx, startIdx + 1)
      startIdx += 1
    }

    //last words
    if (startIdx != tokenText.length) {
      splits :+= tokenText.substring(startIdx)
    }

    splits
  }

  def needsToBeSplitted(prevSplitIdx: Int, idx: Int, text: String): Boolean = {
    val leftChar = if (idx > 0) {Some(text.charAt(idx-1))} else {None}
    val currentChar = text.charAt(idx)
    val rightChar = if (idx < text.length-1) {Some(text.charAt(idx+1))} else {None}

    val splitChars = Set('?', '.', '!', ',', ':', ';', '-', '\"', '\'', '(', ')')

    if (splitChars.contains(currentChar)) {
      if (leftChar.isDefined && leftChar.get == currentChar || rightChar.isDefined && rightChar.get == currentChar ||
        prevSplitIdx+1 < idx && text.substring(prevSplitIdx+1, idx).contains("(") && currentChar == ')') {
        false
      } else if (!leftChar.isDefined || !rightChar.isDefined || leftChar.isDefined && leftChar.get == ' ' || rightChar.isDefined && rightChar.get == ' ' ) {
        true
      } else {
        false
      }
    } else {
      false
    }

  }

  private def tokenizeExample(example: Example): Example = {
    val sentence = example.get(0, Label.OBS)

    //1. split by whitespaces
    var oldExample = new Example(sentence.split(" "))
    var newExample = new Example()
    var startIdx = example.get(0).get(Label.START_IDX).toInt
    for (token: Token <- oldExample) {
      token.put(Label.START_IDX, startIdx)
      if (token.get(Label.OBS).asInstanceOf[String].size > 0) {
        newExample.add(token)
      }
      startIdx += token.get(Label.OBS).asInstanceOf[String].size + 1
    }
    oldExample = newExample

    //2. split by regular expression
    newExample = example.clone()
    for (token: Token <- oldExample) {
      val text = token.get(Label.OBS).asInstanceOf[String]
      var prevSplitIdx = 0
      val splitIndexes = text.toCharArray.zipWithIndex.
        filter{case (chr, idx) =>
          if (needsToBeSplitted(
            prevSplitIdx,
                idx,
                text)) {
            prevSplitIdx += 1
            true
          } else {
            false
          }
      }.map(_._2)
      val tokens = split(token.get(Label.OBS), splitIndexes)

      startIdx = token.get(Label.START_IDX)
      for (newTokenText: String <- tokens) {
        val newToken = token.clone()
        newToken.put(Label.OBS, newTokenText)
        newToken.put(Label.START_IDX, startIdx)
        startIdx += newTokenText.length
        newExample.add(newToken)
      }
    }
    newExample
  }




  def tokenize(examples: Examples): Examples = {
    val newExamples = new Examples()
    for (example <- examples) {
      if (example.size() > 1) {
        logger.error("There was a sentence that was already partly tokenized: \"%s\"".format(example.toString()))
        System.exit(-1)
      }
      newExamples.add(tokenizeExample(example))
    }
    newExamples
  }

}
