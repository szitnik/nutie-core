package si.zitnik.research.iobie.preprocessing.tokenization.abst

import si.zitnik.research.iobie.domain.Examples

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class Tokenizer {
  def tokenize(examples: Examples): Examples
}
