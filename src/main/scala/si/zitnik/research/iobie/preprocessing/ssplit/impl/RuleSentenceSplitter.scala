package si.zitnik.research.iobie.preprocessing.ssplit.impl

import com.typesafe.scalalogging.StrictLogging
import si.zitnik.research.iobie.domain.{Example, Examples, Token}
import si.zitnik.research.iobie.preprocessing.ssplit.abst.SentenceSplitter
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}

import scala.collection.JavaConversions._
import io.Source

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 4:46 PM
 *
 * At first each example contains one token! Example can have attribute DOC_ID and others which is then copied to nex examples.
 * And this token must contain the text of the whole document.
 *
 *
 */
object RuleSentenceSplitter extends SentenceSplitter with StrictLogging {
  //TODO: what about dates, ...
  private val splitChars = Set('?', '.', '!')

  private def split(tokenText: String, splitIndexes: Array[Int]): Array[String] = {
    if (splitIndexes.length == 0) {
      return Array(tokenText)
    }

    var splits = Array[String]()
    var startIdx = 0
    for (idx <- splitIndexes) {
      val text = tokenText.substring(startIdx, idx + 1)
      if (text.length > 0) {
        splits :+= text
      }
      startIdx +=  idx - startIdx + 1
    }

    //last words
    if (startIdx != tokenText.length) {
      splits :+= tokenText.substring(startIdx)
    }

    splits
  }


  def needsToBeSplitted(leftChar: Option[Char], currentChar: Char, rightChar: Option[Char]): Boolean = {
    if (splitChars.contains(currentChar)) {
      if (!rightChar.isDefined) {
        true
      } else if (rightChar.isDefined && (rightChar.get+"").replaceAll("\\s+", "").isEmpty) {
        true
      } else {
        false
      }
    } else {
      false
    }

  }

  private def splitToSentences(example: Example): Examples = {
    val retVal = new Examples()

    val tempExamples = new Examples()
    val token: Token = example.get(0)
      val text = token.get(Label.OBS).asInstanceOf[String]
      val splitIndexes = text.toCharArray.zipWithIndex.
        filter{case (chr, idx) => if (needsToBeSplitted(
                                        if (idx > 0) {Some(text.charAt(idx-1))} else {None},
                                        chr,
                                        if (idx < text.length-1) {Some(text.charAt(idx+1))} else {None})
        ) {true} else {false}}.map(_._2)
      val tokens = split(token.get(Label.OBS).asInstanceOf[String], splitIndexes)

      var startIdx = 0
      for (newExampleText: String <- tokens) {
        val newExample = example.clone()
        newExample.add(new Token(Label.OBS, newExampleText))
        newExample.get(0).put(Label.START_IDX, startIdx)
        startIdx += newExampleText.length
        tempExamples.add(newExample)
      }

    //trim sentences
    for (example: Example <- tempExamples) {
      var sentence = example.get(0).get(Label.OBS).asInstanceOf[String]
      var sentenceTrimmed = sentence.replaceAll("""^\s+""", "")
      val newStartIdx: Int = example.get(0, Label.START_IDX).asInstanceOf[Int] + sentence.size - sentenceTrimmed.length
      example.get(0).put(Label.START_IDX, newStartIdx)
      sentenceTrimmed = sentenceTrimmed.replaceAll("""\s+$""", "")
      example.get(0).put(Label.OBS, sentenceTrimmed)
      if (!sentenceTrimmed.isEmpty) {
        retVal.add(example)
      }
    }

    retVal
  }

  def ssplit(examples: Examples): Examples = {
    val retVal = new Examples()

    for (example: Example <- examples) {
      if (example.size() > 1) {
        logger.error("There was a sentence that was already partly tokenized: \"%s\"".format(example.toString()))
        System.exit(-1)
      }
      val sentences = splitToSentences(example)
      retVal.addAll(sentences)
    }

    retVal
  }

  def ssplit(filename: String, docId: Option[String]): Examples = {
    val text = Source.fromFile(filename).getLines().mkString("\n")

    ssplitText(text, docId)
  }

  def ssplitText(text: String, docId: Option[String]): Examples = {
    val retVal = new Examples()

    val example = new Example(Array(text))
    docId match {
      case Some(x) => example.put(ExampleLabel.DOC_ID, docId.get)
      case _ => {}
    }
    val sentences = splitToSentences(example)
    retVal.addAll(sentences)

    retVal
  }

}
