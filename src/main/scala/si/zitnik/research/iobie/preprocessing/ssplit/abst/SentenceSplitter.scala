package si.zitnik.research.iobie.preprocessing.ssplit.abst

import si.zitnik.research.iobie.domain.Examples

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/27/13
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class SentenceSplitter {
  def ssplit(examples: Examples): Examples
}
