package si.zitnik.research.iobie.domain.parse

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.thirdparty.opennlp.api.ParseTagger
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.{ExampleLabel, Label}
import si.zitnik.research.iobie.domain.IOBIEConversions._
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/22/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
class ParseNodeTest extends FlatSpec {

  "Parse Node Path " must " match the following values" in {
    val example = new Example(Array("The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog", "."))
    new ParseTagger().tag(example)

    assertResult("[Node: fox] [Node: NN] [Node: NP] [Node: NP] [Node: TOP]")(example.get(3, Label.PARSE_NODE).getPathToRoot().mkString(" "))
    assertResult("[Node: over] [Node: IN] [Node: PP] [Node: NP] [Node: TOP]")(example.get(5, Label.PARSE_NODE).getPathToRoot().mkString(" "))

    assertResult("[Node: fox] [Node: NN] [Node: NP] [Node: NP] [Node: PP] [Node: IN] [Node: over]")(ParseNode.getNodePath(example.get(3, Label.PARSE_NODE), example.get(5, Label.PARSE_NODE), NodePath.INCLUSIVE).mkString(" "))

    assertResult("[Node: NP]")(ParseNode.getClosestCommonRootNode(new Constituent(example, 6, 9), example.get(ExampleLabel.PARSE_TREE)).toString)
    assertResult("[Node: PP]")(ParseNode.getClosestCommonRootNode(new Constituent(example, 5, 9), example.get(ExampleLabel.PARSE_TREE)).toString)
    assertResult("[Node: NP]")(ParseNode.getClosestCommonRootNode(new Constituent(example, 0, 5), example.get(ExampleLabel.PARSE_TREE)).toString)

    assertResult("[Node: NP]")(ParseNode.getClosestCommonRootNode(new Constituent(example, 2, 5), example.get(ExampleLabel.PARSE_TREE)).toString)
    assertResult("[Node: NP]")(ParseNode.getClosestCommonRootNode(new Constituent(example, 4, 6), example.get(ExampleLabel.PARSE_TREE)).toString)
    assertResult("[Node: PP]")(ParseNode.getClosestCommonRootNode(new Constituent(example, 5, 7), example.get(ExampleLabel.PARSE_TREE)).toString)
  }
}