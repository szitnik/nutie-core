package si.zitnik.research.iobie.preprocessing.ssplit.impl

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.{Examples, Example}
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/28/13
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
class RuleSentenceSplitterTest extends FlatSpec {

    "Sentence " must " be splitted as follows" in {
      val text = "Contrary to popular belief, Lorem Ipsum is not simply random text.         " +
        "It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old? " +
        "    \n  \r \r\n        Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia! ddd   "
      val example = new Example(Array(text))


      val splitted = RuleSentenceSplitter.ssplit(new Examples(Array(example)))
      //splitted.printStatistics()

      /*
      for (sentence <- splitted.getLabeling(Label.OBS).toArray) {
        println("\"%s\"".format(sentence))
      }

      for (example <- splitted) {
        println("XXXXXXX: %s".format(text.substring(example.get(0, Label.START_IDX).asInstanceOf[Int])))
      }
      */

      assertResult(4){splitted.size()}
      assertResult(splitted.get(0).get(0, Label.OBS)){"Contrary to popular belief, Lorem Ipsum is not simply random text."}
      assertResult(splitted.get(1).get(0, Label.OBS)){"It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old?"}
      assertResult(splitted.get(2).get(0, Label.OBS)){"Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia!"}
      assertResult(splitted.get(3).get(0, Label.OBS)){"ddd"}

    }
}
