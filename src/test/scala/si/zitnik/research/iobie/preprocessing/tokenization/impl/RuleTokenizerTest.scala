package si.zitnik.research.iobie.preprocessing.tokenization.impl

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.{Examples, Example}
import scala.Array
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 3/29/13
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
class RuleTokenizerTest extends FlatSpec {

  "Sentence " must " be splitted as follows" in {
    val sentence1 = "Contrary to popular bel - ief, Lor-em Ips:um is not s???imply         random text."
    val sentence2 =  "It has roots in a piece of classical Latin literature from 45 BC,  ?? making it over 2000 years old?"
    val sentence3 =  "Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia!"
    val sentence4 = "ddd"
    val examples = new Examples(Array(
      new Example(Array(sentence1)),
      new Example(Array(sentence2)),
      new Example(Array(sentence3)),
      new Example(Array(sentence4))
    ))

    val tokenized = RuleTokenizer.tokenize(examples)

    /*
    tokenized.printStatistics()

    for (example <- tokenized) {
      for (token <- example) {
        println("%d \"%s\"".format(token.get(Label.START_IDX), token.get(Label.OBS)))
      }
    }
    */

    assertResult(15){tokenized.get(0).size()}
    assertResult(0){tokenized.get(0).get(0, Label.START_IDX)}
    assertResult(9){tokenized.get(0).get(1, Label.START_IDX)}
    assertResult(12){tokenized.get(0).get(2, Label.START_IDX)}
    assertResult(20){tokenized.get(0).get(3, Label.START_IDX)}
    assertResult(24){tokenized.get(0).get(4, Label.START_IDX)}
    assertResult(26){tokenized.get(0).get(5, Label.START_IDX)}
    assertResult(29){tokenized.get(0).get(6, Label.START_IDX)}
    assertResult(31){tokenized.get(0).get(7, Label.START_IDX)}
    assertResult(38){tokenized.get(0).get(8, Label.START_IDX)}
    assertResult(45){tokenized.get(0).get(9, Label.START_IDX)}
    assertResult(48){tokenized.get(0).get(10, Label.START_IDX)}
    assertResult(52){tokenized.get(0).get(11, Label.START_IDX)}
    assertResult(70){tokenized.get(0).get(12, Label.START_IDX)}
    assertResult(77){tokenized.get(0).get(13, Label.START_IDX)}
    assertResult(81){tokenized.get(0).get(14, Label.START_IDX)}

    assertResult(22){tokenized.get(1).size()}
    assertResult(0){tokenized.get(1).get(0, Label.START_IDX)}
    assertResult(3){tokenized.get(1).get(1, Label.START_IDX)}
    assertResult(7){tokenized.get(1).get(2, Label.START_IDX)}
    assertResult(13){tokenized.get(1).get(3, Label.START_IDX)}
    assertResult(16){tokenized.get(1).get(4, Label.START_IDX)}
    assertResult(18){tokenized.get(1).get(5, Label.START_IDX)}
    assertResult(24){tokenized.get(1).get(6, Label.START_IDX)}
    assertResult(27){tokenized.get(1).get(7, Label.START_IDX)}
    assertResult(37){tokenized.get(1).get(8, Label.START_IDX)}
    assertResult(43){tokenized.get(1).get(9, Label.START_IDX)}
    assertResult(54){tokenized.get(1).get(10, Label.START_IDX)}
    assertResult(59){tokenized.get(1).get(11, Label.START_IDX)}
    assertResult(62){tokenized.get(1).get(12, Label.START_IDX)}
    assertResult(64){tokenized.get(1).get(13, Label.START_IDX)}
    assertResult(67){tokenized.get(1).get(14, Label.START_IDX)}
    assertResult(70){tokenized.get(1).get(15, Label.START_IDX)}
    assertResult(77){tokenized.get(1).get(16, Label.START_IDX)}
    assertResult(80){tokenized.get(1).get(17, Label.START_IDX)}
    assertResult(85){tokenized.get(1).get(18, Label.START_IDX)}
    assertResult(90){tokenized.get(1).get(19, Label.START_IDX)}
    assertResult(96){tokenized.get(1).get(20, Label.START_IDX)}
    assertResult(99){tokenized.get(1).get(21, Label.START_IDX)}

    assertResult(12){tokenized.get(2).size()}
    assertResult(0){tokenized.get(2).get(0, Label.START_IDX)}
    assertResult(8){tokenized.get(2).get(1, Label.START_IDX)}
    assertResult(18){tokenized.get(2).get(2, Label.START_IDX)}
    assertResult(20){tokenized.get(2).get(3, Label.START_IDX)}
    assertResult(22){tokenized.get(2).get(4, Label.START_IDX)}
    assertResult(28){tokenized.get(2).get(5, Label.START_IDX)}
    assertResult(38){tokenized.get(2).get(6, Label.START_IDX)}
    assertResult(41){tokenized.get(2).get(7, Label.START_IDX)}
    assertResult(56){tokenized.get(2).get(8, Label.START_IDX)}
    assertResult(64){tokenized.get(2).get(9, Label.START_IDX)}
    assertResult(67){tokenized.get(2).get(10, Label.START_IDX)}
    assertResult(75){tokenized.get(2).get(11, Label.START_IDX)}

    assertResult(1){tokenized.get(3).size()}
    assertResult(0){tokenized.get(3).get(0, Label.START_IDX)}
  }
}

