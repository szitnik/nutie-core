package si.zitnik.research.iobie.statistics.cluster.util

import org.scalatest.FlatSpec
import scala.Array

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/18/12
 * Time: 10:31 AM
 * To change this template use File | Settings | File Templates.
 */
class KuhnMunkresTest extends FlatSpec {

  "KuhnMunkres test 1 " must "match the following values" in {
    // m = 3 workers, n = 3 jobs
    val test1 = HungarianAlgorithmScalaWrapper.hgAlgorithm(Array(Array(1.0, 4.0, 5.0), Array(5.0, 7.0, 6.0), Array(5.0, 8.0, 8.0)))


    assertResult(18.0)(test1._2)
  }

  "KuhnMunkres test 2 " must "match the following values" in {
    // m = 5 workers, n = 5 jobs
    val cost2 = Array(Array[Double](10, 19,  8, 15, 19),
                      Array[Double](10, 18,  7, 17, 19),
                      Array[Double](13, 16,  9, 14, 19),
                      Array[Double](12, 19,  8, 18, 19),
                      Array[Double](14, 17, 10, 19, 19))

    val test2 = HungarianAlgorithmScalaWrapper.hgAlgorithm(cost2)
    assertResult(79.0)(test2._2)
  }

/*
  "KuhnMunkres test 3 " must "match the following values" in {
    // m = 3 workers, n = 4 jobs
    val cost3 = Array(Array[Double](1, 4, 5, 2),
                      Array[Double](5, 7, 6, 2),
                      Array[Double](5, 8, 8, 9))

    val test3 = HungarianAlgorithmScalaWrapper.hgAlgorithm(cost3)
    assertResult(21.0)(test3._2)
  }
*/

  /**
   * Test from paper: "Uncertain programming model for uncertain optimal assignment problem"
   */
  "KuhnMunkres test 5 " must "match the following values" in {
    val cost5 = Array(Array[Double](3.00, 4.25, 3.00, 3.25, 4.00),
                      Array[Double](3.00, 3.25, 2.00, 5.25, 3.00),
                      Array[Double](4.25, 3.25, 4.00, 4.75, 3.00),
                      Array[Double](6.00, 4.50, 5.00, 6.00, 4.00),
                      Array[Double](3.25, 3.00, 3.75, 4.00, 3.00))

    val test5 = HungarianAlgorithmScalaWrapper.hgAlgorithm(cost5)
    assertResult(22.5)(test5._2)
  }
}
