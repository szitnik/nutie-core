package si.zitnik.research.iobie.statistics.cluster

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable.{ArrayBuffer, HashSet}
import si.zitnik.research.iobie.domain.cluster.Cluster
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 12/11/12
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */
class BLANCClusterStatisticsTest  extends FlatSpec {
  private val originalExample = new Example(Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R"))
  private val corefExample = new Example()
  corefExample.add(new Constituent(originalExample, 0, 1))
  corefExample.add(new Constituent(originalExample, 1, 2))
  corefExample.add(new Constituent(originalExample, 2, 3))
  corefExample.add(new Constituent(originalExample, 3, 4))
  corefExample.add(new Constituent(originalExample, 4, 5))
  corefExample.add(new Constituent(originalExample, 5, 6))
  corefExample.add(new Constituent(originalExample, 6, 7))
  corefExample.add(new Constituent(originalExample, 7, 8))
  corefExample.add(new Constituent(originalExample, 8, 9))
  corefExample.add(new Constituent(originalExample, 9, 10))
  corefExample.add(new Constituent(originalExample, 10, 11))
  corefExample.add(new Constituent(originalExample, 11, 12))

  "BLANC scoring test " must "match the following values" in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    realClusters.add(c1r)
    val c2r = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c2r)
    val c3r = new Cluster(corefExample(4).asInstanceOf[Constituent])
    realClusters.add(c3r)
    val c4r = new Cluster(corefExample(5).asInstanceOf[Constituent])
    realClusters.add(c4r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(3).asInstanceOf[Constituent])
    c2i.add(corefExample(4).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(5).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)

    val (blancP, blancR, blancF1) = BLANCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.655){math.round(blancP*1000.0)/1000.0}
    assertResult(0.718){math.round(blancR*1000.0)/1000.0}
    assertResult(0.667){math.round(blancF1*1000.0)/1000.0}
  }
}
