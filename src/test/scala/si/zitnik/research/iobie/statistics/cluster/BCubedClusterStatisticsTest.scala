package si.zitnik.research.iobie.statistics.cluster

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import collection.mutable.{ArrayBuffer, HashSet}
import si.zitnik.research.iobie.domain.cluster.Cluster
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/22/12
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
class BCubedClusterStatisticsTest extends FlatSpec {
  private val originalExample = new Example(Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R"))
  private val corefExample = new Example()
  corefExample.add(new Constituent(originalExample, 0, 1))
  corefExample.add(new Constituent(originalExample, 1, 2))
  corefExample.add(new Constituent(originalExample, 2, 3))
  corefExample.add(new Constituent(originalExample, 3, 4))
  corefExample.add(new Constituent(originalExample, 4, 5))
  corefExample.add(new Constituent(originalExample, 5, 6))
  corefExample.add(new Constituent(originalExample, 6, 7))
  corefExample.add(new Constituent(originalExample, 7, 8))
  corefExample.add(new Constituent(originalExample, 8, 9))
  corefExample.add(new Constituent(originalExample, 9, 10))
  corefExample.add(new Constituent(originalExample, 10, 11))
  corefExample.add(new Constituent(originalExample, 11, 12))

  "BCubed scoring test - NLP-Evaluation.pdf example" must "match the following values" in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c1r)
    val c2r = new Cluster(corefExample(4).asInstanceOf[Constituent])
    c2r.add(corefExample(5).asInstanceOf[Constituent])
    realClusters.add(c2r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    c2i.add(corefExample(4).asInstanceOf[Constituent])
    c2i.add(corefExample(5).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)

    val (bCubedP, bCubedR, bCubedF1) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(2.0/3.0){bCubedP}
    assertResult(2.0/3.0){bCubedR}
    assertResult(2.0/3.0){bCubedF1}
  }

  "BCubed scoring test - LingPipe API example, Bagga&Baldwin example 2" must "match the following values" in {
    /**
     *  reference = { {1, 2, 3, 4, 5}, {6, 7}, {8, 9, A, B, C} }
        response = { { 1, 2, 3, 4, 5, 8, 9, A, B, C }, { 6, 7} }

        which produce the following values for method calls:

        Method	Result
        equivalenceEvaluation()	PrecisionRecallEvaluation(54,0,50,40)
        TP=54; FN=0; FP=50; TN=40
        mucPrecision()	0.9
        mucRecall()	1.0
        mucF()	0.947      --> see MUC test
        b3ElementPrecision()	0.583
        b3ElementRecall()	1.0
        b3ElementF()	0.737
     */

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    c1r.add(corefExample(4).asInstanceOf[Constituent])
    realClusters.add(c1r)
    val c2r = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c2r.add(corefExample(6).asInstanceOf[Constituent])
    realClusters.add(c2r)
    val c3r = new Cluster(corefExample(7).asInstanceOf[Constituent])
    c3r.add(corefExample(8).asInstanceOf[Constituent])
    c3r.add(corefExample(9).asInstanceOf[Constituent])
    c3r.add(corefExample(10).asInstanceOf[Constituent])
    c3r.add(corefExample(11).asInstanceOf[Constituent])
    realClusters.add(c3r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    c1i.add(corefExample(4).asInstanceOf[Constituent])
    c1i.add(corefExample(7).asInstanceOf[Constituent])
    c1i.add(corefExample(8).asInstanceOf[Constituent])
    c1i.add(corefExample(9).asInstanceOf[Constituent])
    c1i.add(corefExample(10).asInstanceOf[Constituent])
    c1i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c2i.add(corefExample(6).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)

    val (bCubedP, bCubedR, bCubedF1) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.58){math.round(bCubedP*100)/100.0}
    assertResult(1.0){bCubedR}
    assertResult(0.74){math.round(bCubedF1*100)/100.0}
  }

  "BCubed scoring test - Bagga&Baldwin example 1" must "match the following values" in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    c1r.add(corefExample(4).asInstanceOf[Constituent])
    realClusters.add(c1r)
    val c2r = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c2r.add(corefExample(6).asInstanceOf[Constituent])
    realClusters.add(c2r)
    val c3r = new Cluster(corefExample(7).asInstanceOf[Constituent])
    c3r.add(corefExample(8).asInstanceOf[Constituent])
    c3r.add(corefExample(9).asInstanceOf[Constituent])
    c3r.add(corefExample(10).asInstanceOf[Constituent])
    c3r.add(corefExample(11).asInstanceOf[Constituent])
    realClusters.add(c3r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    c1i.add(corefExample(4).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c2i.add(corefExample(6).asInstanceOf[Constituent])
    c2i.add(corefExample(7).asInstanceOf[Constituent])
    c2i.add(corefExample(8).asInstanceOf[Constituent])
    c2i.add(corefExample(9).asInstanceOf[Constituent])
    c2i.add(corefExample(10).asInstanceOf[Constituent])
    c2i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)

    val (bCubedP, bCubedR, bCubedF1) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.76){math.round(bCubedP*100)/100.0}
    assertResult(1.0){bCubedR}
    assertResult(0.86){math.round(bCubedF1*100)/100.0}
  }

  "BCubed scoring test - Cai&Strube, Table 1, Example 1" must "match the following values" in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)
    //val c2r = new Cluster(corefExample(3).asInstanceOf[Constituent])
    //realClusters.add(c2r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    //val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    //identifiedClusters.add(c2i)

    val (bCubedP, bCubedR, bCubedF1) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){math.round(bCubedP*100)/100.0}
    assertResult(0.44){math.round(bCubedR*100)/100.0}
    assertResult(0.62){math.round(bCubedF1*100)/100.0}

    val (bCubedPAll, bCubedRAll, bCubedF1All) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_ALL)
    assertResult(0.556){math.round(bCubedPAll*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRAll*1000)/1000.0}
    assertResult(0.556){math.round(bCubedF1All*1000)/1000.0}

    val (bCubedPRN, bCubedRRN, bCubedF1RN) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_R_AND_N)
    assertResult(0.556){math.round(bCubedPRN*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRRN*1000)/1000.0}
    assertResult(0.556){math.round(bCubedF1RN*1000)/1000.0}

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.667){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.606){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "BCubed scoring test - Cai&Strube - Table 1, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    c1i.add(corefExample(4).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)

    val (bCubedP, bCubedR, bCubedF1) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){math.round(bCubedP*100)/100.0}
    assertResult(0.44){math.round(bCubedR*100)/100.0}
    assertResult(0.62){math.round(bCubedF1*100)/100.0}

    val (bCubedPAll, bCubedRAll, bCubedF1All) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_ALL)
    assertResult(0.375){math.round(bCubedPAll*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRAll*1000)/1000.0}
    assertResult(0.448){math.round(bCubedF1All*1000)/1000.0}

    val (bCubedPRN, bCubedRRN, bCubedF1RN) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_R_AND_N)
    assertResult(0.375){math.round(bCubedPRN*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRRN*1000)/1000.0}
    assertResult(0.448){math.round(bCubedF1RN*1000)/1000.0}

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.5){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.526){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "Bcybed scoring test - Cai&Strube - Table 2, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    identifiedClusters.add(new Cluster(corefExample(2).asInstanceOf[Constituent]))

    val (bCubedPAll, bCubedRAll, bCubedF1All) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_ALL)
    assertResult(0.667){math.round(bCubedPAll*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRAll*1000)/1000.0}
    assertResult(0.606){math.round(bCubedF1All*1000)/1000.0}

    val (bCubedPRN, bCubedRRN, bCubedF1RN) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_R_AND_N)
    assertResult(0.667){math.round(bCubedPRN*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRRN*1000)/1000.0}
    assertResult(0.606){math.round(bCubedF1RN*1000)/1000.0}

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.667){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.606){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "BCubed scoring test - Cai&Strube - Table 3, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)

    val (bCubedPAll, bCubedRAll, bCubedF1All) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_ALL)
    assertResult(0.556){math.round(bCubedPAll*1000)/1000.0}
    assertResult(1.000){math.round(bCubedRAll*1000)/1000.0}
    assertResult(0.714){math.round(bCubedF1All*1000)/1000.0}

    val (bCubedPRN, bCubedRRN, bCubedF1RN) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_R_AND_N)
    assertResult(0.556){math.round(bCubedPRN*1000)/1000.0}
    assertResult(1.000){math.round(bCubedRRN*1000)/1000.0}
    assertResult(0.714){math.round(bCubedF1RN*1000)/1000.0}

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.556){math.round(bCubedPSys*1000)/1000.0}
    assertResult(1.000){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.714){math.round(bCubedF1Sys*1000)/1000.0}
  }


  "BCubed scoring test - Cai&Strube R&N, Table 3, Example 2" must "match the following values" in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    realClusters.add(c1r)
    //val c2r = new Cluster(corefExample(3).asInstanceOf[Constituent])
    //realClusters.add(c2r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(9).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)
    val c4i = new Cluster(corefExample(10).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (bCubedPAll, bCubedRAll, bCubedF1All) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_ALL)
    assertResult(0.778){math.round(bCubedPAll*1000)/1000.0}
    assertResult(1.0){math.round(bCubedRAll*1000)/1000.0}
    assertResult(0.875){math.round(bCubedF1All*1000)/1000.0}

    val (bCubedPRN, bCubedRRN, bCubedF1RN) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_R_AND_N)
    assertResult(0.556){math.round(bCubedPRN*1000)/1000.0}
    assertResult(1.0){math.round(bCubedRRN*1000)/1000.0}
    assertResult(0.714){math.round(bCubedF1RN*1000)/1000.0}

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.556){math.round(bCubedPSys*1000)/1000.0}
    assertResult(1.0){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.714){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "Bcubed scoring test - Cai&Strube - Table 4, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)
    val c4i = new Cluster(corefExample(9).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(1.0){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.714){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "Bcubed scoring test - Cai&Strube - Table 4, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c4i = new Cluster(corefExample(9).asInstanceOf[Constituent])
    c4i.add(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.8){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.656){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "Bcubed scoring test - Cai&Strube - Table 5, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    c2i.add(corefExample(9).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(10).asInstanceOf[Constituent])
    c3i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)
    val c4i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.714){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.625){math.round(bCubedF1Sys*1000)/1000.0}
  }

  "Bcubed scoring test - Cai&Strube - Table 5, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    c2i.add(corefExample(9).asInstanceOf[Constituent])
    c2i.add(corefExample(10).asInstanceOf[Constituent])
    c2i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c4i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (bCubedPSys, bCubedRSys, bCubedF1Sys) = BCubedClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters), BCubedType.BCUBED_SYS)
    assertResult(0.571){math.round(bCubedPSys*1000)/1000.0}
    assertResult(0.556){math.round(bCubedRSys*1000)/1000.0}
    assertResult(0.563){math.round(bCubedF1Sys*1000)/1000.0}
  }


}
