package si.zitnik.research.iobie.statistics.cluster

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import collection.mutable._
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/21/12
 * Time: 12:26 PM
 * To change this template use File | Settings | File Templates.
 */
class MUCClusterStatisticsTest extends FlatSpec {

  "MUC scoring" must "match the following values" in {
    //example from NLP-Evaluation.pdf
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1, 1))
    corefExample.add(new Constituent(originalExample, 1, 2, 1))
    corefExample.add(new Constituent(originalExample, 2, 3, 1))
    corefExample.add(new Constituent(originalExample, 3, 4, 1))
    corefExample.add(new Constituent(originalExample, 4, 5, 2))
    corefExample.add(new Constituent(originalExample, 5, 6, 2))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    val c2r = new Cluster(corefExample(4).asInstanceOf[Constituent])
    c2r.add(corefExample(5).asInstanceOf[Constituent])
    realClusters.add(c1r)
    realClusters.add(c2r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    val c2i = new Cluster(corefExample(4).asInstanceOf[Constituent])
    c2i.add(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    c2i.add(corefExample(5).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    identifiedClusters.add(c2i)


    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.75){mucP}
    assertResult(0.75){mucR}
    assertResult(0.75){mucF1}
  }

  "MUC scoring test 2, vilain example 1" must "match the following values" in {
    //vilain example 1
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    identifiedClusters.add(c2i)


    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){mucP}
    assertResult(2.0/3){mucR}
    assertResult(0.8){mucF1}
  }

  "MUC scoring test 3, vilain example 2" must "match the following values" in {
    //vilain example 2
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)


    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){mucP}
    assertResult(0.5){mucR}
    assertResult(2.0/3){mucF1}
  }

  "MUC scoring test 4 - vilain Table 1, example 1" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)


    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){mucP}
    assertResult(2.0/3){mucR}
    assertResult(0.8){mucF1}
  }

  "MUC scoring test 4 - vilain Table 1, example 2" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)


    //same as previous but changed identified and real
    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(identifiedClusters), ArrayBuffer(realClusters))
    assertResult(2.0/3){mucP}
    assertResult(1.0){mucR}
    assertResult(0.8){mucF1}
  }

  "MUC scoring test 4 - vilain Table 1, example 3" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)


    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){mucP}
    assertResult(1.0){mucR}
    assertResult(1.0){mucF1}
  }

  "MUC scoring test 4 - vilain Table 1, example 4" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)


    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){mucP}
    assertResult(2.0/3){mucR}
    assertResult(0.8){mucF1}
  }

  "MUC scoring test 4 - vilain Table 1, example 5" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)

    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(1.0){mucP}
    assertResult(0.5){mucR}
    assertResult(2.0/3){mucF1}
  }

  "MUC scoring test 4 - vilain Examples with more complexity, Example 1" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))
    corefExample.add(new Constituent(originalExample, 6, 7))
    corefExample.add(new Constituent(originalExample, 7, 8))
    corefExample.add(new Constituent(originalExample, 8, 9))
    corefExample.add(new Constituent(originalExample, 9, 10))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    c1r.add(corefExample(4).asInstanceOf[Constituent])
    c1r.add(corefExample(6).asInstanceOf[Constituent])
    c1r.add(corefExample(7).asInstanceOf[Constituent])
    c1r.add(corefExample(9).asInstanceOf[Constituent])
    realClusters.add(c1r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(3).asInstanceOf[Constituent])
    c2i.add(corefExample(4).asInstanceOf[Constituent])
    c2i.add(corefExample(5).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(6).asInstanceOf[Constituent])
    c3i.add(corefExample(7).asInstanceOf[Constituent])
    c3i.add(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)

    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.5){mucP}
    assertResult(0.5){mucR}
    assertResult(0.5){mucF1}
  }

  "MUC scoring test 4 - vilain Examples with more complexity, Example 2" must "match the following values" in {
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))
    corefExample.add(new Constituent(originalExample, 6, 7))
    corefExample.add(new Constituent(originalExample, 7, 8))
    corefExample.add(new Constituent(originalExample, 8, 9))
    corefExample.add(new Constituent(originalExample, 9, 10))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)
    val c2r = new Cluster(corefExample(3).asInstanceOf[Constituent])
    c2r.add(corefExample(4).asInstanceOf[Constituent])
    c2r.add(corefExample(5).asInstanceOf[Constituent])
    c2r.add(corefExample(6).asInstanceOf[Constituent])
    realClusters.add(c2r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    c2i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c3i.add(corefExample(6).asInstanceOf[Constituent])
    c3i.add(corefExample(7).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)

    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.5){mucP}
    assertResult(0.4){mucR}
    assertResult(2*0.5*0.4/(0.5+0.4)){mucF1}
  }

  "MUC scoring test - LingPipe API example" must "match the following values" in {
    /**
     *  reference = { {1, 2, 3, 4, 5}, {6, 7}, {8, 9, A, B, C} }
        response = { { 1, 2, 3, 4, 5, 8, 9, A, B, C }, { 6, 7} }

        which produce the following values for method calls:

        Method	Result
        equivalenceEvaluation()	PrecisionRecallEvaluation(54,0,50,40)
        TP=54; FN=0; FP=50; TN=40
        mucPrecision()	0.9
        mucRecall()	1.0
        mucF()	0.947
        b3ElementPrecision()	0.583
        b3ElementRecall()	1.0
        b3ElementF()	0.737    --> see BCubed test
     */
    val originalExample = new Example(Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R"))
    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 3, 4))
    corefExample.add(new Constituent(originalExample, 4, 5))
    corefExample.add(new Constituent(originalExample, 5, 6))
    corefExample.add(new Constituent(originalExample, 6, 7))
    corefExample.add(new Constituent(originalExample, 7, 8))
    corefExample.add(new Constituent(originalExample, 8, 9))
    corefExample.add(new Constituent(originalExample, 9, 10))
    corefExample.add(new Constituent(originalExample, 10, 11))
    corefExample.add(new Constituent(originalExample, 11, 12))

    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    c1r.add(corefExample(3).asInstanceOf[Constituent])
    c1r.add(corefExample(4).asInstanceOf[Constituent])
    realClusters.add(c1r)
    val c2r = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c2r.add(corefExample(6).asInstanceOf[Constituent])
    realClusters.add(c2r)
    val c3r = new Cluster(corefExample(7).asInstanceOf[Constituent])
    c3r.add(corefExample(8).asInstanceOf[Constituent])
    c3r.add(corefExample(9).asInstanceOf[Constituent])
    c3r.add(corefExample(10).asInstanceOf[Constituent])
    c3r.add(corefExample(11).asInstanceOf[Constituent])
    realClusters.add(c3r)

    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(2).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    c1i.add(corefExample(4).asInstanceOf[Constituent])
    c1i.add(corefExample(7).asInstanceOf[Constituent])
    c1i.add(corefExample(8).asInstanceOf[Constituent])
    c1i.add(corefExample(9).asInstanceOf[Constituent])
    c1i.add(corefExample(10).asInstanceOf[Constituent])
    c1i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(5).asInstanceOf[Constituent])
    c2i.add(corefExample(6).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)

    val (mucP, mucR, mucF1) = MUCClusterStatistics.scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.9){mucP}
    assertResult(1.0){mucR}
    assertResult(0.95){math.round(mucF1*100)/100.0}
  }


}