package si.zitnik.research.iobie.statistics.cluster

import org.scalatest.FlatSpec
import collection.mutable.{ArrayBuffer, HashSet}
import si.zitnik.research.iobie.domain.cluster.Cluster
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.domain.Example
import scala.collection.JavaConversions._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 2/9/13
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */
class CEAFClusterStatisticsTest extends FlatSpec {
  private val originalExample = new Example(Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R"))
  private val corefExample = new Example()
  corefExample.add(new Constituent(originalExample, 0, 1))
  corefExample.add(new Constituent(originalExample, 1, 2))
  corefExample.add(new Constituent(originalExample, 2, 3))
  corefExample.add(new Constituent(originalExample, 3, 4))
  corefExample.add(new Constituent(originalExample, 4, 5))
  corefExample.add(new Constituent(originalExample, 5, 6))
  corefExample.add(new Constituent(originalExample, 6, 7))
  corefExample.add(new Constituent(originalExample, 7, 8))
  corefExample.add(new Constituent(originalExample, 8, 9))
  corefExample.add(new Constituent(originalExample, 9, 10))
  corefExample.add(new Constituent(originalExample, 10, 11))
  corefExample.add(new Constituent(originalExample, 11, 12))

  "CEAF scoring test - Cai&Strube - Table 1, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.5){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.571){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 1, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    c1i.add(corefExample(4).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.4){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.500){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 2, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    identifiedClusters.add(new Cluster(corefExample(2).asInstanceOf[Constituent]))

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.5){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.571){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 3, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.667){math.round(ceafPSys*1000)/1000.0}
    assertResult(1.0){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.800){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 3, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    c1i.add(corefExample(3).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(9).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)
    val c4i = new Cluster(corefExample(10).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.667){math.round(ceafPSys*1000)/1000.0}
    assertResult(1.0){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.800){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 4, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)
    val c4i = new Cluster(corefExample(9).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (ceafP, ceafR, ceafF1) = CEAFClusterStatistics(ceafType = CEAFType.ORIGINAL).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.4){math.round(ceafP*1000)/1000.0}
    assertResult(0.667){math.round(ceafR*1000)/1000.0}
    assertResult(0.5){math.round(ceafF1*1000)/1000.0}

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.667){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 4, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c4i = new Cluster(corefExample(9).asInstanceOf[Constituent])
    c4i.add(corefExample(8).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (ceafP, ceafR, ceafF1) = CEAFClusterStatistics(ceafType = CEAFType.ORIGINAL).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.4){math.round(ceafP*1000)/1000.0}
    assertResult(0.667){math.round(ceafR*1000)/1000.0}
    assertResult(0.5){math.round(ceafF1*1000)/1000.0}

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.6){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.632){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 5, Example 1 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    c2i.add(corefExample(9).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c3i = new Cluster(corefExample(10).asInstanceOf[Constituent])
    c3i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c3i)
    val c4i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.571){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.615){math.round(ceafF1Sys*1000)/1000.0}
  }

  "CEAF scoring test - Cai&Strube - Table 5, Example 2 " must " match the following values " in {
    val realClusters = new HashSet[Cluster]()
    val c1r = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1r.add(corefExample(1).asInstanceOf[Constituent])
    c1r.add(corefExample(2).asInstanceOf[Constituent])
    realClusters.add(c1r)


    val identifiedClusters = new HashSet[Cluster]()
    val c1i = new Cluster(corefExample(0).asInstanceOf[Constituent])
    c1i.add(corefExample(1).asInstanceOf[Constituent])
    identifiedClusters.add(c1i)
    val c2i = new Cluster(corefExample(8).asInstanceOf[Constituent])
    c2i.add(corefExample(9).asInstanceOf[Constituent])
    c2i.add(corefExample(10).asInstanceOf[Constituent])
    c2i.add(corefExample(11).asInstanceOf[Constituent])
    identifiedClusters.add(c2i)
    val c4i = new Cluster(corefExample(2).asInstanceOf[Constituent])
    identifiedClusters.add(c4i)

    val (ceafPSys, ceafRSys, ceafF1Sys) = CEAFClusterStatistics(ceafType = CEAFType.SYS).scoreExamples(ArrayBuffer(realClusters), ArrayBuffer(identifiedClusters))
    assertResult(0.429){math.round(ceafPSys*1000)/1000.0}
    assertResult(0.667){math.round(ceafRSys*1000)/1000.0}
    assertResult(0.522){math.round(ceafF1Sys*1000)/1000.0}
  }


}
