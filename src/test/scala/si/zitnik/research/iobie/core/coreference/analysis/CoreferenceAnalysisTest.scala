package si.zitnik.research.iobie.coreference.analysis

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.{Examples, Example}
import si.zitnik.research.iobie.algorithms.crf.Label
import org.scalatest.Matchers._
import si.zitnik.research.iobie.core.coreference.analysis.CoreferenceAnalysis

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/9/12
 * Time: 2:32 PM
 * To change this template use File | Settings | File Templates.
 */
class CoreferenceAnalysisTest extends FlatSpec {

  val examples1 = new Examples(Array(new Example(Label.COREF, Array("1", "1", "2", "3", "2", "1", "4", "3"))))
  CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(examples1) should equal (Map(0->1, 1->1, 3->2))

  val examples2 = new Examples(Array(new Example(Label.COREF, Array("1", "2", "4", "4", "4", "2", "1", "1"))))
  CoreferenceAnalysis.getCoreferentMentionsDistanceDistribution(examples2) should equal (Map(5->1, 3->1, 0->3))

}
