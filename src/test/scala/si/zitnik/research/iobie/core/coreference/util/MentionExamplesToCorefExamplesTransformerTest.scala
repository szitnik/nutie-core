package si.zitnik.research.iobie.coreference.util

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.constituent.Constituent
import scala.collection.JavaConversions._
import si.zitnik.research.iobie.core.coreference.util.MentionExamplesToCorefExamplesTransformer

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/13/12
 * Time: 9:01 AM
 * To change this template use File | Settings | File Templates.
 */
class MentionExamplesToCorefExamplesTransformerTest extends FlatSpec {

  "MentionExamples " must " be transformed as here " in {
    val rootExample = new Example(Array("Slavko", "Slavko1", "Slavko2", "Slavko3", "Slavko4", "Slavko5", "Slavko6", "Slavko7", "Slavko8", "Slavko9", "Slavko10", "Slavko11", "Slavko12",  "Slavko13"))
    val mentionExample = new Example()
    mentionExample.addAll(Array("1", "1", "-", "2", "-", "2", "2", "3", "-", "2", "3", "1", "2", "3").
      zipWithIndex.
      filter(!_._1.equals("-")).
      map{ case (value, id) =>
      new Constituent(rootExample, id, id+1, Integer.parseInt(value))}.toSeq)

    assertResult("OCOCCOOOOOO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 0)(0).map(_.get(Label.COREF)).mkString}
    assertResult("OOCCOO;OOOCO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 1).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OOCC;OOOC;OOO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 2).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OOO;OOO;OCO;OO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 3).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OOC;OO;OO;OO;OC"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 4).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OO;OO;OO;OC;OO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 5).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OO;OC;OC;OO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 6).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OC;OO;OO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 7).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OO;OO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 8).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult("OO"){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 9).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult(""){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 10).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult(""){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 11).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
    assertResult(""){MentionExamplesToCorefExamplesTransformer.toCorefExamples(mentionExample, 20).map(_.map(_.get(Label.COREF)).mkString).mkString(";")}
  }

}
