package si.zitnik.research.iobie.thirdparty.opennlp.api

import org.scalatest.FlatSpec

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/21/13
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
class ParseTaggerTest extends FlatSpec {

  "Parse Tree " must "match the following values" in {
    val sentence = Array("The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog", ".")
    val parser = new ParseTagger()
    assertResult("(TOP (NP (NP (DT The) (JJ quick) (JJ brown) (NN fox) (NNS jumps)) (PP (IN over) (NP (DT the) (JJ lazy) (NN dog))) (. .)))".replaceAll(" \\(", "(")){parser.getParseString(sentence)}
  }
}
