package si.zitnik.research.iobie.thirdparty.opennlp.api

import org.scalatest.FlatSpec

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/21/13
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
class SentenceDetectorTest extends FlatSpec {
  private val text = "Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29. Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group. Rudolph Agnew, 55 years old and former chairman of Consolidated Gold Fields PLC, was named a director of this British industrial conglomerate. Those contraction-less sentences don't have boundary/odd cases...this one does."

  "Recognized sentences " must " match the following values " in {
    val sentences = new SentenceDetector().detectSentences(text)

    assertResult("Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29."){sentences(0)}
    assertResult("Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group."){sentences(1)}
    assertResult("Rudolph Agnew, 55 years old and former chairman of Consolidated Gold Fields PLC, was named a director of this British industrial conglomerate."){sentences(2)}
    assertResult("Those contraction-less sentences don't have boundary/odd cases...this one does."){sentences(3)}
  }
}
