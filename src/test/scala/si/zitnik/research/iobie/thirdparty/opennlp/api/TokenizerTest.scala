package si.zitnik.research.iobie.thirdparty.opennlp.api

import org.scalatest.FlatSpec

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/21/13
 * Time: 5:16 PM
 * To change this template use File | Settings | File Templates.
 */
class TokenizerTest extends FlatSpec {

  "Recognized tokens " must " match the following values " in {
    val tokenizer = new Tokenizer()

    val tokens1a = tokenizer.tokenize("Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29.")
    val tokens2a = tokenizer.tokenize("Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group.")
    val tokens3a = tokenizer.tokenize("Rudolph Agnew, 55 years old and former chairman of Consolidated Gold Fields PLC, was named a director of this British industrial conglomerate.")
    val tokens4a = tokenizer.tokenize("Those contraction-less sentences don't have boundary/odd cases...this one does.")

    val tokens1b = Array("Pierre", "Vinken", ",", "61", "years", "old", ",", "will", "join", "the", "board", "as", "a", "nonexecutive", "director", "Nov.", "29", ".")
    val tokens2b = Array("Mr.", "Vinken", "is", "chairman", "of", "Elsevier", "N.V.", ",", "the", "Dutch", "publishing", "group", ".")
    val tokens3b = Array("Rudolph", "Agnew", ",", "55", "years", "old", "and", "former", "chairman", "of", "Consolidated", "Gold", "Fields", "PLC", ",", "was", "named", "a", "director", "of", "this", "British", "industrial", "conglomerate", ".")
    val tokens4b = Array("Those", "contraction-less", "sentences", "do", "n't", "have", "boundary/odd", "cases", "...this", "one", "does", ".")

    checkTokens(tokens1a, tokens1b)
    checkTokens(tokens2a, tokens2b)
    checkTokens(tokens3a, tokens3b)
    checkTokens(tokens4a, tokens4b)
  }

  private def checkTokens(tokens1: Array[String], tokens2: Array[String]) = {
    for ((t1,t2) <- tokens1 zip tokens2) {
      assertResult(t1){t2}
    }
  }
}