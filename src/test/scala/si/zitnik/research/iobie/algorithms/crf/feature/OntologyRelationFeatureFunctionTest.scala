package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 14/03/14
 * Time: 12:07
 * To change this template use File | Settings | File Templates.
 */
class OntologyRelationFeatureFunctionTest extends FlatSpec {
  val modelFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie_ACE2004.ttl"


  "Ontology Relation Feature Function - getRelationBetweenTheTwo" must "match the following examples" in {
    val queryFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getRelationBetweenTheTwo.txt"
    val scoreFun = new OntologyRelationFeatureFunction(Label.OBS, modelFile, queryFile).score


    val example = new Example(Array("John Doe", "Ljubljana", "postville", "iowa", "postville", "Vrbičje 4"))

    assertResult(null){scoreFun(example, 0)}
    assertResult(null){scoreFun(example, 1)}
    assertResult(null){scoreFun(example, 2)}
    assertResult("UORFF=Part-Whole"){scoreFun(example, 3)}
    assertResult("UORFF=Part-Whole"){scoreFun(example, 4)}
    assertResult(null){scoreFun(example, 5)}
  }
}