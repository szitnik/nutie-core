package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
class BigramFeatureFunctionMatchTest extends FlatSpec {
  val scoreFun = new BigramFeatureFunctionMatch(Label.OBS).score

  "Bigram FeatureFunction Match" must "achieve the following scores" in {
    assertResult(null){scoreFun(new Example(Array("Mirko", "Janez")), 0)}
    assertResult(null){scoreFun(new Example(Array("Mirko", "Janez")), 1)}
    assertResult("BM"){scoreFun(new Example(Array("Mirko", "Mirko")), 1)}
  }
}
