package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramAliasFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BigramAliasFeatureFunction().score

  "Bigram Alias Feature Function" must "match the following examples" in {
    val example = new Example(Array("Mirko i Cigo", "Mirko i Cigo", "MiC", "M i C", "Partly Must Match", "Partly Must", "Partly Must Match A", "Partly Must Not Match B"))

    assertResult(null){scoreFun(example, 0)}
    assertResult("BAL=F"){scoreFun(example, 1)}
    assertResult("BAL=F"){scoreFun(example, 2)}
    assertResult("BAL=F"){scoreFun(example, 3)}
    assertResult("BAL=N"){scoreFun(example, 4)}
    assertResult("BAL=F"){scoreFun(example, 5)}
    assertResult("BAL=F"){scoreFun(example, 6)}
    assertResult("BAL=P"){scoreFun(example, 7)}
  }
}
