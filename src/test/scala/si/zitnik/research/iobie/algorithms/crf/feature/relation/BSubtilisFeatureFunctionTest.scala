package si.zitnik.research.iobie.algorithms.crf.feature.relation

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 17/01/14
 * Time: 10:53
 * To change this template use File | Settings | File Templates.
 */
class BSubtilisFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BSubtilisFeatureFunction().score

  "BSubtilis Feature Function" must "match the following examples" in {
    val example = new Example(Array("rpoB", "rpoC", "sigA", "sigK", "adk"))

    assertResult(null){scoreFun(example, 0)}
    assertResult("BSub=Exc"){scoreFun(example, 1)}
    assertResult("BSub=Exc"){scoreFun(example, 2)}
    assertResult("BSub=Low"){scoreFun(example, 3)}
    assertResult(null){scoreFun(example, 4)}
  }
}
