package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
class UnigramIsInQuotedSpeechFeatureFunctionTest extends FlatSpec {
  val scoreFun = new UnigramIsInQuotedSpeechFeatureFunction(Label.OBS).score

  "Unigram Feature Function" must "match with double quotes" in {
    val example = new Example(Array("\"", "Mirko", "\"", "je", "rekel", "\"hec\"", "ni\"", "od", "\"muh."))
    checkQuotes(example)
  }

  "Unigram Feature Function" must "match with single quotes" in {
    val example = new Example(Array("'", "Mirko", "'", "je", "rekel", "'hec'", "ni'", "od", "'muh."))
    checkQuotes(example)
  }

  private def checkQuotes(example: Example): Unit = {
    assertResult(null){scoreFun(example, 0)}
    assertResult("UQS"){scoreFun(example, 1)}
    assertResult(null){scoreFun(example, 2)}
    assertResult(null){scoreFun(example, 3)}
    assertResult(null){scoreFun(example, 4)}
    assertResult("UQS"){scoreFun(example, 5)}
    assertResult(null){scoreFun(example, 6)}
    assertResult("UQS"){scoreFun(example, 7)}
    assertResult(null){scoreFun(example, 8)}
  }


}
