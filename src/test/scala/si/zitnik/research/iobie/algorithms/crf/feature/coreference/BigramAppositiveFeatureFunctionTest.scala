package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent
import si.zitnik.research.iobie.algorithms.crf.Label

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */

class BigramAppositiveFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BigramAppositiveFeatureFunction().score

  "Bigram Appositive Feature Function" must "match the following values" in {
    val originalExample = new Example(Array("Slavko", ",", "the", "researcher"), Label.POS, Array("NN", ",", "NP", "NP"))

    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 1, 2))
    corefExample.add(new Constituent(originalExample, 2, 4))



    assertResult(null){scoreFun(corefExample, 0)}
    assertResult(null){scoreFun(corefExample, 1)}
    assertResult("BA"){scoreFun(corefExample, 2)}
  }
}
