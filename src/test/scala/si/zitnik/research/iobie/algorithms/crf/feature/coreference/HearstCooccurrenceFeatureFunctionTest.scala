package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/2/13
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
class HearstCooccurrenceFeatureFunctionTest extends FlatSpec {
  val ff = new HearstCooccurrenceFeatureFunction()

  "Hearst feature function " must " match the following values" in {
    val originalExample1 = new Example(Array("Slavko", "was", "the", "researcher"))


    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample1, 0, 1))
    corefExample.add(new Constituent(originalExample1, 3, 4))



    assertResult(null){ff.score(corefExample, 0)}
    assertResult("BHCOC=A"){ff.score(corefExample, 1)}
    assertResult(null){ff.score(corefExample, 2)}
  }

  "Hearst match function " must " match the following values" in {
    assertResult(null){ff.matchText("mirko")}
    assertResult("BHCOC=A"){ff.matchText("was the")}
    assertResult(null){ff.matchText("wasa")}
    assertResult("BHCOC=B"){ff.matchText("or another")}
    assertResult("BHCOC=B"){ff.matchText("or the other")}
    assertResult("BHCOC=C"){ff.matchText("other than")}
    assertResult("BHCOC=C"){ff.matchText("other than an")}
    assertResult("BHCOC=D"){ff.matchText("such as an")}
    assertResult("BHCOC=E"){ff.matchText("including the")}
    assertResult("BHCOC=F"){ff.matchText("especially an")}
    assertResult("BHCOC=G"){ff.matchText("of the")}
    assertResult("BHCOC=G"){ff.matchText("of all")}
  }

}
