package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.util.properties.{IOBIEProperties, IOBIEPropertiesUtil}

/**
  * Created with IntelliJ IDEA.
  * User: slavkoz
  * Date: 14/03/14
  * Time: 12:07
  * To change this template use File | Settings | File Templates.
  */
class OntologyTokenFeatureFunctionTest extends FlatSpec {
   val modelFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/iobie_ACE2004.ttl"


   "Ontology Token Feature Function - getTypesByName" must "match the following examples" in {
     val queryFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getTypesByName.txt"
     val scoreFun = new OntologyTokenFeatureFunction(Label.OBS, modelFile, queryFile).score


     val example = new Example(Array("John Doe", "postville", "Slavko", "iowa", "John Doe", "Vrbičje 4"))

     assertResult("UOTFF=PER"){scoreFun(example, 0)}
     assertResult("UOTFF=Population-Center"){scoreFun(example, 1)}
     assertResult(null){scoreFun(example, 2)}
     assertResult("UOTFF=State-or-Province"){scoreFun(example, 3)}
     assertResult("UOTFF=PER"){scoreFun(example, 4)}
     assertResult(null){scoreFun(example, 5)}
   }

  "Ontology Token Feature Function - getSupertypesByName" must "match the following examples" in {
    val queryFile = IOBIEPropertiesUtil.getProperty(IOBIEProperties.IOBIE_RESOURCES) + "/ontology/sparqls/getSupertypesByName.txt"
    val scoreFun = new OntologyTokenFeatureFunction(Label.OBS, modelFile, queryFile).score


    val example = new Example(Array("John Doe", "postville", "Slavko", "iowa", "John Doe", "Vrbičje 4"))

    assertResult(null){scoreFun(example, 0)}
    assertResult("UOTFF=GPE"){scoreFun(example, 1)}
    assertResult(null){scoreFun(example, 2)}
    assertResult("UOTFF=GPE"){scoreFun(example, 3)}
    assertResult(null){scoreFun(example, 4)}
    assertResult(null){scoreFun(example, 5)}
  }
 }