package si.zitnik.research.iobie.algorithms.crf.feature

import si.zitnik.research.iobie.domain.Example
import org.scalatest.FlatSpec
import org.scalatest._

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/24/12
 * Time: 12:54 PM
 * To change this template use File | Settings | File Templates.
 */
//@RunWith(classOf[JUnitRunner])
class UnigramPronounTypeFeatureFunctionTest extends FlatSpec {
  val scoreFun = new UnigramPronounTypeFeatureFunction().score

  "Pronoun Feature Function" must "correctly name pronouns" in {
    assertResult(null){scoreFun(new Example(Array("Mirko")), 0)}
    assertResult("UPR=relative"){scoreFun(new Example(Array("whichever")), 0)}
    assertResult("UPR=indefinite"){scoreFun(new Example(Array("no one")), 0)}
    assertResult("UPR=indefinite"){ scoreFun(new Example(Array("none")), 0) }
  }

}
