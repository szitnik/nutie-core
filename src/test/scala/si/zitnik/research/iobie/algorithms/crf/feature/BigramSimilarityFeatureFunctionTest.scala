package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import com.wcohen.ss.JaroWinkler
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramSimilarityFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BigramSimilarityFeatureFunction(similarity = new JaroWinkler()).score

  "Bigram Similarity Feature Function" must "match the following values" in {
    val example = new Example(Array("Mirko", "Janez", "Janževec", "Janko", "Manko", "Zanko", "Zanko"))

    assertResult(null){scoreFun(example, 0)}
    assertResult("BSim=1"){scoreFun(example, 1)}
    assertResult("BSim=3"){scoreFun(example, 2)}
    assertResult("BSim=2"){scoreFun(example, 3)}
    assertResult("BSim=3"){scoreFun(example, 4)}
    assertResult("BSim=3"){scoreFun(example, 5)}
    assertResult("BSim=4"){scoreFun(example, 6)}
  }

}
