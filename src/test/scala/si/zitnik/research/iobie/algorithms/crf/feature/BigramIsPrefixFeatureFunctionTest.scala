package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramIsPrefixFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BigramIsPrefixFeatureFunction().score
  val example = new Example(Array("Marko", "Markovič", "Kovič"))

  "Bigram is Prefix Feature Function" must "match the following examples" in {
    assertResult(null){scoreFun(example, 0)}
    assertResult("BPF=T"){scoreFun(example, 1)}
    assertResult("BPF=F"){scoreFun(example, 2)}
  }
}
