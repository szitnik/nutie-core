package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.algorithms.crf.Label
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 10:55 AM
 * To change this template use File | Settings | File Templates.
 */
class UnigramFeatureFunctionTest extends FlatSpec {
  val scoreFun = new UnigramFeatureFunction(Label.POS, "UPF").score

  "Unigram Feature Function" must "return POS tags" in {
    assertResult("UPF=null"){scoreFun(new Example(Array("Mirko")), 0)}
    assertResult("UPF=NP"){scoreFun(new Example(Array("Mirko"), Label.POS, Array("NP")), 0)}
  }


}
