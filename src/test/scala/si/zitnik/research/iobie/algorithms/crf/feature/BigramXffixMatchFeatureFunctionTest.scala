package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramXffixMatchFeatureFunctionTest extends FlatSpec {
  val scorePrefix = new BigramXffixMatchFeatureFunction().score
  val scoreSuffix = new BigramXffixMatchFeatureFunction(xffixLength = -3, userPredicate = "BSX").score
  val example = new Example(Array("Marko", "Marko", "Darko", "Murko", "Ljubljana"))


  "Bigram Prefix Match Feature Function" must "match the following examples" in {
    assertResult(null){scorePrefix(example, 0)}
    assertResult("BPX=T"){scorePrefix(example, 1)}
    assertResult("BPX=F"){scorePrefix(example, 2)}
    assertResult("BPX=F"){scorePrefix(example, 3)}
    assertResult("BPX=F"){scorePrefix(example, 4)}
  }

  "Bigram Suffix Match Feature Function" must "match the following examples" in {
    assertResult(null){scoreSuffix(example, 0)}
    assertResult("BSX=T"){scoreSuffix(example, 1)}
    assertResult("BSX=T"){scoreSuffix(example, 2)}
    assertResult("BSX=T"){scoreSuffix(example, 3)}
    assertResult("BSX=F"){scoreSuffix(example, 4)}
  }

}
