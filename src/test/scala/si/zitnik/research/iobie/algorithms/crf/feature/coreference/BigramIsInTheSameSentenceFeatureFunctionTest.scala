package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 11/2/12
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramIsInTheSameSentenceFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BigramIsInTheSameSentenceFeatureFunction().score

  "Bigram Appositive Feature Function" must "match the following values" in {
    val originalExample1 = new Example(Array("Slavko", ",", "the", "researcher"))
    val originalExample2 = new Example(Array("Slavko", ",", "the", "researcher1"))

    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample1, 0, 1))
    corefExample.add(new Constituent(originalExample1, 1, 2))
    corefExample.add(new Constituent(originalExample1, 2, 4))
    corefExample.add(new Constituent(originalExample2, 0, 1))
    corefExample.add(new Constituent(originalExample2, 1, 2))
    corefExample.add(new Constituent(originalExample2, 2, 4))



    assertResult(null){scoreFun(corefExample, 0)}
    assertResult("BIssS=T"){scoreFun(corefExample, 1)}
    assertResult("BIssS=T"){scoreFun(corefExample, 2)}
    assertResult("BIssS=F"){scoreFun(corefExample, 3)}
    assertResult("BIssS=T"){scoreFun(corefExample, 4)}
    assertResult("BIssS=T"){scoreFun(corefExample, 5)}
  }
}
