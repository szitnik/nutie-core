package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.thirdparty.opennlp.api.ParseTagger
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 1/22/13
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */
class ParseTreePathFeatureFunctionTest extends FlatSpec {

  "Parse Node Path Feature Function " must " match the following values" in {
    val example = new Example(Array("The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog", "."))
    new ParseTagger().tag(example)

    val corefExample = new Example()
    corefExample.add(new Constituent(example, 1, 4))
    corefExample.add(new Constituent(example, 7, 9))

    val ff = new ParseTreePathFeatureFunction()



    assertResult(null)(ff.score(corefExample, 0))
    assertResult("BPT=JJ/NP/NP/PP/NP/JJ/.../JJ/NP/NP/PP/NP/JJ")(ff.score(corefExample, 1))
  }
}