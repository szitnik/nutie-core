package si.zitnik.research.iobie.algorithms.crf.feature

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 4:41 PM
 * To change this template use File | Settings | File Templates.
 */
class BigramIsSuffixFeatureFunctionTest extends FlatSpec {
  val scoreFun = new BigramIsSuffixFeatureFunction().score
  val example = new Example(Array("Marko", "vič", "Kovič"))

  "Bigram is Suffix Feature Function" must "match the following examples" in {
    assertResult(null){scoreFun(example, 0)}
    assertResult("BISF=F"){scoreFun(example, 1)}
    assertResult("BISF=T"){scoreFun(example, 2)}
  }
}
