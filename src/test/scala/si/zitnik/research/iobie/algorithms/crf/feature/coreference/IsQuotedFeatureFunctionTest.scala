package si.zitnik.research.iobie.algorithms.crf.feature.coreference

import org.scalatest.FlatSpec
import si.zitnik.research.iobie.domain.Example
import si.zitnik.research.iobie.domain.constituent.Constituent

/**
 * Created with IntelliJ IDEA.
 * User: slavkoz
 * Date: 10/26/12
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */

class IsQuotedFeatureFunctionTest extends FlatSpec {
  val scoreFun = new IsQuotedFeatureFunction().score

  "Is Quoted Feature Function" must " match the following values " in {
    val originalExample = new Example(Array("\"Slavko'", ",'", "the", "\"researcher"))

    val corefExample = new Example()
    corefExample.add(new Constituent(originalExample, 0, 1))
    corefExample.add(new Constituent(originalExample, 0, 2))
    corefExample.add(new Constituent(originalExample, 0, 4))
    corefExample.add(new Constituent(originalExample, 2, 3))
    corefExample.add(new Constituent(originalExample, 2, 4))



    assertResult("UQuoted"){scoreFun(corefExample, 0)}
    assertResult("UQuoted"){scoreFun(corefExample, 1)}
    assertResult(null){scoreFun(corefExample, 2)}
    assertResult("UQuoted"){scoreFun(corefExample, 3)}
    assertResult(null){scoreFun(corefExample, 4)}
  }
}
