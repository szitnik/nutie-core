
organization := "si.zitnik"
        name := "IOBIE"
     version := "1.2.0"
    licenses := Seq(("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0")))
 description := "Intelligent Ontology-based Information Extraction"
  developers := List(Developer("szitnik", "Slavko Zitnik", "slavko zitnik fri uni lj si", url("http://zitnik.si")))
   startYear := Some(2011)

fork in run := true
fork in Test := true
javaOptions in run ++= Seq(
  "-Xms1G", "-Xmx8G", "-XX:MaxMetaspaceSize=2G", "-XX:+UseConcMarkSweepGC")
mainClass in (Compile, run) := Some("si.zitnik.research.iobie.web.JettyLauncher")

assemblyJarName in assembly := s"${name.value}-${version.value}"
mainClass in assembly := Some("si.zitnik.research.iobie.web.JettyLauncher")
test in assembly := {}
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) =>
    (xs map {_.toLowerCase}) match {
      case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) => MergeStrategy.discard
      case _ => MergeStrategy.discard
    }
  case _ => MergeStrategy.first
}

scalaVersion := "2.12.3"

scalacOptions ++= List(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-target:jvm-1.8",
  "-unchecked",
  "-Ywarn-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-unused",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Xlint"
)

javacOptions ++= Seq(
  "-Xlint:deprecation",
  "-Xlint:unchecked",
  "-source", "1.8",
  "-target", "1.8",
  "-g:vars"
)

resolvers ++= Seq(
  "Scala-Tools Maven2 Repository" at "https://oss.sonatype.org/content/groups/scala-tools",
  "Scala-Test Maven2 Repository 2" at "https://oss.sonatype.org/content/groups/public",
  "Scala-Test Maven2 Repository 3" at "https://oss.sonatype.org/content/repositories/scala-tools",
  "Scala-Test Maven2 Repository" at "https://oss.sonatype.org/content/repositories/snapshots",
  "sonatype-snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases",
  "IESL Nexus repostory" at "http://iesl.cs.umass.edu:8081/nexus/content/repositories/releases"
)

libraryDependencies ++= Seq(
  "org.apache.commons" %  "commons-math" % "2.2",
  "org.apache.jena" %  "jena-core" % "2.11.1" exclude("org.slf4j", "slf4j-log4j12") exclude("log4j", "log4j"),
  "org.apache.jena" %  "jena-arq" % "2.11.1" exclude("org.slf4j", "slf4j-log4j12") exclude("log4j", "log4j"),
  "org.scala-lang" %  "scala-library" % "2.12.3",
  "junit" %  "junit" % "4.10" % "test",
  "org.apache.httpcomponents" %  "httpclient" % "4.1.2",
  "xml-apis" %  "xml-apis" % "1.4.01",
  "org.slf4j" %  "slf4j-api" % "1.7.25",
  "ch.qos.logback" %  "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %  "scala-logging_2.12" % "3.7.2",

  "com.github.fommil.netlib" %  "all" % "1.1.2",
  "org.json" %  "json" % "20090211",
  "xom" %  "xom" % "1.2.5" exclude("xml-apis", "xml-apis"),
  "joda-time" %  "joda-time" % "2.0",
  "org.apache.opennlp" %  "opennlp-tools" % "1.5.2-incubating" exclude("jwnl", "jwnl"),
  "edu.washington.cs.knowitall" %  "reverb-core" % "1.4.0",
  "log4j" %  "log4j" % "1.2.17",
  "nz.ac.waikato.cms.weka" %  "weka-stable" % "3.6.10",
  "tw.edu.ntu.csie" %  "libsvm" % "3.17",
  "org.json4s" %  "json4s-jackson_2.12" % "3.5.3",
  "org.scalatest" %  "scalatest_2.12" % "3.0.4",
  "junit"             %  "junit"       % "4.12",
  "org.scalatra" %% "scalatra" % "2.5.1",
  "org.scalatra" %% "scalatra-scalatest" % "2.5.1" % "test",
  "org.scalatra" %  "scalatra-json_2.12" % "2.5.1",
  "org.eclipse.jetty" % "jetty-webapp" % "9.4.7.v20170914" % "container;compile",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",

  "org.scalanlp" %% "breeze-natives" % "0.13.2",
  "org.scalanlp" %  "breeze_2.12" % "0.13.2" exclude("com.github.fommil.netlib", "all") exclude("com.typesafe", "scalalogging-log4j_2.10") exclude("org.apache.logging.log4j", "log4j-core"),
)

enablePlugins(ScalatraPlugin)