IOBIE
==============

Intelligent Ontology-based Information Extraction
--------------

*description TBA*

System dependencies:

- JDK 1.8

- CRFsuite 0.12

as of transition to SBT, below text is obsolete .....

CRFSuite install on Mac (see [http://www.chokkan.org/software/crfsuite/](http://www.chokkan.org/software/crfsuite/)):

1. Install LBFGS:
   
    - Download [https://github.com/downloads/chokkan/liblbfgs/liblbfgs-1.10.tar.gz](https://github.com/downloads/chokkan/liblbfgs/liblbfgs-1.10.tar.gz)
    - `./configure`
    - `make`
    - `make check`
    - `sudo make install` 

2. Install CRFSuite

    - Download [https://github.com/downloads/chokkan/crfsuite/crfsuite-0.12.tar.gz](https://github.com/downloads/chokkan/crfsuite/crfsuite-0.12.tar.gz).
    - `./configure`
    - `make`
    - `make check`
    - `sudo make install`


Build and run procedure:

- `sbt run` and then select JettyLauncher class

To develop and auto code reload/server restart, use:

- `sbt ~reStart`

Build to package procedure:

- `sbt package`

IOBIE-driven web application, nutIE:

[https://bitbucket.org/szitnik/nutie](https://bitbucket.org/szitnik/nutie)
